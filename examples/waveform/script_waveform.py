import ccfit2.waveform as wfrm
import ccfit2.ac as ac

if __name__ == '__main__':
    # Each file contains a waveform experiment at a given DC Field frequency
    files = [
        'example_data/waveform_0p32mHz.dat',  # 0.32mHz
        'example_data/waveform_0p56mHz.dat',  # 0.56mHz
        'example_data/waveform_1mHz.dat',  # 1mHz
        'example_data/waveform_1p8mHz.dat',  # 1.8mHz
        'example_data/waveform_2p4mHz.dat',  # 2.4mHz
        'example_data/waveform_3p2mHz.dat',  # 3.2mHz
        'example_data/waveform_4p2mHz.dat',  # 4.2mHz
        'example_data/waveform_5p5mHz.dat',  # 5.5mHz
        'example_data/waveform_7p5mHz.dat',  # 7.5mHz
        'example_data/waveform_10mHz.dat',  # 10mHz
        'example_data/waveform_12mHz.dat',  # 12mHz
        'example_data/waveform_16mHz.dat',  # 16mHz
        'example_data/waveform_21mHz.dat'  # 21mHz
    ]

    # Load all files into list of lists of waveform Experiments
    # First dimension is ordered by temperature
    experiments = wfrm.Experiment.from_files(files)

    # Show that there is only one temperature
    print(len(experiments))

    # Show that there are 13 experiments - these correspond to the 13
    # different files (frequencies)
    print(len(experiments[0]))

    # Collapse to a single list since we only have one temperature
    experiments = experiments[0]

    # Now carry out Fourier transform of each experiment's field and moment
    # values and generate fourier transformed experiment objects
    ft_experiments = [
        wfrm.FTExperiment.from_experiment(experiment)
        for experiment in experiments
    ]

    # again, we still have 13 fourier transformed experiments
    print(len(ft_experiments))

    # and their fundamental frequencies should be quite close to those in the
    # file names (note conversion to milliHertz)
    print('Fundamental Frequencies (mHz):')
    for ft_exp in ft_experiments:
        print(1000. / ft_exp.period)

    # We now convert these FTExperiments into ccfit2 an AC experiment object
    # note we've set the mass and molecular weight of our sample
    ac_experiment = wfrm.FTExperiment.create_ac_experiment(
        ft_experiments, experiments, mass=50, mw=2000
    )

    # We can create a Cole-Cole plot for this data
    ac.plot_colecole(ac_experiment)

    # We can also save the waveform ac data to file in the form of a "fake"
    # magnetometer output, so we can use it in the ccfit2 ac command line
    # program
    ac.save_ac_magnetometer_file(ac_experiment, verbose=True)

    # Or, we can even fit this data to a generalised Debye model in this script
    # see test_cases/ac/script_ac.py for more on this part

    # Fit every variable in the model using internal guess values
    fit_vars = {
        var: 'guess'
        for var in ac.GeneralisedDebyeModel.PARNAMES
    }
    ac_model = ac.GeneralisedDebyeModel(
        fit_vars=fit_vars,
        fix_vars={},
        experiment=ac_experiment
    )
    ac_model.fit_to(ac_experiment)

    # and then visualise the result
    ac.plot_single_fitted_cole_cole(ac_experiment, ac_model)
    ac.plot_single_fitted_susceptibility(ac_experiment, ac_model)
