from ccfit2 import dc
import numpy as np


all_measurements = []
fnames = [
    'example_data/VSM_2K_0Oe.dat',
    'example_data/VSM_3K_0Oe.dat',
    'example_data/VSM_4K_0Oe.dat',
    'example_data/VSM_6K_0Oe.dat',
    'example_data/VSM_9K_0Oe.dat',
    'example_data/VSM_13K_0Oe.dat',
    'example_data/VSM_16K_0Oe.dat',
    'example_data/VSM_20K_0Oe.dat',
    'example_data/VSM_23K_0Oe.dat'
]

# loop over each file
for fname in fnames:

    # Read current file
    measurements = dc.Measurement.from_file(fname)

    # Trim measurements to ignore values at saturating and decaying field
    diff_thresh = 1e-8
    # Forward difference of field
    field_diff = np.abs(
        np.diff([measurement.dc_field for measurement in measurements])
    )

    # Flip field diff and find first value > threshold, then index to unflipped
    n_pts = len(measurements)
    cut_at = n_pts - np.argmax(np.flip(field_diff) > diff_thresh) - 1

    # Cut to stable field values only
    measurements = measurements[cut_at:]

    # Add to list of all measurements
    all_measurements += measurements

# Group measurements into experiments based on temperature
# and cut data points with moments < 0.01 * initial moment
all_experiments = dc.Experiment.from_measurements(
    all_measurements,
    cut_moment=0.01
)

# all_experiments is a list of lists of experiments with
# first dimension as DC field, second as temperature
# An experiment is set of datapoints with same DC field and temperature

# This will be 1, since there is only 1 DC field strength for all measurements
print(len(all_experiments))
# This will be 9, since there are 9 different temperatures
print(len(all_experiments[0]))

# Since we only have 1 dc field strength, we can remove the first dimension
experiments = all_experiments[0]

# Plot all decays on one plot
dc.plot_decays(experiments)

# Fit decays to exponential function

# Fit parameters
fit_vars = {
    'tau*': 'guess',
    'beta': 'guess'
}
# Fixed parameters
fix_vars = {
    't_offset': 0.,
    'm_eq': 0.,
    'm_0': 'guess'
}

# Create models, one per experiment
# specifying type of model, and which parameters will
# be fitted and which are fixed
model_to_use = dc.ExponentialModel
models = [
    model_to_use(fit_vars, fix_vars, experiment)
    for experiment in experiments
]

# For each model and accompanying experiment, fit model
for model, experiment in zip(models, experiments):
    model.fit_to(experiment)

    # Plot decay and fit - just one, final iteration of loop
    dc.plot_fitted_decay(experiment=experiment, model=model, show=True)

# Save parameters to file
dc.write_model_params(models, file_name='dc_params.csv')

# Save modelled data to file
dc.write_model_data(experiments, models, file_name='dc_model.csv')
