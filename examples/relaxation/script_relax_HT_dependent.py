import ccfit2.relaxation as rx
import numpy as np

# To fit a field and temperature-dependent dataset, we will first find starting
# parameters from the individual temperature-dependent dataset in 0 Oe, and
# the field-dependent dataset at 12 K

# ccfit2 output files to be read in
# All files are AC files from fits to the Generalised Debye Model
# Temperature-dependent dataset in 0 Oe field
file_0Oe = ['tau_vs_t_0Oe.csv']

# Field-dependent dataset at 12 K
file_12K = ['tau_vs_h.csv']

# Two temperature-dependent datasets in 0 Oe and 800 Oe fields, one field-
# dependent dataset at 12 K
files = [
    'tau_vs_t_0Oe.csv',
    'tau_vs_t_800Oe.csv',
    'tau_vs_h.csv'
]

# Make TauTDataset from 0 Oe file
dataset_0Oe = rx.TDataset.from_ccfit2_csv(file_0Oe)

# Make TauHDataset from 12 K file
dataset_12K = rx.HDataset.from_ccfit2_csv(file_12K)

# Make TauHTDataset from all files
dataset = rx.HTDataset.from_ccfit2_csv(files)

##########################################################################
# Perform individual fit of 0 Oe temperature dependence to obtain
# initial parameters for Orbach, Raman and field-independent QTM

# Select temperature-dependent models to use
models_0Oe = [
    rx.LogOrbachModel,
    rx.LogQTMModel,
    rx.LogRamanModel
]

# Fit parameters for 0 Oe temperature-dependent dataset
# Separated by model
# ccfit2 will decide which set of parameters goes with which model
# so the order doesn't matter here
# All parameters are fit at this stage
fit_vars_0Oe = [
    {'u_eff': 900, 'A': -8},
    {'R': -4, 'n': 4},
    {'Q': -1}
]

# Parameters to fix, one dictionary for each model
# No parameters are fixed at this stage
fix_vars_0Oe = [{}, {}, {}]

# Create MultiLogModel as combination of individual LogTauT models
multilogmodel_0Oe = rx.MultiLogTauTModel(
    models_0Oe,
    fit_vars_0Oe,
    fix_vars_0Oe
)

# Fit to experiment
multilogmodel_0Oe.fit_to(dataset_0Oe)

# write fitted parameters to file
rx.write_model_params(multilogmodel_0Oe, 'relaxation_model_params_0Oe.csv')

# Save selected fitted values from 0 Oe temperature-dependent data to use as
# starting values in subsequent fit
for logmodel in multilogmodel_0Oe.logmodels:
    if isinstance(logmodel, rx.LogOrbachModel):
        A_0Oe = logmodel.final_var_values['A']
        Ueff_0Oe = logmodel.final_var_values['u_eff']

    if isinstance(logmodel, rx.LogRamanModel):
        R_0Oe = logmodel.final_var_values['R']
        n_0Oe = logmodel.final_var_values['n']

    if isinstance(logmodel, rx.LogQTMModel):
        Q_0Oe = logmodel.final_var_values['Q']


##########################################################################
# Perform individual fit of 12 K field dependence to obtain
# initial parameters for field-dependent QTM and Direct/Raman-II

# Select field-dependent models to use
models_12K = [
    rx.LogFDQTMModel,
    rx.LogRamanIIModel,
    rx.LogConstantModel
]

# Fit parameters for 12 K field-dependent data
# Separated by model
# ccfit2 will decide which set of parameters goes with which model
# so the order doesn't matter here
# All parameters are fit at this stage
fit_vars_12K = [
    {'Ct': 0.1},
    {'Q': -1., 'Q_H': 8., 'p': 4.},
    {'C': -14., 'm': 4.}
]

# Parameters to fix, one dictionary for each model
# No parameters are fixed at this stage
fix_vars_12K = [{}, {}, {}]

# Create MultiLogModel as combination of individual LogTauH models
multilogmodel_12K = rx.MultiLogTauHModel(
    models_12K,
    fit_vars_12K,
    fix_vars_12K
)

# Fit to experiment
multilogmodel_12K.fit_to(dataset_12K)

# write fitted parameters to file
rx.write_model_params(multilogmodel_12K, 'relaxation_model_params_12K.csv')

# Save selected fitted values from 12 K field-dependent data to use as
# starting values (or fixed values) in subsequent fit
for logmodel in multilogmodel_12K.logmodels:

    if isinstance(logmodel, rx.LogFDQTMModel):
        p_12K = logmodel.final_var_values['p']
        Q_H_12K = logmodel.final_var_values['Q_H']

    if isinstance(logmodel, rx.LogRamanIIModel):
        C_12K = logmodel.final_var_values['C']
        m_12K = logmodel.final_var_values['m']


#########################################################################
# Perform global fit with field and temperature dependent data
# Field-dependent terms Q_H and p are initially fixed from 12 K fit
# Initial Orbach, Raman and field-independent QTM and Direct/Raman-II
# parameters are taken from 0 Oe temperature-dependent and 12 K field-
# dependent individual fits

# Select field- and temperature-dependent models to use
models = [
    rx.LogFTDRamanIModel,
    rx.LogFTDOrbachModel,
    rx.LogFTDQTMModel,
    rx.LogFTDRamanIIDirectModel
]

# Parameters to fit along with initial guesses
# Separated by model
# The field-dependent Direct/Raman-II term 10**C * H**m becomes the field- and
# temperature-dependent term 10**G * H**x * T**y
# Here we fix y = 1 for Direct relaxation, so 10**C = 10**G * T
# Therefore using the C value at 12 K, G = C - log10(12)
# ccfit2 will decide which set of parameters goes with which model
# so the order doesn't matter here
fit_vars_initial = [
    {'R': R_0Oe, 'n': n_0Oe},
    {'A': A_0Oe, 'u_eff': Ueff_0Oe},
    {'Q': Q_0Oe},
    {'G': C_12K - np.log(10), 'x': m_12K}
]

# Parameters to fix, one dictionary for each model
# In this case, we fix the temperature exponent for the Direct process (y) to 1
# The field-dependent QTM values of Q_H and p are also kept fixed
# ccfit2 will decide which set of parameters goes with which model
# so the order doesn't matter here
fix_vars_initial = [{}, {}, {'Q_H': Q_H_12K, 'p': p_12K}, {'y': 1}]

# Create MultiLogModel as combination of individual TauHT models
multilogmodel_initial = rx.MultiLogTauHTModel(
    models,
    fit_vars_initial,
    fix_vars_initial
)

# and fit to experiment
multilogmodel_initial.fit_to(dataset)

# Save selected fitted values to use as starting values in subsequent fit
for logmodel in multilogmodel_initial.logmodels:

    if isinstance(logmodel, rx.LogFTDQTMModel):
        Q_fit = logmodel.final_var_values['Q']

    if isinstance(logmodel, rx.LogFTDRamanIModel):
        R_fit = logmodel.final_var_values['R']
        n_fit = logmodel.final_var_values['n']

    if isinstance(logmodel, rx.LogFTDOrbachModel):
        A_fit = logmodel.final_var_values['A']
        Ueff_fit = logmodel.final_var_values['u_eff']

    if isinstance(logmodel, rx.LogFTDRamanIIDirectModel):
        G_fit = logmodel.final_var_values['G']
        x_fit = logmodel.final_var_values['x']


#######################################################################
# Perform final field- and temperature-dependent global fit, with all
# parameters free

# Parameters to fit with initial parameters
# Separated by model
fit_vars_final = [
    {'R': R_fit, 'n': n_fit},
    {'A': A_fit, 'u_eff': Ueff_fit},
    {'Q': Q_fit, 'Q_H': Q_H_12K, 'p': p_12K},
    {'G': G_fit, 'x': x_fit}
]

# Parameters to fix, one dictionary for each model
# in this case, we fix the temperature exponent for the Direct process (y) only
fix_vars_final = [{}, {}, {}, {'y': 1}]

# Create MultiLogModel as combination of individual LogTauHT models
multilogmodel_final = rx.MultiLogTauHTModel(
    models,
    fit_vars_final,
    fix_vars_final
)

# and fit to experiment
multilogmodel_final.fit_to(dataset)

# write fitted parameters to file
rx.write_model_params(multilogmodel_final,'relaxation_model_params_final.csv') # noqa

# Print final values of models (fitted and fixed)
print("Final Fitted Parameters:")
for logmodel in multilogmodel_final.logmodels:
    print("Values:", logmodel.final_var_values)
    print("Std. Dev.:", logmodel.fit_stdev)

# Note that plotting of both field and temperature-dependent datasets is not
# implemented
