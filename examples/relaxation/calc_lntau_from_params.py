import ccfit2.ac as ac
import ccfit2.dc as dc
import pandas as pd
import numpy as np


'''
This script is an example of how users can calculate <ln(tau)>, sigma_ln(tau),
and optionally fit_upper_ln(tau), and fit_lower_ln(tau) from a given set of
AC or DC model parameters

There are four examples
1. Generalised Debye Model as csv file containing alpha and tau
2. Generalised Debye Model as very old CCFIT2_cmd.py params.out file
3. Exponential Decay model as csv file containing beta and tau*
4. Exponential Decay model as very old CCFIT2_cmd.py params.out file

These are provided in the hope that they can be adapted to suit your
needs/inputs.

See the ccfit2 online documentation for more assistance/information.
'''

'''
Example 1: Generalised Debye Model with csv file

In this example, the user has a csv file containing a set of tau and alpha
values, and wishes to create a new input file for ccfit2 relaxation.
'''

# First, load the tau and alpha values from a csv file
# this example has commas as the delimiter
# and a comment at the top of the file
data = pd.read_csv(
    'ac_generalised_debye_alpha_tau.csv',
    comment='#',
    skipinitialspace=True,
    delimiter=','
)

# Calculate <ln(tau)>, and add it to the dataframe with the
# header ccfit2 expects
data['<ln(tau)> (ln(s))'] = ac.GeneralisedDebyeModel.calc_lntau_expect(
    data['tau (s)']
)

# and do the same for sigma_lntau
data['sigma_ln(tau) (ln(s))'] = ac.GeneralisedDebyeModel.calc_lntau_stdev(
    data['alpha ()']
)

# If your original file contains the standard deviation of alpha and tau,
# then you can also compute the upper and lower bounds of ln(tau) based
# on those uncertainties.

# If not, just delete these lines
[upper, lower] = ac.GeneralisedDebyeModel.calc_lntau_fit_ul(
    data['tau (s)'], data['tau-s-dev (s)']
)
data['fit_upper_ln(tau) (ln(s))'] = upper
data['fit_lower_ln(tau) (ln(s))'] = lower

# Then save this data to a new csv file which can be read by
# ccfit2 relaxation
data = data.to_csv('ac_generalised_debye_with_lntau.csv', index=False)

'''
Example 2: Generalised Debye Model with very old ccfit2 params.out file

In this example, the user has a very old CCFIT2_cmd.py file containing a set
of tau and alpha values, and wishes to create a new input file for
ccfit2 relaxation.
'''

# First, load the old datafile

# Load the headers (column names)
data = pd.read_csv(
    'ac_generalised_debye_oldccfit2_alpha_tau.out',
    comment='#',
    skipinitialspace=True,
    delimiter='  ',
    engine='python'
)
headers = data.keys()

# Then load the actual values
# this file has 3 comment rows and a header row, so skiprows=4
values = np.loadtxt(
    'ac_generalised_debye_oldccfit2_alpha_tau.out',
    skiprows=4
)
data = pd.DataFrame(values, columns=headers)

# Remove old tau bounds to avoid confusion
old_headers = ['tau_ln_ESD-up (s)', 'tau_ln_ESD-lw (s)']
for old in old_headers:
    if old in data.keys():
        data.pop(old)

# Calculate <ln(tau)>, and add it to the dataframe with the
# header ccfit2 expects
data['<ln(tau)> (ln(s))'] = ac.GeneralisedDebyeModel.calc_lntau_expect(
    data['tau (s)']
)

# and do the same for sigma_lntau
data['sigma_ln(tau) (ln(s))'] = ac.GeneralisedDebyeModel.calc_lntau_stdev(
    data['alpha']
)

# If your original file contains the standard deviation of alpha and tau,
# then you can also compute the upper and lower bounds of ln(tau) based
# on those uncertainties.

# If not, just delete this line
[upper, lower] = ac.GeneralisedDebyeModel.calc_lntau_fit_ul(
    data['tau (s)'], data['tau_err (s)']
)

data['fit_upper_ln(tau) (ln(s))'] = upper
data['fit_lower_ln(tau) (ln(s))'] = lower

# Then save this data to a new csv file which can be read by
# ccfit2 relaxation
data = data.to_csv(
    'ac_generalised_debye_oldccfit2_with_lntau.csv', index=False
)


'''
Example 3: Exponential DC Decay data

In this example, the user has a csv file containing a set of tau* and beta
values, and wishes to create a new input file for ccfit2 relaxation.
'''

# First, load the tau and beta values from a csv file
# this example has commas as the delimiter
# and a comment at the top of the file
data = pd.read_csv(
    'dc_exponential_beta_taustar.csv',
    comment='#',
    skipinitialspace=True,
    delimiter=','
)

# Calculate <ln(tau)>, and add it to the dataframe with the
# header ccfit2 expects
data['<ln(tau)> (ln(s))'] = dc.ExponentialModel.calc_lntau_expect(
    data['tau* (s)'], data['beta ()']
)

# and do the same for sigma_lntau
data['sigma_ln(tau) (ln(s))'] = dc.ExponentialModel.calc_lntau_stdev(
    data['beta ()']
)

# If your original file contains the standard deviation of alpha and tau,
# then you can also compute the upper and lower bounds of ln(tau) based
# on those uncertainties.

# If not, just delete this line
[upper, lower] = dc.ExponentialModel.calc_lntau_fit_ul(
    data['tau* (s)'], data['tau*-s-dev (s)'],
    data['beta ()'], data['beta-s-dev ()']
)

data['fit_upper_ln(tau) (ln(s))'] = upper
data['fit_lower_ln(tau) (ln(s))'] = lower

# Then save this data to a new csv file which can be read by
# ccfit2 relaxation
data = data.to_csv('dc_exponential_with_lntau.csv', index=False)

'''
Example 4: Exponential DC Decay data

In this example, the user has a very old CCFIT2_cmd.py file containing a set
of tau and beta values, and wishes to create a new input file for
ccfit2 relaxation.
'''

# First, load the old datafile

# Load the headers (column names)
data = pd.read_csv(
    'dc_exponential_oldccfit2_beta_tau.out',
    comment='#',
    skipinitialspace=True,
    delimiter='  ',
    engine='python'
)
headers = data.keys()

# Then load the actual values
# this file has 3 comment rows and a header row, so skiprows=4
values = np.loadtxt(
    'dc_exponential_oldccfit2_beta_tau.out',
    skiprows=4
)
data = pd.DataFrame(values, columns=headers)

# Remove old tau bounds to avoid confusion
old_headers = ['tau_ln_ESD-up (s)', 'tau_ln_ESD-lw (s)']
for old in old_headers:
    if old in data.keys():
        data.pop(old)

# Calculate <ln(tau)>, and add it to the dataframe with the
# header ccfit2 expects
data['<ln(tau)> (ln(s))'] = dc.ExponentialModel.calc_lntau_expect(
    data['tau (s)'], data['beta ()']
)

# and do the same for sigma_lntau
data['sigma_ln(tau) (ln(s))'] = dc.ExponentialModel.calc_lntau_stdev(
    data['beta ()']
)

# If your original file contains the standard deviation of alpha and tau,
# then you can also compute the upper and lower bounds of ln(tau) based
# on those uncertainties.

# If not, just delete this line
[upper, lower] = dc.ExponentialModel.calc_lntau_fit_ul(
    data['tau (s)'], data['tau_err (s)'],
    data['beta ()'], data['beta_err ()']
)

data['fit_upper_ln(tau) (ln(s))'] = upper
data['fit_lower_ln(tau) (ln(s))'] = lower

# Replace tau with taustar, since this is what ccfit2 now expects
data['tau (s)'].rename('tau* (s)')

# Then save this data to a new csv file which can be read by
# ccfit2 relaxation
data = data.to_csv(
    'ac_generalised_debye_oldccfit2_with_lntau.csv', index=False
)
