
import ccfit2.relaxation as rx
import ccfit2.utils as ut
import numpy as np
from qtpy import QtWidgets


# MULTIPLE RELAXATION MECHANISMS WITH INTERACTIVE WINDOW
# Generate some fake relaxation data using model
temperatures = np.arange(1, 100)

parameters = {
    'R': -5.4, 'n': 4.32
}
# Calculate rates
rates = 10**rx.LogRamanModel.model(
    parameters, temperatures
)

parameters = {
    'A': -11.54,
    'u_eff': 957
}
# Add more to rates
rates += 10**rx.LogOrbachModel.model(
    parameters, temperatures
)

parameters = {
    'Q': -0.13,
}
# Add more to rates
rates += 10**rx.LogQTMModel.model(
    parameters, temperatures
)

# and add some noise
ut.cprint('Adding noise to rates', 'black_yellowbg')
logrates = np.log10(rates)
logrates += np.random.rand(len(logrates)) * 0.1
rates = 10**logrates

# Create dataset of rate and temperature values
dataset = rx.TDataset(rates, temperatures)

# Create QT app for interactive window
app = QtWidgets.QApplication([])

# Open interactive window and get user selected values
rmodels, fit_vars, fix_vars, exited = rx.interactive_fitting(dataset, app=app)

if exited:
    exit()

# Create MultiLogModel as combination of individual models using user values
multilogmodel = rx.MultiLogTauTModel(
    rmodels,
    fit_vars,
    fix_vars
)

# Fit to experiment
multilogmodel.fit_to(dataset)

# # Plot fitted model and dataset
rx.plot_fitted_rates(dataset, multilogmodel)
