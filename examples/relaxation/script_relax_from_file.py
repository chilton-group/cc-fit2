import ccfit2.relaxation as rx

# ccfit2 output files to be read in
# Here, the first is an AC file, second is a DC file
files = [
    'ac_generalised_debye_params.csv',
    'dc_exponential_params.csv'
]

# Make TDataset from these files
dataset = rx.TDataset.from_ccfit2_csv(files)

# Check the data was read in successfully by plotting
# rate against temperature
rx.plot_rates(dataset)
# and ln(time) against 1/T
rx.plot_times(dataset)

# Select models to use
models = [rx.LogRamanModel, rx.LogOrbachModel, rx.LogQTMModel]

# Parameters to fit
# Separated by model
# ccfit2 will decide which set of parameters goes with which model
# so the order doesn't matter here
# empty dictionary is for QTM, where no parameters are fitted
fit_vars = [
    {'R': -3., 'n': 2.},
    {'A': -7., 'u_eff': 1000.},
    {}
]
# Parameters to fix, one dictionary for each model
# in this case, we fix the QTM parameter
fix_vars = [{}, {}, {'Q': 2.447}]

# Create MultiLogModel as combination of individual models
multilogmodel = rx.MultiLogTauTModel(
    models,
    fit_vars,
    fix_vars
)

# and fit to experiment
multilogmodel.fit_to(dataset)

# Print final values of models (fitted and fixed)
for logmodel in multilogmodel.logmodels:
    print("Values:", logmodel.final_var_values)
    print("Std. Dev.:", logmodel.fit_stdev)

# Plot fitted model and experiment
rx.plot_fitted_rates(dataset, multilogmodel)
rx.plot_fitted_times(dataset, multilogmodel)

# Plot difference between experimental and model tau
rx.plot_rate_residuals(dataset, multilogmodel)
