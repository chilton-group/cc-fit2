import ccfit2.relaxation as rx
import numpy as np

temperatures = np.linspace(30, 100, 100)

# Define Raman Parameters
parameters = {
    'R': np.log10(1.664E-6), 'n': 2.151
}

# Calculate Raman rates
rates = 10**rx.LogRamanModel.model(
    parameters, temperatures
)

# Define Orbach Parameters
parameters = {
    'A': -np.log10((1.986E-11)**-1),
    'u_eff': 1223 / 0.695034800  # convert cm^-1 to K using boltzmann
}
# Add Orbach term to rates
rates += 10**rx.LogOrbachModel.model(
    parameters, temperatures
)

# Add some noise to rates
# done in log domain for simplicity
logrates = np.log10(rates)
logrates += np.random.rand(len(logrates)) * 0.1
# then convert back to linear domain
rates = 10**logrates

dataset = rx.TDataset(rates, temperatures)

# Select models to use
models = [rx.LogRamanModel, rx.LogOrbachModel]

# Parameters to fit
# Separated by model
# ccfit2 will decide which set of parameters goes with which model
# so the order doesn't matter here
fit_vars = [
    {'R': -3, 'n': 3},  # Raman
    {'A': -10, 'u_eff': 2000}  # Orbach
]
# Parameters to fix, one dictionary for each model
# in this case, no parameters are fixed
fix_vars = [
    {},  # Raman
    {}  # Orbach
]

# Create MultiLogModel as combination of individual models
multilogmodel = rx.MultiLogTauTModel(
    models,
    fit_vars,
    fix_vars
)

# and fit to hypothetical data
multilogmodel.fit_to(dataset)

# Print final values of models (fitted and fixed) and uncertainties
print("Final Model Values:")
for logmodel in multilogmodel.logmodels:
    print("Values:", logmodel.final_var_values)
    print("Uncertainties:", logmodel.fit_stdev)


# Plot fitted model and hypothetical data
rx.plot_fitted_rates(dataset, multilogmodel)
rx.plot_fitted_times(dataset, multilogmodel)

# Plot difference between hypothetical and model tau
rx.plot_rate_residuals(dataset, multilogmodel)
