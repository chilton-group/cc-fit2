from ccfit2 import ac


# Load experimental data - all measurements in a single output file
# all_experiments is a list of lists of experiments with
# first dimension as DC field, second as temperature
# An experiment is set of datapoints with same DC field and temperature
all_experiments = ac.Experiment.from_file('example.ac.dat', 50, 2000)

# This will be 1, since there is only 1 DC field strength for all measurements
print(len(all_experiments))
# This will be 19, since there are 19 different temperatures
print(len(all_experiments[0]))

# Cut down first dimension, since there is only 1 DC field strength
# for all measurements
experiments = all_experiments[0]

# Plot raw experimental data
ac.plot_colecole(experiments)

# Name of model
chosen_model = ac.GeneralisedDebyeModel

# Print the parameter names of the current model
print(chosen_model.PARNAMES)

# Dictionary of variables to fit in Model
# in this case, all are fitted and we assign the value 'guess' to each
# so ccfit2 will generate some initial guesses using the raw ac data
fit_vars = {
    name: 'guess'
    for name in chosen_model.PARNAMES
}
# Variables to fix in Model
# in this case, none are fixed
fix_vars = {}

# Make list of models, one per experiment
# Here using same fit and fit variables for each
models = [
    chosen_model(
        fit_vars, fix_vars, experiment
    )
    for experiment in experiments
]

# Fit model to experiment
# Update each subsequent model's guess using the previous one's best fit
# parameters.
# For the first model, use its own parameters
prev_fit = models[0].fit_vars
for model, experiment in zip(models, experiments):

    # Update this model with previous guess
    for key in fit_vars.keys():
        model.fit_vars[key] = prev_fit[key]

    # Perform fit using previous guess
    model.fit_to(
        experiment
    )

    # If fit successful, update next guess with fitted params
    if model.fit_status:
        prev_fit = model.final_var_values
    # Else keep the current guess
    else:
        prev_fit = model.fit_vars

# Print model variables and their standard deviations if fit was successful.
# Then print standard deviation in ln(tau) instrinsic to the Generalised Debye
# Model
for model in models:
    if model.fit_status:
        print('Fit:', model.final_var_values)
        print('std_dev:', model.fit_stdev)
        print('sigma(ln(tau)):', model.lntau_stdev)
        print()

# Plot experiment with fitted model on top
ac.plot_fitted_colecole(experiments=experiments, models=models)

# Save fitted model data (chi' and chi'')
ac.write_model_data(experiments, models)

# Save fitted model parameters
ac.write_model_params(models)
