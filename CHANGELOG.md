# CHANGELOG


## v5.11.0 (2025-02-28)

### Documentation

- Add dc images
  ([`9408abb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9408abb5e9a7c7f6671fd1f98707b7caca95deb0))

- Add images
  ([`55a4ac3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/55a4ac32ce00b465556ec69f92bdc87f3208cabb))

- Add images and update relaxation instructions
  ([`2e411eb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2e411ebbcbf5a1b96d7cca7b3197b9ea7cd8852d))

- Add version number to package name [skip-ci]
  ([`ad2e768`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ad2e76890affb1d3acc1b4764265b15b8a1538bd))

- Standardise text
  ([`a436c51`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a436c518f24c0700e3533065d00adf79702aff7d))

- Update whatsnew [skip-ci]
  ([`b6871a0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b6871a0c33cd710e06f24f4095f3e811483d7212))

### Features

- Add finalise and update fit buttons to FitWindow
  ([`7505841`](https://gitlab.com/chilton-group/cc-fit2/-/commit/75058418ced524e81f65e1fb173aedee076ba53d))

- Add update fit button to FitWindow
  ([`585fe3d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/585fe3d1a095e1bfc6049ccde4ec9c6ba258b47c))

- Add update fit button to FitWindow
  ([`fb8dde1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fb8dde14ab3431be4cf7b332728051fd8e157b70))

- Add update fit button to FitWindow
  ([`6419850`](https://gitlab.com/chilton-group/cc-fit2/-/commit/64198505d35652a9c2e1acef709e11283d8c83e7))

### Performance Improvements

- Change Fit button to Finalise
  ([`b08d8e3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b08d8e374f07dcbf0cf8eb550d1153fece72d87c))

- Change number formatting in dc fit plot title
  ([`fa83397`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fa833977d1fcabff207eac2e895a09502a59abe5))

- Change plot title
  ([`128e899`](https://gitlab.com/chilton-group/cc-fit2/-/commit/128e8994a6adf9b6c160b787d88d535a7f34549f))


## v5.10.0 (2025-02-24)

### Bug Fixes

- Add qtpy shim import
  ([`18c4912`](https://gitlab.com/chilton-group/cc-fit2/-/commit/18c4912cfe3892efba7c0796fcbcaca249de0c9e))

### Build System

- Add empty scripts init
  ([`e39c8cb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e39c8cb673d8b58d943eab222d1c709d0672a58b))

- Remove contents
  ([`f043ede`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f043edeb2523c4516a7065a41a9973328d60f82e))

### Continuous Integration

- Update build [skip-ci]
  ([`6fb96bf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6fb96bf049ebd7ee052a2367b74fb0cf20bb5600))

### Documentation

- Update whatsnew
  ([`6fb5c09`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6fb5c096d479812073f45e8e9ff7ec4a37c3c34a))

### Performance Improvements

- Add tab compatibility
  ([`b8db5e1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b8db5e152a18e783de19fdcc29e947d126a4b3dd))


## v5.9.3 (2025-02-13)

### Bug Fixes

- Add ignore for curve_fit warning in ac flat test
  ([`d498bae`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d498baef9a66331e3c076ec68a42c24dd0e9ca7b))

- Change units of omega (PPD) from cm-1 to K
  ([`4cb9fef`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4cb9fefaaa1e021bc854dc446e6f57391ab89bcb))

### Continuous Integration

- Debug ci config
  ([`e401e23`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e401e23a8bd3f2910519e6fe8772aab35da46cc1))

- Debug ci config
  ([`8d30dc5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8d30dc509f10b71060eeb5b7aa96e48b761be3a3))

- Update build
  ([`9871314`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9871314158c1337b9ba274554b41dc79bfe4a09c))

- Update build
  ([`180d8d3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/180d8d38172bffa20949b12e31bb585e7a83852b))

### Documentation

- Fix typo
  ([`26a729b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/26a729bae2d69ba03d87f5c7942b4b6c202ce1e6))

- Remove debug print
  ([`ec3d482`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ec3d4824cff11967559ae5c5781612541bf691f7))

- Remove x11 installation instructions
  ([`ebed04d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ebed04df7c72de0ac810062c519e7e429fc9bc9a))

- Update ppdraman unit
  ([`66e4d31`](https://gitlab.com/chilton-group/cc-fit2/-/commit/66e4d31e9c267d949af467b229fdac6b9b66e9bf))

- Update whatsnew
  ([`3d3cb98`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3d3cb98a2e60c817c61cdc6b15e035d5a427f5c2))


## v5.9.2 (2024-11-14)

### Bug Fixes

- Ac freq for naming ft and mf plots obtained from field not moments
  ([`b05d7a2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b05d7a20c4098742cd14c757cb378f72a9c52a39))

### Documentation

- Updated WhatsNew
  ([`36cc351`](https://gitlab.com/chilton-group/cc-fit2/-/commit/36cc3511f8c69ce4cdc6b1df717f751a8d684202))


## v5.9.1 (2024-11-08)

### Bug Fixes

- Add dtype specification/enforcement to field, temperature, and depvar attr setters of HDataset and
  TDataset, and enforce dtype in from_ccfit2_csv
  ([`5403c80`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5403c80b5f86719dc0099adb214b3151a94ac89e))

- Add dtype specification/enforcement to field, temperature, and depvar attr setters of HDataset and
  TDataset, and enforce dtype in from_ccfit2_csv
  ([`0d9b4f9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0d9b4f9f23c71302f886f60cd8576d44ae6e5a02))

### Documentation

- Fix duplicate target name using anaonymous hyperlink reference
  ([`db1a1fb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/db1a1fb4a78a2ec2d7aab4ba4546e41072a3061f))

- Fix incorrect formatting
  ([`210f5e1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/210f5e1bca5f1e684c5dce6f36ee4db5dcb628f3))

- Fix typo [skip-ci]
  ([`3f49859`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3f4985907bb7b3f14c5adae06009c6b5368826f2))

- Remove deprecated version number feature
  ([`4326f77`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4326f77871c5e32c168a977df04eb02e9b514f55))

- Update whatsnew [ci-skip]
  ([`ccf7f31`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ccf7f312b6d4a8a7c96b37b24b40e8e7c4e59ba0))


## v5.9.0 (2024-10-28)

### Bug Fixes

- Corrected PPDRaman model equation
  ([`fd155aa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fd155aa7b646b8ccb0b0e630eca4f7fe49c0fd61))

- Fixed superscript parantheses in fitting window
  ([`72be6b0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/72be6b03eb66f817406cfc6ff9f4ac5210597686))

### Continuous Integration

- Update changelog location [skip-ci]
  ([`ceea440`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ceea44032a301926763dffa8bd275a33ffda323d))

### Documentation

- Add Phonon Pair Driven Raman-I field and temperature depeendent model
  ([`7273d71`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7273d71b01bf1d9330c0e732dc5f4746c50c4a4f))

- Correct latex
  ([`d49e5ec`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d49e5ec048133317cf5b5d680294f2037bf76667))

- Fix typo in Phonon Pair-Driven Raman name
  ([`9c19614`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9c19614828126ca7a7fd097917618f8782265192))

- Include PPDRaman-I model in Docs
  ([`707198e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/707198e5ea9ecba8a9050c07988f6b0bf8d0e932))

- Include PPDRaman-I model in Docs modules
  ([`d23d36f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d23d36ff3646dcd11d32d6b90e5707d546ca04cd))

- Update documentation
  ([`96ee43f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/96ee43fc88f6a5074bc3e34107e93b2cd03077ad))

- Update documentation
  ([`62c750b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/62c750b2fd1d2715188e9734144cd2713b063fd6))

- Update whatsnew
  ([`0eb01da`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0eb01dacf8801f843c55f7f43002e2a339abf513))

### Features

- Add alternate PPDRaman model based on the Fourier transform of the two-phonon correlation function
  ([`4a37713`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4a3771378ddc8696932c2832a6f773192512cfaf))

- Added field indeoendent PPDRaman LogTauHT model
  ([`3d4078c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3d4078ceec4f8ee6ab5bdd2744e20b4eec14be6e))

- Added LogTauHTmodel PPDRaman-I model
  ([`fa8dd55`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fa8dd55e915f74c97144b4c79145bfc2421065e1))

### Performance Improvements

- Add ignore for warnings
  ([`09554e1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/09554e18c74a8477ddc53f850d2f6b07681c3319))

- Change w --> omega for ppdraman model
  ([`f1af44a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f1af44ac5ca760eab45f02745200916074d33d77))

- Tweak filter ignore
  ([`ae577a8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ae577a812d0fa1edfb4d1b091d671c7c91c2b908))

- Tweak warnings
  ([`c7d3495`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c7d34950755e57a20f27bb437bd235ba22b6f64d))

### Refactoring

- Change bounds on models
  ([`b54c950`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b54c950872bc6cff7ba8b255d4e7995cdf8fcf12))


## v5.8.1 (2024-09-03)

### Bug Fixes

- Remove deprecated np.NaN instances, replace with np.nan
  ([`648c35b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/648c35bf91de5abe94f9f41321b202886a12ea7a))

### Build System

- Modify dependencies
  ([`2c49e1e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2c49e1e28a305c1a541fb30b0449ad074dd5f983))

### Chores

- Remove old files
  ([`3a3da7a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3a3da7ace2e79a86eec7406f4c5426fac5f6c81d))

### Continuous Integration

- Remove variable
  ([`cf4ed98`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cf4ed9802d3f932304c9d076ac423b00f73279bb))

- Update ci variables
  ([`910333a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/910333a60a65c0a8195f32ad80323c94e15f5159))

### Documentation

- Update documentation
  ([`3eae898`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3eae8982745b4d74407b1d60eef04229c0e0d1a4))


## v5.8.0 (2024-03-20)

### Bug Fixes

- Correct bug in fit lower and upper bound order
  ([`fc4535b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fc4535b15fdbf84954ba2dee46c96c735227b4cb))

### Build System

- Add arrow dependency installation to tests to avoid pandas error
  ([`4447537`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4447537c12f0dcf685f1c53242422d26a79154b9))

- Change script name
  ([`d868cf6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d868cf6f9021098fffe669825a3767fef77c596d))

- Update script name
  ([`245745d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/245745de2680baa2928673c0af7db0f977c7821e))

### Chores

- Remove old file
  ([`d842c73`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d842c73f6d8e7a313a1920ba9cbc2fcee11464fa))

### Code Style

- Add comment to updated csv
  ([`820fe28`](https://gitlab.com/chilton-group/cc-fit2/-/commit/820fe28f0e6187ca17770a070e930483be3d55eb))

- Improve license formatting
  ([`3f8de97`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3f8de97d02b6962363f1daea0c26146e25588d69))

- Improve relaxation cli error print formatting
  ([`17078b3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/17078b3602a3d93f59e7d3587e1e31f616149b1e))

- Rename script, simplify typehints
  ([`3592fcd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3592fcd20281bb6290aab10cd42687d4fccb4d9a))

- Simplify numpy typing typehints
  ([`e59f06a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e59f06a3e569577b1caa588c8cd747df41dce414))

### Documentation

- Add license
  ([`27afe97`](https://gitlab.com/chilton-group/cc-fit2/-/commit/27afe97d629345d150a1ae7a26fb77f475d6f08f))

- Add two tau docs note
  ([`13d2929`](https://gitlab.com/chilton-group/cc-fit2/-/commit/13d29292035e1333edc16ee5184bd0507e8f4941))

- Add update_ac_dc_params and split_rates information
  ([`98d29c8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/98d29c8e43f7a29383eeef7ed76f25a1888bdc63))

- Fix typo
  ([`76b9eda`](https://gitlab.com/chilton-group/cc-fit2/-/commit/76b9eda4d370519af246e4f5a9e3d97aa75fb0d0))

- Fix typo
  ([`d60bf56`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d60bf5633adbc5f69851e1d7cc1974c58811ee16))

- Fix typo
  ([`902a9b5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/902a9b589df5d33936bcd28991c6f331ff629038))

- Fix typo
  ([`64cd8de`](https://gitlab.com/chilton-group/cc-fit2/-/commit/64cd8de72887c5d55f996875344e53786163b784))

- Fix typo
  ([`e6e0c1a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e6e0c1a949ee2381dc86b4cde10e28b6d85e4801))

- Fix typo [skip-ci]
  ([`c034048`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c034048eb33476e865c10f2f106b39367a81a7c2))

- Fix typo [skip-ci]
  ([`79c0413`](https://gitlab.com/chilton-group/cc-fit2/-/commit/79c04137e22bc1aa385573fc01dc0deb8cc79514))

- Improve language
  ([`28d5089`](https://gitlab.com/chilton-group/cc-fit2/-/commit/28d508974aaeda2ec338a9d78b951aeb545db0c5))

- Update docs
  ([`0f9acfc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0f9acfc7d942c57278a6392bab2063e57a4e2a5f))

- Update documentation
  ([`d45e919`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d45e919aa2ed49882c588e4a57340e87f5b9feee))

- Update executable page
  ([`1c76b67`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1c76b67a4e15a055134f90f57fe58905b5e0054e))

- Update executable page
  ([`1bd2f1f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1bd2f1f67ad87f162230622f41b9eca0f9564639))

- Update language
  ([`022109e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/022109e7d6f7f4a910c3b794dc7f25c5f7290293))

- Update two tau warning explanation [skip-ci]
  ([`41a37fd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/41a37fd9221296be1739d0e560f73b4c137799d4))

- Update whatsnew
  ([`ae20fba`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ae20fba04ff6123285b4a1af06ec3273ba6b0a5b))

### Features

- Add relaxation splitter script
  ([`bf230a9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bf230a9e373f6fba845fe67a269af21d642edff7))

- Add script to update old relaxation files
  ([`6e854b9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6e854b9668b8d327d0b9b98437c46aaf3082836c))

### Performance Improvements

- Add additional headers to old files
  ([`2745b7a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2745b7ac3dc94e5085e2c8d94f11897ca5420bc9))

- Add better support for space delimiters
  ([`089868e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/089868e121c5f2cd6a820eb570f655c0c1f2a16b))

- Add catch to suggest new model
  ([`9c70e6b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9c70e6ba2458bfed745a0055076e42cf46ffb164))

- Add hint to use split_rates
  ([`e79d0b2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e79d0b25b263fa83f711354b5f9ba244e8549dfc))

- Add more old headers
  ([`e227346`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e227346472c9820d7b07572dd23099f3c4b80ffb))

- Remove debug print
  ([`4d83c1d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4d83c1d9bdf4adeaa15cc10952403ff3f975969a))

- Rename script
  ([`ac660ba`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ac660baebf2fae27ce23bbbf7ea8fe43bcd13c13))

### Refactoring

- Set split_indices to values needed by np.split
  ([`0c1913a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0c1913ae4e3dd635841363832531cab6969c4733))

- Simplify mean value code, and ac.experiment from measurements
  ([`e940096`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e94009681ba750ad8c9190fc8fcd3eaca4802541))

- Simplify measurement split
  ([`7fc7e0b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7fc7e0bcd76b3aa5e4462bf3e5821719733037bf))

- Simplify return list of ut.find_mean_values()
  ([`02b132f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/02b132f60a4d5a65a6e7b5dea4b936730d96da91))

### Testing

- Add scripts test files
  ([`f232c52`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f232c52a03e4b339d548d539d3f9d5cd9686403b))

- Remove result file
  ([`acb2e00`](https://gitlab.com/chilton-group/cc-fit2/-/commit/acb2e00cc256196c9efa0373d5668edccda8ba6f))


## v5.7.1 (2024-01-05)

### Bug Fixes

- Reinstate symlog field axis in fitted rate plots
  ([`1f5004e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1f5004e4967f8fa1249be9704f5cfe079bce0a62))

### Documentation

- Add link to old ccfit2 file conversion in faq [skip-ci]
  ([`1f945aa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1f945aa3c1a3d8c3ee0163150cca205e3ac87bfe))

- Fix typo [skip-ci]
  ([`37cf859`](https://gitlab.com/chilton-group/cc-fit2/-/commit/37cf8595cee5b79258881461a8f41a97ae88daf2))

- Minor changes/improvements [skip-ci]
  ([`1b9866c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1b9866c9a85e6096e0c983a0618d7fd91136c638))

- Update link to old file conversion script [skip-ci]
  ([`f588fc0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f588fc07bba1dbdcc2903425782a681e524a86cb))


## v5.7.0 (2023-12-04)

### Bug Fixes

- Add check for total model failure in ac and dc
  ([`4c86ab1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4c86ab156ad5827e9766b00c65f975f190280a54))

- Add missing exit for two tau models
  ([`1b6f415`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1b6f415eab9f6217006f336da2a12a28829dd9e2))

- Add missing exit for two tau models
  ([`99bb6f3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/99bb6f3103a921868cc40caec4168966f60531e4))

- Add missing staticmethod decorator to calc_lntau_expect method of ColeDavidsonModel
  ([`2efd134`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2efd134bc5846bae9e12e09e8c310670fa3551b9))

- Change header in save ac magnetometer file to be compatible with from file
  ([`e989b56`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e989b560c30f1d2573eb2b277d15addfc32cc14e))

- Consistency between guess parameters values and guess in GUI
  ([`c5ea991`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c5ea991fbc980f6156c32c9305a631dc719334fc))

- Fix bug in 'guess' fitfix and dataset.from_ac_dc
  ([`3e9c2d6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3e9c2d674656256d0401ff55882a17a3b69d268e))

- Fix bug in multilogtaumodel name attr
  ([`8741d49`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8741d49e3bcb2d17b256e296d7b4e13c27970c5c))

- Fix bug in multilogtaumodel name attr
  ([`d3d163a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d3d163a0a6b698ea8307ab65d3d9165b305831d7))

- Remove blank characters and carriage return from ac input file
  ([`a238231`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a238231e24fdd708dbc44718845bffe3b9fa0c96))

- Remove blank characters before/after delimiter, and carriage-return in magnetometer file
  ([`d6ae421`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d6ae421a3b5fc3729ab3fce8960e725efc5b8e3f))

- Remove bugged multilogmodel final_var_values and fit_stdev
  ([`0755a0a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0755a0af8ae4821a324fe33873372434aeaf2c79))

### Code Style

- Improve pep8 compliance
  ([`fada5ed`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fada5ed33be3f6ad679d6ab3bc6cacff14350922))

- Remove double blank line
  ([`b354cc7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b354cc76de2bbf837a2bf85527e74f245cf9104a))

### Documentation

- Add additional example for old ccfit2_cmd.py files
  ([`65d8c74`](https://gitlab.com/chilton-group/cc-fit2/-/commit/65d8c743aa9cd87f1f826b14563616e9c303b389))

- Add BVV Raman field and temp dependent equation
  ([`0304eda`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0304eda084ef77e2aaa42354298b5e9af188ccfd))

- Add docstring
  ([`1917140`](https://gitlab.com/chilton-group/cc-fit2/-/commit/19171406fbbb06e4ecef9e9509fa4befe6f64e8f))

- Add example script for calculating lntau values from alpha, beta etc
  ([`7125ed7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7125ed7e96f95c68b938d86d5b843c76150740ae))

- Add example script for fitting field and temperature dependent data
  ([`495adc9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/495adc9095b17e7972bcfb127ac7caa0eb36b1fe))

- Add field and temperature dependent models
  ([`d1bb357`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d1bb35781a04f1a5733c95c1a64da2f3c5239817))

- Add manual lntau metric calculation example information
  ([`3217f07`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3217f07753b50efb6664ee1b46c27d9f4ca2882c))

- Add phonon bottleneck to modules page
  ([`9de0d89`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9de0d892327a28312f88089742f702a3935ba021))

- Add phonon bottlenecked terms
  ([`5e2e457`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5e2e457a15868a32927b927ce1a2c872a7b69ba4))

- Add tests section
  ([`0c69f3f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0c69f3f9533356b1b8eb8879cd00f2d78289459c))

- Fix class name
  ([`0900486`](https://gitlab.com/chilton-group/cc-fit2/-/commit/090048646df56caf2425ec44654de5cecec8b0de))

- Fix relaxation model equations
  ([`8fdbc75`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8fdbc75d3f91e720b52b84256a94dbd3a59b4ba7))

- Fix typo in example
  ([`ea9b41e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ea9b41ee34e3646fdc127208925b731611169339))

- Improve docstring
  ([`8bbe192`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8bbe1927a871f339f30960f4c6db81581845f507))

- Improve docstrings and compatibility with sphinx
  ([`f0c6f68`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f0c6f6887fe5614c805b367ac9ad943e9c2ca7a0))

- Improve docstrings for all model classes
  ([`fde1aff`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fde1aff97ce6cd2fe24ca655f5652d46d1969cb5))

- Improve docstrings for model classes
  ([`f17f07f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f17f07f5aefe0b2dd1b5ea2901a69dcd11f95e9d))

- Improve experiment docstring
  ([`81a5397`](https://gitlab.com/chilton-group/cc-fit2/-/commit/81a53976d7668328d4089882b6aeaa27153f3133))

- Improve model docstrings
  ([`7f7de83`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7f7de83f92263c47b82ba406e715cf8fa49cd76e))

- Improve model docstrings
  ([`a781f07`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a781f0746102f32d81e51871cd09d0bff972fbc0))

- Improve model docstrings
  ([`b3256f1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b3256f1b73a6aec83863964418022839c2291ebb))

- Improve model docstrings
  ([`affcfd2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/affcfd2a4f761cdb49009650dcb410a3b979699c))

- Modify author contributions
  ([`bb159e1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bb159e1dee40d8621ca6f4da05b327793d0ae332))

- Modify description of models in write_model_parameters
  ([`b39a6d3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b39a6d39e8f0d163ceea97e9a880617762b86de2))

- Remove extraeneous docstrings
  ([`8dd09f4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8dd09f40aedc661c7ff766702d89f4b7e1066f60))

- Remove manchester
  ([`845ec89`](https://gitlab.com/chilton-group/cc-fit2/-/commit/845ec891e9f4d5573328fb5b0e16260128ce9ed0))

- Remove whitespace
  ([`dfc84a6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dfc84a6c2740cd44b9e9209c9949cebe21c233a1))

- Remove whitespace
  ([`eb5d406`](https://gitlab.com/chilton-group/cc-fit2/-/commit/eb5d40650cc1e4e50631e646eb0c5bcada0d8f61))

- Udpate model docstrings
  ([`f595ce0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f595ce05f65f9d24aaf93bf694b14b480124c858))

- Update ac script example
  ([`4194a2c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4194a2cb338dd3ef3b3132ab085f6a320fae8eeb))

- Update ac scripting example
  ([`2e99dbe`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2e99dbed4e6eac71daa670481b9bf84e3fe8fb75))

- Update ac scripting example
  ([`bead98f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bead98f600bb968217d323d4c3b73e30e7a06a90))

- Update comments in test example
  ([`289cc0a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/289cc0a553924562ed2c9b2e185a19025a8abeab))

- Update docstring
  ([`11c96b7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/11c96b74219ba0d52f0fda61906de2b61d553d25))

- Update docstrings for field and temperature dependent models
  ([`04e3996`](https://gitlab.com/chilton-group/cc-fit2/-/commit/04e399670d5ab4a78f83553d22d6c0ba4736de45))

- Update documentation
  ([`5f156fb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5f156fb1620aa4a6758993deffbc041bd59c80a8))

- Update example
  ([`e52474e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e52474e451d2083b9a24c25f3f5df62fc6e108cc))

- Update example comments
  ([`4a8c47d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4a8c47d1b1abbacdea88564e2b1bafdcec813f4b))

- Update example comments
  ([`28bf55a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/28bf55a450bf55cd1cbe1f68db40aa3325e6a2f7))

- Update example documentation
  ([`da9d525`](https://gitlab.com/chilton-group/cc-fit2/-/commit/da9d5256527ccde1f5f5368defec962e35405ed0))

- Update examples
  ([`e471b6f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e471b6f0b3ee55049fd230ceea19afa2bcddf8ca))

- Update examples with simpler parameter print
  ([`b887129`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b887129456287b4719ad4ab761daea18b0a6beb3))

- Update modules docs
  ([`1a71bd6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1a71bd69a936a9e0450d2da2297325ee5f643d6d))

- Update relaxation class names
  ([`9af8411`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9af8411d9a95e20b404a859ecaea074fa5a31343))

- Update relaxation module
  ([`6553e76`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6553e76e2c87c81d5b8fe57800e9d46ae4a82765))

- Update scripting docs
  ([`dcb6f1e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dcb6f1e9e653e96b9bffea630f71a55d2703bae7))

- Update scripting documentation
  ([`225c7c8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/225c7c8810465c0eef1364ee1f6bec0b530b7a9b))

- Update scripting example
  ([`4e69fb4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4e69fb4fbd077576e4f45715346a0f8b7325024b))

- Update scripting example
  ([`0f69b20`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0f69b209726981fa34c0a487be02bd1df2dd3991))

- Update scripting example
  ([`d891764`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d8917640340bfb421258aab0e73f5a2241991dcb))

- Update test
  ([`6dce4a3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6dce4a36115a1a2a2d9af687bd97267e089f3ab5))

- Update to reflect HT fitting, phonon bottlenecks and BVV Raman-I
  ([`3933d0a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3933d0a8b58f761ebd26c8cd8e89c7dbc706e35f))

changed Brons-Van-Vleck Raman to Brons-Van-Vleck Raman-I

- Update whatsnew
  ([`180d7a0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/180d7a0276042ca8f41236d96ea54fc352280b2f))

- Update whatsnew
  ([`91e8055`](https://gitlab.com/chilton-group/cc-fit2/-/commit/91e8055f2d02548e72336c4a6e12bafbc347e107))

- Update whatsnew
  ([`294d597`](https://gitlab.com/chilton-group/cc-fit2/-/commit/294d597ad0c2fd4088ba9e765d4e9bef86b0d91c))

- Update whatsnew
  ([`016be86`](https://gitlab.com/chilton-group/cc-fit2/-/commit/016be866f4f9c949e5f7145d1b4baf9b25177b94))

- Update whatsnew
  ([`4bd0677`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4bd0677507afcb888a1d544968a464c93331278c))

- Update whatsnew
  ([`2bfe79c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2bfe79cdf0e0b36a824b68f2356b1b8d1d4fc312))

- Update whatsnew.rst [skip-ci]
  ([`b41613b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b41613bf18734a1ea83cd0f11aac83d44778fe96))

- Update whatsnew.rst [skip-ci]
  ([`063fe18`](https://gitlab.com/chilton-group/cc-fit2/-/commit/063fe18b07c211d7d8e9933f04e57fa576db6785))

- Update whatsnew.rst [skip-ci]
  ([`6dc78a7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6dc78a7bdb6a246e4729d34ad15ba6eeefe708b0))

### Features

- Add Brons-Van-Vleck Raman process with field and temp dependence
  ([`6ec4a77`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6ec4a771a54cf53e414b32cb3451269f57414bb3))

- Add field- and temp-dependent relaxation models
  ([`6ec080a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6ec080a4fd4e67178030eb5d6af13698a123f2d0))

includes general Raman-II / Direct relaxation model that has H and T dependencies, Orbach model
  (field-independent), QTM model (temperature-independent) and Raman-I model (field-indpendent).

- Add final_var_values and fit_stdev attr to multilogmodels
  ([`805865b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/805865b2b3d62481ce8cfc885580e723256ac62b))

- Add model class for fitting field- and temp-dependent tau
  ([`e9e20c3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e9e20c37345d8b26426b6ad07fd74e56ddab3723))

- Add multilog model for combining field- and temp-dependent models
  ([`dc43468`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dc43468b1cf48a88730fbb65f406aeae4de982c7))

- Add phonon bottleneck term for raman and raman+qtm
  ([`2e74f7f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2e74f7f768ae32db9ce9cc8fc522987edf43afa6))

- Add support for just sigma_lntau, just fitu/l or both in from_ccfit2_csv
  ([`9817e0b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9817e0b6a92b8acb875cb48d8e67fbf4f5861a46))

- Add TauHTDataset class for exp. rates as a function of field and temp
  ([`736e644`](https://gitlab.com/chilton-group/cc-fit2/-/commit/736e644c7867e11290cb04fd0c2025fee2d4f8e2))

New class that has field and temp as parameters, sorted by field and then temp

- Support LogHTModel and MultiLogHTModel in write_model_params
  ([`7c18ec9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7c18ec9a522b76fb96b0c56f55eaeb433453ef45))

### Performance Improvements

- Add backwards compatibility decorator for guess arg of fit_to and print deprecation notice
  ([`5dd3f6a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5dd3f6a5044601009b3198cac028ede2b0a5151c))

- Add check of logmodel types in multilogmodel init
  ([`34b3e44`](https://gitlab.com/chilton-group/cc-fit2/-/commit/34b3e446dc3465e2d063915394949101b4710f55))

- Improve --single_plots help
  ([`c625ad5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c625ad548c11236a59f2cd490c8ff78728ba24bc))

- Improve lntau metric methods
  ([`e24f640`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e24f6401088f252de7cd9d65c97f8abe89fe449d))

- Improve sorting via field and temp from csv files
  ([`4159c0f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4159c0ff5ab726f3a648d4f642d856d98da6a8f9))

- Only show model pot on single plots if model.fit_status is true
  ([`63a12f6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/63a12f685a01e37bed0b32c3215633c9e7ecdef4))

- Standardise guess string in fitfix
  ([`70b29e8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/70b29e8dc33adb59b0970db577f2d80dc69e332f))

- Tweak exception in mulitlogtaumodel init
  ([`293e15b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/293e15b3cf7505fe7bc0c63bcd2a4fe8fedc1eb9))

### Refactoring

- Add catches for mismatching model and datasets
  ([`d25ff5f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d25ff5fa640a9729163b42c974fedbfff165d71e))

- Add public method for calculation of lntau metrics
  ([`ef019f7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ef019f7df8ed4b7d746dd947e3f80b05ee252da6))

- Change letter for direct/RamanII field and temp dependent process
  ([`143c34b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/143c34b431cf628506e179f1eb671ec5383d124e))

- Change letter used for direct/Raman-II constant, fixed bounds
  ([`e51b9a0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e51b9a0c0f24b3e71d7c31f5da37e450a8cd6011))

- Create ABC for LogTauModel, MultiLogTauModel, and Dataset
  ([`4a7514c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4a7514c3b4952a936e57c905332af2bc48a085d1))

- Further improve T and H datasets, and add HTDataset inheritance
  ([`162ebdf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/162ebdf2c1ccf7a45de1438f6c5cfe04b16e6962))

- Improve dataset initialisation
  ([`e52a4bb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e52a4bbddcb69d13588ade3b1fadc0ec3dd6f14f))

- Improve dataset instantiation
  ([`1a45552`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1a4555272a90af33318375ca52e413903a8edf23))

- Improve dataset instantiation
  ([`bc397b1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bc397b1bca2c0a7b357a1a30f35ad2c8e2df3cf9))

- Improve temperature and field sorting in Dataset classmethods
  ([`a3315ed`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a3315ed83a054485e37170159288ef8edd45c7c0))

- Reinstate temp and field sorting in Dataset classmethods
  ([`48b0772`](https://gitlab.com/chilton-group/cc-fit2/-/commit/48b0772e62c48a226e069d76fc7ec3e2b2777f2f))

- Remove broken double tau model support
  ([`a31acf3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a31acf343a895f2fbfa3b4af225df7354a7d01e6))

- Remove guess optional argument from ac, dc, and relaxation model.fit_to() methods
  ([`752222f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/752222f98abcbca32f474c6f1f95e2ad1ac8f15b))

- Remove support for params files with TauHTDataset
  ([`79a9789`](https://gitlab.com/chilton-group/cc-fit2/-/commit/79a97892b2b516df1064d1251f2c45d8ba4a712c))

- Replace TauTDataset and TauHDataset with TDataset and HDataset, add warning for old classes
  ([`9812deb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9812deb4ca314dfb4394dbdf9dc7588c10e53268))

- Simplify dataset handling in write_model_data
  ([`c884bcf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c884bcf6d24c3acd60e25942e19e7dbb3de59186))

- Simplify dataset handling in write_model_data
  ([`d9c3c83`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d9c3c8322f5de23ee4549e71d7f81aa43bf07c63))

- Simplify dataset handling in write_model_data
  ([`8b31853`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8b31853ea669f1a30c9d52c0b987cab029521b90))

- Simplify header parsing in relaxation csv reader
  ([`ad2d912`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ad2d912f0256f65eebb1588f0553ab1e276210c2))

- Simplify header parsing in relaxation csv reader HTDataset
  ([`94d2b68`](https://gitlab.com/chilton-group/cc-fit2/-/commit/94d2b680970219a6b56327e66bd6f69887ff7a47))

- Simplify relaxation cli function
  ([`ba18555`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ba18555fe06743b6720e8a362cd12ccb43560901))

- Update return types for new abc
  ([`dd5983e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dd5983ee523beb6f5da1e64415658f53c48d8136))

### Testing

- Add temperature-dependent relaxation rates in multiple fields for tests
  ([`0c8828f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0c8828fdc44bf9b2db853322dfe4bc13f9e021f9))

- Add test for field- and temp-dependent datasets
  ([`d1b06d0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d1b06d0d7b1a03776537a1c731e11321f825f3ff))

- Update unittest
  ([`a6d6171`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a6d6171d4b0cdb6c9c02a38c229939ac44ca2241))


## v5.6.6 (2023-11-13)

### Bug Fixes

- Change header in save ac magnetometer file to be compatible with from file
  ([`05dcfcc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/05dcfcc527777a82d7b94481532b97c385451609))

- Remove blank characters and carriage return from ac input file
  ([`b0f3969`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b0f3969177dde5f850cbd540fce9496e83824862))

- Remove blank characters before/after delimiter, and carriage-return in magnetometer file
  ([`c71e589`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c71e589ba0426e72a922f31d886ba5e200cca616))

### Code Style

- Remove double blank line
  ([`1967e12`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1967e12d2bd9aa199d01cce6a0c1a8dc7baaf590))

### Documentation

- Update whatsnew.rst [skip-ci]
  ([`c9abace`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c9abace2f78c3549623c2b11f199a57f25f2ee21))


## v5.6.5 (2023-11-01)

### Bug Fixes

- Print final variable values to .csv
  ([`8f18c6f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8f18c6facb3b6d99df09e8c9a322a115a39a0d9a))


## v5.6.4 (2023-10-31)

### Bug Fixes

- Add warning message to relaxation mode when .out extension specified
  ([`60bb982`](https://gitlab.com/chilton-group/cc-fit2/-/commit/60bb982a817aea6f41915c8948a754582475be88))


## v5.6.3 (2023-10-31)

### Build System

- Add tests
  ([`1f3d92d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1f3d92d56c85903095b566548cdbfa6fcaafab0e))

- Modify ci
  ([`07f7f18`](https://gitlab.com/chilton-group/cc-fit2/-/commit/07f7f18af877445727a6ed62860e10c739fe7e22))

- Update ci
  ([`ff541a1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ff541a18bd8e36fd7142c9fd4b72475fc969f08a))

- Update ci
  ([`a889ab1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a889ab1de2681508760dda5e09b44c71777315eb))

- Update ci
  ([`44df702`](https://gitlab.com/chilton-group/cc-fit2/-/commit/44df702a822514b0783a7ef23ccd0ec9ccd5ce04))

- Update ci [skip-ci]
  ([`53ef929`](https://gitlab.com/chilton-group/cc-fit2/-/commit/53ef9298393042c556fa733e1b9993788e6fceb7))

- Update ci [skip-ci]
  ([`3f975cc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3f975cc41932089f018e41dddce2342b297227b8))

### Code Style

- Improve variable names for ftexperiments
  ([`a70bd70`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a70bd702a5d2c84e36dee8c87d95e5e85ef3eddd))

### Documentation

- Add experimental latex build config
  ([`bdd2d06`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bdd2d06ca1e99dff3d971d360347cbbc43881c0c))

- Update dc script
  ([`b57b1a9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b57b1a9e06168bff92476c24d8e74bb51ab9704a))

- Update documentation for example scripts
  ([`314774d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/314774d214724dd58d7f6e4529631ab4798a3895))

- Update example data
  ([`98aae24`](https://gitlab.com/chilton-group/cc-fit2/-/commit/98aae244aa8924f28d9a6c2c831b7e10a68ef21d))

- Update example data location
  ([`e0a3e95`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e0a3e957b0bb019d85deeed432d3c2679c92cd42))

- Update example locations
  ([`14c8b05`](https://gitlab.com/chilton-group/cc-fit2/-/commit/14c8b059862ad26f01809f10fd910cb4c82ff031))

### Performance Improvements

- Add single experiment support to save_ac_magnetometer_file
  ([`c417110`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c4171102df6b2018d6b0ede014018f36684bc71f))

- Improve flat function
  ([`e5bacb1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e5bacb12dbb62d1715a625d8c5d34a4819fe47d5))

- Improve single experiment and single model support in cole-cole and susc plots, improve docs
  ([`fef9b11`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fef9b11a5853b5c4687deb1ab290a416c8045504))

### Refactoring

- Change dc.Experiment.from_measurement return type to match ac counterpart
  ([`77697bd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/77697bd9acbdb4a88f68bfdd23fb36b6da977779))

- Clean up matplotlib button generation code
  ([`8561834`](https://gitlab.com/chilton-group/cc-fit2/-/commit/856183446fbd4077fadc6af96d03a3ab416e56a2))

- Convert HEADERS_GENERIC to list
  ([`ac7da91`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ac7da91a3883196700b8f2de6955c2e0ae21322b))

- Ensure correct type returned by Waveform.Experiment.from_files()
  ([`e8c5fdc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e8c5fdc2aae8182058158a049cfa68eae1accb4e))

- Improve ac magnetometer file output
  ([`8782874`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8782874607eac5b8651e5bbdbcd903332a60ff09))

- Improve csv comment specification
  ([`a5bbec3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a5bbec31e6a6c743eb663560be3f71f47226491b))

- Move old file load to protected function
  ([`7265370`](https://gitlab.com/chilton-group/cc-fit2/-/commit/726537026879d2931cb8f53561db2f482c1fee27))

- Remove sys.exit from backend functions and replace with exceptions
  ([`126f09e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/126f09e233bb99a67e0a0f9cae24791c1ba78369))

- Rename FTResult to FTExperiment
  ([`553062d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/553062dae2957ad7aa12b2b01e9b69a452217624))

- Simplifies magnetometer file parsing
  ([`0d3755b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0d3755bd1e803baa788a1920856a47a06973c1ca))

- Update old ccfit2 file load in relaxation
  ([`3164cbf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3164cbf66e7994fde3148e8c8f226f495290c6c6))

### Testing

- Add ac output save tests
  ([`13e755b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/13e755bd48693ee64c2de6bae933f6e2ca56a900))

- Add ac_test
  ([`6128501`](https://gitlab.com/chilton-group/cc-fit2/-/commit/61285019cc9d727a346411b492ee1873ebd6c387))

- Add dc test
  ([`7b92d3e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7b92d3e2d690fd5389f5a3a999127bae0c598999))

- Add flat unittest
  ([`2ee3ad9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2ee3ad9fd37aade01284aaa1fcddda0dddc12de9))

- Add magnetometer save to example script
  ([`de0a3b0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/de0a3b0c61266aedb844182c7a2929fd9c265ad4))

- Add Model unittest
  ([`00ef111`](https://gitlab.com/chilton-group/cc-fit2/-/commit/00ef1113d7952b123f9825709361b5361300f636))

- Add multiple tau tests
  ([`6a99bfe`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6a99bfe9fe50391d8f44468081015a73e3d0949a))

- Add new test data location
  ([`61f4f9f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/61f4f9f869712ef33c4a426bde785068724b082b))

- Add pytest config
  ([`55bc1ea`](https://gitlab.com/chilton-group/cc-fit2/-/commit/55bc1ea5ac1ae27e49eb48de35854078f3968038))

- Add relaxation tests
  ([`cc0ea39`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cc0ea39546bd1678b3e0891d302a23139dbd1b38))

- Add waveform scripting example
  ([`353999e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/353999e9ab7fee51310e4c629d9dccce7376ac9e))

- Change example script names
  ([`1b0f971`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1b0f97177cc2ce542cb7a0e21f915384a7de6a24))

- Lowercase directory names
  ([`2a76020`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2a760205b269b14d569845a17d17d383384fdca3))

- Move example files
  ([`994b2c0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/994b2c07a56215d2cfad08e648741f0eb14796fe))

- Move examples to examples folder
  ([`b265e58`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b265e58856ab11df7ab3f55e2077adb5f95c0a7f))

- Move test files
  ([`5a4233c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5a4233c6a6bf67d41215e1138db952f196d49ed7))

- Move test files to test/data directory
  ([`05761e6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/05761e6745d5951f8981bea1056ae27559f87b3e))

- Remove old test file
  ([`0ca54bb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0ca54bb3cbb2192f2858f5ec4effd50dd03662c0))

- Remove old test files
  ([`4d84435`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4d844353c3b65958d7894ed5fe552bbfc1d6746a))

- Remove results
  ([`7b56e7f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7b56e7f4d61812497ae6f2b11bba4f54e73e981d))

- Remove test results
  ([`ca1d1a1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ca1d1a118f0e800965b1c4ac705222aea1806614))

- Update ac unittest
  ([`4ec3952`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4ec3952264d664e9c406bf0a9ba492e26becc64c))

- Update ac unittests
  ([`b992ad3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b992ad39aa357353c6c00b04f279afbbcb66cc94))

- Update dc unittest
  ([`e7daa8c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e7daa8c24b82db2db3cffe5c3dc5920372b68bb3))

- Update example
  ([`fb94998`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fb9499806a13a5cde5cf027b93d498977eee459a))

- Update examples
  ([`295df01`](https://gitlab.com/chilton-group/cc-fit2/-/commit/295df012eed41b4edc2a643079ab5c1165922228))

- Update path for test files
  ([`6480ef0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6480ef0ccbef70f6d3fb864a395aab9b20a38af4))

- Update script example
  ([`b8f05c0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b8f05c0e43bf1f7892e9d6ebdb162027d388be8c))

- Update test data
  ([`9548830`](https://gitlab.com/chilton-group/cc-fit2/-/commit/954883038875e01c4219e3b758254421803316a9))

- Update test file
  ([`847c5e0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/847c5e02a9c2f6701f8b1d544af2958e097c6b37))

- Update test files for tau vs h relaxation
  ([`5004f26`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5004f2603f4b4b8e80dc605c40d73d86299d1052))

- Update test names
  ([`1a87420`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1a874205504228d83230613abad41c4956ead1aa))

- Update tests
  ([`0be9505`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0be9505764e5d8ecc4743e6e7d62007fed9db4cd))

- Update tests and add waveform test
  ([`bbb1d91`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bbb1d91cb8541a29be4a065d4e9f6347727649e4))

- Update waveform unittest
  ([`e4ceb15`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e4ceb15f2f73fad8cb95e25bc909ec12755dfa78))


## v5.6.2 (2023-10-24)

### Bug Fixes

- Fix missing units in Havriliak Negami Model
  ([`5d0d57c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5d0d57c199c080dd81b7ef9aa50c5dd2f7aab483))

### Documentation

- Update whatsnew
  ([`75004af`](https://gitlab.com/chilton-group/cc-fit2/-/commit/75004af9215a3767be75f90661e207524af26e50))


## v5.6.1 (2023-10-23)

### Bug Fixes

- Fix bug in double tau models
  ([`c88bc99`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c88bc997652da5bcf59614527a918bf90cd2ef8e))

### Documentation

- Update relaxation headers
  ([`e5d58ae`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e5d58aeb2a72e86b3ba806f4f9fa776deda466b8))

- Update relaxation script documentation
  ([`43e9cf4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/43e9cf40be9e1da390756149e404fa50eb4c3de5))

### Testing

- Update example files
  ([`ed73c1e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ed73c1ea1efbd37e0528c59dda82066367e85362))


## v5.6.0 (2023-10-23)

### Bug Fixes

- Add catch for nan in lntau_stdev and lntau_ul
  ([`a92faa0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a92faa03ac79ff1a6e09e6b34c17542805a9c4aa))

- Add delimiter specification for headers
  ([`424ef42`](https://gitlab.com/chilton-group/cc-fit2/-/commit/424ef425438b428684b2b38355352d37f3299c27))

- Fix bug in relaxation model data csv
  ([`eed6d7e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/eed6d7e1b4405006c94a7de69fab458094008e4b))

- Fix bug in relaxation model data csv write
  ([`a83eb86`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a83eb86cc8d2214b8ca9d8dcfc310b74126b9d11))

- Remove debug print
  ([`d615a91`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d615a91a9857331771aa6902c936d435a63ab338))

- Remove extraneous check of lntau_stdev in setter
  ([`4cd8edf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4cd8edf38031c66d8dce0c0485f7439e56a6bca1))

### Build System

- Bump packages
  ([`613fdbf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/613fdbfa8650b1fe63ca9617f66177a25ced683a))

### Documentation

- Add configuration page
  ([`62318cf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/62318cf75b080a299fc295c95017814d89d67605))

- Add csv information
  ([`09db723`](https://gitlab.com/chilton-group/cc-fit2/-/commit/09db7236dae53f08924b46dd2a1b19bdec1e05c1))

- Update docs
  ([`06ab473`](https://gitlab.com/chilton-group/cc-fit2/-/commit/06ab473b35ebee611263bedacc7ea64d065804e9))

- Update docs
  ([`ff56a6f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ff56a6fdc5dc1e54862e0ade1ccce0baa6bc5a3d))

- Update documentation
  ([`ddab4fe`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ddab4feee915271318004f1b50c32bf2a50008ad))

- Update documentation
  ([`c39bea5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c39bea5728989f26c7f7840bea5ebf57d3f0a914))

- Update documentation
  ([`7f959b7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7f959b7c9ef7c35adddd7f4b4a1bf9341bfdfe2f))

- Update environment variables section
  ([`ad2efc2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ad2efc2bdebe961b1d65168ea3f2333604b0a386))

- Update examples
  ([`f56126e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f56126efd06c4166adabbfaabac39afc23dda24c))

- Update install
  ([`ed6986f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ed6986fc567d41357d115020ba42467cef2ca92f))

- Update relaxation file spec
  ([`7eb8224`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7eb82242c424262ac6dc0b57c10358414aa2867c))

### Features

- Add ac model parameter csv output
  ([`8ac235b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8ac235b9124a775f19d4a3a8084c1a8fc75e5ba2))

- Add csv output for ac model file
  ([`7e99fa2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7e99fa206749993ea715055beb8a7fb1bc23b107))

- Add csv output to relaxation model data
  ([`f3c9c76`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f3c9c76c199a5f9fa7689a727f056069b8f0bc00))

- Add csv output to relaxation model parameters file
  ([`d76d88a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d76d88a7318fb20feaf12fd9390197770c27f8c5))

- Add dc model data csv output
  ([`32242c1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/32242c18872bbd36b4f51078243e117c22ad166d))

- Add dc model params csv output
  ([`0c46b3b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0c46b3b2b7287433745603547a87470a77e19ab9))

- Add new csv _params.csv reader
  ([`56d7adb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/56d7adb4eb4434230439f43bc82289346e8a316f))

- Add units output to _params.csv file
  ([`79ce438`](https://gitlab.com/chilton-group/cc-fit2/-/commit/79ce438f72b1ea578b194366786036dbd394db21))

### Refactoring

- Add different headers in relaxation
  ([`1b86e64`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1b86e641893130f265c082c057732531d6aacebd))

### Testing

- Update example
  ([`d6636a8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d6636a8bca032944ece6ee281302af22fca7a20b))

- Update example
  ([`394a483`](https://gitlab.com/chilton-group/cc-fit2/-/commit/394a4837365346b0804b4df0153090a4c659973f))

- Update example file
  ([`439f3ce`](https://gitlab.com/chilton-group/cc-fit2/-/commit/439f3ce1dcedf1952eaac6a6fa9d4dea300042d0))

- Update example file
  ([`663c2ac`](https://gitlab.com/chilton-group/cc-fit2/-/commit/663c2ac93b65bd4aa01d01ef7430eb62eb1a0559))

- Update example file
  ([`fc52eb4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fc52eb415f1ec0d2bb9aa91d0ee167c5e83dd7ca))

- Update example file
  ([`3d853c9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3d853c92a0206c4cb86ad1cdebcd1f471587c02e))

- Update test
  ([`994ef2f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/994ef2fc3b78b2f6ecf0a72c70d52b37131b79be))

- Update test
  ([`d5eb4c8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d5eb4c871e11c2c845877ae09b2c656c73844ddf))


## v5.5.0 (2023-10-18)

### Features

- Add parallelised file load
  ([`314f781`](https://gitlab.com/chilton-group/cc-fit2/-/commit/314f7817103dfd653753ed03f32e020ef6c83093))

- Add parallelised file load
  ([`af45ef4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/af45ef4db03ea4136ab9725034104b6f8d774537))

- Add parallelised file load
  ([`e6aa021`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e6aa021d167e755e490626ffaeabe987aee6234c))

### Refactoring

- Improve num threads definition
  ([`3ad8b27`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3ad8b274e5113ff2fbf35f3e1a9bf919c90fc52e))

- Move thread definition to function
  ([`5f60137`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5f60137a0247347140c2846f533c85767b88c2fa))

- Remove use of operator module
  ([`8298355`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8298355c0dd1c7818ca08f13ce4cc78ccc54865e))

- Simplify file header checks
  ([`62ade14`](https://gitlab.com/chilton-group/cc-fit2/-/commit/62ade1472b9c47897f6f30133cc1272b9cf520ca))

- Simplify thread number settings
  ([`7c2b40b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7c2b40ba806da1b0bbc1d744f02babd51f8a9750))


## v5.4.1 (2023-10-18)

### Bug Fixes

- Fix spurious tick labels in susceptibility plots
  ([`84d2d1b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/84d2d1b45a32ffe60cf2001ce1a3c25e2b72bd81))

### Documentation

- Update whatsnew
  ([`25cc88e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/25cc88e773c513419154acf2cb2e2b1a5f8ca5b3))


## v5.4.0 (2023-10-17)

### Bug Fixes

- Correct overlapping 0 and 1 tick in interactive fit plot when T = 1K point present
  ([`739c97f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/739c97fbb8edae04e79fe274518dbb30f074dfe9))

- Multiple consectutive data points in changing field no longer leads to incorrect division of
  frequency blocks
  ([`981dcd0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/981dcd0dc0776f9752cb00e74c2cb8738b3014b7))

### Code Style

- Force all minor log ticks for susceptibility, correct axis label font
  ([`ede4909`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ede49095a8943a90b0f422e7bf17c00f9c479dad))

- Improve grammar
  ([`3ffe686`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3ffe686fd987430927e875a9bfa3c3ee39680a84))

- Improve grammar
  ([`dddf599`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dddf599ca33793f580870d76d2d3ca0835b17593))

- Remove prominence kwarg and docstring
  ([`0e8e717`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0e8e7178032d15c060218fc195d122f48a9d3e83))

### Documentation

- Fix typo
  ([`4e417eb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4e417ebb5560e60de7afdc8348d35e9b94dfb51c))

- Fix typo
  ([`eebb302`](https://gitlab.com/chilton-group/cc-fit2/-/commit/eebb302e96fd17bd5025a16dcc26c9aabf6bb591))

- Typo in comments
  ([`0f46c0a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0f46c0ac09b5b205f4d33cfc523ea9a99311f86e))

- Update ac docs
  ([`61f0144`](https://gitlab.com/chilton-group/cc-fit2/-/commit/61f014485fe66d556a3f91fba02ad1b28ea33ff9))

- Update ac docs
  ([`bb0444b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bb0444ba84be743b8fac97cb0e556c48054ee580))

- Update comments
  ([`d39fea5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d39fea511fd626744924aadccffaa50e045a038b))

- Update comments
  ([`2dfb3dc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2dfb3dcf814f4fe3c11db987519d86c23e6f1498))

- Update dc docs
  ([`30c3f09`](https://gitlab.com/chilton-group/cc-fit2/-/commit/30c3f0922ec571500e63e1ac4fe7ceb65d4caebc))

- Update documentation
  ([`8d16615`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8d1661553df20fe0123e6e36d2c0e6e2d9382e99))

- Update documentation and help
  ([`6179d7a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6179d7ad365c00b45ae8be4d86a65e2214bb10db))

- Update paper references and README
  ([`d6f49d6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d6f49d68bd8d30f52a25ed07d87defdbfc05a7a4))

- Update waveform docs
  ([`e95cbc1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e95cbc1d7e23e403e3502ae885e03e9326167a39))

- Update waveform documentation, improve waveform parsing image
  ([`99fc585`](https://gitlab.com/chilton-group/cc-fit2/-/commit/99fc5856f5903321a85147f49573340a480ccdc5))

- Update whatsnew
  ([`d4adda5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d4adda51bdb36970a93b92f81dc2361d9999eee1))

### Features

- Add option to plot raw waveform data
  ([`9469b7d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9469b7db1cd894db2e414d1b8192afd515a3e2cf))

### Performance Improvements

- Add minor log ticks to susceptibility plots
  ([`b8a6b4b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b8a6b4bc85a2d58e6f42b9c3d394801728bf4c9f))

- Correct relaxation time and rate plot window title
  ([`2d0a5c4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2d0a5c46e9c999d4794fe2d034660064b90ee9e1))

- Fix typo
  ([`f183b8b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f183b8b1a015783df419cd9c5cba8e27db0be215))

- Force int to float in float type attr setters
  ([`8bca308`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8bca3088933dcc7a7c4594bb59751d71ef061a8c))

- Improve chi prime and chi double prime font style
  ([`cbd5e7d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cbd5e7dac144764d76127abc8f2e273e39bdbc02))

- Improve waveform frequency division
  ([`c3557fd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c3557fd54646403be16e5d9c2ca7b3edda9079a5))

### Refactoring

- Enforce 1 dc field frequency per file in waveform
  ([`d087106`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d087106fe545e440da632e2326c9275983632e81))

- Enforce 1 dc field frequency per file in waveform
  ([`7973e7f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7973e7f81254c04006d7e93248fee11ea7511d65))

- Improve waveform file parsing and dc frequency separation. Remove field_thresh argument from
  waveform cli. Move field_window arg of waveform cli to optional
  ([`5121242`](https://gitlab.com/chilton-group/cc-fit2/-/commit/51212425ad3efe3dec82f9e628dee21d9eb2c143))

- Improve waveform frequency separation
  ([`3cc8b1d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3cc8b1d3e8754fce5c48ea75fd82fa13979410b0))

- Improve waveform frequency separation
  ([`6ad6329`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6ad6329ef5d2a16b60adab749e048808df8f8bd1))

- Move field_window to keyword argument
  ([`d25c85b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d25c85bfacae3cb796cf57aa175a74f471bc7563))

- Remove --field_thresh, remove comments, correct style mistakes
  ([`1ff4d19`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1ff4d196dacf3c184fc8a980f7a44c633616f758))

### Testing

- Add waveform test files
  ([`017e982`](https://gitlab.com/chilton-group/cc-fit2/-/commit/017e9823a7a02f36f0b4c37b1649435dbc861e16))

- Update interactive relaxation test
  ([`c8f6567`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c8f65676dd87ebdd2d345833050f459950742849))


## v5.3.0 (2023-08-21)

### Documentation

- Add documentation for new mf_ft plot
  ([`fb40f04`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fb40f04578dc0587bfa04ef50d51c220af8d5afd))

- Update documentation
  ([`288d9ed`](https://gitlab.com/chilton-group/cc-fit2/-/commit/288d9edbca6e860c46588c784adac106dc0fdddd))

- Update documentation
  ([`63370f4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/63370f4100142552fa40df7ff717d969c9f5d42d))

- Update whatsnew
  ([`ede998e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ede998ecd106c8abbbdb618918228931c3474637))

### Features

- Add moment, field, and ft plot to waveform
  ([`fcf548b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fcf548b3d4c91c350d0b33e2fd198d1bcf5337dc))

- Add moment/field vs time plots
  ([`deeacce`](https://gitlab.com/chilton-group/cc-fit2/-/commit/deeaccec5d54a06fd325a6f8f8919dad1090f619))

- Add moment/field vs time plots
  ([`3a3282f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3a3282f6f956ad59ae6826a6bce0bba568f0ee54))

- Add separate moment/field vs time and ft plots
  ([`5a27638`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5a276383b2806fdb00fc9c92dde61a410be397e7))


## v5.2.3 (2023-08-18)

### Bug Fixes

- Correct perioid definition for waveform
  ([`c418536`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c418536f79ce9b8e2a1ddec65869a0272058ea3c))

### Documentation

- Update whatsnew
  ([`b65aa91`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b65aa919a80aeab0a5bc81500af2fbe5d95108c6))


## v5.2.2 (2023-08-18)

### Bug Fixes

- Add close for waveform FT plots and DC decay plots
  ([`d29949a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d29949ad21fb7e0ac5143eebee4a47bd93579e3b))

- Swap definition of alpha1 and D_chi1 initial guesses
  ([`9847d63`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9847d63893753bda3c376be4f88fcf5bb6bb717e))

- Swap definition of alpha1 and D_chi1 initial guesses
  ([`2310548`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2310548b0c0fa4d16fab6eb5a8a72571b305749e))

- Swap definition of alpha2 and D_chi2 initial guesses
  ([`901b6be`](https://gitlab.com/chilton-group/cc-fit2/-/commit/901b6bedabfb044c791ed9b228452102c50347a4))

- Swap definition of alpha2 and D_chi2 initial guesses
  ([`d809568`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d809568123396e038d4137905b62d0ce6605d283))

### Code Style

- Improve pep8 compliance [ci-skip]
  ([`92da3aa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/92da3aa9b3d3ae3306e1dd9cfb5171fd1b80ec43))

- Improve pep8 compliance [skip-ci]
  ([`25d324c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/25d324c8a692b26b0c90b69b79e7cb6817c54b82))

### Continuous Integration

- Add build for docs dockerfile [ci-skip]
  ([`c0dc7a3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c0dc7a336832feea9c82e25f23d360a900753d4b))

- Add dockerfile
  ([`97af89e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/97af89e708a0d67b0645cc4286c31c79dbca743a))

- Add manual dockerfile build [ci-skip]
  ([`6aeb1ce`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6aeb1ce771dff4388d4991b2867f4bc2947e7412))

- Add only for dockerfile build [ci-skip]
  ([`8dacef5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8dacef5f8c30b983dbdd046362ac098e1c280871))

- Change main to master in dockerfile build [skip-ci]
  ([`6318c37`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6318c37d93ea5ebdf2a6058ebe1752e50054f5ef))

- Move dockerfile
  ([`a718668`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a718668bf9de61730cb32d279836ac7c5a21cad3))

- Simplify ci config [skip-ci]
  ([`9dafcb6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9dafcb6afb0004a9a686eab712975464401b18fd))

- Update ci config [skip-ci]
  ([`969037b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/969037ba1336b6c310b88fb279db4ff11018fd18))

- Update docker image name [skip-ci]
  ([`859edae`](https://gitlab.com/chilton-group/cc-fit2/-/commit/859edae098a1ab1f043664eb300bf5436fb94790))

- Update dockerfile
  ([`a233d58`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a233d581c94ac6f99d8cdf93b9e2f4232b3ae9ea))

- Update dockerfile
  ([`4ddb35e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4ddb35e91271bde645b36d3890ed5d9576089454))

- Update dockerfile
  ([`4f4ff01`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4f4ff01950a8db18284fcd77bf2462ba161afc33))

- Update dockerfile build rules [skip-ci]
  ([`7fa7a49`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7fa7a49625531a2ab534db0ebdffec67159a2386))

- Update image for docs [skip-ci]
  ([`6b4fda0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6b4fda09d2dea42ae5a94f2d4040a2b1ea091bdf))

### Documentation

- Add favicon [skip-ci]
  ([`81db9f6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/81db9f6289eee8fc0a581033d717772af7364fa6))

- Fix typo [skip-ci]
  ([`9ddc29f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9ddc29ff248a692a654a259266cc073d5985ea79))

- Fix typos [skip-ci]
  ([`0db381a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0db381a1695719c30569261d73361cb897292d3d))

- Update citation page [skip-ci]
  ([`efaab3c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/efaab3c1332a511fe359f9b0d693c36446c3e874))

- Update docs image name [skip-ci]
  ([`7cb8df6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7cb8df600801e86215bc39014346ef9ed067011c))

- Update docs language and improve directions for new users
  ([`24a1e39`](https://gitlab.com/chilton-group/cc-fit2/-/commit/24a1e3900d57dbda2209150fec00f8f4aa7e1b9d))

- Update language, improve flow
  ([`0e62f72`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0e62f72a217a0d10d6c31314296b834c6a4dc0f1))

- Update susc_plot default
  ([`d58c473`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d58c47363e82589b3d802a29596586abd8669ad4))

### Performance Improvements

- Modify precision in waveform ft plot title and save name
  ([`4b5e707`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4b5e707b04829c33ff4e90ac7605f3f99882d9ab))


## v5.2.1 (2023-08-03)

### Documentation

- Add changes to whats new [ci-skip]
  ([`c902708`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c902708874c4cfd3ea257f0210d81f054d91efb2))

- Add update information [ci-skip]
  ([`21a2f1b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/21a2f1b208213d489b0df1aaa186d9d0e8346cf1))

- Add update information [ci-skip]
  ([`2fa1ba7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2fa1ba7cce2dfb3ae5e7ddb853107e8d04a3a288))

- Fix typo [ci-skip]
  ([`99f769f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/99f769f62d08b99a3e98c233936ff49ba0125267))

- Improve formatting [ci-skip]
  ([`6ac2d32`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6ac2d32f5cf565120561947275617dd916736cd0))

- Update docs version number redirect
  ([`9172787`](https://gitlab.com/chilton-group/cc-fit2/-/commit/91727870017390f40d8dec0c5ab0516e4121ba97))

- Update version number
  ([`fd4c6f9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fd4c6f9102a5dad4ac08b4c1346a9fe90653f0a4))

### Refactoring

- Reorganise dc model constructors to match ac and relaxation, add check for duplicate keys in
  fix/fix
  ([`7398e4e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7398e4ea555f0af934ff5f1a9352f454ebac790b))


## v5.2.0 (2023-08-02)

### Bug Fixes

- Correct calculation of on-screen residual for >1 model function
  ([`a2759d3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a2759d304c2e4c4f820bb1eec071bf0e5e6cd21f))

### Build System

- Move old exe to old [skip ci]
  ([`624d78e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/624d78e18db71b5f62f27b701d83dc641e541bd3))

### Continuous Integration

- Add qt dependencies to docs build
  ([`d99ec8a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d99ec8a4b0144ee8f1e8ae0aa887b94ba966d27c))

- Edit docs pipeline
  ([`37d1366`](https://gitlab.com/chilton-group/cc-fit2/-/commit/37d1366d54c41dc7f991ec63312b7b8fd41f33ac))

- Update badge name
  ([`32900fb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/32900fba9589ec7e7dbfeefd8f4ae70dd133727c))

- Update build procedure for python semantic release v8
  ([`6fa31c7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6fa31c796227239fe0903908d5ae23e7d56b0b2e))

- Update docs pipeline
  ([`366b4fb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/366b4fb75f197ce0f907bb4fa08af3aeb67305d2))

### Documentation

- Add docstring and return type
  ([`9694bd5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9694bd5a2101a34d8e4569207bb7e4b231914739))

- Improve language
  ([`a36e771`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a36e771e8c5e525781ede6f08728f705a14a9e1b))

- Update contributing
  ([`af60fc7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/af60fc75aeb608438bef81efb1d0615db9c6f9dc))

- Update FAQ
  ([`f3cf78c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f3cf78c977808f4344869451a9b2fd0555cc38ea))

- Update installation instructions
  ([`1c2f9e1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1c2f9e111797bdf5451adb327032d3bd4c1c14b7))

### Performance Improvements

- Remove slow model rates which dominate fitted_times plot
  ([`7852b4c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7852b4c95b71bfa4799ace72b6df6b07805b6fb4))

### Testing

- Update initial guesses
  ([`f8ed188`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f8ed188f769d4227367c721b311e35372d688ea2))


## v5.1.4 (2023-07-06)

### Bug Fixes

- Change bounds in relaxation.logdirectmodel
  ([`a83b405`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a83b4057d871a230b609058c6ddbec7c9c63f64e))

- Empty data values are now correctly caught
  ([`c80d139`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c80d1397552dc079c0dbbb20fb951c4e8eb4a8ec))

- Fixed bug when trying to plot the result of a failed fit
  ([`32ddab1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/32ddab15a743d5775f2b041388cec447a89f3212))

### Build System

- Remove old requirements file
  ([`4115dd6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4115dd63f7e4675d84ec14819042b9a850a49d17))

### Chores

- Clean up code
  ([`20836bf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/20836bf41c15bbda876886ae5267d2d53db15ccb))

### Documentation

- Fix typo
  ([`8e8c4c3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8e8c4c37537b9bebe97dd084fa5968d254e4f252))

- Fix typo
  ([`9402002`](https://gitlab.com/chilton-group/cc-fit2/-/commit/94020027c755f9fae0618a45f47ac3c9cc900d97))

- Fix typo
  ([`c22e64e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c22e64e6288d17e83e6c6f267178f0489a3cda41))

- Improve language
  ([`16cd08e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/16cd08eb8d23ad434618cb167f5a82025d6bf56d))

- Update file information section
  ([`31204ee`](https://gitlab.com/chilton-group/cc-fit2/-/commit/31204ee6cbd58353bc716bedbbf5902d877f09da))

- Update language
  ([`fcaaecf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fcaaecfe6574d5f55ed48da5cc8ed62d4838eaa0))

### Testing

- Move test files for waveform
  ([`a0d6b3a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a0d6b3a8e2ae0c81b90dda54482cad71aab44324))


## v5.1.3 (2023-06-28)

### Bug Fixes

- Add support for H=0 points in fitwindow, fix bug in xticks of final fitted rate plot
  ([`ea02d46`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ea02d463c427fa40651c241ee646d7f86ead4072))

- Add support for H=0 points in fitwindow, fix bug in xticks of final fitted rate plot
  ([`b7864c0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b7864c0bc353d8bab703976b04aa3c2892c43cd4))

- Add support for H=0 points in fitwindow, fix bug in xticks of final fitted rate plot
  ([`70fa983`](https://gitlab.com/chilton-group/cc-fit2/-/commit/70fa983f0d73c328bfba2be37b6cf044607c7a80))

- Add support for H=0 points in fitwindow, fix bug in xticks of final fitted rate plot
  ([`414c985`](https://gitlab.com/chilton-group/cc-fit2/-/commit/414c98537a56f7d5b6482f6435dbeded68fa3fb7))

- Add support for H=0 points in fitwindow, fix bug in xticks of final fitted rate plot
  ([`31a26df`](https://gitlab.com/chilton-group/cc-fit2/-/commit/31a26df53e3279a1eb3172c8115bfa8b97ac465d))

- Can now enter negative values for parameters
  ([`98fb936`](https://gitlab.com/chilton-group/cc-fit2/-/commit/98fb9363c0fff160637cb428c3e0f39e8e9d8608))

- Change bounds in relaxation.logdirectmodel
  ([`d02a933`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d02a93321766be4f43f3cb2d1fdafd3ec7a679f7))

- Correct subscript italicisation in FDQTM variable name
  ([`d96f53c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d96f53c8310026a494b8e52d8e3c3b068e2b40ef))

- Correct subscript italicisation in FDQTM variable name
  ([`3519c36`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3519c366312008865db830036b33b9256109de0c))

- Empty data values are now correctly caught
  ([`6ae0365`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6ae03653a619b6364edee554d2d9212d6bdf836a))

- Fix reset button
  ([`a5e354a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a5e354a5ecad41f28bbc120efa6dcd93f3739a41))

- Fix window clip issue
  ([`2eac19e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2eac19e4d94b0806bb22aeeb6bfa9d37463a15fc))

- Fixed bug when trying to plot the result of a failed fit
  ([`1bda15b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1bda15b92d1888ae1ac1114d0fec5f20fea73ed8))

- Remove asterisk from BVV model file names
  ([`afdb543`](https://gitlab.com/chilton-group/cc-fit2/-/commit/afdb543a7b5992dfa082be581ec600a36eff62e8))

- Remove duplicate
  ([`45b368b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/45b368b288b520395e466c714ad275a515fb6bb7))

### Build System

- Move old exe to old [skip ci]
  ([`782009a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/782009a80003a5492006dc839a7ff1bc7f1dd085))

- Remove author and author email
  ([`d7bba74`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d7bba74b2d93fc520d045801945e5e62d591af00))

This information is given on the authors page of the docs, and we can't have more than one author
  here. Additionally, listing a single email will undermine our wish for users to properly describe
  bugs using GitLab's Issue page, rather than contacting individuals and having them write the issue
  on the reporter's behalf.

- Remove old requirements file
  ([`3cffaba`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3cffabaa76464969155b5b1f934588d03e24637a))

### Chores

- Clean up code
  ([`cb82334`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cb82334127160c5f486728ca6ce6fbedb2705b1d))

### Code Style

- Improve pep8 compliance and remove debug print
  ([`1dde726`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1dde7264a3b46d49f38256c27d8afe2f6269ddbf))

### Continuous Integration

- Add qt dependencies to docs build
  ([`5b7292e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5b7292e8beb546206502d70ea112ba27d5171f1e))

- Edit docs pipeline
  ([`dc4c441`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dc4c44174382d67814c6c39dd164aa2053e28917))

- Update badge name
  ([`7dbec69`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7dbec6900bb46d874ca2135bdab2bd6235e23cf7))

- Update build procedure for python semantic release v8
  ([`2c7628b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2c7628beaaabe177c903194b3720b0f266070286))

- Update docs pipeline
  ([`7e80ddd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7e80ddd7b29b7d7e90e923f87379a971bd43d898))

### Documentation

- Add comment
  ([`4da8601`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4da86010655dc186c2a2b0e5f5b288891b93638b))

- Added required space
  ([`6a7a0d5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6a7a0d563285a448352f36a61e86a1526450f51c))

- Added What's New section
  ([`73b0c0a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/73b0c0a66ac0130f7c9664e48efe3dcc839be8e9))

- Added What's New section
  ([`b4878e1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b4878e1b4cb666cb53a5170e1ccd6cafa0d887bb))

- Fix formatting
  ([`46b9b2f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/46b9b2fe42883ec0634b00ca59d0ef3782915d2f))

- Fix formatting
  ([`801498c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/801498c5ace3f342ea8f759e959f726f2cb6eece))

- Fix typo
  ([`29d5d33`](https://gitlab.com/chilton-group/cc-fit2/-/commit/29d5d33fe71d9f550a22895c5a7311efd0900080))

- Fix typo
  ([`f7af176`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f7af17640b195a862408578fc1e8cd8d0b0c8682))

- Fix typo
  ([`ad878e0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ad878e05f91715e9e13628d0662f45d65df54120))

- Improve language
  ([`8763909`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8763909f850f5a38eaad06f993916260cd49e773))

- Improve language
  ([`b13195c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b13195c6f77103aaf8d056edc0edd52b8cfe84be))

- Update contributing
  ([`a2d1464`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a2d1464e8f46de68885cce7395ab99453b928f7a))

- Update docs
  ([`e399a35`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e399a35aae3a081d37eb064f2b0338cfa8fb6789))

- Update FAQ
  ([`7236ab2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7236ab2e1eb9b0f2f327c176c8f8f2bd434ec19f))

- Update file information section
  ([`89c1626`](https://gitlab.com/chilton-group/cc-fit2/-/commit/89c16264858f3b384c59dfee2440704f642fad68))

- Update formatting, add more changes
  ([`80711fb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/80711fb10e9231ac952459a9058e36ab32b79a50))

- Update formatting, add more changes
  ([`19e957d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/19e957d8dcab89bfec347c8fc56882852d8802da))

- Update formatting, add more changes
  ([`455a31e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/455a31eb3f9018d8972119784046568d3c6f265d))

- Update installation instructions
  ([`cfc6e74`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cfc6e747ca6ad82a621654f5a87b32fa54f9a215))

- Update language
  ([`856747e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/856747eed0a1dee20a304739a3c4fa5bbfbb309a))

- Update link, reorder index
  ([`ae14702`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ae1470204a503632a82ed2dccd66ad829cb81529))

- Update tense
  ([`18701ea`](https://gitlab.com/chilton-group/cc-fit2/-/commit/18701ea844e8673394bd25ca8e27e5dcf847f8c8))

### Features

- Add residual sum of squares output to fit window
  ([`0082d79`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0082d7960d0fd41c326f1229d882b78a127215fd))

- Add residual sum of squares output to fit window
  ([`084a386`](https://gitlab.com/chilton-group/cc-fit2/-/commit/084a38644d2cea896c05e1c206898e7e7d607d3e))

### Performance Improvements

- Improve residual plot y axis label
  ([`6c3dc0d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6c3dc0d9a7c70f929a587d3ae70f585f30ad86c6))

- Improve residual plot y axis label
  ([`001a822`](https://gitlab.com/chilton-group/cc-fit2/-/commit/001a82212e3850abecad73fe35cc75e3c28ea0de))

- Remove divide by error warning in FitWindow
  ([`49d94e9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/49d94e9eeb536ced4dd62565b140f0a73c997cba))

- Remove divide by error warning in FitWindow
  ([`38f3249`](https://gitlab.com/chilton-group/cc-fit2/-/commit/38f3249a8769f7cc8f73348e4a68b5529832bef7))

- Remove rate floor in fitted rate and time plots
  ([`4c16b0d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4c16b0dee7c7ff78b9168156d8c9a0ed61adf324))

- Remove rate floor in fitted rate and time plots
  ([`7b542b4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7b542b494f9ce2696caa0a46506aec748a278dc2))

- Switch residual to use log10 rates
  ([`e8796aa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e8796aa0f9bb974e7230019c590e96d1f145157a))

- Switch residual to use log10 rates
  ([`ae1c274`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ae1c274315be221bb06cb918c92ae76ca4677a97))

### Refactoring

- Condense relaxation.py output filenames code into one line
  ([`ef24da9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ef24da9b7c707f8774b8dfb33ae47785cd1b2436))

- Increase negative bounds on relaxation models.
  ([`e7b9f77`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e7b9f77602620275eced296701c972fe4cb81b70))

- Increase negative bounds on relaxation models.
  ([`716c78c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/716c78c23192c6b95b1e70255d7558d86830a365))

- Remove extraneous arguments, improve docstrings
  ([`21d83f4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/21d83f4770354030b8123c323f7960cf65976a83))

### Testing

- Attempt to implement error widget
  ([`748f143`](https://gitlab.com/chilton-group/cc-fit2/-/commit/748f14364f26cd7b666a8faf4a57ce979c77c154))

- Attempt to implement error widget
  ([`0bcdba5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0bcdba5382abb6925e0268aac6d4e1832673f086))

- Move test files for waveform
  ([`fb58ad6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fb58ad6c24fbf6a84880a1c62c7fb264f06b17e4))


## v5.1.2 (2023-06-23)

### Bug Fixes

- Correct variable name in FD Raman parameter file printing
  ([`f9c0929`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f9c09296f8b1c03f383fa5f279cdf8b2c01bf2cb))

### Build System

- Add issue templates
  ([`1628184`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1628184a33a274c22e974494863e9ccf1fca0daa))

- Move issue templates
  ([`144196f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/144196ff6c98ae1b9877a58047660b640c927e8f))


## v5.1.1 (2023-06-22)

### Bug Fixes

- Fix bug with rate residual plot temperature ticks
  ([`2f34ed5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2f34ed5e5c9a37f83ea4e7eb049bec78824328e0))


## v5.1.0 (2023-06-22)

### Documentation

- Fix typo
  ([`c25f723`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c25f723b1bd2d8a5ae83eb71c567772a22937df2))

### Features

- Add option to hide fit parameters on relaxation plots
  ([`f886435`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f88643590d1f362860aa66e5e3c584d3e2c4f117))


## v5.0.2 (2023-06-22)

### Bug Fixes

- Add missing H=0 point on field plots, replcae --process with --x_var, improve manual scrolling for
  relaxation cli
  ([`321cd71`](https://gitlab.com/chilton-group/cc-fit2/-/commit/321cd718621fc62334e6bd82285dde21226a92a3))

- Correct labels for FDQTM
  ([`e023add`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e023addadea21cb45d1b16aa31bf2a143aaf2cee))

### Testing

- Add test files
  ([`ad801a4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ad801a47ab8b10029727c29c42c2ad843d05158e))

- Rename test files
  ([`b7eb22b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b7eb22b939c7ad0dcd35a86944137d533f7a8b3e))


## v5.0.1 (2023-06-22)

### Bug Fixes

- Add option to specify x axis type (field, temperature) in x axis formatting functions
  ([`8b51ec8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8b51ec83c4b52f3bf817d88dfca79927acfbc26b))

- Fix bug in tau vs H relaxation fitting and plotting
  ([`e013457`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e013457219265dddfdea93448810155ade9624dd))

### Documentation

- Add LICENSE
  ([`c4c3247`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c4c3247b8b0e09ba87f5320d506c4fa3b5ea626e))

- Fix typo and improve language
  ([`c8f50bf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c8f50bf52fa7a11d5550e5be9601c7596f5f4854))

- Update license link
  ([`04ac170`](https://gitlab.com/chilton-group/cc-fit2/-/commit/04ac170b99b29f03ee395784b25535fa4d71504f))

### Performance Improvements

- Improve performance of ac --select_T when there are many temperatures
  ([`7395440`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7395440f460e89c2f6d75ebaac507a17b8575892))

- Improve x label in time plot
  ([`5bc9da8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5bc9da8e947b3098927dbb39f692af693fe99a7d))

- Improve y axis label in ln(tau) vs 1/T plot
  ([`b65d7db`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b65d7db30515c202e8bcc9ac8a67c7bd47c6f239))

- Remove parse_unknown args, executable version no longer in use
  ([`f3649ca`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f3649caa3a4091cdddfdb0399820b462851b10b2))


## v5.0.0 (2023-06-16)

### Bug Fixes

- Add catch for insufficient frequencies
  ([`18c71dd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/18c71ddd741750655dad1795adca362249c4594b))

- Add missing resort of field split indices
  ([`9a75204`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9a75204a71692b4cc7aa24513a4f2a38201b6fd5))

- Add missing typehint for meas_dc_fields setter
  ([`f5ab07e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f5ab07e910db72ea8030d0198bbb194b75bfcd15))

- Add uncertainty formatting for m_eq
  ([`ccdd995`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ccdd9957923ddfcef83c1d58630b15fcbaadf25c))

- Add underscore to all Double Exponential output files
  ([`b7f9afb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b7f9afb7ea879e2b2154a45123d2a41325233dbc))

- Change parser style for waveform and relaxation to match ac and dc
  ([`0d959a1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0d959a1a843612d75d47bfffe3930fd38b5c51ed))

- Correct dc units_mm defs
  ([`e253e73`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e253e732ae3d93e554e90ac70a277ed6e4d755dc))

- Correct type for meas_dc_fields setter/getter
  ([`c8283fa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c8283fa02df5c27dc70c12ee89de8b1dca2d194b))

- Correct varnames_MM in ac
  ([`5ddf1d1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5ddf1d1c30f26aaf0d275f0b6fc3e946220ed64f))

- Enforce tau1>tau2 for ac double tau models
  ([`f692407`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f692407a690dba146f36fdc80fc65dff61f852a5))

- Fix bug in tau reordering for 2tau ac models
  ([`1274f9f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1274f9f246316e7dcf78775173e0d94764caff5c))

- Fix incorrect variable names in calibrate_meq
  ([`8ee9304`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8ee9304bddadf229de87c3a21e7ffe75d2beb498))

- Fixed issue where intial chi_S and chi_total guesses where outside the parameter bounds.
  ([`41e9e85`](https://gitlab.com/chilton-group/cc-fit2/-/commit/41e9e851954e5a9f3b29576270ca5e34575ea2ce))

- Program exits if --process decays is seleceted in dc module
  ([`7c2b7d7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7c2b7d73bafdbd60841e7b1fcb9cc0b77e49f0ed))

- Re-enable coelcole and susc when single given
  ([`a0f3980`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a0f3980359c62ba19c4f6872b549a92233b4e6a7))

- Remove check for abs(components) > abs(error)
  ([`6c0b2e1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6c0b2e1504304dfee8e09040f725a1ea70f4df66))

- Remove check for meas_dc_fields setter
  ([`84d3760`](https://gitlab.com/chilton-group/cc-fit2/-/commit/84d3760628a3f1f931f8ae2a5cb85ae52b7661f4))

- Remove debug print
  ([`fdeea42`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fdeea42b32f7e14b2cc348d1b62ca8041b43c186))

- Remove debug print
  ([`07cdde7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/07cdde7fc927bfffee563d7de83ab43eeb59e941))

- Remove duplicate meas_dc_fields docstring
  ([`4548e45`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4548e456d92540d08b3f22e82a3d816666d4b0d0))

- Remove reordering of tau in double tau models
  ([`4e7f52b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4e7f52b39eaf05e0e240e59626cb237e3cf9e571))

- Remove repeated split values in measurement --> experiment split
  ([`cc767de`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cc767def7c4eda327d4f0b1d564dfd13a0d01ffe))

- Remove spaces from filenames
  ([`aa4ba94`](https://gitlab.com/chilton-group/cc-fit2/-/commit/aa4ba94684ff0c4787d35ccde4c9bfd5d9682b1a))

- Remove ticks in select_T plots
  ([`4b93bb0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4b93bb0b75b2e8705ec3fc411166b02d86db1ff1))

- Restore rep_dc_field in ac subprogram
  ([`85e8a25`](https://gitlab.com/chilton-group/cc-fit2/-/commit/85e8a2574bf139edf14318a690d47371725d7650))

- Update call to calibrate_meq
  ([`513e23d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/513e23dd5beb8c74b404f7f2a39d338f87bc4c04))

- Update guess values for Orbach, Raman, QTM
  ([`d15a081`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d15a08132b086913752108312ad2d5e3715bb18c))

### Build System

- Add dependency
  ([`328e3cf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/328e3cf9f5fb0f6604c0cfb9577f2b3eadd297cb))

### Code Style

- Add missing return None
  ([`39688c8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/39688c8d9438343195e68c4106105ea4ac6ce04a))

- Improve pep8 compliance
  ([`0e30dbe`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0e30dbee4cbc34ed20ccd9107129e2b4a187c528))

- Improve pep8 compliance
  ([`79c4ae2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/79c4ae2a2dab9f4d0620f558fdbd9945725e4c25))

- Improve pep8 compliance
  ([`e08bcc0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e08bcc004651708c3bc8e464ac1cf5ff4dfdf8da))

- Improve pep8 compliance
  ([`6c5620f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6c5620f2b252711943e0168574bdf794bcd8fff8))

- Improve pep8 compliance
  ([`829cd96`](https://gitlab.com/chilton-group/cc-fit2/-/commit/829cd96f7555a61306e92f1747709c7445037e74))

- Pep8 compliance
  ([`8174513`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8174513777e38c06b5f4949cfca1be010b04df79))

- Remove define
  ([`f54ca82`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f54ca8205b7a2daf327c2dbc3d8697ea9acc4919))

- Remove noqa
  ([`a35fc0b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a35fc0b5a9b9b0835d2af32839d796566248c636))

### Documentation

- Add comments
  ([`efa192b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/efa192ba9e991b3a0f409a928bcc1caa886ef29f))

- Add comments
  ([`686b3ac`](https://gitlab.com/chilton-group/cc-fit2/-/commit/686b3ac50bd851be59fa8e5b53eb589a40ad581f))

- Add comments
  ([`d100b3c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d100b3c4ca4bef76983b62c413d798310e1821d3))

- Add guidance for dc magnetometer file contents
  ([`3d68d87`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3d68d874cf1d767caffe985a96e8c9771d17fdb0))

- Add m_eq_calibration docs
  ([`d4acbbe`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d4acbbe41770982f837abcf025b716774351a15a))

- Add m_eq_calibration docs
  ([`a7891c1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a7891c1c50f27f228b140cdbc2819b94726cb7b5))

- Add missing blank line
  ([`6ff16ca`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6ff16ca2513baa85356178784fd82f044fb6d2dc))

- Add missing blank line in dc cli
  ([`47c6eb9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/47c6eb96cd614c01ee6867c9363db6e7b9a75213))

- Add new line
  ([`c46c8d9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c46c8d9c04c0992e3a1cb43a70179ca3ba56e9da))

- Add new line
  ([`1866d65`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1866d65de35fd4bef16a7a905ffd617a0a6b5d7d))

- Add newline
  ([`8f68830`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8f688305efa1c28b4c96abfef82b1e6ab6405fa8))

- Add plot_times and plot_fitted_times
  ([`c890864`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c890864e3c66615b809d94d19a620ccb0bf93a32))

- Add ref to phospholyl paper in ac cli page
  ([`a70e0db`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a70e0dbb4e88a3650e957b28252722c7dfae0aec))

- Add return
  ([`dbc9f7b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dbc9f7b048d3f5481d86a43b059e17ebb646a778))

- Add WSL wayland bugfix
  ([`718b936`](https://gitlab.com/chilton-group/cc-fit2/-/commit/718b936b01cfb68ac63499636342c7a2304107ad))

- Change help for relaxation plot_type
  ([`110815c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/110815c04abc7708a6c555269c99d28d331d0ed4))

- Field_calibration documentation
  ([`c466b45`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c466b459c741b3a76572544faea6713ed0607360))

- Field_calibration documentation
  ([`06b75d4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/06b75d40fc7885558ba9c5eee9d132419dc2fdc6))

- Fix typo
  ([`a7cc43e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a7cc43e82464d9fb2b9cbc7b7799160c73d2449d))

- Fix typos
  ([`4a52a9c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4a52a9c9dfd79bdd42e8c6d5b9e845a55a1efad8))

- Remove unneccessary noqa
  ([`22a977f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/22a977f903e002f5d10f65a4cf95439b74606dc2))

- Update author contributions
  ([`7ba9262`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7ba9262524e0a0df9a1b38be1ca0cc2951af16ba))

- Update author contributions
  ([`cb0f29b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cb0f29bf82958d06a3d2f278cc641b32cfd31bdd))

- Update authors
  ([`1d3ab58`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1d3ab58c6e25a4fce871b8cfba777af8799a15b5))

- Update authors link and title
  ([`f80dbec`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f80dbecc5b8732f7308f69956b0aa2309217f385))

- Update docstring
  ([`b9ce6fc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b9ce6fc5b6420f38c7148885e8f1355156f10379))

- Update docstring
  ([`7e2ac57`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7e2ac577c1139ecc8747185acac924a8caf9306d))

- Update docstrings
  ([`27a005b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/27a005b6d7f1bde127350c2879b6dab7306d60e9))

- Update docstrings
  ([`539f558`](https://gitlab.com/chilton-group/cc-fit2/-/commit/539f558ae7fe0d3e07a1cdaf32c84ed642312584))

- Update docstrings
  ([`8aea9f1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8aea9f1cd7d9c3ccf9d786b47845dd3691775870))

- Update docstrings
  ([`54fa221`](https://gitlab.com/chilton-group/cc-fit2/-/commit/54fa2217a0f7239e4f2fb4803a3fdb7635f54c29))

- Update docstrings
  ([`753fe80`](https://gitlab.com/chilton-group/cc-fit2/-/commit/753fe8073e3334e2904ef489c51166bb951b62bc))

- Update fitfix arguments and fix table wrapping
  ([`38657ef`](https://gitlab.com/chilton-group/cc-fit2/-/commit/38657ef754c7d471240697d76e7cf146611c2e7b))

- Update help
  ([`3810038`](https://gitlab.com/chilton-group/cc-fit2/-/commit/38100385327dc136bc287bd6a6bd3077c3c1bf2a))

- Update optional arguments
  ([`521de0d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/521de0d10ab765d872b0b8eef38d5ac4e366ae06))

- Update relaxation module plot names
  ([`2e1da6a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2e1da6a82bfdfdbc0ee0cb1c0cb6b5209dbd7844))

- Update relaxation test docs
  ([`e57d6da`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e57d6dadec006865fa60ecbb901f453ea69c9e07))

- Update table defaults
  ([`16f7d83`](https://gitlab.com/chilton-group/cc-fit2/-/commit/16f7d83407a4528ec82563b3b6c380d1744d2839))

### Features

- Add --M_eq calibration option
  ([`a00a74a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a00a74a1bd9db38969a653f99c01523182028f56))

- Add --M_eq calibration option
  ([`a9aeea1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a9aeea13a977cc6da8ae317cd01fafcf84bafe5b))

- Add encoding check and specify encoding for all ccfit2 output files
  ([`4197481`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4197481d78ef87722e7c3261d29362bc37e3a5fe))

- Add error bool in Measurement.from_file, add check for abs(components) > abs(error)
  ([`a8f89ad`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a8f89ad52ff94e77d886bc685cd6ea386fd2ac7d))

- Add ln(tau) vs 1/T relaxation plot
  ([`d88c123`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d88c123045aa741a901e3edd96beffbf77a98a6f))

- Add option for ln(tau) vs 1/T or 1/H
  ([`35a84af`](https://gitlab.com/chilton-group/cc-fit2/-/commit/35a84af40cfbcdbdfb22879c58f2304ab3f9f35c))

- Add plot_times
  ([`de10c6f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/de10c6ff6f39941d30274c0f2bbc63cc2c00e39e))

- Add units property to each model
  ([`4ee4170`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4ee4170a2d5a73584c8ede08dfba68d909ca920a))

- Calibrating fields for dc decays is now possible
  ([`5f2b70e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5f2b70e2406146b12e178c122b72531713379afc))

- Can calibrate field for a set of DC decay measurements
  ([`661a608`](https://gitlab.com/chilton-group/cc-fit2/-/commit/661a6080f4caf5e664cda07d5afd762bf2fc709b))

### Performance Improvements

- Change dp for temperature
  ([`8cb22d6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8cb22d6ed0cda28b095c3e87aa0821dbe521b0c9))

- Change filename orderinng
  ([`bf3ee8e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bf3ee8e456c57c7caa974919705409e4af59498c))

- Change type for isinstance, add warning ignore
  ([`583d983`](https://gitlab.com/chilton-group/cc-fit2/-/commit/583d98341dcec064ef81a7e0ffba321298acd975))

- Enforce tau*1 > tau*2 for doubleexponential model
  ([`fa54b92`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fa54b927cab9ff119621c58daf6ab11e7b91bae1))

- Improve ac single plot filenames
  ([`5efc790`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5efc79079ceba2db7d09c79af576944332e03d40))

- Improve file parsing
  ([`0604e3a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0604e3aa761b71520662287a352e7043a69e1c88))

- Improve measurement->experiment splitting
  ([`c3f01e1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c3f01e19651314cfc5b9a089c2f0f972f49cef3c))

- Improve typehints, add catch for nan, improve docstrings
  ([`c105e97`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c105e97a80c804ed12aa04bab90e6b89ddb5e94c))

- Modify file names for rate and time plots, add option to plot both
  ([`5e0290c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5e0290c77cdc76049a87b553aeb9acf53c425950))

- Modify plot size when --show_params=False in plot_fitted_decay
  ([`51e91f1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/51e91f1ca8896123510925e8c0a3b2d3f4711e46))

- Modify string formatting for m_eq in dc decay plots
  ([`321046f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/321046f87fe13466e0cf589ed9774e3c9f4a6e43))

- Standardise plot optional arguments
  ([`d44c733`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d44c733e4eb023d212d1556abacdd3d8cc7b86e1))

### Refactoring

- Add catch for M_eq calibration
  ([`944cf98`](https://gitlab.com/chilton-group/cc-fit2/-/commit/944cf98c4a5381d208e554ee4326b65cfd67bd91))

- Change catch for empty calib_Meq, remove placeholder comment
  ([`f749ccc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f749ccc3b10895349553229f9834024cc4e3063c))

- Move mintime and moment cut to dc.Experiment.from_measurements()
  ([`fbf35e8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fbf35e862503aa2c6b4853fd502a0fd4c1521e35))

- Move mintime and moment trimming to experiment.from_measurement
  ([`e167837`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e167837b958c437ba9ca94286bdb58f17bf46bbb))

- Remove calibration kwarg of write_model_params
  ([`c6e412f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c6e412f9cdd9b7e752dc07dda92f46ab2fc61c93))

- Remove usage of args tuple in calibrate_meq
  ([`a8ac4e0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a8ac4e061c3d9d0d64ebb5986842d3898c3283d9))

- Rename plot_fitted_tau and plot_tau_residuals to _rates_
  ([`7b49115`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7b491153e51c86aacfcf932e6fc67c020ff8de86))

- Simplify select_T tick config
  ([`579ebc1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/579ebc14e4e02b36c969d185c5113ebc664f89e8))

### Testing

- --m_eq_calibration test files
  ([`0a2ad63`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0a2ad63196ab0553164fe89bad0d22586a65c33f))

- --m_eq_calibration test files
  ([`8f5ce57`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8f5ce571c7844914855bacc92aaa03e1ccd9c833))

- Remove old test file
  ([`8c29e9f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8c29e9fbdad8ad102248258e52df5477bc4bb4b8))

- Remove old test file
  ([`2a88389`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2a883892c03fe36e33a3a405382330ae830d2628))

- Update test code
  ([`53437ab`](https://gitlab.com/chilton-group/cc-fit2/-/commit/53437abab972357bf63c352ddc63b13f650ba02e))


## v4.5.1 (2023-04-01)

### Bug Fixes

- Remove non-standard setting of active RadioButton, manually set active color as white
  ([`94d13fd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/94d13fd027abd75bd449de06a69f802d250ca262))

### Documentation

- Added Raw SQUID Data files for MPMS 3 using VSM mode, one decay per data file
  ([`0ab1e47`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0ab1e4756720a24fd43c05d79954d365aaabe97f))

### Testing

- Add description of dc infield decay test
  ([`b1cffec`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b1cffec6fe35d8af273a10732a6e5df42beebeaf))

add text file describing parameters for multiple infield dc decays

- Add in-field dc decay datafiles
  ([`d757e68`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d757e682fd03d448dd6438868a1c4a48001ed095))

add a set of in-field dc decay files measured with dc scan mode to tests. Include metadata with
  calibrated fields and M_eq values

- Add notes
  ([`f903f8d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f903f8d90a1a1edb73e975745db7b20e7b3aea28))

- Remove incorrect test files
  ([`3f71a3c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3f71a3cc343cddbaccbce4691ade9eb23f24c36c))

- Remove test files
  ([`f20c104`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f20c1047023f415b1e8bb17c352c2d7420222e7a))

- Rename test files
  ([`9ec4587`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9ec4587748048d6107b04ee0ea3048cb8768fa2c))


## v4.5.0 (2023-03-10)

### Bug Fixes

- --plot_axes now works when --process plot option is selected
  ([`87978c7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/87978c7034058708e2005f408ddf2413ef7ae630))

- Add additional ac headers, and correct headers used by waveform
  ([`9f781e2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9f781e2dd9427e0f4c8a896f2d57893adddfbdb7))

- Add catch for missing relaxation file headers
  ([`d1c84d9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d1c84d939dbe7f57171caa684cab87bd4c7ec0ba))

- Add Direct fitted parameters to legend of plot_fitted_relaxation
  ([`fc18ea8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fc18ea819559c3fc2cbacd4d1b047025eb50feba))

- Add manual bound for alpha*gamma
  ([`76bdf94`](https://gitlab.com/chilton-group/cc-fit2/-/commit/76bdf94bb170584246aa56ed5d98b95c13b518d1))

- Add missing ac filename
  ([`85917c2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/85917c25f67b3eba702e4307c6977106f0128917))

- Add missing dc arg
  ([`cb8f960`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cb8f96018fc9da46d470039b30d82b82725927df))

- Add support for multiple measurements as single list
  ([`aec5977`](https://gitlab.com/chilton-group/cc-fit2/-/commit/aec5977a0f55fdc9e530848833a37a46d6baa675))

- Add support for new qt5 code to dc and relaxation modes
  ([`78ad716`](https://gitlab.com/chilton-group/cc-fit2/-/commit/78ad716753dd6ebd880ed60bdeaae749e1b122c3))

- Add support for squid moment columns, add flag for failed creation of measurements list
  ([`01885de`](https://gitlab.com/chilton-group/cc-fit2/-/commit/01885de35f9f08e3fbb49836b5000b31cacd1adb))

- Alter weighting of residuals in TauXModel fit routines
  ([`d11d570`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d11d5707b5156184f4f39afddf0a509002b9240b))

- Bug in modeldata kwarg
  ([`1f85d17`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1f85d172e8708feaf65fab93ab2731606fa204fa))

- Bug in specifying fixed parameter value
  ([`206f90c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/206f90c984867e2ed8ae8dc82af1eccce5401896))

- Catch early exit on file dialog and mass_mw entry window
  ([`c329c43`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c329c436a42b3db3d9a0ce1841e2d6af331e9b86))

- Change ref to gui
  ([`a5b25b1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a5b25b1f14d59f0113c071acfe0c87904a1fd27f))

- Changed help in t_offset arg parser to describe control of t_offset parameter.
  ([`f58ec0a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f58ec0a81a36022bfee7cd0bb3b83b6f412b45a5))

- Correct call to gui
  ([`147288b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/147288b7119271b31a456663f0fc77fe6444b67d))

- Correct cprint dict key
  ([`3f88145`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3f8814588d172875b73fcddd4af686eb06890cd1))

- Correct dc results folder name
  ([`69bce62`](https://gitlab.com/chilton-group/cc-fit2/-/commit/69bce626e846c589524048fd30afebe43edc91fa))

- Correct docstrings, default values and typehints, reimplement save option for dc plotting
  ([`1202bf1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1202bf1ab23fb273f0e01b7692dd94c2e74100ec))

- Correct errorbars in interactive fit window
  ([`4153de0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4153de00c590839228735cfe101f162804108d3d))

- Correct expectation value and sigma for Havriliak Negami model
  ([`588ff74`](https://gitlab.com/chilton-group/cc-fit2/-/commit/588ff74683198a8915ab99c15cf0d0e857d5fc23))

- Correct experiment temperature reference in dc plot mode
  ([`0130ce0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0130ce02ee1e91b418b6d524d416aad333fa0238))

- Correct file names in single mode
  ([`cae1182`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cae118244b9ca909b237b48199802b8d65a9f91b))

- Correct plotting bug in relaxation fits
  ([`e4e8201`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e4e82016ed6020f6ca99556cf80719baf51a2a96))

- Correct print statements and reenable fitted tau plot save in ac mode
  ([`33281d9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/33281d9cc934dd832d7f807e6390629d8575cb11))

- Correct upper/lower header in from_files
  ([`19d012c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/19d012c2eb39674502efa0d2e4b8488556149673))

- Correct use of ac headers in dc_mode_func
  ([`273a58b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/273a58be02eef1d0c108442ba70e47a98830df7a))

- Corrected Havriliak-Negami ESDs
  ([`ff63d96`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ff63d96e53043e914dc20b94a5c70c849d65463a))

- Corrected stretched exponential fitting function
  ([`bf7ab55`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bf7ab551aaff175678b74c5a40a9a2e71ed6dd10))

- Dc modual now correctly shows decays when -save_plots is active.
  ([`0b68c9b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0b68c9bc366eeb35b72cf9adc07f3853caa18ec7))

- Default values for dc module now consistent
  ([`22244a8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/22244a883cff13ee2967773245dbcca2200e8a20))

- Fix ac single measurement save plot bug
  ([`f762f61`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f762f61d84d5026577c8a9c9dd18fba04fbbad5b))

- Fix bug in dc cli
  ([`0710221`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0710221ef174399d38583031c3d0f1d3b21c4a99))

- Fix bug in dc module
  ([`cc002d7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cc002d73c48f394fcf1bb25763ad9c6879f336b1))

- Fix bug in dc module
  ([`c6b27d1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c6b27d10dd06135b628ec9e37de378ce75a4faae))

- Fix bug in twomaxima lntau function
  ([`24bc2f8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/24bc2f8225a43967820442ac9c4d608590ad01d9))

- Fix bug in TwoMaximaModel setters, and improve temperature colorbar labels
  ([`0b45133`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0b45133ab059f7c06a101c9a54c7c6d1256e7923))

- Fix bug with doublespinbox in interactive fit window
  ([`c0e9b23`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c0e9b23bbe4bebd9be50b887f4942b6365279a11))

- Fix bug with toggleable radiobuttons in relaxation plot
  ([`0c3bc79`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0c3bc790c28a4bde6419e507ce0692c39660fa41))

- Fix cli parser help
  ([`4628b6d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4628b6dadfa4896b96225e9e630257dec55464db))

- Fix deprecated features in old CMD code
  ([`5df93b5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5df93b5ec8abe3092fe63ddcf8c8712e8bcd7216))

- Fix double underscore in ac relaxation fitting
  ([`c7c0ec7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c7c0ec7732deee8c01d9455c1b458e1dda8b5489))

- Fix incorrect args to hidden plot functions
  ([`98c35b0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/98c35b0e682bcc137c2208f29e400bf21c01de4d))

- Fix macos bug with fitwindow parameters
  ([`80b3d5d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/80b3d5de6b191f4775ff32826aab9cb4a2f0f038))

- Fix missing sqrt in Havriliak-Negami standard deviation
  ([`db41dee`](https://gitlab.com/chilton-group/cc-fit2/-/commit/db41dee8073e0a785e844d98c9b3c49c83b48841))

- Fix print to command line bug
  ([`fff6c33`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fff6c33e77a4c1754f3e0c47e99e189b2af0eb5f))

- Fix radiobutton pre-selection bug
  ([`7c7d66b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7c7d66b045283c9b8f18ee4180e3137bd6652107))

- Fix saved plot filenames in ac
  ([`ef9e1c0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ef9e1c0edfc71297e9932f8bd3ca3b3985bd67f4))

- Fix susceptibility negative/positive threshold function
  ([`9c14848`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9c14848774ccfe8e48fb2b4c8371f007b8d1420b))

- Fix twomaximamodel standard deviation
  ([`9510c17`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9510c174dab5a9cc35cfac59ea2f1dbf593fb716))

- Fix unit conversion for ac susceptibilities when driving field different between points
  ([`44f58ab`](https://gitlab.com/chilton-group/cc-fit2/-/commit/44f58abb6aa3919b48aaf9b417cc1d0cd46793e3))

- Fixed bug when multiple experiments are in one file in dc module
  ([`dbc5f92`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dbc5f92010bbbc619b1f6f67956b791aed68855d))

- Improve doublespinbox behavior
  ([`efbd843`](https://gitlab.com/chilton-group/cc-fit2/-/commit/efbd84388407b40a8f06b97cf1a296dd86b2ce6f))

- Improve temperature/field resolution on final fitted tau plot
  ([`685a115`](https://gitlab.com/chilton-group/cc-fit2/-/commit/685a11570643c356034939d9ba728255280ade9b))

- Lower field threshold for dc mode experiment creation
  ([`f5aa558`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f5aa5585e7c6eff543d7c9552786aafa5f67252b))

- Modify default values of lograte_stdev and rate_ul_diff in tauhdataset
  ([`2465b4b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2465b4bdd9a92737be4a403735574f3ecc95ab4b))

- Modify default values of lograte_stdev and rate_ul_diff in tautdataset, and fix bug with y limits
  of rate plots
  ([`7178358`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7178358802b4d017a22858ff9318908ebf3f83f1))

- Move save to before show in relaxation rate residuals plot
  ([`ac0600d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ac0600d2be10e4dddbc924d23f8396ce23169eef))

- Plot now correctly shows when --process plot option is active
  ([`db4af3b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/db4af3bf6b5a3c96fa7f0fcc2dac3eb07a7f2e87))

- Program correctly exits before fitting the relaxation profile when --process decays is selected
  ([`820675f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/820675fe9f022bc2151517ad631b0905248d7eda))

- Re-add missing ac header
  ([`2d27c60`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2d27c607b748a8d428d0fb9c338fff28c6001348))

- Reimplement brons-van-vleck terms by default
  ([`e0dec79`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e0dec79baaacdfbedd2c383a7a9c529eead15cbd))

- Reimplement susceptibility and colecole plots with qt to avoid no-window bug
  ([`4158c76`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4158c7643db7109e7b9e9cbc662c78a6dd2b3b2a))

- Reinstitute log_normal changes
  ([`b38c2d4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b38c2d4fb528f25eee5092cbf326376663fe2731))

- Remove debug print
  ([`b69e171`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b69e171968134dbf852e3eb2f7ac02c9b600cba7))

- Remove debug print
  ([`51ce561`](https://gitlab.com/chilton-group/cc-fit2/-/commit/51ce561347cf0b70301cf50d39875d2237c3c14a))

- Remove debug print
  ([`0dbdb44`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0dbdb44da055e3aef72105b3ff0b7f4d8233387b))

- Remove debug print
  ([`d7d8c3e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d7d8c3e8142f9a13d845e37635dfdec52bbe03fc))

- Remove debug print
  ([`affb040`](https://gitlab.com/chilton-group/cc-fit2/-/commit/affb0403c5fabf3aee58c59a834a08d0bdcb6b71))

- Remove debug print
  ([`b7bcb25`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b7bcb255d4c660537df5a4090dff1cdc7cc18e6a))

- Remove debug print
  ([`826cafb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/826cafb887a1ec8642ba44a78909b0bf40f6f809))

- Remove debug print
  ([`0020d37`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0020d37e481cd2f17a7e386f8b8dfd258a996719))

- Remove debug print
  ([`854e705`](https://gitlab.com/chilton-group/cc-fit2/-/commit/854e705f8e1c2cf032ec6f2372779997b591e613))

- Remove debug print and fix incorect parameter and experiment number mismatch
  ([`bd83f28`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bd83f28186b02b562468bcf368df16e9bb102784))

- Remove duplicate plt.show() call in plot_decays
  ([`8652dcf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8652dcfef0adec4aa0e2b719996a6127fb332c61))

- Remove hard coded Generalised Debye, add folder to raw cc and susc plots, fix bug with raw cc and
  susc plots
  ([`7cc878c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7cc878c5a5b44a00c98de5c74cc69e27fd20c039))

- Restore plot
  ([`9b8e7ed`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9b8e7ed1ac8d0782ed11e9cb935f5fd6a9550b4b))

- Revert --dfield_thresh default to 1E-8
  ([`b0c06c9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b0c06c996e52a4a815eb621c694f6661a2b9fdb9))

- Support for MacOS matplotlib backend
  ([`0bbbb1c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0bbbb1cf177bb491818085fe7ff199c7fa707258))

- Tweak Havriliak Negami bounds and residual modification
  ([`b44acb7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b44acb76bbd6f8149372482408eeb7a60314ebf0))

- Tweak residual weights
  ([`30ddd4d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/30ddd4d876efbbed8457fe145269d4fe767303f3))

- Update calls to matplotlib in old exe code
  ([`217f6b4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/217f6b48bf5c7ad316c122914b69e3469399634d))

- Update calls to plot_fitted_tau and plot_tau_residuals in cli for dc and relaxation modes
  ([`dd37523`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dd375235c9c0d76185af22b9b5edba99fda8c12e))

- Update calls to plot_fitted_tau and plot_tau_residuals in relaxation mode
  ([`cb72632`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cb72632da5e5721ce53e3b2f886c036630a48a63))

- Update matplotlib radiobuttons and slider
  ([`1748dc9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1748dc97c4e2487abdc5f93a5b27ed099c50a25f))

### Build System

- Add explicit numbers for dependencies
  ([`6e412ec`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6e412ec75b35ee36e80515d9286f9715dd41aeb0))

- Bump matplotlib
  ([`8372d51`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8372d51cfe89da7e0c07dc8611c158355af89124))

- Bump python requirement
  ([`e81066d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e81066da328f01a337faf15b4de020b72c1769cf))

- Update setup reqs
  ([`dd3af99`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dd3af99bc03bea5dfddbd818118976eb5c3fcfab))

### Chores

- Clean up head
  ([`a8936ad`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a8936ad17e2e0bb1383270fda6c4f432bc810f17))

- Clean up old code
  ([`2dd3b80`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2dd3b80b18ae19f4d7af52b1d61f8a375ffda174))

- Cleanup test names
  ([`42e90b2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/42e90b2685eeb6d14b776377ac29542a772a3f3f))

- Remove old code
  ([`f93ce64`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f93ce64b2880cf1a6bb8d2c46a20bbf5f250e1ec))

- Remove old code
  ([`721ee0b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/721ee0b51da0bfba2378f68fc0dc283d7ae69623))

### Code Style

- Add newlines to print
  ([`022e756`](https://gitlab.com/chilton-group/cc-fit2/-/commit/022e7560888cda7575585bd25a56c61ead7972a8))

- Add unicode print
  ([`891b919`](https://gitlab.com/chilton-group/cc-fit2/-/commit/891b919821b1a6abfaf973beffb504cf716053fe))

- Improve docstrings
  ([`ecaf2fe`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ecaf2fe14672325830356d6102b7b3b699448731))

- Improve pep8 compliance
  ([`4bcd580`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4bcd58023af343788cb0f9f498aa74a56281a8ed))

- Improve pep8 compliance
  ([`a902776`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a9027768fbf84ee25dcc669f55075c25a9182b71))

- Improve pep8 compliance
  ([`35b418f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/35b418f25155bb3a17770478b3270002ed790a44))

- Improve pep8 compliance
  ([`8538cee`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8538cee4ac2d9171161bf63210fca1f44b833b8f))

- Improve pep8 compliance
  ([`23fb492`](https://gitlab.com/chilton-group/cc-fit2/-/commit/23fb492b95cbcb5ecdbb47bb54888ec1bf1e987d))

- Improve pep8 compliance
  ([`bedf60a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bedf60ac46e6eedc7371437bb2fa6bc3ffecb889))

- Improve pep8 compliance
  ([`d14038d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d14038d0c63330f479dac6a1efdacd4405e3006c))

- Improve pep8 compliance
  ([`15480ab`](https://gitlab.com/chilton-group/cc-fit2/-/commit/15480ab0e9b7d452412ecc6442b36d2de80a5f0c))

- Improve pep8 compliance
  ([`b743f17`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b743f179bfcf9560053786f3517ccfa3b6e53cc4))

- Improve pep8 compliance
  ([`2879393`](https://gitlab.com/chilton-group/cc-fit2/-/commit/287939332705381734b43ceee3b5747fd2154163))

- Improve pep8 compliance
  ([`2de12c9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2de12c9dac109b1f3a466d6d4f3573c657637f05))

- Improve pep8 compliance
  ([`dbdbeda`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dbdbeda2c8e1efc566a5257310f21deda594e8af))

- Improve pep8 compliance
  ([`3c765b6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3c765b610dbd7bc94d29b2b91017b5af0ace76e8))

- Improve pep8 compliance
  ([`c87ae1d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c87ae1d2160b65dc94f18ad8f454f694ca75f579))

- Improve pep8 compliance
  ([`0308e31`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0308e3154747628148d94a21095a80acab4e5209))

- Improve pep8 compliance
  ([`7b8efc8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7b8efc87db3cc723226afbf737ad2c8635696d13))

- Improve pep8 compliance
  ([`d59c8ce`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d59c8ce1521eaa7850fd9d963d449ee032f5274b))

- Improve pep8 compliance
  ([`521129e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/521129e0c1677c4b139c3068decc3df35d8f7529))

- Improve pep8 compliance
  ([`02cdccd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/02cdccdc88d196ea99932789ef41edeb758676b4))

- Improve pep8 compliance
  ([`a160786`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a160786b20e007934fd1c2366255ab535dc9ccce))

- Improve pep8 compliance
  ([`f853105`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f8531059c6b0984e510732ffb4af35b65d5adb2c))

- Improve pep8 compliance
  ([`5d86b2a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5d86b2af29ffdc5f0b2278796441efee75672f10))

- Improve pep8 compliance
  ([`54a1fca`](https://gitlab.com/chilton-group/cc-fit2/-/commit/54a1fca91bb34d758b29b1486e1a4de657b3a8f6))

- Improve pep8 compliance
  ([`51e03d2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/51e03d25be09314d85575abf7b00ad0f8922ed15))

- Improve pep8 compliance
  ([`a2c9624`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a2c96242cb98b85206f1ef994fcc402e818ea81b))

- Improve pep8 compliance, remove debug prints, improve non-math fonts
  ([`da0329e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/da0329e2b70aa25b337472c246f49f93c719c6df))

- Improve variable names
  ([`ee4b78e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ee4b78e3686098fc112675fbb481bfe3adf537f5))

- Update variable name
  ([`1ef774b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1ef774b2addc4f4fd498ab3b25c2eb0e6a38c462))

### Continuous Integration

- Update ci pipeline for new docs
  ([`a8c8f4e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a8c8f4e9aa76fce78d16741b04f2cfe92e078b22))

### Documentation

- Add --deselect_T
  ([`dfa5f51`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dfa5f51e0823f59079f960654107ce5d87102ecf))

- Add ac and dc fitting information
  ([`291a752`](https://gitlab.com/chilton-group/cc-fit2/-/commit/291a752c4635d81e274a9064fe6e6dcdce7f3c59))

- Add ac and dc theory sections
  ([`c7d0c2a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c7d0c2a2b8fcc0ffd2111ec6541e9acb6e4399dc))

- Add ac plotting and saving section
  ([`9899672`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9899672708043e2c55815182f177ac2462b8fd3f))

- Add anaconda install section
  ([`5ab005e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5ab005e823dceb335dbb6cde5eaaabbb65310589))

- Add bib to waveform cli page
  ([`33d17ba`](https://gitlab.com/chilton-group/cc-fit2/-/commit/33d17ba6fe68fc557ecd74e5e85142e76b2baeaf))

- Add bounds for other params
  ([`5a6d798`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5a6d79834329c834ab1f53cdb5039be1b088099c))

- Add citation
  ([`8f6880d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8f6880d15a095f0d3428c2285cb61fee51c2888e))

- Add cole-davidson docs
  ([`c0a3fb9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c0a3fb9313eeaccc46650f7b23473d6ace536179))

- Add Cole-Davidson to module docs
  ([`8f3041e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8f3041e2e9e75a3a2c795557c72a9105a7f6f2b2))

- Add comment
  ([`5ec436b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5ec436b7e2ceff79a7f182a6bf294b062863af65))

- Add comment
  ([`557858f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/557858f48e7d9a731b04479b1c959d910e7a3b88))

- Add comments
  ([`4398993`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4398993a02c833ea415a6300d9fab1542c91966f))

- Add comments
  ([`e45d392`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e45d392c2bf96220b1ebb75b9d5e7c2c99e670df))

- Add comments
  ([`d47904a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d47904a73ff319c4e4a31321cf05c898f44378d9))

- Add contributing and bug reporting, move authors
  ([`9dd3e89`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9dd3e89e5464cb8d971c7e46718cca0eb393b471))

- Add copybutton to codeblocks
  ([`bc64e71`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bc64e7182e5324a43e2ab36beb69b9ff027a403d))

- Add dc file creation section
  ([`3e456ec`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3e456ecd2f17a3a97749bf873d395f66d001dc4c))

- Add dc plotting method section
  ([`62fe97b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/62fe97bf99d1bfec3c5e80b3163cb0388d84c39d))

- Add DC scripting tutorial
  ([`0ff3cfe`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0ff3cfe0bc010e414f5a78d0a78a40e60f1a564c))

- Add docstring to make_parbox
  ([`f20935e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f20935ee05dae676b2531c00b9d1eb39fcc2581e))

- Add docstrings
  ([`a5a0607`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a5a06072481ddc8ef93e4a19f4d3a6aa25505105))

- Add docstrings
  ([`981307e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/981307e8cb19fb643005ceb1c28ae04aaec0912a))

- Add docstrings
  ([`4e83a29`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4e83a296f023c582d5d89338ab156cda4d5ba59e))

- Add docstrings, improve pep8 compliance
  ([`b26aef9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b26aef913155e1ef71dae2a038e7dab1864e2474))

- Add documentation to FitWindow
  ([`52ebe51`](https://gitlab.com/chilton-group/cc-fit2/-/commit/52ebe51fcc35d9bcf0a5cf48de314f5415237587))

- Add error headers to ac docs
  ([`3cbc6c4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3cbc6c4ca22f4046d501b6d005daf75517995aba))

- Add example for scripting usage of ac module
  ([`bee83b1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bee83b15815045c11fae3c1da44235decb7f1155))

- Add faq, remove executable installation
  ([`e83da89`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e83da89808616c77640da67eaf835398cf25dc99))

- Add final relaxation scripting examplel
  ([`5146c43`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5146c430a3c04c54023fce25f5c7018579e8e92e))

- Add flow diagram
  ([`37ce045`](https://gitlab.com/chilton-group/cc-fit2/-/commit/37ce04502b892e49c7f251c97c9f9ec0b7d1be49))

- Add footbibliography to relaxation scripting
  ([`9236c37`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9236c376f187d9d05c03f813a652e492b6961d3e))

- Add header
  ([`9381256`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9381256fd416d8c7ecdcad75aa331b53018cf93d))

- Add license link and source code link
  ([`81dd1b9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/81dd1b91209d1926b99783019873e7f03dc89426))

- Add link
  ([`a47129f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a47129ff6b7f0cf3f9ae9153401c6e7832b3dee4))

- Add link to euler-gamma
  ([`0eeab54`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0eeab54ad8f5639356db8be268579ed8d902061c))

- Add missing docstrings for Dataset classmethods
  ([`669729f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/669729fe8ae695d79fea7a5a8ff3b3e2b1fc9abd))

- Add missing example info
  ([`0e772cb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0e772cb8af5bc1340e6b9b376c24e3bf7a7f085d))

- Add missing extension character
  ([`89b2d3b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/89b2d3bdc4c57c59e5eca30d3dd8a07cb9a1b145))

- Add new ref
  ([`cedecf3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cedecf390e395ab4f108e2541b8f568dc245d8b0))

- Add note for single exponential
  ([`8dcd5e5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8dcd5e51ff8fb1cc93f199165feedf256d5c4e68))

- Add raises
  ([`45794db`](https://gitlab.com/chilton-group/cc-fit2/-/commit/45794db9e1ceb5af4af05b2113b4318a323f7f0f))

- Add Raises to docstrings
  ([`efcb4b5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/efcb4b5dbbfdc2383c70405a01469f27990ea074))

- Add ref for equal chi model
  ([`a74fc6d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a74fc6dd06744e61d3ac72467b553d330b9328c0))

- Add relaxation fit weights
  ([`9949840`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9949840135406f31a56ed5e2a16ebfa9ba5a43be))

- Add relaxation scripting tutorial
  ([`9316d0d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9316d0d0340f737198decd8e6486473fe56faa33))

- Add relaxation theory section
  ([`359a88b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/359a88beeaad05efc9e4699209b11619cb09fa0d))

- Add return typehint to dc decays
  ([`928c9eb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/928c9eb6a5140611922ce117ce262ea56cd58b6a))

- Add return types
  ([`eb21519`](https://gitlab.com/chilton-group/cc-fit2/-/commit/eb21519f6c90b55e5cb8f094d9dd580d427744cb))

- Add save to relaxation scripting example
  ([`ac064ac`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ac064ac8b3426f015c8e32a31edd845d1b835ef2))

- Add stats page
  ([`f0f288a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f0f288a2b7a8fc74af6f41e7c07ede51f837c6e0))

- Add temp thresh to dc, add note about moment header selection
  ([`724b645`](https://gitlab.com/chilton-group/cc-fit2/-/commit/724b64572eddb5892d655fba1d72341530280763))

- Add theory splash
  ([`52a9003`](https://gitlab.com/chilton-group/cc-fit2/-/commit/52a90035a02606f1ab50b4e37514947593b2d849))

- Add to contributing rules
  ([`a0b6535`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a0b65354dae2165d569a6d88ce719443e2d057ca))

- Add to faq
  ([`281e3af`](https://gitlab.com/chilton-group/cc-fit2/-/commit/281e3af9907211c371d52b011b4f1a828cb7d4eb))

- Add TwoMaximaModel ac documentation
  ([`c2dc1e7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c2dc1e7b794f9fa40c29cabe6fe4a55320983a6a))

- Add units to docstring and to dc model classes
  ([`5b7a9d8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5b7a9d814ba8079dc54498b3144fd964ef87cf0a))

- Add upper and lower bounds description for double stretched expo model
  ([`4ea1e45`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4ea1e4555396b43d38fd41ed14646a8c7564bce1))

- Add upper and lower bounds description for double stretched expo model
  ([`6a4305a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6a4305a7723bb5d11307c8224d776d0446f0c8ec))

- Add upx info
  ([`f9d804b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f9d804b4050b5be61fe59ab371b62212ad1e942d))

- Add version number and image to docs, add auto-updating year
  ([`7b2820c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7b2820cb30cc0cf7dba07694f447906d9a602403))

- Add waveform cli docs
  ([`51dd30a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/51dd30a6a5e6ff26dfca569060396b18c9d924c6))

- Add waveform module docs
  ([`c192456`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c1924560fd57ee4e2de080a71a3f922bede91a15))

- Add waveform reference
  ([`b30b923`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b30b923c64589276c131ab5a3e8c0e0d0b2fe29e))

- Add weights definition
  ([`34704ba`](https://gitlab.com/chilton-group/cc-fit2/-/commit/34704ba2d2733c1a31f8cb5d188fb182bbd567ef))

- Add windows envvar
  ([`97f7e6b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/97f7e6bfaa745d40050c005b22d6720f8b9c6738))

- Add wsl installation section
  ([`a261175`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a261175ad4721a1016c91d66d40e0ed9ee546142))

- Added how to use mono-exponential in double exponential model docs
  ([`1a70f5f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1a70f5fd72405e7259eb0b2a0256e1e815abb60c))

- Added numerical description of Euler-Gamma constant
  ([`c291bd5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c291bd518b3ee4a132368015449e2b2057746828))

- Added Raw SQUID Data files for MPMS 3 using VSM mode, one decay per data file
  ([`9812ee3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9812ee34a8882840422ff912f13f96dcdb7d2a36))

- Additional help for waveform module
  ([`e2504b9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e2504b915d8148b7917d96b8dd2c289afd496fad))

- Additional help for waveform module
  ([`cbeca2a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cbeca2a0cf5f2fc0f49064522fb80f22476c5507))

- Better equation alignment
  ([`ae800d9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ae800d997273917d76e8be87d5b82b1008bc0305))

- Change alpha to gamma in cole-davidson
  ([`1cf5c58`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1cf5c583d09fe1810256c12551b7507d53302421))

- Change cli help
  ([`0e0bac5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0e0bac5a1c3ef16bef4ae2ea5cd9174546fd9454))

- Change Double G. to Double Gen.
  ([`900ca1b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/900ca1b6f761b098276318ad2a0f1bdc891705b7))

- Change unit brackets
  ([`cae4f20`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cae4f20e806c176e7a68fd5b57f05893479d6870))

- Correct latex
  ([`af5b108`](https://gitlab.com/chilton-group/cc-fit2/-/commit/af5b10893256b9ff809a6ff95ae3fdd2df8b7edd))

- Correct py domain
  ([`33aa2cc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/33aa2ccd5060c6628619a1a6f1610f316f15571a))

- Correct python domain
  ([`93f723e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/93f723e2ee8d7304dd39baa0830bf8474946e832))

- Corrected wrong info
  ([`4af3e39`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4af3e391e4147bf54c241d2f55ba8b3cb054acba))

- Fix codeblock
  ([`42de136`](https://gitlab.com/chilton-group/cc-fit2/-/commit/42de136e361e75849d6e534dfef51fee312f593f))

- Fix codeblock typo
  ([`8add0a3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8add0a3fb5b801b571db0a23227bbefe4f492e7b))

- Fix language
  ([`ee97e98`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ee97e98d5194a84f9ca74d91bdf225c3a50d59b2))

- Fix mistake in generalised debye and twomaxima equations
  ([`c9a2aa9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c9a2aa9b3a60bdc59bd05b99b90a35e5601af48c))

- Fix params.out name
  ([`db115c4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/db115c4aed55f78b561394c8402055c838f523d4))

- Fix ref to paper, and update executable section
  ([`33f2551`](https://gitlab.com/chilton-group/cc-fit2/-/commit/33f25518d0ed99a229e48828984826f0845ce744))

- Fix spelling
  ([`293b74e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/293b74eba7ceead6f5a56599750fc59959da5104))

- Fix superscript
  ([`8cefcd0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8cefcd0316a5a04ed809c9d77221c08c3438ad23))

- Fix syntax
  ([`9d3cc89`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9d3cc89696708e1f6ad0a9edf8137ed1fb091fab))

- Fix typo
  ([`da7c4ba`](https://gitlab.com/chilton-group/cc-fit2/-/commit/da7c4babb6214d5ebd4bcf713b1039706ed3cddf))

- Fix typo
  ([`2f14468`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2f14468702bbe6d72f0abaf9e5aa6f5f76ba845b))

- Fix typo
  ([`01e4aee`](https://gitlab.com/chilton-group/cc-fit2/-/commit/01e4aeee85774a6125736d31c59f850faa7fe106))

- Fix typo
  ([`3f6eb64`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3f6eb649fc61ef3ea720ab81f0d24088942b9de4))

- Fix typo
  ([`6f34b98`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6f34b984cec4884bd376ad449f68864aec229b85))

- Fix typo
  ([`333a242`](https://gitlab.com/chilton-group/cc-fit2/-/commit/333a2428615d93ab5659c448c231db3e6b871d0e))

- Fix typo
  ([`6c3d837`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6c3d837dbbe0b36d96950a9321bcd64074b29807))

- Fix typo
  ([`43a3cdb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/43a3cdbee038500c784df403495806cfa456600b))

- Fix typo
  ([`70e0d04`](https://gitlab.com/chilton-group/cc-fit2/-/commit/70e0d041a274a054de64e25383652d9277292356))

- Fix typo
  ([`68fc7a7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/68fc7a7e79efbfe3592a29b06aa14b4d39485b42))

- Fix typo
  ([`72c1956`](https://gitlab.com/chilton-group/cc-fit2/-/commit/72c19565229b243a4cd0d2610001ca63c3ceebfb))

- Fix typo
  ([`256e1ca`](https://gitlab.com/chilton-group/cc-fit2/-/commit/256e1ca1efec930e917e1148461a8657cd613c7f))

- Fix typo
  ([`8537045`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8537045807ea717f09d8e22aa515252bd7ba9adc))

- Fix typo
  ([`7a6963b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7a6963bf359f8a7c5abe0c2930bf66205ed17ca4))

- Fix typo
  ([`e3cca82`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e3cca823845837833429ae71124a5d944d32d688))

- Fix typo
  ([`caf3c6d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/caf3c6d41209b134ef2414ca409a6aa4e2e39e81))

- Fix typo
  ([`1c0336f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1c0336fb396910951f281104fcacee7c43227555))

- Fix typo
  ([`8b4bb56`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8b4bb567cdc2a3bf8d12f95dccaf98809abc7000))

- Fix typo
  ([`126da99`](https://gitlab.com/chilton-group/cc-fit2/-/commit/126da991f0cf590265065d82126fe4297283714a))

- Fix typo
  ([`00bad07`](https://gitlab.com/chilton-group/cc-fit2/-/commit/00bad0706ad48cf476695faf5f5b82468bcfff9c))

- Fix typo
  ([`403fd86`](https://gitlab.com/chilton-group/cc-fit2/-/commit/403fd867aca5c3420946dbc40a3270b32cfe00ba))

- Fix typo in dc cli
  ([`d4f46b4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d4f46b43ce49c3cea55ddbf2616dfbf545e8dcac))

- Fix typo in docstring
  ([`3e067b8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3e067b811b6856e3e804db9577064dbb1646ff30))

- Fix typo in eqn
  ([`3c77a5d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3c77a5d8a1b3ca29076d53522ee87f14319dd37f))

- Fix typo in Havriliak-Negami equation
  ([`8116128`](https://gitlab.com/chilton-group/cc-fit2/-/commit/81161282978cea82a62da87fb1a7781f638b5335))

- Fix typo in link
  ([`edea0ca`](https://gitlab.com/chilton-group/cc-fit2/-/commit/edea0ca03c8e0f78748a5ea55aa9d17222a7a638))

- Fix typo, add waveform to home
  ([`5261152`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5261152214dd5d8c392eac92fa551aa9ab77a63a))

- Fix typos
  ([`a042acf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a042acfc991f731d73777f2df9d01189b6334fff))

- Fix units
  ([`3ba5553`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3ba5553b6c496577340041758ee7f0455acc2294))

- Fixed formatting
  ([`e6d3d15`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e6d3d1595266266e2e04d45486ba3949d8d0cdd5))

- Fixed formatting
  ([`0b18e6a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0b18e6abb7b1cdbda345dd0aa4ad288855b41f17))

- Fixed formatting
  ([`2283de8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2283de88f3d515f4383d56b498a33bf2b554795a))

- Fixed formatting of dc decay docs
  ([`e99d058`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e99d0580bc7d80abf482cf23e2f566e1896742c4))

- Fixed formatting of dc decay docs
  ([`209bf56`](https://gitlab.com/chilton-group/cc-fit2/-/commit/209bf563a53aa28b484a9fe1d0e415e23ac50737))

- Further info on Two Maxima Model
  ([`8cdf837`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8cdf837ca87f8d501adf48a327215dfa3b130b1f))

- Harmonise output file descriptions
  ([`eb27d25`](https://gitlab.com/chilton-group/cc-fit2/-/commit/eb27d25eea71208fdc14441578d3a2f2cd110fff))

- Improve argument entries
  ([`5b47871`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5b4787190f02861a97ff643563020fdc87399fda))

- Improve consistency
  ([`9cb37fe`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9cb37fe326c8e83709c74b1db16c679ebadeddcc))

- Improve description
  ([`6e50e55`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6e50e551d8cf5ba56c791f233ef50f886a6e92b4))

- Improve docstring
  ([`efc2706`](https://gitlab.com/chilton-group/cc-fit2/-/commit/efc2706ecce20f540bb314bd9860ff31f70d33dc))

- Improve docstring formatting
  ([`a14bca2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a14bca2f097666d979b0b9e569d581bfd28c97e7))

- Improve docstrings
  ([`df21039`](https://gitlab.com/chilton-group/cc-fit2/-/commit/df210391de930a1eca79954e9912ada2483ab15f))

- Improve formatting
  ([`13fc2d6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/13fc2d68204be8f60a08d8edb5c54d2ad62f0822))

- Improve HEADERS_SUPPORTED docs
  ([`2e71a86`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2e71a86e14b53b6500cb3754813d22f3f1ea30b9))

- Improve help
  ([`cb8543e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cb8543e09427c696a3c0968f6a00465cf6dbcf09))

- Improve language
  ([`db52fec`](https://gitlab.com/chilton-group/cc-fit2/-/commit/db52fec32fb065dc482885ddc18da4294928302a))

- Improve language
  ([`dd5e267`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dd5e2678b643943a140587da2393b70bf52d4099))

- Improve typehints
  ([`cdbe342`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cdbe342d51d2d4e02954d6ec95d2c4b56e087a7c))

- Link bugs to faq
  ([`bbf7128`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bbf7128c285d1a07d7d880b467f71607b932eeb0))

- Minor updates
  ([`83ca194`](https://gitlab.com/chilton-group/cc-fit2/-/commit/83ca1949e1aadda4229df9b374c87d5607fef9a5))

- Minor updates
  ([`26cffd2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/26cffd2c4712716032fe8aa8b274265171a4d215))

- Minor updates
  ([`7daff3d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7daff3da9e234d08cd57406274a4392132e04821))

- Minor updates
  ([`12b7271`](https://gitlab.com/chilton-group/cc-fit2/-/commit/12b727196b1c0d4317e1f3676de0542a8a0744b1))

- Minor updates to text
  ([`79093e7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/79093e7fe69cccfd2c4c6543bb712f4980f11f44))

- Modify help for relaxation mode, improve pep8 compliance
  ([`39d9d64`](https://gitlab.com/chilton-group/cc-fit2/-/commit/39d9d64a823038cea3e3ed77278c77ac4de93c96))

- Move authors
  ([`71c1c48`](https://gitlab.com/chilton-group/cc-fit2/-/commit/71c1c487015a970ea9858e684ae66a9d800b71b7))

- Move files
  ([`93c2277`](https://gitlab.com/chilton-group/cc-fit2/-/commit/93c2277fc570f3a76285544d2f4c324237cf7c2f))

- Remove duplicate docstring
  ([`a3ce464`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a3ce464016de72f9b11b345e833c23650b08f89d))

- Remove exe reference
  ([`97f80cf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/97f80cfafa90fc8c5da6f8202933aed7641d5e7b))

- Remove fit uncertainty warning explanation
  ([`370be98`](https://gitlab.com/chilton-group/cc-fit2/-/commit/370be983139b8c1fe6b1b191d83d20d399fad499))

- Remove gamma between 0 and 1 bounds
  ([`b606925`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b6069255f2cd6c0c2c1af8236acec1a2da38dc17))

- Remove incorrect todo
  ([`262c4a4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/262c4a44bc47f3c57e387a70343f84fad9f2b134))

- Remove numerical description of Euler-Gamma
  ([`85bee13`](https://gitlab.com/chilton-group/cc-fit2/-/commit/85bee1351df2a709c62c6c5e6d105905b7724247))

- Remove old equation reference
  ([`6debf7c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6debf7c717290fb9d0ce57b17b21711c0a9f58f2))

- Remove old pages
  ([`f1c7dc9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f1c7dc9dfd92f2106e2ef92c033acb9d573c04b2))

- Remove old relaxation optional arg row
  ([`24eee8e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/24eee8e37637c3d229eb29f19e4a787cfdb8ed0a))

- Remove unneccessary typehints
  ([`f23880a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f23880a8f81f7528b421cd9be88529a5581a2637))

- Remove whitespace
  ([`2fc2d1d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2fc2d1d07583d6cf102ffab6eb09f192dbca4f7d))

- Simplify docstrings
  ([`14ae010`](https://gitlab.com/chilton-group/cc-fit2/-/commit/14ae0107678343a7841416b580e53822c6174a80))

- Simplify text and add initial theory section for dc
  ([`eb4e046`](https://gitlab.com/chilton-group/cc-fit2/-/commit/eb4e046c999ecde13278046333fc906d4aec36dd))

- Tweak language
  ([`93ceb99`](https://gitlab.com/chilton-group/cc-fit2/-/commit/93ceb99f7b7c6ef2109f0142057fe0a38e1e6348))

- Typo on docstring for dfield_thresh
  ([`4d5c42e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4d5c42ea189c091c0f7cf88aca936698c9c0e324))

- Update
  ([`69a7ca9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/69a7ca9e059f2c23dd6b3946f669c4fb75a80c58))

- Update ac example
  ([`6756b89`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6756b89a3e381fdf6384015debbb49ed6d9fc01a))

- Update ac measurement and ac experiment frequencies docstrings and units
  ([`825f313`](https://gitlab.com/chilton-group/cc-fit2/-/commit/825f313eb0ce83a9eecae7a6be014805859ae715))

- Update AC sections
  ([`c5a258f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c5a258f9ed9ba9b4034a42596b1c21bf74fa2c06))

- Update cli usage example
  ([`205747c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/205747c219d5f0796ace68c0f269f786fd00890b))

- Update codeblocks
  ([`6dcca05`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6dcca0530857e7b4b2ba7844638d40e8e7712457))

- Update contribs
  ([`63a89e8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/63a89e806a0eadfacc3548b5a948ff0b51e88c1e))

- Update contribs
  ([`c6ada17`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c6ada17f96ac400924ffb015b5788dc19a623bee))

- Update contribs
  ([`ceb6605`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ceb6605cbee527ecf514a902aa1abe2198959013))

- Update contribs
  ([`5a019ff`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5a019ff28144162a4f850dbc654702f23fdbce3a))

- Update contribs
  ([`c6d1aca`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c6d1aca291f7c28222a6e2224acc19bec0f6c1e0))

- Update contributing
  ([`85fd55f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/85fd55fd814ab863f5a84e427fc1d0d4f22e16ed))

- Update contributing guidelines
  ([`d100177`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d1001779cd507472df7c76eea8d527198b32dccf))

- Update dc and ac options
  ([`bba1751`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bba17517d7eee9161974314622e548ef71be2db6))

- Update dc docs
  ([`910dd05`](https://gitlab.com/chilton-group/cc-fit2/-/commit/910dd0563d825b223eec6fe1006687f3bd76582b))

- Update DC docs with relevant equations
  ([`98f19a6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/98f19a6397906b0f9149dc5525907c5fb421f475))

- Update dc file extensions, update waveform docs
  ([`ad0bdc4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ad0bdc42bfb98c65dda4fe61a2afe20474e916a6))

- Update dc page and add bibtex refs
  ([`dd1457d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dd1457d7eada690f4ccc4e7a531408623c6512a3))

- Update dc parameter specificationn
  ([`d3cafd8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d3cafd8b2ac8ac2a35075c29dc55f86f58e86247))

- Update dc params file name
  ([`54ec18d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/54ec18dcd65a142249678aa6edb2985ab4f62d48))

- Update dc reference for phospholoy, add missing moment headers
  ([`e1e6b3a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e1e6b3a2de08c31fab7211ba90041adf985e5685))

- Update definition of relaxation rates
  ([`333d459`](https://gitlab.com/chilton-group/cc-fit2/-/commit/333d459a9b9086ab3d193353ff21f2b478d213b6))

- Update dict typehints and docstrings
  ([`f696acd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f696acd388955bfe7903df1ef3ab3cea7f0f210b))

- Update doc strings and dc args metavar
  ([`4e217f3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4e217f3ca43c59c5743e29cdeb2583f5767f0019))

- Update docs
  ([`338dca1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/338dca1f7734bd3b9dd84521cafca2d342d6030a))

- Update docs
  ([`87659b5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/87659b597627ec693caa877ea1df77f9fa64847d))

- Update docs make command
  ([`b3909b4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b3909b41f1a192cafdffaa67d58ee729c1030a0b))

- Update docs with errors/warnings and output files
  ([`00e227b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/00e227b88ea6905b88480df95fa4fd0d20ee0858))

- Update docstrinbg
  ([`c9bd15c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c9bd15c37a122151f42c1949c72458fea0700425))

- Update docstring
  ([`36d54b6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/36d54b6ab8cb79ab38d424c0d9474de0edfd62fd))

- Update docstring
  ([`cafa4c5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cafa4c575d30ebf7de34d68ca241f1b61a9836a2))

- Update docstring
  ([`2dd8199`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2dd8199f4045fd7e6c65cb086b47064dfa03cbfd))

- Update docstring
  ([`f4e8e49`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f4e8e491244a13092519e9a0b1d669b5655bb199))

- Update docstring
  ([`3cc09e7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3cc09e7805bfc38447f36f4c6eea5cd78390eee7))

- Update docstring
  ([`fa985cb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fa985cb47f25be5c1e790b5643d3e8ca660e8c58))

- Update docstring
  ([`860a9f6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/860a9f6c51f052b7b163b80662cd6cd249d50433))

- Update docstring
  ([`1b87213`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1b87213631350050cf808bce519f1cfd48fa4db9))

- Update docstring formatting
  ([`279db1a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/279db1a3f80be34112555ca0a2d922e14ad43522))

- Update docstring formatting
  ([`71b5b9b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/71b5b9b82b947b02eee2e34a5bda3c9e956a2b93))

- Update docstring language
  ([`75fd066`](https://gitlab.com/chilton-group/cc-fit2/-/commit/75fd0660d71d993df4c14582016aeb6a1e98e42f))

- Update docstring tk reference
  ([`eff7bba`](https://gitlab.com/chilton-group/cc-fit2/-/commit/eff7bba28d2b0c4dfcafcee7070b046dfffdf07e))

- Update docstrings
  ([`1e3bfc8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1e3bfc8c98fc7c2404f2fd78baee0e41c5d1e8af))

- Update docstrings
  ([`a5703a3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a5703a36727fc95fb253d2684d084f40a6ad3ec9))

- Update docstrings
  ([`a65dc5e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a65dc5e5fcbf526d759edd0418533bc7aa3dcb3a))

- Update docstrings
  ([`b533301`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b5333013176ba63452c7ea7d9135e36fd05fc639))

- Update docstrings
  ([`e83f3f4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e83f3f4dda1ea4c9f0fd9d32f01d41d035cac13d))

- Update docstrings
  ([`aca2a49`](https://gitlab.com/chilton-group/cc-fit2/-/commit/aca2a495844d9613cbc4154700474b66d81ecebd))

- Update docstrings and docs formatting
  ([`32ae3ed`](https://gitlab.com/chilton-group/cc-fit2/-/commit/32ae3edd2c9559b92f25fbd6b5421c663b6791b5))

- Update docstrings and typehints
  ([`c4dedea`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c4dedea9205463c51d430a302141f3e89bd95155))

- Update docstrings for plot to numpy style
  ([`8133cbd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8133cbdcb6e31e06d51945e9e00401e0e94e1f2d))

- Update documentation and docstrings for AC, DC and Relaxation
  ([`7b698f0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7b698f0650c181a481257ea7b910a46c9130e5c7))

- Update documentation and file names
  ([`55b9133`](https://gitlab.com/chilton-group/cc-fit2/-/commit/55b91331348726493eabb037f265945d67849317))

- Update documentation file name references
  ([`08e84ca`](https://gitlab.com/chilton-group/cc-fit2/-/commit/08e84cac597ce4c573cb7ce896cc95927e5c4c3c))

- Update documentation instructions
  ([`b3def0a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b3def0ae72346c75a8a14ee2c701307e65755119))

- Update equation formatting
  ([`b363b12`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b363b12e01d679329d5a52106457b3a2cf88d6b5))

- Update example description
  ([`f33b1d3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f33b1d311ab90787551d93a6467d0421f675ecb4))

- Update executable
  ([`f0dac04`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f0dac0400aacfb7cac1c030b17d0f384f38effa1))

- Update executable install
  ([`9b09ce1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9b09ce11dd0d42efde8300cd6047bd95bce75a70))

- Update executable references
  ([`50134ff`](https://gitlab.com/chilton-group/cc-fit2/-/commit/50134ffa40390089c18bad92c4859e5aa94a3c6a))

- Update executable section
  ([`3218ec7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3218ec709e3dd068f8be87912eee6355ad6c0858))

- Update flow diagram
  ([`272c54d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/272c54d0930df40353a57bf7ac094c250476367b))

- Update flow diagram
  ([`243c11f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/243c11f55aee6d38cc7b8a2de71f3fded3862661))

- Update fonts section
  ([`6146469`](https://gitlab.com/chilton-group/cc-fit2/-/commit/61464690e002c249dedc713a289bf785818a2cb9))

- Update formatting
  ([`2c6ba21`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2c6ba21c2938e82fd7e9d63fdabae20a578ecc88))

- Update header text
  ([`708535c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/708535cd73749042bd82e00660fc328af73b5417))

- Update help
  ([`2345038`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2345038059c64ab3f00b7948f9503718ee9e8837))

- Update image
  ([`a534429`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a5344291174843090c6e3d091ea53325bc532417))

- Update image
  ([`d97add9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d97add97d6b01ca299764c5656da5775176d6cc2))

- Update input file format title
  ([`7c46305`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7c46305e25525d763c7942ddcb7eb195eeb801f1))

- Update installation
  ([`594ee97`](https://gitlab.com/chilton-group/cc-fit2/-/commit/594ee971cad87328755407c87f36816a2c4b0443))

- Update installation codeblocks
  ([`f6d6919`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f6d69191bd4379c8f4a7459e666edacd21bdf9ea))

- Update installation page
  ([`8bf59e2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8bf59e2f985f680431fc24558529846521a713a8))

- Update language
  ([`4b1dc0a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4b1dc0ac8f5b2b04499a78f9257b742cbda0db10))

- Update language
  ([`b3831a4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b3831a47aa27dd762f35b6178d0259b9c430dce1))

- Update language
  ([`a550986`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a550986a2beb469dd60cc2176f501ff42f15d5cc))

- Update language
  ([`c65b2e0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c65b2e0a8a69eca1efabfceb4de3e8481ad7ac9a))

- Update language
  ([`1e869be`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1e869beb37a12afa54d93a6d68ada4c546386448))

- Update language
  ([`092d5ca`](https://gitlab.com/chilton-group/cc-fit2/-/commit/092d5ca5e9bb948ca61ed52111a58de626c48506))

- Update language and formatting
  ([`4b50741`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4b50741ae024d5c3a0cb6fc8a073dac08cf48944))

- Update latex formatting
  ([`62ed61c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/62ed61c524e1daa3ef5dcfc1aefa1b3119f49bd2))

- Update layout
  ([`1331d8d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1331d8d0e35101194498892bad7bebbc3cc4c788))

- Update layout
  ([`bf07738`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bf07738602007a952eee01c30c9a4c48c030c232))

- Update layout
  ([`bf44f4a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bf44f4a95be95d364c59f8f69ba3c6ede2b8a09e))

- Update name of double exponential model
  ([`6fa94bc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6fa94bcf49c70a205f0a348b89a0106d7e70cccf))

- Update optional arguments
  ([`9e24470`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9e24470ccf6053e2fe1ec2b1c0d49e24ab73b9d0))

- Update readme formatting
  ([`7d4c118`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7d4c118abbf5f9df50ae8509ea4ccb9f8f3b9814))

- Update readme, add executable compilation to docs
  ([`33a936f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/33a936f4019153b3eff9cf048638c46de4f495e3))

- Update readthedocs name
  ([`be5a3aa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/be5a3aa00f5bf599b3b2f13dd0761aa271bf539b))

- Update ref
  ([`a4b5441`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a4b54410789e00d2aa8858587db9e04e4bde7b6a))

- Update references
  ([`daffd26`](https://gitlab.com/chilton-group/cc-fit2/-/commit/daffd26e17bf8ec3d591dc95230eb2bce4171698))

- Update requirements
  ([`e12e743`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e12e743bfb8acb549b50bd1333ed6813103da1eb))

- Update return typehint
  ([`207311c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/207311c66857decd85d71208c8cfc54c32b9340f))

- Update style
  ([`e7c2e40`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e7c2e40043dcf38850fd338cdf4c1e4416ce0196))

- Update style of arguments
  ([`dd9e60f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dd9e60f9f43ec5a47c0c91fff5b0c49fea967cd6))

- Update table formatting
  ([`5bfbdc8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5bfbdc89f0ac716ee02451487bea895ea572b6ca))

- Update text
  ([`e90de5b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e90de5bede401d017de557a9ec8f75a8ee7e903c))

- Update text
  ([`61f56ca`](https://gitlab.com/chilton-group/cc-fit2/-/commit/61f56ca049d60317a249a4788a5cba94e99af79a))

- Update text
  ([`87d1e49`](https://gitlab.com/chilton-group/cc-fit2/-/commit/87d1e49af84b47f055e7962525c7619ed24cdaf4))

- Update tone
  ([`8fa4a83`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8fa4a834cc9affaa7f41eb8eabc2fdbab6714dcf))

- Update tutorial
  ([`7515205`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7515205b3c163fbfe5af73cc3fd62b1805cee715))

- Update typehints
  ([`ce90825`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ce90825837231d67a27eaed917b884e289571379))

- Update typehints and docstrings
  ([`65d1c5f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/65d1c5fa2fc28c91cf98656b3631094812e7b645))

- Update typng
  ([`4bb2df9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4bb2df9e7251d8e22576c9aa319cbc41735ed00a))

- Update typo
  ([`b9d466e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b9d466e49e363069dc4c73ef83c433bcf0d75375))

- Update units
  ([`8af740b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8af740b5d813a277fae95ad88ac4d8fdde7f0e5f))

- Update units
  ([`bef34cf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bef34cff3e9977e24392451fc36d7c27abce49fc))

- Update units
  ([`0aff845`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0aff845befe1983fe6d573ebef73a65bda24172f))

- Update waveform docs
  ([`150e1aa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/150e1aa9eee94a754985fcdc688a3d23bb033924))

- Update waveform docs with table of headers
  ([`b845496`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b845496797c9a9ee5b5006dc7950347c9760438d))

- Update wildcard support description
  ([`1618475`](https://gitlab.com/chilton-group/cc-fit2/-/commit/161847549e5b5580fc269382839458cd13135649))

- Update windows exe instructions
  ([`f7e1741`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f7e174143d6cec022274d34af19f6d533ff47bff))

- Update wording
  ([`577756b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/577756bf028a4d0105fd7aa4dfc4654700c89a2f))

- Used correct equation and parameters in Two Maxima model docs.
  ([`45758a4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/45758a45f82e12890eda4b972501e4726965dee5))

### Features

- Add --deselect_T option which is --select_T but with all preselected
  ([`0a42a01`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0a42a011c4d4da4b3d588783f98aefda281d4511))

- Add ability to skip fields in ac
  ([`1cc2bc6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1cc2bc6a69c9d20647623d6c22fa22623704c589))

- Add ac_field driving field attr to Experiment
  ([`2842cc7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2842cc77361803efed141f83f4cfe64c34476e87))

- Add check for empty column of squid datafile
  ([`5120d63`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5120d637eb7a31336e892e35a372d6ba6f3341cb))

- Add check for type in dataset.from_files, update docstring
  ([`8921244`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8921244dcd8e08ec950531a304e4f6572a33dd15))

- Add classmethod for Relaxation experiment creation from AC data
  ([`e844d2b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e844d2b469f617cc4e2ee5b6c7dcf35f03ab0262))

- Add cli fit/fix interactivity for relaxation part of ac mode
  ([`ea984a6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ea984a699ebf2b67cf127f0b2c1cc67fa7f46051))

- Add Cole-Davidson model for ac susceptibility
  ([`4dbf47b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4dbf47bbd2d0ced0b8c4c8039f1f9c4762571474))

- Add coloured terminal output and improve dc file handling
  ([`7b5f56a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7b5f56a225a4d85ee06453bfa792397fdfaa2d75))

- Add dc mode link to relaxation fitting
  ([`63a46c3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/63a46c3bb6c753e24cfda792c0c45edeb3ec3be8))

- Add double stretched exponential function model to fit to decay data
  ([`71f2116`](https://gitlab.com/chilton-group/cc-fit2/-/commit/71f211658672900966924d8352e1bd72ff76eeaa))

- Add double stretched exponential function model to fit to decay data
  ([`bfeb6fa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bfeb6fab19159beb0828135c0d7ee10a441ad888))

- Add Experiment.from_file convenience method
  ([`db056f1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/db056f1f11adbac6d33810610d0792704c99a630))

- Add field output to model parameters, add support for <ln(tau)> in Debye
  ([`fa705c4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fa705c4a9747cc980eb09a998884e6c0d541f84e))

- Add fig and ax return to relaxation plot functions
  ([`1538394`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1538394430a336e1e7499431ededb993045117dd))

- Add fontsize environment variable
  ([`cd7ef6b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cd7ef6ba1d8987dc519ce821069e0524e5faf706))

- Add from_dc method to relaxation.experiment
  ([`b5cdc58`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b5cdc58a91ccb7b8e2c9b2c25e052e1fadb30770))

- Add Havriliak-Negami model
  ([`a3e77a8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a3e77a84a2afbefda1e135558ad510e4a821699f))

- Add LogDirectModel
  ([`b7b6064`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b7b6064fe4f61ba81b9792ad038275c096a8f043))

- Add LogQTMModel class
  ([`2833d20`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2833d20785bc8996cc7d515fe94fad2a2d77c3a4))

- Add LogRamanModel
  ([`51485db`](https://gitlab.com/chilton-group/cc-fit2/-/commit/51485db3b50f6f249162d6ae672bdde671ced382))

- Add minor ticks to all plots
  ([`5be2cae`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5be2caea75c5fecab27f699255f4a401c712c854))

- Add minor ticks to single colecole and suscpetibility plots
  ([`cdaf4c1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cdaf4c1f43fffba0d46e91f681a93bb049d1736d))

- Add new dependencies
  ([`f457cc7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f457cc7efae11970e65836381bb346e86beb6f96))

- Add new interactive qt5 tau vs T code
  ([`20adb5b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/20adb5b4e4eedcc94f6fb957f26c8e22d9390f6c))

- Add option to disable weighting of residual in relaxationprofile
  ([`c4520a9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c4520a9025cc81ca6a45687d7e8a9abcd4755669))

- Add option to plot individual cole-cole and susceptibilites for each temperature
  ([`e922eed`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e922eedf5cf899cf88bfdf829ff50b90eb67566e))

- Add option to specify multiple parameter files to relaxation mode
  ([`f1686ac`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f1686acf32a25b887ebed6294895664f3232105e))

- Add option to specify plot font format in cli using env var
  ([`177662b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/177662b79bcc61d8c34fb52cf2403aed68d15d18))

- Add option to specify type of moment to use in DC mode
  ([`1c88ff5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1c88ff5b46a9117eb25e5488d39d851b1b33345b))

- Add output for relaxation model parameters
  ([`213761f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/213761ffde6f48a69c12853e8d32dcd5fd7b9e19))

- Add return of fit and ax to dc plot functions, update documentation
  ([`260792b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/260792bf5c6fc0b7025aeafc8de9f251f35d2ae9))

- Add setter/getter for lograte_pm
  ([`1f0214f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1f0214fb2aced6bf16ceef0723a6df80e8fe929f))

- Add single decay plot method
  ([`8e6f832`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8e6f832c8f8bef8f6b4d380ef7e80dd8c9f1f455))

- Add support for font environment variable in cli
  ([`ffcd7f6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ffcd7f6e1adddedead8fd53ce81c75fef82dd2d1))

- Add support for missing errorbars
  ([`eb546ba`](https://gitlab.com/chilton-group/cc-fit2/-/commit/eb546ba09a85734f5f71d631121199e6a1de03dd))

- Add support for plotting MultiLogModel, add stdev calculation and final parameter assignment
  ([`ef55170`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ef551703f45eb999f06ba9ce7a5b734aeb812041))

- Add support for windows wildcard expansion
  ([`9d83e52`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9d83e52ab35bef23d5df05d2b2945647b6a70250))

- Add t_offset and m_0 as fitable values
  ([`ff42479`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ff42479d5541c9d8d3ab6c155264fa40b11cb615))

- Add tau residuals plot
  ([`cbeda95`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cbeda95d27bbc89cdb4885451b98ea0211b2a43a))

- Add tau vs H fitting
  ([`41a3c74`](https://gitlab.com/chilton-group/cc-fit2/-/commit/41a3c74b62b66123971c8ab6f2f8f946e1ee86f3))

- Add temperature and field print to ac mode verbose output of initial guesses
  ([`6f80c49`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6f80c49a85c8f1bafb2eb76d4ac862b8b82d3428))

- Add textboxes for parameters which are tied to sliders
  ([`56d425e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/56d425e41bfe2306563143aaa450294959b18775))

- Add time sorting to dc measurement
  ([`8c88177`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8c8817769f1115497afd05c09ea8bb938a3846ae))

- Add toggle for models using keyboard number keys
  ([`4df4629`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4df4629f5b9a0771abfd5a8b1e49605d09f109e4))

- Add wip dc model code
  ([`daf0385`](https://gitlab.com/chilton-group/cc-fit2/-/commit/daf038585b735f26d109df24a6716c908c859b95))

- Add wip gui code
  ([`d31aec2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d31aec2c138e55f4d2fe0a38f1c6d7b12ecd452b))

- Added --plot_axes option to define if you want linear or log x and y axes
  ([`5f6f2e4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5f6f2e44dc1a538fb10676bbd4e00af453398609))

- Added --plot_axes option to define if you want linear or log x and y axes
  ([`d81b789`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d81b78968b40e93d43cf3d665c95a29d9bad2820))

- Can calibrate field for a set of DC decay measurements
  ([`5480f1b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5480f1b63690511ffa77b91737993fed5aad68b1))

- Can now choose whether to fix or fit M_0, M_eq and t_offset when fitting decays to the double
  stretched expo model
  ([`30c9d44`](https://gitlab.com/chilton-group/cc-fit2/-/commit/30c9d44c563d18d77b226ed489329aa514748916))

- Can now remove showing fitted params on decay plots, made showing and saving decay plots default
  in line with ac module
  ([`01c9019`](https://gitlab.com/chilton-group/cc-fit2/-/commit/01c9019483730b2c1c0e1640b50c75a8e71b6e61))

- Improve wip dc model code
  ([`8040504`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8040504f44f38ba1385cd559392b71a159e37e11))

- Make bound headers optional in relaxation mode
  ([`b9eac0f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b9eac0fa67cf61ab837204cd192024f2dad209bf))

- Modify negative susceptibility threshold to include errors
  ([`771a4d7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/771a4d77712474508e611b0178e62b8291833dfa))

- Move twomaxima to doublegdebye, change twomaxima to hidden paper def, add more room for ac models
  in interactive select, add support for double tau models in relaxation
  ([`8fcbf23`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8fcbf23a4105975c466ffb3740325522507ac862))

- Much improved dc squid file parsing and measurements->experiments
  ([`41d5f00`](https://gitlab.com/chilton-group/cc-fit2/-/commit/41d5f009c4e552160c5a054aa569ee569c4f1b48))

- New dc cli for new dc code, move ac model verbose option to ac cli
  ([`f4dd78b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f4dd78be9ed27fb42585515e8e6d1448734ec6b5))

- Remove parameters=0 from dc decay plots
  ([`39d7cca`](https://gitlab.com/chilton-group/cc-fit2/-/commit/39d7ccac22f919a251319dd7eb4c333bce57aeb3))

- Switch to new waveform code
  ([`691a440`](https://gitlab.com/chilton-group/cc-fit2/-/commit/691a440d313ed04ddd93e240969ea2e7d096090f))

- Wip MultiLogModel class which combines LogModels
  ([`1ed1aa9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1ed1aa982c57c696c33df240684da8057cfb1e28))

- Working MultiLogModel class with barebone features
  ([`df82fad`](https://gitlab.com/chilton-group/cc-fit2/-/commit/df82fadc0dee69ed0719068da3d690e32f52387e))

### Performance Improvements

- Ac cli now fits current field relaxation data immediately after susceptibility data
  ([`f14c068`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f14c06895842e3a5b6a9f8f8fd89f031179552b1))

- Add background color
  ([`8401e64`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8401e64c276c9f2d16bf948c4b7d67a3f92de3ce))

- Add error checking for colorbar entries
  ([`1ce5b08`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1ce5b083e06e4fbae4dd47c737596ee3bd894f14))

- Add exceptions for colorbar with no temperatures
  ([`1ac9e6e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1ac9e6ed157be7b7bfc7ec80e64607a35e59a35a))

- Add header checking to waveform, improve docstrings
  ([`90d4880`](https://gitlab.com/chilton-group/cc-fit2/-/commit/90d48805f5f3761500709f74c2d1745b2c83664e))

- Add specific parser options for parameters of double and stretched exponential, and check
  exclusion between them based on specified model
  ([`4432e47`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4432e47d645a76546ff468ad255a95c72e97c0fb))

- Add units to relaxation parameter file
  ([`dd6b134`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dd6b134de962520675b38686d4b0515d971883cc))

- Add verbose argument to each print
  ([`357aea1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/357aea1b0f57fa5a78d65dee162dcd37b2a09763))

- Add verbose option to all print statments
  ([`79fded3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/79fded3344ce358b9d665c3c380051315a3cce7c))

- Add verbose print for final ac parameters
  ([`b51bff6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b51bff6e585b91f1ba9a2bc432e1eb84a6746c50))

- Add verbose to all print statements
  ([`fbb4f62`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fbb4f62b43b8d310cb38d3aec16ee5a8b5b90138))

- Change dc decays to loglog
  ([`cded5ac`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cded5acc620a0d7df22e1f5c583cedda23bee161))

- Change filename format for ac mode
  ([`b1262ea`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b1262ea137597358b35bdce8ca743afb8396ce25))

- Change fit and reset button colors
  ([`8c04bc5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8c04bc52033f4a239af9051b47664759aaa0a83a))

- Change font weight on reset and fix buttons
  ([`521bec3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/521bec3089adcbcdaad78ebc3b8e6fe54064ed25))

- Change waveform default file extension for output
  ([`b8189e4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b8189e4256be0f08cde4b119a154d6009dfc249d))

- Colecole and susceptibility show up together in ac cli
  ([`636477b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/636477b28148747a05b65971b10e965e03f66dd5))

- Disable default save for plots, improve docstrings
  ([`4c8c9fc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4c8c9fcf55edcfd35f1ae61c9c9e61ffccd6038d))

- Even better x tick labels on fit window
  ([`ffbd46e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ffbd46e80d7874c59a1fa05810a3760293b1fffd))

- Fix y axis lims in interactive fit window, change raman default params for slider and textbox
  ([`da80d6f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/da80d6f08f3a0b0424847522ddc2964888b6d37b))

- Improve ac filenames for plot save
  ([`8bb7b94`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8bb7b94c5b3811cf08889dc588b65d3cf70b02a3))

- Improve ac model button location
  ([`076a9a1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/076a9a18291815202380a00cf8a65e3a0b8244be))

- Improve dc parameter from_file option
  ([`a30e8a6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a30e8a6ee21b2e1a22f6cb81870df00fd07d2bbe))

- Improve erorr message
  ([`719b6ee`](https://gitlab.com/chilton-group/cc-fit2/-/commit/719b6eea6d85660b260e5b5079ee2bd04ab674c8))

- Improve filename consistency
  ([`0470531`](https://gitlab.com/chilton-group/cc-fit2/-/commit/04705319e4bc15fc3e98a78f87473bffc2593c69))

- Improve fit cutoff
  ([`1e935e4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1e935e4b8fba1330c6ac01cb50e2e1ad0aa1e217))

- Improve fit methodology, clearer variable names, improve plots
  ([`4ee3504`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4ee3504688cf6aa9432237c0b966780297664094))

- Improve fitted decay plot save/show
  ([`3e0890e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3e0890eca54d43cc70dcab8b7b60082d7ab94544))

- Improve interactive fit window and qt5 plotting
  ([`e34b78d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e34b78d5674b09478fa0a52a8d01b69953c85b11))

- Improve line styles in interactive fit window and final fit window
  ([`8e8e211`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8e8e211cf7ca7e053fc9cfa7005f81e1b1a32d9d))

- Improve param and model filename consistency
  ([`3563bee`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3563beeade37d5dad2678c646e3ed50e8e14b873))

- Improve relaxation fit plotting appearance and code, catch user quit of fitting window
  ([`3c790e8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3c790e834b6286d3f8e1636da15803b19ed44167))

- Improve relaxation rate plot output
  ([`b75ecbc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b75ecbcd81a3916e5e37f58544aa70e2f020b408))

- Improve resolution in final fitted tau plot
  ([`a7bf8db`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a7bf8dbfddafad42cb60779b796648f63ed294f3))

- Improve single plot mode
  ([`6d6f81e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6d6f81e61ba183d47b59a7b25b10d06c8cdd3eb2))

- Improve susceptibility x ticks
  ([`487f639`](https://gitlab.com/chilton-group/cc-fit2/-/commit/487f639e64491e8a98c783178725db300c3e3220))

- Improve tau vs H plotting
  ([`5d4666f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5d4666f2c3310d3312f1967fbcc2473db24c6aa0))

- Improve tau vs t/h x axis ticks
  ([`a01805b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a01805ba955e8fe008e94aef0d8a50f135c3488d))

- Improve terminal colors
  ([`1015649`](https://gitlab.com/chilton-group/cc-fit2/-/commit/10156499669c54e4ba2f41e5468e9687752afff6))

- Improve terminal output
  ([`930a15c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/930a15c709593abb1c6a67697b925f1140bcd7da))

- Improve warning for two_maxima model
  ([`7e35eaa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7e35eaa4bd8392f9dbe027835240ede5e4399ba6))

- Improve x tick behaviour in fitted tau plots
  ([`fb006b5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fb006b5ac5d2a99670e38a14e52043ce9046c50b))

- Improve x ticks on interactive fit window
  ([`2820ec6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2820ec68b041d3dc67775060d094201ed281c4b9))

- Match minor and major tick lengths on rates plot when minor labels shown
  ([`dc36940`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dc369407b4ab0634b73788a8173e6ad9cdfdd2e1))

- Minor changes to help formatting
  ([`0fa5cba`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0fa5cba1b53bf59d048ee56945f6aac609a9b9da))

- Modify save parameter of plot_relaxation to default False
  ([`d7d59e9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d7d59e9231c84a9877beffeda5e599c91bcaa625))

- More tweaks to x tick labels in fitted_tau plots
  ([`5643b30`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5643b309e9a4da113c7f875009a1568186d4d901))

- Move all plot functions to gui
  ([`c21254f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c21254fe8df161d59098fa9cc0172e0849517169))

- Move close for single_plots
  ([`8d7e6fd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8d7e6fdb5607ab1d049f877f068a5096c374cb45))

- Redefine fit uncertainty in tau as arising from uncertainty in sigma(ln(tau)) expression due to
  fitted parameters and fit routine
  ([`4be1e5a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4be1e5adb513943b0bce033ae198a7357b24ca9d))

- Remove arrows in mass/mw popup textentry
  ([`69e8aac`](https://gitlab.com/chilton-group/cc-fit2/-/commit/69e8aacea8e6f0fbe2dd9a80535bf2ed94fada64))

- Remove colors for windows users, add envvar to reenable
  ([`3e7cdc0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3e7cdc00fccc56c2dbb1a3b148399110ea0a4c26))

- Remove qt plots from non-interactive window, fix disappearing plot bug for good. Add keyboard
  shortcuts to fit window
  ([`d26cb5a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d26cb5a58eee62b34cae8c33e0bf9759bc92ee11))

- Show susceptibility and colecole together
  ([`8aa4c38`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8aa4c383eb712b31e5e72644f3e67f514f79f474))

- Tweak decimal places in window title
  ([`8f8e80d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8f8e80d6a7847591fdd57d0f4bcae88e55e4d050))

- Tweak plot ticks for rate plots
  ([`68f1752`](https://gitlab.com/chilton-group/cc-fit2/-/commit/68f1752939e8971a687e3f48fe2c82bfd0d93187))

- Tweak rate cutoff in model plot
  ([`8bf7478`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8bf74782c658048e29cc8e819fb928d0e2d0b156))

- Update default decay plot filenames
  ([`40389d3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/40389d32d9cdaf5e9f751571fbf6aed6b8c95dbd))

- Use SVD for all standard deviation calculations, add stats module containing svd code
  ([`59520a1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/59520a1e79f0964d5c77c9122a08644e4a4017ac))

### Refactoring

- Add abstract classes for HTML units and varnames
  ([`005ea38`](https://gitlab.com/chilton-group/cc-fit2/-/commit/005ea38ddbfb32b8eca7f746c8deb1ffaf83aace))

- Add data_header and header name checks to dc mode
  ([`6ab1cdc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6ab1cdcda162ece268529b589af32b202fecd2d2))

- Add expectation value to new model classes, add fit/fix to abstract base class
  ([`3be02a9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3be02a91425e23c1b9dc49fa0bc39258e13a74f3))

- Add instantiation to from_experiment method of model
  ([`fc22364`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fc22364fe96bc845021649dff31406bbf2c953e6))

- Add ln(tau) expectation and std dev for tau1, tau2 in TwoMaximaModel
  ([`4f5b617`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4f5b6179dfde5fec836053038fffb70d26c4c404))

- Add new method for writing model decay data
  ([`f0e23bd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f0e23bddcdf570014e1edeea59cd1a0817eba169))

- Add new Model ABC, and WIP OrbachModel
  ([`37aece6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/37aece6a63363f71178a4277abd1e9af9a269763))

- Add optional argument for Brons-Van-Vleck term
  ([`ab90d71`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ab90d714535a6994cb9a5b055b19b8d598847a27))

- Add optional argument for Brons-Van-Vleck term
  ([`d603399`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d603399efa008d1a52da25e0ee40f4798249060a))

- Add setters and getters for Model properties, simplify Exponential __init__, rename fit and fit to
  _vars, improve fitted decay plot function
  ([`6fa3927`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6fa39272e29531382172681a4bb32ab266807d8b))

- Add wip test and boltzmann value
  ([`740e75f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/740e75f5dbb0bbe9924f2df19bff1913b09d2c52))

- Alter bounds for havriliak negami
  ([`0156527`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0156527e3ab9428f36d7692fe5e4926e5fb23571))

- Alter calls to ac experiment
  ([`142441e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/142441ec501518acdfc029352408373f1fb1c270))

- Alter name of tau data write function, improve relaxation docs
  ([`cd0791b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cd0791bb51653c03cebdfdfd75c56fd804908915))

- Change colecole window title text
  ([`be6ae3d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/be6ae3d6f3f298afcf99fd83a5abf5449c1f19fa))

- Change dc options wording
  ([`3a03257`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3a032570d2c914bab786c60be9cb7eb3b09daca2))

- Change defaults for some dc decay args
  ([`258837c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/258837ce841b2544391d6388ae669d4058de8b64))

- Change exponential model equation
  ([`3065dd7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3065dd709f51862c9cf467506824e53971c1a07e))

- Change fitting routine name
  ([`a797846`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a79784645d40e0028dc146a9ad818367812f0b12))

- Change headers in parameter file
  ([`b11bc8e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b11bc8eed966d6a78ab5040f23033b15a78820b8))

- Change in_field to no_field_discard
  ([`72a299c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/72a299ccae38d318bbe33ad1081bed3d23e6578d))

- Change mean_temperature to rep_temperature, matching AC, and add setter/getter. Add 'guess' for
  t_offset
  ([`190e067`](https://gitlab.com/chilton-group/cc-fit2/-/commit/190e067b3d3e82dc16b05fd94cf57ed9a0cd104d))

- Change minimisation objective function
  ([`c6fb279`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c6fb279ce689db225f5269f5cd678f6d2ce9b2ea))

- Change name of DoubleGDebyeEqualRatio
  ([`48b5dd5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/48b5dd55a0cab29207bc78acebceda14a1976d8e))

- Change references to DC Exponential
  ([`d19690f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d19690fb642f27ff291a3042ff54656c4bb49b04))

- Change sus_<> to <>_sus
  ([`ab3c532`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ab3c532fd08093857f778759caaa7cc301d2907e))

- Complete refactor of waveform module
  ([`82b8172`](https://gitlab.com/chilton-group/cc-fit2/-/commit/82b81724d9aa04e6b6d346fb2c295dab0c22ca3b))

- Condense Model init, add set_initial_vals as abstract method, update docstrings
  ([`02b0626`](https://gitlab.com/chilton-group/cc-fit2/-/commit/02b0626f51683b64051498e75cd9342e37ede903))

- Decouple HEADERS_SUPPORTED key order from Measurement constructor in from_files
  ([`9adb69c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9adb69c8835155edf551af986a9cf28bf3dabc05))

- Fix spelling mistake
  ([`089c856`](https://gitlab.com/chilton-group/cc-fit2/-/commit/089c856934386b34219f523fc6ae3e3308552f89))

- Implement new LogTauHModel and TauHDataset classes, remove old code
  ([`13aca67`](https://gitlab.com/chilton-group/cc-fit2/-/commit/13aca67909e3f06b090c8520ebd8be3f77a3848c))

- Improve ac dc relaxation interface, add uncertainties to plot, and propagate through fitting
  ([`41410ec`](https://gitlab.com/chilton-group/cc-fit2/-/commit/41410ec85a93b102d7ef7f6631c7a33a832f6e85))

- Improve dc measurment.from_file method
  ([`1b7d8c7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1b7d8c7d1cd9fa159cac090323326cc9d82cc96a))

- Improve model decay output file
  ([`7da02e6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7da02e67d7bee184a741f5a75b8bb6300ac8c4b7))

- Improve name of residual_from_float_list, add docstring
  ([`2100ef7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2100ef71abedcb80b2660890bd9d5f8107ee20e0))

- Improve naming of LogOrbachModel
  ([`d74fba8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d74fba8770665306e4c7893db99aedff6d934e60))

- Improve parameter specification for dc
  ([`c1f7d1e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c1f7d1e700463af9ca2ec047c74908d4675279cb))

- Improve pep8 compliance
  ([`1fba71b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1fba71ba738edc943c2233e5edff0ad68a7bcc8b))

- Improve waveform measurements class
  ([`63b1c84`](https://gitlab.com/chilton-group/cc-fit2/-/commit/63b1c848ce673f95bef4d1486106ea4672031892))

- Introduce LogOrbachModel class
  ([`a3d2aad`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a3d2aad6b50fd0bb4cd90aa89f0f0f411c18b410))

- Limit decimal points in ac plot filenames
  ([`41ec20d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/41ec20d5a0e169bc1c4729f72b5d45cbd800d03c))

- Minor changes
  ([`6ef1097`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6ef1097dfd7d84132bf7809611c8fdf445338953))

- Minor changes
  ([`a12afd1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a12afd13c67ee4ec21ccd5fe1e5054d68be6b0cc))

- Modify residuals in LogModel
  ([`de240f9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/de240f935e259e93f0be9c0ed3890b957958fdb2))

- Move colecole selector out of from_experiments, and out of Model ABC
  ([`3758f9e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3758f9efd719e5c15ddcbf5b609e368b75180d6c))

- Move dc decay plot out of cli
  ([`0c7c9a3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0c7c9a3cca1483a00413882fdd2e61a7ce9b0c0d))

- Move duplicated flat() code to Model
  ([`174942a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/174942aaca13136d137684ff9533cd1a3d98cad5))

- Move experiment.temperature to experiment.rep_temperature as single value, matching AC, add
  experiment docstrings
  ([`aab57c0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/aab57c0e790e7d8e7d5311222c59fb9e464f805a))

- Move find_mean_values to utils, add temperature threshold to dc experiment generation
  ([`75c35c0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/75c35c0d3b058f4e8f2458ebebbfaf2a79c1f5c9))

- Move glob out of ProcessWaveform, add Measurement class
  ([`6a2db16`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6a2db16f2dd076699e0d19de18217a3d2d7337a9))

- Move header lookup to utils, simplify defaults of measurement.from_file
  ([`6dbffba`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6dbffbaa25453a915c722fd9e0f84d21bc707c91))

- Move initial values to object method
  ([`034cb26`](https://gitlab.com/chilton-group/cc-fit2/-/commit/034cb26f1899789fceceeffd8b0a13292d2496f7))

- Move lntau expectation and std deviation to Model ABC from Exponential
  ([`906ab0a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/906ab0a30ef9b70fba377f7bc210a7ed9fa43775))

- Move model data output out of Model
  ([`5780945`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5780945e35da0b5049372178cda9ff038861657f))

- Move model param file creation to standalone function
  ([`6446ba9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6446ba948d213989dda0ee9647b0d6181ca88375))

- Move phase to function
  ([`1a3524b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1a3524b2774c5b90e4b7420c30e9cfa66fce1fd7))

- Move plotting out of classes
  ([`8ee759e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8ee759ec0837892cf37e7b9b70f9070a10acdafe))

- Pass expectation value of tau from dc.py to relaxation.py for --model stretched
  ([`4d000f7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4d000f772118b5a3cd8600d26d2d50c39792c6fa))

- Redefine fit uncertainty in tau for tau vs H
  ([`2d25c77`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2d25c77d7e95198cfed27e1e6468e55ab7e00eff))

- Redefine fit uncertainty in tau for tau vs H
  ([`908686b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/908686b23dc5989526d27bcd677833180ad344e1))

- Reenable fitted cole cole and susceptibility plots in ac cli
  ([`6f96619`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6f96619d01e073f8557c6400a0939321d5daef72))

- Refactor AC models to match DC model, allows fit and fix of any parameter in model
  ([`451e84b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/451e84b794029ddf9e9dd072d362861b02cee147))

- Reimplement old waveform data split method, add docstrings
  ([`141fad2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/141fad271bbb07cf6b763a20721ff8665c79375c))

- Remove alpha from debyemodel guess method
  ([`f58970d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f58970d5ac607aafa46d2083152d2e49deb78294))

- Remove checkbuttons and replace with toggleable radiobuttons
  ([`e995077`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e995077c04549755e91b9b417ee6cfcea38a22e5))

- Remove custom action in dc cli parser and use choices
  ([`bee01bf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bee01bf2e7fe0dc98d2e9756b579035576d9b9d5))

- Remove frame on fix selector
  ([`46111d4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/46111d403f9698c4c23b5cf3dc500c6102521b0c))

- Remove large_tau_fit_uncert
  ([`21c6c2f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/21c6c2f417f55d5c16e2c573a2c32cc44ed06454))

- Remove old dc code
  ([`4e5be9d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4e5be9dcea376ff8615b7ce6775b386629212a9b))

- Remove redundant code
  ([`e384d1f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e384d1fd6ff824d4886ab9e5b9730a9c9f96ddfb))

- Remove redundant Dataset class, remove write_params method from Model classes, update docstrings,
  rename mean_temperature and mean_dc_field to more meaningful names
  ([`5db0745`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5db07453de82a5c8b3f67bddaf3374bad019ad0b))

- Remove redundant FITHEADERS
  ([`0d100dc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0d100dc87b8c690e2fc6bc8585e7e53bfb5a2071))

- Remove redundant function for bounds, add comments
  ([`10934cc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/10934cc70612fec3c63cef418147915facd02512))

- Remove redundant model.from_experiments
  ([`0e1be33`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0e1be33a2395f8d3bf0bb225979af73ab4817fdf))

- Remove test code
  ([`011333b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/011333b77ed16f83dd7a5279c81eed2d759df052))

- Remove type hinting on derived class constant definitions
  ([`636e8ed`](https://gitlab.com/chilton-group/cc-fit2/-/commit/636e8edff9282bacc01aab6800d3689fe6bd5406))

- Remove verbose option from models, remove redundant init from DebyeModel and GeneralisedDebyeModel
  ([`64094b8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/64094b886c693b856d2c0736ac0774907fc4b16e))

- Rename axis utility functions, add docstrings, simplify code overall
  ([`37e5512`](https://gitlab.com/chilton-group/cc-fit2/-/commit/37e55128b6b77cda1a8ddcf21ff73903ff664008))

- Rename dc model
  ([`bfa799c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bfa799c5cba55d5969530a42f64dea89478b84c5))

- Rename DoubleGDebyeEqualRatio, add DISP_NAME const attr to all ac/dc models
  ([`e3f470d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e3f470dc41f3e3f4b6b129eadecc6814c73b110f))

- Rename Exponential to ExponentialModel
  ([`e1819da`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e1819da689eeb1ff9e572a9410b7350bb5e7d2c7))

- Rename Exponential to ExponentialModel, consistent with AC
  ([`b7683ff`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b7683ffe7e38f891a423fa5e0446f58468cd3ae0))

- Rename fit_temperature --> temperature
  ([`6e109d5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6e109d5be7c77e53aad1aad9ac0bcece20ba9d12))

- Rename fitted plot function
  ([`a5aeea9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a5aeea9e0b810f21fad4c14f77675953cbba0d0a))

- Rename in_field to no_discard
  ([`28bd7da`](https://gitlab.com/chilton-group/cc-fit2/-/commit/28bd7daf04a1977806da01fe8bd709f8b748e943))

- Rename tau to tau*, change tau expectation to ln tau expectation, update plot_fitted_decay with
  parameters
  ([`be7a8b6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/be7a8b631218dd1e4d63a02818a20acd04c5f164))

- Rename test folder
  ([`5eaa490`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5eaa490c30e2106e698219db018a44f23d12b4f1))

- Rename TwoMaxima
  ([`9876a98`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9876a989fa52dc9c649429856156c1756e77f04a))

- Renamed model parameter print function
  ([`071a7d7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/071a7d7322dea210527f25643a11bdc55a4f54b9))

- Reorder methods
  ([`39abd6d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/39abd6deac519eae6c5f19e5d695ad7f0752bc26))

- Replace custom button code
  ([`7f047cc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7f047cc41ddccdb9dadfd5cdc678448cce0e8a24))

- Replace deprecated matplotlib functions
  ([`525cd22`](https://gitlab.com/chilton-group/cc-fit2/-/commit/525cd2212487e8fee93b30e412bf17a68674a144))

- Roll back to old correlation matrix calculation
  ([`7bff3bc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7bff3bc61b9de7d9578ea181a0c2d8b74dd0d35a))

- Simpler variable names
  ([`555504e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/555504ea83ecb116cd02f67b7c76377dfefd6629))

- Simplify AC model selection plot
  ([`770d21a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/770d21ac4114d4dc05b37f7d8e491ba2f3176e89))

- Simplify Debye and GeneralisedDebye model equations
  ([`001a0eb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/001a0ebf37ae94bead5b4d1d8e4131edcec800cf))

- Simplify fit and fix variables
  ([`0493045`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0493045c20b99faa4a4e29ca335a50ff7252a2aa))

- Temperature-->temperatures in model
  ([`ebc29ab`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ebc29abae1a86f175c5cc3b2bf893d3756a8f2c3))

- Tighter susceptibilities plot
  ([`94b36c9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/94b36c95ce68d0f458b0eca4a63e35941adb9538))

- Tighter susceptibilities plot
  ([`f5396c8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f5396c818bd797996adec692fe1261261592cfca))

- Trim decimal places in plot filenames
  ([`f8aa87b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f8aa87b9ecaeefc33da1264e962d73740dd4f964))

- Trim decimal places in plot filenames
  ([`ce276b6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ce276b61d2839536e76f56badcc6fd66e8f5efca))

- Trim decimal places in waveform plot filenames
  ([`8403e48`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8403e48c49b671dc112f989560dc9327a3d37f7f))

- Trim decimal places in waveform plot filenames
  ([`9202b9b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9202b9b463712c39ef841eafd250e0acb19a53d5))

- Tweak dc experiment sorting
  ([`f0b946f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f0b946f886b0b160f248593ba9329fca1023baed))

- Update reference to rep_temperature
  ([`9dc84f7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9dc84f7f2a7d3043abc766c7384a43f8ad5f322d))

- Update relaxation to work with new ac module
  ([`7f9e921`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7f9e921cb2a3e80765669b0482bddfdedd183a48))

### Testing

- Add .ac.dat test file that contains AC susceptibility data that is best fitted to a
  Havriliak-Negami model
  ([`f6e8ca3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f6e8ca3a260cd6f9bc11075f3f5c67023f9fb88c))

- Add AC susceptibility data file with 2 different driving amplitudes
  ([`072532c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/072532c3cb145170515116484582fd677b9618f1))

- Add autocutoff for changing field and cutoff for 0.01*initial mag
  ([`5b13bd2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5b13bd284ae1d2cb2c4b5901e88c647d9eff09d6))

- Add comments
  ([`b2215f4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b2215f456591d78f88d9990fbc4aee111856b25b))

- Add comments
  ([`a27deb9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a27deb93ad135a23ba511528b3846e81da27e1a8))

- Add comments
  ([`f09aa83`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f09aa83cf19381d53b9c652b949f0b1b5d6a8625))

- Add comments
  ([`cc686a2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cc686a28fc3215e92e47aab407337667369efe8b))

- Add comments
  ([`b47a1a4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b47a1a408926283b897f9d730db02156377d1332))

- Add dc-->relaxationr test code
  ([`3208550`](https://gitlab.com/chilton-group/cc-fit2/-/commit/320855006c6bafb8a83245736009b75fa9e0334b))

- Add interactive relaxation example
  ([`9da54cb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9da54cb024c234b4bcf77327ab820fc957c6a46d))

- Add model decay output print
  ([`d2a67b6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d2a67b668a815abb0902f0b7405d01e8fa22cc5c))

- Add model parameter file creation (output)
  ([`2857edc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2857edcd1ae9762e5996a16de8fa7d8a62017827))

- Add new dc test code
  ([`04dd292`](https://gitlab.com/chilton-group/cc-fit2/-/commit/04dd2926808977826168b7ad7510be27a7c31d16))

- Add noise to test of relaxation mode
  ([`54d412e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/54d412e2766e7c83b9295ab53a44ea013c4530ea))

- Add notes
  ([`c1411a9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c1411a9d9d0a6231aa3562a5eaeca73ce2e359f5))

- Add parameter output for ac test
  ([`f292a20`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f292a208eefa6a67203c4584e2d0005c94a14640))

- Add single model test
  ([`fd131aa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fd131aade5dd62f63b06d6460cc178faadb8bdbd))

- Add test code for AC--> Relaxation
  ([`caf8d3f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/caf8d3f862d88cccf2592533a8e01f29de6f8b39))

- Add test data file for relaxation module
  ([`5018cf7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5018cf7848f6a9424c0149943caec24b1888c4d9))

- Add test file
  ([`7857608`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7857608dcc78303809673633a3930d1053f4f719))

- Add test for relaxation mode
  ([`a8c2fcb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a8c2fcbeddb8e0a8537b291c2792da8f2368482f))

- Calibrate theoretically calculated magnetisation curves using saturation magnetisation to obtain
  M_eq at different fields
  ([`fc082ba`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fc082baeb67b2b2614530fbb7c45a5a78311f52c))

- Change relaxation test to LogOrbachModel
  ([`a06cd1b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a06cd1bbc8a8dea7b6e0167f54d3fef397b414c1))

- Fix bug in ac test file
  ([`fe134b0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fe134b095c7c2d25c8cac60aa5fbe03e994a6287))

- Fix dc test plot
  ([`b0326a4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b0326a43e77a54060edee2fc99550a4b31becd56))

- Fix model parameters file name in ac test
  ([`a95c093`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a95c0930f2dca32f3a9b3fcf33d7e7e7f936ab45))

- Move example files
  ([`4d37b56`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4d37b56b23e270ce46f07b0cf73b09049dcab819))

- Move test file
  ([`01cc1fb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/01cc1fb39a50f980e11fb1c949342a7f66eb23bc))

- Move test file
  ([`f38f091`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f38f091e10d8a74467d3e927d8db9f6dd2ced287))

- Move waveform test files
  ([`02a990d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/02a990d89e97c0a597fee0a970a2690734173bac))

- Refactor test code
  ([`77f7cd5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/77f7cd597bf9db4b8843d96eb904544347a194f6))

- Remove ac relaxation test
  ([`08ce865`](https://gitlab.com/chilton-group/cc-fit2/-/commit/08ce865fc2e710680a57b040008d8c18d79bd17a))

- Remove fit feed forward
  ([`e4b76f5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e4b76f5133e1544b1fc1ffe260ec66fab0fb1334))

- Remove fit feed forward
  ([`e2161e1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e2161e1d31703ad1ecb596ad7054800ef4ef44c0))

- Remove from_experiments call in dc mode test code
  ([`897d246`](https://gitlab.com/chilton-group/cc-fit2/-/commit/897d246ed7a7b8d0a4aa165f8a5d7d8400e8d68d))

- Remove incorrect test files
  ([`eaa883b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/eaa883b1717ec78d0c3ca117ef2153ffa001ed77))

- Remove old dc test
  ([`1db1ce7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1db1ce76eb9b70d45701b3f191fd5f7c5f6064a6))

- Remove test files
  ([`e71d1a8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e71d1a8e4000b2fb7ff3a97ce2465ed02283a846))

- Rename ac test files
  ([`4659eb3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4659eb3171d94a9ccbbc3c01c0e897382d577ba5))

- Rename test files
  ([`4c35338`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4c35338c4e8e018ae0424dd01873ed97dae4e274))

- Simplify ac example
  ([`cebc9f8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cebc9f886197ced8e53b3bb0f5974b06044f50bc))

- Test of MultiLogModel class
  ([`53f2c6b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/53f2c6b1f43401c4e32c43988af978390c067ab5))

- Update ac test code
  ([`ce0a204`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ce0a2045fe695c33e237424b94c3673edec49c95))

- Update ac test code
  ([`195581f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/195581fca5bd2ef033068d4f7b473107b35caa25))

- Update call to write_model_data
  ([`9a28f48`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9a28f484c84c642e89ef089728f44659a60d3cc6))

- Update comments
  ([`2c4a7e2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2c4a7e2a574f3f1849d5fddf8b7dfca01925c2ef))

- Update dc test code
  ([`1359caa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1359caa06b371344ee2e3cfafd081a5a5a5f5318))

- Update dc test for new dc attributes
  ([`fbde119`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fbde119c7457724aea2f384de0e715dd9c09459e))

- Update DC test script
  ([`e521931`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e521931ea8894f7935dd4abc2dc0bd7842031b37))

- Update filename
  ([`bf374d8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bf374d85b8fe913627c97524bd0001557ed0c5c4))

- Update fit and fix
  ([`cd28970`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cd28970e9d683e33d33399b7c9d4ac26bf5616c9))

- Update fit routine name
  ([`43e90bb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/43e90bb5b09e63c0520a9814ce7676dade18502d))

- Update for new TwoMaximaModel code
  ([`0f3dd4a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0f3dd4a886bf344e7eba2123781bced7505f9c77))

- Update guess beta
  ([`dc2df5e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dc2df5e7603085ecfec4aa597c7acd0feb695bdb))

- Update ref to experiment.rep_temperature
  ([`0fa4004`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0fa4004d955d91f2ed576a92ba88eeb583de4710))

- Update relaxation test file
  ([`4fa6134`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4fa6134fa306f7f196bd50d42b349056fd66130b))

- Update relaxation test names
  ([`7f0a562`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7f0a56285924b65536ee000add6751572161d97d))

- Update relaxation tests
  ([`4d31b37`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4d31b37f6ec4d7f0478cb1b5aaa43fcd021c35bd))

- Update test code
  ([`47e3a74`](https://gitlab.com/chilton-group/cc-fit2/-/commit/47e3a74ad8e2862e4fb9cb5ae4a34a216de0f321))

- Update test code
  ([`458ec8b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/458ec8b7de1cc2ffacd816f8373a66413c98987f))

- Update test code
  ([`8b08d56`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8b08d56c37e1155cd51d7be4f4a129318dbd87b4))

- Update test code
  ([`92aa8fc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/92aa8fcb2393f234c58945f37190ab35e6b1501e))

- Update test file
  ([`9d83be4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9d83be42d9094edd750fba743ab37b37965762ff))

- Update test for new MultiLogModel support
  ([`e9ea4bd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e9ea4bd082f96fccf48d6fe7455a6fd935ed0399))

- Update test script
  ([`9cc28f5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9cc28f522a03eee19ff83f4811a81e2b4bd84f4c))

- Update tests
  ([`0692ff7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0692ff7d821abada12b6ac6c294f1397c268f5d4))

- Update tests
  ([`98ec098`](https://gitlab.com/chilton-group/cc-fit2/-/commit/98ec098412b543eeae339f5240dba59fc2df824a))

- Update time trim
  ([`f9128eb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f9128ebf9778086487950c4a9e3aff84a3bb8382))

- Wip testing of MultiLogModel class
  ([`1a59a21`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1a59a21666319e29e297646407c2ec72d8a44921))


## v4.4.2 (2023-02-28)

### Bug Fixes

- Changed Generalised Debye model ESDs to those calculated by zorn from logarithmic moments
  ([`78000c6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/78000c647a0f3a4ad8156e055a0a06da055c9049))

- Fix bug with --M_eq multiple option
  ([`9aaf7bd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9aaf7bd87ae99a82cba5b59cbcd3adb36b8d9e09))

- Fix johnston ESDs formulae
  ([`9c8e910`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9c8e9106c0a092d6593438c0655fc1211ac0198a))

- Updated stretched exponential function ESDs to those calculated by Zorn from logarithmic moments.
  ([`b6b8fbc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b6b8fbc419424a8589c15b1bf2973f88da89d115))

### Refactoring

- Add expectation values and ESDs for streteched exponential function. added ln tau and error in ln
  tau in stretched_MeqX_params.out file
  ([`6651f0d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6651f0dbc21a0e36e8a1035dc2f330be9b59aa8f))

- Add headers for stretched expo fit params.out file, add exp(<ln tau>) value to individual decay
  plots, simplify decay plots filenames
  ([`03f1322`](https://gitlab.com/chilton-group/cc-fit2/-/commit/03f1322d87fd66fdb010f8ae6beee456bad64602))

- Improve pep8 compliance
  ([`b97dfa7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b97dfa799adb6c5e5dbd3ab945c07c01b12346aa))

- Improve pep8 compliance
  ([`a708398`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a708398ece6e7c1cd5445fe0250bcb3730d50c62))

- Improve pep8 compliance
  ([`4310759`](https://gitlab.com/chilton-group/cc-fit2/-/commit/43107591a3ae7532816ad348a1c8a091234411f1))

- Improve pep8 compliance
  ([`c22c0b9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c22c0b915361785d8f4a311693f0ddbbc477eb12))

- Remove redundant hard coded headers generic list
  ([`483e905`](https://gitlab.com/chilton-group/cc-fit2/-/commit/483e90531ccbb119b0d7b7c2f4c3a86254a06795))

- Removes the need for initial flat fields
  ([`14d866d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/14d866d9b8c3daca3f57964d2b11b5a8b86e0e31))

### Testing

- Added additional ac test case
  ([`c1e03dc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c1e03dc029deee8166ea0c76b5182d7c03778706))

- Added additional dc test cases
  ([`07df74c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/07df74c245f9dba66241e576cf897ba25b6251b3))

- Added waveform template sequence
  ([`953943c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/953943c7f3271e21ba05254c8be27cb9243a595c))

- Remove random files in dc test cases folder
  ([`cf32252`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cf32252eaa8bc39239db4538e6701f885e90355f))

- Updated test cases for waveform
  ([`f3032ff`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f3032ffed576e68ca6a90e21324bfd6dd670f2a3))


## v4.4.1 (2023-02-16)

### Bug Fixes

- Updated Generalised Debye model ESDs to those calculated by Zorn from logarithmic moments
  ([`65092f7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/65092f737d744a009751a0830ff99795c2f8e006))

### Documentation

- Add new link, fix table
  ([`af54b9e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/af54b9e000007a7ba9c0c211e0116a909c3a880d))


## v4.4.0 (2023-02-16)

### Bug Fixes

- Remove superfluous ticks on select_T plots
  ([`c89f674`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c89f67463fe5feddb114ba2e1959204de06ffbc6))

- Tweak max number of columns
  ([`a68ecb3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a68ecb3a98e0dd12cec759e11264c7793ba35ade))

### Documentation

- Update field_thresh default
  ([`c0fc0a6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c0fc0a6e8486bf391434bb54bea3f2818998b833))

### Features

- Add better select_T plotting and small temperature set test file
  ([`0eb8b1f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0eb8b1fbdb4a6a08fca5dfe6cb271002538e9cf5))


## v4.3.0 (2023-02-14)

### Bug Fixes

- Add final temperature sort of experiments with equal field in from_Measurement
  ([`f407366`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f4073662cd17ac55abe4461e808c4ca0d69a4038))

- Add indexing of HEADERS_SUPPORTED and remove integer indexing of dict
  ([`963f880`](https://gitlab.com/chilton-group/cc-fit2/-/commit/963f880cb001381fe37a0868cfa08427ca22334f))

- Field_threshold set to 1 Oe
  ([`85634e2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/85634e2f56775a9e4d2562ca0b96d36445567b6c))

- Optional field_round passsed to Measurement()
  ([`ac26275`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ac26275b0ce2c4ebeba8f5514c9542299a341f95))

- Set verbose = False in from_experiment()
  ([`ed23db4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ed23db41f3a0779dc1a124c24f43ed70e7b3fc23))

- Verbose
  ([`ae29a52`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ae29a52f4b2f297d2775bc7ee66b0461d92c4f82))

### Documentation

- Add clearer error message
  ([`3a397fd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3a397fd83a6638d3090111fe03a96d577efedaee))

- Add comments
  ([`64bfdbd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/64bfdbd8b4eee7be08a42fbbd147f39733c9f8f6))

- Remove round_field attribute from docstring
  ([`cd90475`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cd9047542a9c5bb948d400df965903249a44aaff))

### Features

- Option to round field values added to cli
  ([`9b50eb7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9b50eb7c9e42052abc4dc287fab2dbd07441f781))

### Refactoring

- Add mean_dc_field attribute using setter/getter, remove rounding, fix error in parsing similar
  fields
  ([`bf9f8be`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bf9f8be5d8a6b94463f2a3cac62739de5707abd8))

- Make optional argument optional, remove extraneous float
  ([`64e094f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/64e094fe0227a3d01e5779ce700d4b2056db5c6e))

- Remove old prints
  ([`92351af`](https://gitlab.com/chilton-group/cc-fit2/-/commit/92351afcbba143ba88b0db47e1c139c381aa6638))

- Remove unncessary sorting
  ([`1a1c27d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1a1c27d0de61c733afda7fe965845ae0f820dba5))


## v4.2.13 (2023-02-14)

### Bug Fixes

- Linear frequency shown in --process plot
  ([`d4f7238`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d4f7238f941a01d3063c932f4962a10d0f9c1527))


## v4.2.12 (2023-02-10)

### Bug Fixes

- Reverse to linear freqs in plot and file
  ([`8f52110`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8f52110a706b4f7e24bd2fe8030162446b78eb07))


## v4.2.11 (2022-11-20)

### Bug Fixes

- Susceptibilities relabelled in HEADER_GENERIC
  ([`f7a1dab`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f7a1dabdcf78e885d6f5cbbde20897d2d2939247))


## v4.2.10 (2022-11-17)

### Bug Fixes

- Add catch for unknown arguments
  ([`21cfde9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/21cfde9e82b02f119c2536512016bb5381f21c27))

- Add ccfit2_exe hidden argument
  ([`abde526`](https://gitlab.com/chilton-group/cc-fit2/-/commit/abde526c7bc086145998c34418cb3ce6db1c9934))

- Data_pointer optional value is supported
  ([`b917eeb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b917eebdd2e01a6e3aaba8f7c72c639ec11d1eaa))

- Remove comma
  ([`93af2e7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/93af2e7487b31762538c7080928589b1aa425ee8))

### Build System

- Add test for missing data header
  ([`8eb0969`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8eb096925e8d8b9614585aee068a098f4cf6111c))

### Refactoring

- Change comments and remove sys exit
  ([`05aab86`](https://gitlab.com/chilton-group/cc-fit2/-/commit/05aab86211b6f9f826318041a36b0246428e87dc))

- Improve error message
  ([`1f827ef`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1f827ef5bcb3a0ec3a9cbd45fa94ec9ec6fa7438))

- Move error message out of backend function and into interface
  ([`9ab36bb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9ab36bb3dd981bcb768464c64da3b05eab6dbff9))

- Simplify defaults
  ([`1d2f29e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1d2f29ee4dc829eee177ea61ba2b011409c15894))


## v4.2.9 (2022-11-08)

### Bug Fixes

- Ac exits properly if --data_pointer absent
  ([`8adba33`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8adba332c11a691037b1a2c0187effb11ef35af8))

### Documentation

- Correct user instructions
  ([`91c5141`](https://gitlab.com/chilton-group/cc-fit2/-/commit/91c5141066786bf833f3e159d27f0ba980bc0061))


## v4.2.8 (2022-11-08)


## v4.2.7 (2022-11-08)

### Bug Fixes

- Correct file names for multiple fields
  ([`b266ec7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b266ec7ae2f4bfe621487ca39b838d18a6af045d))

- Enable in-field processes when field > 0
  ([`b1ad639`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b1ad63972f6ef655028356be87da102c17c68ac3))


## v4.2.6 (2022-10-28)

### Bug Fixes

- Correct author name and email
  ([`2b1d107`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2b1d107f7481bb329a2c275be31bb2f57b3a8251))


## v4.2.5 (2022-10-24)

### Bug Fixes

- Improve legend location in ac susceptibility and cole cole plotting, and add more colors
  ([`048f041`](https://gitlab.com/chilton-group/cc-fit2/-/commit/048f041173cc87d849497cfc49d7c15e9fe6b365))


## v4.2.4 (2022-10-24)

### Bug Fixes

- Add sigma arg for relaxation mode
  ([`8ea3b82`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8ea3b82f9052f63f1570970291667354e9895f54))


## v4.2.3 (2022-10-24)

### Bug Fixes

- Remove zf_thresh keyword
  ([`0f57a40`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0f57a408bf2eb2301307100afc425e400db0106a))


## v4.2.2 (2022-10-21)

### Bug Fixes

- Improve colorbar tick positions
  ([`452e079`](https://gitlab.com/chilton-group/cc-fit2/-/commit/452e07946ef5ab0709fc9a698930973804416cbb))

- Improve colorbar tick positions
  ([`5756439`](https://gitlab.com/chilton-group/cc-fit2/-/commit/57564399514da130aedc6fc7b6e67e1f12642553))


## v4.2.1 (2022-10-21)

### Bug Fixes

- Restore colours and style for process plot plots
  ([`ce2a096`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ce2a0967456badb48b901f7955925e87301f4f3f))

### Documentation

- Fix ref style
  ([`066151c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/066151c2934d8e1967d3edbdd52d26c664e52c41))

- Improve tables
  ([`6384abb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6384abbc32dcf90805213b85654921f85c2bba3c))

### Performance Improvements

- Switch select_T default to ignore all rather than fit all
  ([`6a12502`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6a125027975ef51dd528635aed4608bc28c0f3df))


## v4.2.0 (2022-10-20)

### Build System

- Remove force docs from build_and_release
  ([`88bcc7a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/88bcc7a2c0794673ae55f187a06f9b190877fc8f))

### Documentation

- Add ac theory ref
  ([`2c5cd90`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2c5cd90386893ef1d12d028bc2b98cfc2712fb9f))

- Add dc theory ref
  ([`f2820fe`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f2820fe8019c3ad2f7c96e9d3e7787498e434a94))

- Fix stretched exp eqn
  ([`dad8a14`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dad8a14b03caa20c65cfe7bd945351f95d8417d4))

- Fix superscripts
  ([`438e581`](https://gitlab.com/chilton-group/cc-fit2/-/commit/438e5812b4752e045434685bab352ddd49ecf244))

- Fix table
  ([`1611527`](https://gitlab.com/chilton-group/cc-fit2/-/commit/16115277cc06ba42a65f875de214476d3c3c3bf5))

- Fix tables
  ([`13023ec`](https://gitlab.com/chilton-group/cc-fit2/-/commit/13023ecf64a048b6161472e051b6be794cc659c7))

- Fix theory refs
  ([`0528fb6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0528fb684758ff42b9ef7a1851eb67668261551e))

- Update codeblocks
  ([`1242ac3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1242ac34e953002db3798534fd8b686c576fb6e8))

- Update equations
  ([`91d0220`](https://gitlab.com/chilton-group/cc-fit2/-/commit/91d02203131638ec5f71e584f10337893f9d5f09))

- Update equations in table
  ([`27c7614`](https://gitlab.com/chilton-group/cc-fit2/-/commit/27c7614ca5dd6a38905a292a5b20d54c0c746caf))

- Update math
  ([`0e31caf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0e31cafb49e9eaf084bf53b919783dc3a6976934))

- Update table style
  ([`2a4e760`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2a4e760b7d6eb155eb03f7103753a9827cdf149f))

- Update tables
  ([`c8eea76`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c8eea7695bcf969aaf5237bfcd68f2a01711991a))

### Features

- Allow select_T with process plot
  ([`1672577`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1672577b8268c8425ac6ebc0e70426efaa4ef90c))


## v4.1.2 (2022-10-19)

### Bug Fixes

- Remove bug in parser
  ([`161924e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/161924e698a840af20ef4c4216bc9c3c09b76518))

### Documentation

- Add dc page and update ac page
  ([`436245a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/436245a509600d696ddcef87c5c121bf903121b0))


## v4.1.1 (2022-10-19)

### Bug Fixes

- Add verbose for dc
  ([`edc03f5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/edc03f560c034bc8a1e0835d15e1de5d0766e444))

- Add verbose option for ac
  ([`624552f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/624552f6fc96ee2e957e7852ff070a6c912dadb7))

- Add verbose option for relaxation mode
  ([`5e85e04`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5e85e04fd7729adc37745a5acf714104d76d1f41))

### Refactoring

- Remove gui error box from command line interface and add hidden arg for exe mode
  ([`c206619`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c2066195df9c60eec24dea355925d9ef13805fa4))

- Use sys for exit
  ([`4c3da60`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4c3da605850677949ab4940260437d4ed8552d3a))


## v4.1.0 (2022-10-19)

### Features

- Add option to specify zero-field cutoff
  ([`147ef87`](https://gitlab.com/chilton-group/cc-fit2/-/commit/147ef87870943663a7c9016046a8978747d2abe3))


## v4.0.5 (2022-10-19)

### Bug Fixes

- Enable discard off when select_T indicated
  ([`0093075`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0093075af11ee6ec8252c98fdd0bb5c61c9d4fa4))

- Refactor dc field parsing and splitting
  ([`df47b4f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/df47b4faf4e7a3e2eb55925158510981c424c5a0))

- Remove bug in select_T
  ([`d18cdf4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d18cdf40f3de38bc83d23c23a2aba8de4f974cac))

### Documentation

- Fix wrong docstring
  ([`ceb98cc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ceb98ccc3a8c460acda186b5e2fbfb8b67084f1a))

- Update docstring
  ([`4063fb7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4063fb7739ddf5fb60eb7c53cf5b23ebb2e5afcf))


## v4.0.4 (2022-10-18)

### Bug Fixes

- Use correct directory separator in windows
  ([`1adbea5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1adbea5c75cb8476505415cd77aa14fd1fb67742))

### Build System

- Remove need for package in docs build
  ([`88be7d5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/88be7d535fa6c33d05e248b12000733e22ceb528))


## v4.0.3 (2022-10-10)

### Bug Fixes

- Use absolute path for results dir creation
  ([`7d87f69`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7d87f6992dc79eb09b34dcec69d21091b0af5854))


## v4.0.2 (2022-10-10)

### Bug Fixes

- Correct time variable for plotting
  ([`0962750`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0962750c4b8871deaa2183bed30b509f57049514))


## v4.0.1 (2022-10-10)

### Bug Fixes

- Add missing decorator
  ([`b4a8db3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b4a8db366e671aeb0e0708a37983f5f68ededa85))

- Add missing skip for unfitted experiments
  ([`2b2a673`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2b2a6734d7fead7b5d58e38ffa4e5d3dbe06e6c9))

### Build System

- Update docs build
  ([`9e3664a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/9e3664a401736a2e2eec6d7906e32446c64777a1))

### Documentation

- Update README.md
  ([`ecd330b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ecd330bb7c32a4b561d51db9e9ccb8c69df64479))


## v4.0.0 (2022-10-07)

### Bug Fixes

- Add sys
  ([`683d6ff`](https://gitlab.com/chilton-group/cc-fit2/-/commit/683d6ffda183754e0cc375ec0fc807c51b579898))

- Add sys
  ([`1dc990e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1dc990e6a19550db409c2a5b1f60483f8dbe6de2))

- Add utf-8 encoding for windows compat
  ([`c037bc7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c037bc7918874da18b77d058a990132c64c0537e))

- Bug in dc field ac mode
  ([`a4dd55a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a4dd55a4449a6c605b6865cc00675ca536bab529))

- Bug in glob
  ([`e0f927e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e0f927e1a4e2f487186a9a00f138ef0b3dd15102))

- Bugs in relaxation mode
  ([`00ae8f7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/00ae8f7617ef3464c53c10fa25ccb72473da50e5))

- Change field to dc_field
  ([`44580c1`](https://gitlab.com/chilton-group/cc-fit2/-/commit/44580c13139b1695658cfff2918bae1b9e612dc0))

- Connect new ac module to relaxation rate fitting in cli
  ([`f93de11`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f93de11361ed70e3a3f95fbc25abe3f54ff0e428))

- Correct filename variable
  ([`ac30341`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ac303419657781cff3781a7f5a3c03fa8cfead74))

- Correct parser name
  ([`45103fa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/45103fa257ed47ca1a15b5de70820a9ab27443e2))

- Disable minor ticks in heatmap
  ([`0953af3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0953af3b7668f1ea4ed0923716dca67e1693ac21))

- Fix bug on linux machines where final fit is not displayed
  ([`98db5f9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/98db5f9d5bab6218da65aa0794267565bd10af46))

- Fix bugs in dc fitting, simplify slider behaviour, improve pep8 compliance
  ([`78d3758`](https://gitlab.com/chilton-group/cc-fit2/-/commit/78d37589f1271ca993fa4663066e65508dc55eb6))

- Fix incorrect dc_field specification when >1 field strength used
  ([`41469c6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/41469c6904742ded059ebf2742a18333944731b4))

- Fully fix bug with resetting fit model
  ([`4ce4b92`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4ce4b92e6097de28707393ff6e4fee4ce94227f2))

- More backend bugs
  ([`6208e9b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6208e9b0a3183a5be9648c2b2ff0882c2b219846))

- Move format for print statement
  ([`b4f7aab`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b4f7aab2113b25409abe0b14212bc13286f45119))

- Move to tkagg backend on mac due to macos backend bugs
  ([`765c12a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/765c12a9665fefb4977bf2f1017ffa6456650a28))

- Reinstate colecole and susceptibility
  ([`2958114`](https://gitlab.com/chilton-group/cc-fit2/-/commit/29581149de0d14fa94b607fee4fdcd7cffd8850f))

- Reinstate verbose
  ([`2b7f781`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2b7f781c7d341a89bc71d1f0def005852c1cadee))

- Remove extra underscore in file name
  ([`fc89e34`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fc89e34e46dc03b577acb6a8c5ee18636b362751))

- Remove initial results directory creation
  ([`b903948`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b90394877b7dcce499acab7079595e98a1e62e2e))

- Remove reference to args in ac.py and improve pep8 compliance
  ([`474c538`](https://gitlab.com/chilton-group/cc-fit2/-/commit/474c538166cb4f2896c6a2d7731fce6b7e8ea383))

### Build System

- Add .gitignore
  ([`b45ea4b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b45ea4b1a37d47d14901f78fe8dbb5f755b8a8cd))

- Add ci/cd pipeline
  ([`7da74bb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7da74bbcb47afbfdbb0145981b16584275e991bd))

- Add docs vers number variable
  ([`b2548b9`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b2548b965a0c32a255e80882e980b97ce93447fe))

- Add setup and pyproject
  ([`31f2d02`](https://gitlab.com/chilton-group/cc-fit2/-/commit/31f2d02eb4234ac0a26a655e96592f239256489b))

- Bump python vers
  ([`d4695a0`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d4695a02882a16e1d4f41c66d751507a873098d3))

- Change email
  ([`32bef4b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/32bef4b36670d2554f8f93788edbf4732560f226))

- Change exe name
  ([`31cd5a6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/31cd5a64d925f38e111375648926b63593af8298))

- Correct package name
  ([`2e66c98`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2e66c98beb376f6cce5a395e411ddc53aaf04cb3))

- Match vers number to current
  ([`dfe3a24`](https://gitlab.com/chilton-group/cc-fit2/-/commit/dfe3a24d2ee00da97a34013eddbc465f53794105))

- Remove dash and underscores
  ([`ca98a74`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ca98a743b1e732dd493cf5bca0b00114f619493a))

- Remove egg
  ([`29be77e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/29be77e07dc93ae2cbf18ae81f9e108ba775cdc8))

- Remove pyc files
  ([`b01678f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b01678f474cd6c6726f1d39248d13a6cbb19e137))

- Remove underscore and dashes
  ([`a72e6a6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a72e6a6c458460ac4e3e2a1d4b7c7c94278a7f85))

- Remove underscore in name
  ([`e9fe800`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e9fe800d3e6aadb62f7e12f73d85b7f84fa1ef80))

- Update ci/cd
  ([`cc40f17`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cc40f17c97c7e9f1412bedbd083eef5eac193247))

- Update exe name
  ([`deb1e91`](https://gitlab.com/chilton-group/cc-fit2/-/commit/deb1e91c7422b4d2dd6e753cd2c0d44b84b3c460))

- Update exe name
  ([`a93a62c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a93a62cdd7c67776dc446e56454bdc5acf92a63a))

- Update package name
  ([`b62e8bd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b62e8bdc55c27b038ad0717bd40a15dc2ef9cb61))

- Update vers variable location
  ([`d6b9a1e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d6b9a1e3399a180c7566623b8ef6cfb4a043a39d))

### Code Style

- Change comments
  ([`e94f732`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e94f73230b335b69efc92671b88d79c8d1c6abef))

- Improve flake8 compliance
  ([`55a28c2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/55a28c235f11ca40be03bbcdaca6be934f54744e))

- Improve flake8 compliance
  ([`898c5dc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/898c5dc2aa5caf4eb5d7b837317e83be3f8dffc4))

- Improve pep8 compliance
  ([`e4163da`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e4163da099ecdd0c1e6d229d1ebc679034315ba4))

- Improve pep8 compliance
  ([`65d3154`](https://gitlab.com/chilton-group/cc-fit2/-/commit/65d3154558b38fa3c887c26deecb6abe7bf8efc4))

- Improve pep8 compliance
  ([`46156f3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/46156f3481867e6777b22bcc9c5bb6e6298270d7))

- Improve pep8 compliance
  ([`4d864bd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4d864bd9171dd3b79edc3f6dc1d23a2a88d0dc90))

- Improve pep8 compliance
  ([`66f2434`](https://gitlab.com/chilton-group/cc-fit2/-/commit/66f2434e99dfa19741a2d238580961f53e4ec923))

- Improve pep8 compliance
  ([`32d707c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/32d707ccb807ed763e5718490439477f5fe7ff9f))

- Improve pep8 compliance
  ([`4f9d3fc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4f9d3fc0f5f8a8082eeb2d99aeef5607b26d7d6c))

- Improve pep8 compliance
  ([`ef58f95`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ef58f9525fa8112762f0efb150f507ddebf5eb74))

- Improve pep8 compliance
  ([`b67834f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b67834f4f0fb738c7777787fd4b4922389aa97f7))

- Improve pep8 compliance
  ([`447371a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/447371a23c7e8913a80ae63e49faf3896a1d99ed))

- Improve pep8 compliance
  ([`7ff394c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7ff394ccdb7085208c50a4da6506cb486fdecd05))

- Improve pep8 compliance
  ([`7f62e92`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7f62e9212e7c67b4aacf6089b2898a164b1c7d53))

- Improve pep8 compliance
  ([`e1a23e3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/e1a23e3d86920786e4b6ad225d06f2b72955bc31))

- Improve pep8 compliance
  ([`32d5bd2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/32d5bd2b1bc5bb84130e0f47c546a6fd4aeac450))

- Improve pep8 compliance
  ([`be28681`](https://gitlab.com/chilton-group/cc-fit2/-/commit/be286810cd880097732b380843a7e823e84723c2))

- Improve pep8 compliance
  ([`b08fb35`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b08fb351768eb37e9f0243a54afdfc1565f2f3c7))

- Improve pep8 compliance
  ([`ed46edd`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ed46edd039b650066f686a01369483910622a4be))

- Improve pep8 compliance
  ([`bb1f126`](https://gitlab.com/chilton-group/cc-fit2/-/commit/bb1f126dad5cf4ddfa1450c6c45ad9a5cc249f49))

- Minor changes
  ([`c39eff2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c39eff296b0c39e6741cb9582e382c5819e9f362))

- Minor stylistic changes
  ([`d5d37df`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d5d37df21bf9304b0a21b9d883797616150bbb89))

- Remove unnecessary import and add module docstring
  ([`5c5644d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5c5644ddfa150236254eee994e5de93649b32296))

- Rename models variable to be more representative of field separation
  ([`014d411`](https://gitlab.com/chilton-group/cc-fit2/-/commit/014d41119a392dcd081818d7eeb7071e299e61ab))

### Documentation

- Add ac explanation
  ([`4638c92`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4638c92f26d7918b673d10763d84e5723109291a))

- Add better help
  ([`36a3633`](https://gitlab.com/chilton-group/cc-fit2/-/commit/36a36334988c6a92a933eb8c3ae47dd7643d49c8))

- Add CCFIT2 executable install instructions
  ([`00adb6e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/00adb6e97ecac7d3588a941dba6af5194d2eac4d))

- Add comment
  ([`b0468f6`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b0468f6cb5555c27d4c84c2c92345810464f5665))

- Add comments
  ([`4591ac8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/4591ac8a9ac2ae3d28f2f3a67649be98270459ae))

- Add docstring
  ([`532f588`](https://gitlab.com/chilton-group/cc-fit2/-/commit/532f588c5b440546df92cdcc1c38ab599c4aa4c1))

- Add header
  ([`a8d434c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a8d434cb38fafa94326ca3b4f93c200b3f05a287))

- Add headers
  ([`80133ef`](https://gitlab.com/chilton-group/cc-fit2/-/commit/80133ef9e7117aec5d8cf04ddbcf54e72f1dcae5))

- Add installation instructions
  ([`1c4247f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1c4247ffef4c21a6ee6081b58c9231738981f925))

- Add pages
  ([`64aa61a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/64aa61addbd5f6ba1053b99de9076bce7b44d7c8))

- Add pages
  ([`1078d7e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/1078d7eec56c5540c10fdab8c9b49d015ce010f8))

- Add pages
  ([`2194143`](https://gitlab.com/chilton-group/cc-fit2/-/commit/21941432002185d8932433cc60687cf9f5b9dbf4))

- Add todo
  ([`303918e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/303918e77db0a3c8d3b4b7f8b83557664884da4f))

- Add todo
  ([`b693bcc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b693bcc4c860a392dae371d8ca56b7a3a36fa860))

- Change package name
  ([`2aba26b`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2aba26bba264315458f3c94c65959dbe55115ba9))

- Finish AC usage section
  ([`87757f5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/87757f58237b9882bc6de9d76f721b5d4d25dcb2))

- Improve docstrings
  ([`7ea017e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7ea017e76ba75f2570840abd696d864418b170de))

- Improve docstrings
  ([`fcb3ebf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fcb3ebf2fded04a9e2f1eb90603f43d8044e95ac))

- Minor changes
  ([`7b7fbf2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/7b7fbf28f8898148950ef0bc2776611fc50537c5))

- Minor changes
  ([`496b6d5`](https://gitlab.com/chilton-group/cc-fit2/-/commit/496b6d5dd648576ee1ba19ec3141edb56e62345c))

- New ci code for new docs
  ([`cbbc08a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/cbbc08a366c5a0767cb0fa08ba4435c6258c4d96))

- Remove boilerplate
  ([`3abdc6a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3abdc6aee64d89d4f713be882b5dce0c08bebac8))

- Remove build
  ([`26cf197`](https://gitlab.com/chilton-group/cc-fit2/-/commit/26cf197852670f152639e6e22e05a17dd197ec24))

- Remove build
  ([`270b916`](https://gitlab.com/chilton-group/cc-fit2/-/commit/270b916488989585f0f2a6974a2c486c09b0cc48))

- Update ac keywords
  ([`0d0ac28`](https://gitlab.com/chilton-group/cc-fit2/-/commit/0d0ac2899d0ba9355204abb8b92b65254bbc2330))

- Update boilerplate to match package
  ([`f143c23`](https://gitlab.com/chilton-group/cc-fit2/-/commit/f143c23b20dcef36e77a29c785064d90132af29c))

- Update docsting
  ([`8aaad5c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8aaad5cbddfc000620f883b907ee9f8ede37bf80))

- Update docstrings
  ([`3b37782`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3b377823d012da8fc3ed4283df9d8e7ea0a34b5a))

- Update docstrings
  ([`fb644d4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/fb644d4b9b9dbd931b004cfa7a0afd330d1a174d))

- Update docstrings
  ([`629a1fb`](https://gitlab.com/chilton-group/cc-fit2/-/commit/629a1fb53f0f36cc81d4909174a4aeedcd182fab))

- Update docstrings
  ([`48612a7`](https://gitlab.com/chilton-group/cc-fit2/-/commit/48612a7e7a1d12b20021161196a1567efc4f28fd))

- Update docstrings
  ([`64ca74e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/64ca74e91ef94b4840fe3b1cb0a89cef1c1d2aee))

- Update help
  ([`a1538d3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a1538d3d8ef78c9bdeafe34098b78caecced7327))

- Update help
  ([`396e99e`](https://gitlab.com/chilton-group/cc-fit2/-/commit/396e99e285b7be6a79f99d222486264f42a6412d))

- Update links
  ([`da61632`](https://gitlab.com/chilton-group/cc-fit2/-/commit/da6163238d4494b2bcb0499e437a9afd8f6d410f))

- Update links
  ([`347b18a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/347b18a16d0a1da790cbdecde27a4514c8db6273))

- Update manual with new keywords and file names
  ([`8b768d8`](https://gitlab.com/chilton-group/cc-fit2/-/commit/8b768d87999a11302d4ceac37848576e74cf35b3))

- Update page name
  ([`914e644`](https://gitlab.com/chilton-group/cc-fit2/-/commit/914e644bd7b970890b07097ad5e1acdbb178b32d))

- Update page names
  ([`6917a81`](https://gitlab.com/chilton-group/cc-fit2/-/commit/6917a81076cf1dbd3563d5a7c9a87f89634b7bb4))

- Update pages
  ([`3d45e81`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3d45e81ed0e245af8860e0464cbf30a713c387ba))

- Update readme
  ([`04bde31`](https://gitlab.com/chilton-group/cc-fit2/-/commit/04bde316164bc1a42daf7d2577137140456cb8f4))

- Update readme with new package name
  ([`76d0e00`](https://gitlab.com/chilton-group/cc-fit2/-/commit/76d0e009cee6f6032088289c2389c83b8116c78a))

### Features

- Add new dc classes
  ([`a7e937d`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a7e937d180aee56a94138625692bbdfb7dd93124))

- Reinstate and refactor --select_T option
  ([`71c97af`](https://gitlab.com/chilton-group/cc-fit2/-/commit/71c97afbcf11f25288f7e8cec0522656020c8737))

- Rewrite ac module with more object oriented approach, call package parser from executable
  ([`8678576`](https://gitlab.com/chilton-group/cc-fit2/-/commit/86785764268d966f7fa9e15da562763b36b666cb))

### Refactoring

- Add error for no fits selected
  ([`5903a2f`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5903a2fd999baca9080809150081ce62cc7de32c))

- Add file save for cole cole and susceptibility plots
  ([`0359402`](https://gitlab.com/chilton-group/cc-fit2/-/commit/03594028e490a1e9cda0a0a763387b1ad070612a))

- Add flat check and disable relaxation profile for two maxima
  ([`38b0e7c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/38b0e7ca51e0439c9007d8b54e18446bf5448402))

- Add job name and save directory to colecole and suceptibility plots
  ([`ee97584`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ee9758450f33062a116d9ddc9cfb35d940acdbd5))

- Add warning for no fitted points
  ([`2d7677a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/2d7677adb93360a0ea64edfc5787269b815633c6))

- Break up gui (tk+matplotlib) code into own module, and simplify ac file parsing
  ([`caf0147`](https://gitlab.com/chilton-group/cc-fit2/-/commit/caf0147edacb301a73e92e96515398f458467544))

- Clean up HEAD
  ([`d416105`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d41610552b92224764fc9fbb716a4aeb1ebfdbf9))

- Fix bug in dc mode parser
  ([`ddde715`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ddde7151537fa2b857952f31540b9da72d9bddb0))

- Further fixes for qt5 bug, refactor file name handling and remove old code
  ([`39bdc5c`](https://gitlab.com/chilton-group/cc-fit2/-/commit/39bdc5c36c101166b7e17035bdc11c87b566e005))

- Further tweaks to plotting block handling
  ([`05cbb38`](https://gitlab.com/chilton-group/cc-fit2/-/commit/05cbb385dff90d43e337d3e262efbbfe9da6e9ca))

- Improve pep8 compliance
  ([`3e33b52`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3e33b526d6c9270d64ce5fbf3a6a3987d0ad0ccd))

- Move buttons to gui, rename discard func variables
  ([`ab86dbc`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ab86dbc25e7c193ee99efe0efbd14e81f8b6a3f0))

- Reinstate --no_discard argument
  ([`a2e893a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a2e893a45f04c2ba689085680a29c79c354bd595))

- Reinstate --process option
  ([`64d8cdf`](https://gitlab.com/chilton-group/cc-fit2/-/commit/64d8cdf8931d55736ed0c8d882745103b85ee819))

- Reinstate data_pointer argument
  ([`097ec48`](https://gitlab.com/chilton-group/cc-fit2/-/commit/097ec48d97d0842b962347cbfc2196b226abd473))

- Reinstate verbose and minor tweaks to blocking handling
  ([`d473853`](https://gitlab.com/chilton-group/cc-fit2/-/commit/d473853be6ae349cbf6897f14c9994960e2c6346))

- Remove datasets from ac main
  ([`957db43`](https://gitlab.com/chilton-group/cc-fit2/-/commit/957db433d25f299d1682591118b3a63c01d3a0ab))

- Remove debug printing
  ([`b3d6c17`](https://gitlab.com/chilton-group/cc-fit2/-/commit/b3d6c17888c0abdaf93288c2dc2e9e049192299c))

- Remove duplicate code and point to cc_fit package
  ([`c81f904`](https://gitlab.com/chilton-group/cc-fit2/-/commit/c81f90431d24670ec548619c86be3425f844363e))

- Remove filter flats option and enable by default, add threshold value
  ([`525b6c4`](https://gitlab.com/chilton-group/cc-fit2/-/commit/525b6c4900d2befc65d71fa6ef471f40d3b77144))

- Remove old argument
  ([`ee1b49a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ee1b49a84b564f69c3ba77913fadc2ca03692a95))

- Remove old gui code
  ([`3fce4b2`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3fce4b2f6478411d9f731cf4c29a75cc8c06851f))

- Remove parser module and fold into ac objects, add docstrings
  ([`3df0e50`](https://gitlab.com/chilton-group/cc-fit2/-/commit/3df0e504d65756d169dd0856362bf0aabb8b2f46))

- Rename ac headers
  ([`da43cf3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/da43cf3981151be99196b9a2e65257fb6711cc64))

- Separate main into subprograms and add separate cli
  ([`de1bc5a`](https://gitlab.com/chilton-group/cc-fit2/-/commit/de1bc5ac6bf4a130fcdd087f70d1603aefcd96c6))

- Simplify ac main code
  ([`5c9eb52`](https://gitlab.com/chilton-group/cc-fit2/-/commit/5c9eb52cc8a042127b34a8da21e2d1a77fb9f6f1))

- Simplify if statement
  ([`ea75f96`](https://gitlab.com/chilton-group/cc-fit2/-/commit/ea75f96386a05fc32079ab92d88435e1791fc2c8))

- Uncomment colecole and suceptibility plots
  ([`a8bebfa`](https://gitlab.com/chilton-group/cc-fit2/-/commit/a8bebfaef885ca4432ab251824af82765a58f5a6))

### Testing

- Add dc test files
  ([`45fadc3`](https://gitlab.com/chilton-group/cc-fit2/-/commit/45fadc308f975f833bd1d2d9730a8cd980e56e87))
