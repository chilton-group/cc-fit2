.. _faq:

FAQ
---

This page contains some frequently asked questions. Please consult this page before raising a GitLab issue!


Errors ...
^^^^^^^^^^

... ``This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.``

    If ``ccfit2`` does not run and gives this error, then read the instructions :ref:`here <wsl_qt>`. 

... ``qtpy.QtBindingsNotFoundError: No Qt bindings could be found``

    You need to install either ``pyqt5`` or ``pyqt6`` using ``pip``.

... ``'ccfit2' is not recognized as an internal or external command, operable program or batch file.``

    You are attempting to run ``ccfit2`` in a command prompt window that does not have python enabled/installed.
    If you are using python/anaconda on a public machine, try ``pip install ccfit2 --user`` instead.
    *We do not offer support/help for this.*


I want to...
^^^^^^^^^^^^

... change the plot font family/size or plot file extension

    Read the instructions :ref:`here <Configuration>`. 

... only fit specific temperatures in my ac data

    Use the ``--select_T`` optional argument in ``ccfit2 ac``.

... use my own values of :math:`\alpha`, :math:`\beta`, :math:`\tau`, etc. in ``ccfit2 relaxation``

    Yes you need to produce an input file compatible with the ``relaxation`` subprogram using the expressions for
    :math:`\langle \ln(\tau)\rangle` and :math:`\sigma_{\ln\tau}` (see :ref:`AC <ac-theory>` and :ref:`DC <dc-theory>`).
    You can do this manually, or can use ``ccfit2`` to easily do this in a python script - see :ref:`here <relax_script_convert>` for an example.

... use an old ``CCFIT2_cmd.py`` ``*_params.out`` file as input to relaxation.

    Yes, use the ``update_ac_dc_params`` command.

... Simultaneously fit temperature and field depedence of relaxation rates

    This can be done by using ``ccfit2`` in a python script. An example is given
    in the ``ccfit2`` repository in ``ccfit2/examples/relaxation/script_relax_HT_dependent.py``.