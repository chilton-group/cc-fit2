.. _Installation:

Installation and Configuration
==============================

Setting up python
-----------------

If you are experienced with python and installing packages, go to the :ref:`next section <pip_install>`.

If you are new to python, we recommend using `Anaconda <https://www.anaconda.com/download>`_ - see the
`Anaconda guide <https://docs.anaconda.com/free/anaconda/install/>`_ for more information. Anaconda may be available
at your institution through your IT provider - **note we do not offer support for installing Anaconda/python**.

If you're using Anaconda, run the following command in your Anaconda enabled terminal to install
the ``pip`` package manager.

.. code-block:: bash

    conda install pip

.. _pip_install:

Installation
------------

The ``ccfit2`` python package and its command line interface can be installed using the ``pip`` package manager

.. code-block:: bash

    pip install ccfit2

You will also need to install either ``pyqt5`` ``pyqt6``, or ``pyside6`` using ``pip``, e.g.

.. code-block:: bash

    pip install pyqt6

All three packages are supported by ``ccfit2`` for maximum compatibility with pre-existing instances of ``qt``.

To test your installation was successful, run the following command

.. code-block:: bash

    ccfit2 -h

You should see the help text for ``ccfit2``. You are now ready to start using ``ccfit2``, head to the :ref:`guide` pages for more information, 
or over to :ref:`configuration` to customise your installation.

.. _wsl_qt:

Windows Subsystem for Linux (WSL) Users
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

WSL users may see the following error when running the ``ccfit2`` command line interface ::

    This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.

This is because WSL is either missing some of the libraries it needs to install QT5/6, or because you do not have an X11 server.

To install the required QT dependencies, run the following command in your WSL terminal.

.. code-block:: bash

    sudo apt-get install '^libxcb.*-dev' libx11-xcb-dev libglu1-mesa-dev libxrender-dev libxi-dev libxkbcommon-dev libxkbcommon-x11-dev


You might also get the following warning when running ``ccfit2`` if you installed ``pyqt6``

.. code-block:: bash

    qt.qpa.plugin: Could not load the Qt platform plugin "wayland" in "" even though it was found.

To remove this, add the following to the bottom of your ``~/.bashrc`` file.

.. code-block:: bash

    QT_QPA_PLATFORM=xcb

and then close and re-open your WSL terminal.

.. _plot_config:

Updating
--------

To update ``ccfit2``, run

.. code-block:: bash

    pip install ccfit2 --upgrade
