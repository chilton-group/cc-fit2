What's New
==========

To update ``ccfit2`` to the latest version, run

.. code-block:: bash

    pip install ccfit2 --upgrade

Version 5.11.0
--------------
 - Relaxation: Add ability to test-fit models in FitWindow

Version 5.10.0
--------------
 - General: Fix package layout error which stopped scripts from running
 - Scripts: Add support for tab delimited files in update_ac_dc_params script

Version 5.9.3
-------------
 - Waveform: Fix incorrect unit of :math:`\omega` in PPDRamanModel

Version 5.9.2
-------------
 - Waveform: Fix the definition of period of waveform

Version 5.9.1
-------------
 - Relaxation: Fix parsing error when parameter ``.csv`` file contains integer values of field or temperature

Version 5.9.0
-------------
 - Relaxation: Added alternative zero-field Raman-I model ``PPDRamanModel`` based on the Fourier transform of the two-phonon correlation function

Version 5.8.1
-------------
 - General: Bump all dependency versions to latest
 - General: Fix bug with usage of np.NaN

Version 5.8.0
-------------
 - AC/DC : Add ``update_ac_dc_params`` script for updating old ``CCFIT2_cmd.py`` ``_params.old`` files
 - Relaxation: Add ``split_rates`` script for splitting two :math:`\tau` ``_params.csv`` files

Version 5.7.1
-------------
 - Relaxation: Fix missing H=0 point in rate vs H plots

Version 5.7.0
-------------
 - AC: Remove guess option from ``fit_to`` method of ``Model``
 - AC: Improve ``Model`` class docstrings
 - DC: Remove guess option from ``fit_to`` method of ``Model``
 - DC: Improve ``Model`` class docstrings
 - Relaxation: Remove guess option from ``fit_to`` method of ``LogModel``
 - Relaxation: Rename ``TauTDataset`` and ``TauHDataset`` to ``TDataset`` and ``HDataset``, with deprecation notice for old class names
 - Relaxation: Improve ``LogModel`` class docstrings
 - Relaxation: Add ability to fit field- and temperature-dependent datasets with classes ``HTDataset`` and ``LogTauHTModel``
 - Relaxation: Add field- and temperature-dependent models for Raman-II/Direct and Brons-Van-Vleck Raman-I
 - Relaxation: Add temperature-dependent models for phonon bottlenecks on Raman, Raman and QTM or Direct processes
 
Version 5.6.6
-------------
 - General: Add support for spaces either side of delimiter in magnetometer files.

Version 5.6.5
-------------
 - Relaxation: Fix incorrect parameter output in ``model_params.csv`` file

Version 5.6.4
-------------
 - Relaxation: Add warning when files with ``.out`` extension used

Version 5.6.3
-------------
 - Test: Add unit tests
 - General: Add option to specify additional comment lines in ``.csv`` file creation
 - DC: Change ```dc.Experiment.from_measurement`` return type to match ac counterpart

Version 5.6.2
-------------
 - AC: Fix missing units in Havriliak-Negami model class

Version 5.6.1
-------------

 - AC: Fix broken fitting of two tau models
 - DC: Fix broken fitting of two tau models

Version 5.6.0
-------------

 - General: Parameter and Model output files are now ``.csv`` format
 - AC: Fix bug in ``Experiment.from_file()``

Version 5.5.0
-------------

 - General: File parsing is now parallelised in the command line interface

Version 5.4.1
-------------
 - AC: Fix random x tick labels in X' subplot of susceptibilities plots

Version 5.4.0
-------------

 - Waveform: Enforced 1 DC field frequency per magnetometer file
 - Waveform: Improved removal of unstable DC field points
 - Relaxation: Fix overlapping x = 0 tick label in interactive fit plot
 - AC: Fix axis label fonts when using ccfit2_fontname envvar

Version 5.3.0
-------------

 - Waveform: Add field/moment vs time plot option

Version 5.2.3
-------------

 - Waveform: Correct period definition in Fourier Transform

Version 5.2.2
-------------

 - AC: Update initial guess values for Double Generalised Debye Model parameters

 - Waveform: Fix bug with overlapping plots when using ``--plot_ft``

Version 5.2.1
-------------
 - AC: Add check for same parameters in fit and fix

 - DC: Modify constructor to match AC and Relaxation
 - DC: Add check for same parameters in fit and fix

 - Relaxation: Add check for same parameters in fit and fix

Version 5.2.0
-------------
 - DC: Support for negative parameter values in command line interface

 - Relaxation: Remove arbitrary bounds from all parameters
 - Relaxation: Add residual value to interactive fit window
 - Relaxation: Remove space in output file name when using Brons-Van-Vleck product models
 - Relaxation: Improved appearance of fitted time plots

Version 5.1.3
-------------
 - Relaxation: Add support for :math:`H` = 0 Oe points in :math:`\tau^{-1}` vs :math:`H` fit window
 - Relaxation: Correct subscript italicisation in FDQTM variable name

Version 5.1.0
-------------
 - Relaxation: Add option to hide fitted parameters when viewing or saving plots

Version 5.0.2
-------------
 - Relaxation: Add support for :math:`H` = 0 Oe points in final :math:`\tau^{-1}` vs :math:`H` plots
 - Relaxation: Correct labels for FDQTM model

Version 5.0.1
-------------
 - AC: Improve support for large numbers of temperatures in ``--select_T`` window

Version 5.0.0
-------------
Major overhaul of ``ccfit2``.

 - EXE: ``cc_fit2.exe`` is deprecated. Please install the ``ccfit2`` package
 - Package: ``ccfit2`` is now an object-oriented python package with a detailed API
 - Documentation: The pdf documentation has been fully deprecated

 - AC: Support for MPMS files as input
 - AC: Add Havriliak-Negami, Cole-Davidson, and Double Gen. Debye models
 - AC: New output file format

 - DC: Support for MPMS and SQUID files as input
 - DC: Decay models overhauled using protocol developed in :footcite:t:`blackmore_dc_2023`
 - DC: Support for linear and/or logarithmic axes in DC decay plots
 - DC: New output file format

 - Relaxation: Support for :math:`\ln(\tau)` vs :math:`1/T` plots
 - Relaxation: Interactive fit window overhauled. Now possible to toggle models
 - Relaxation: Support for multiple ``ccfit2`` AC and/or DC output files
 - Relaxation: New output file format
 - Relaxation: New definition of model parameter uncertainties. Please see :ref:`here <relax-fit>`

 - General: Terminal output improved with colour-coded information (Mac/Linux)

.. footbibliography::
