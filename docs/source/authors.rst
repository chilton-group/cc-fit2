Authors
=======

The ``ccfit2`` package is written by current and ex-members of the `Chilton Group <https://www.nfchilton.com/>`_.

`Dr. Daniel Reta <https://www.danielreta.com/>`_ created ``ccfit2``.

`Dr. Jon Kragskow <https://www.kragskow.dev/>`_ completely rewrote ``ccfit2`` from scratch in Version 5.0.0.

`Dr. William Blackmore <https://www.nfchilton.com/people>`_ authored the :py:class:`DoubleExponentialModel <ccfit2.dc.DoubleExponentialModel>` class, the ``--field_calibration`` and ``--M_eq_calibration`` features, and tested ``ccfit2=5.0.0``.

`Dr. Gemma Gransbury <https://www.nfchilton.com/people>`_ authored the :py:class:`ColeDavidsonModel <ccfit2.ac.ColeDavidsonModel>` class, introduced fitting of field- and temperature-dependent datasets, phonon bottleneck models and tested ``ccfit2=5.0.0``.

.. _citation:

Citation
--------

To acknowledge ``ccfit2`` in your work, please state the ``ccfit2`` version number, and cite both of the following publications.

.. bibliography::
   :list: enumerated
   :enumtype: upperroman

   reta_uncertainty_2019
   blackmore_dc_2023
   
The version number can be obtained using the terminal command

.. code-block::

   pip show ccfit2

If this results in an error, then you are probably using a very old version of ``ccfit2`` and need to update.