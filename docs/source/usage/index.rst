Usage
=====

.. toctree::
   CCFIT2_cmd <executable>
   Command Line Interface <cli/index>
   Scripting <scripting/index>
   :maxdepth: 3
   :caption: Contents:
   :hidden:

The primary method of using ``ccfit2`` is via its command line interface (CLI). Through this interface, 
users can easily process data from AC Susceptibility, DC magnetisation decay, and DC Waveform experiments, and
can subsequently model the relevant magnetisation dynamics (relaxation rates). To get started with the CLI, head over to the
:ref:`guide` guide.

More advanced users may be interested in using the the ``ccfit2`` package in their own python code, where they can take advantage
of ``ccfit2``'s object-oriented design to rapidly process large datasets, automate workflow, or even add their own custom models.
For details and examples, see :ref:`guide_scripting` and :ref:`modules`.

.. figure:: flow_diagram.png
    :align: center

    Figure 1: Graphical representation of the ``ccfit2`` workflow containing the ``ac``, ``waveform``, ``dc``, and ``relaxation`` subprograms.
