.. _guide:

Command Line Interface
======================

.. toctree::
   :maxdepth: 1
   :hidden:

    AC Susceptibilities <ac_cli>
    DC Decays <dc_cli>
    Relaxation Fitting <relaxation_cli>
    Waveform <waveform_cli>

The ``ccfit2`` command line interface (CLI) contains four different subprograms that can be listed using the terminal command

.. code-block:: bash

    ccfit2 -h

More information can be found on each of these subprograms by typing their name followed by ``-h`` into a terminal, e.g.

.. code-block:: bash

    ccfit2 ac -h
    ccfit2 dc -h
    ccfit2 waveform -h
    ccfit2 relaxation -h

Further explanations of the functionality included in the command line interface are given within these pages.