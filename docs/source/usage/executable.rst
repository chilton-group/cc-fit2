Executable
==========

The ``CCFIT2.exe`` executable and ``CCFIT2_cmd.py`` command line programs are no longer supported and **should not be used**.

To install the new ``ccfit2`` interface, use the instructions given :ref:`here <Installation>`.