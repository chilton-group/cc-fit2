.. _relax-script:

Relaxation Fitting
==================

The following examples demonstrate how to use the ``ccfit2`` ``relaxation`` submodule to fit relaxation rate data in a python script.

The first example demonstrates how this can be done using a ``ccfit2`` ``*params.csv`` file which contains the results of 
fitting an AC and/or DC experiment to their respective models using ``ccfit2``.

The second demonstrates how easily ``ccfit2`` can be used to fit theoretical relaxation rate data.

.. _relax_script_fromfile:

Fitting data from a file
^^^^^^^^^^^^^^^^^^^^^^^^

The specific input files used in this example are available at ``ccfit2/examples/relaxation/``,
and the full python code at ``ccfit2/examples/relaxation/script_relax_from_file.py``.

First, we create a list of the files we want to read. Here we are using relative paths
since this python script will run in the same directory as the input files. We then create a 
:py:class:`TDataset <ccfit2.relaxation.TDataset>` object which will contain the relaxation rates, associated temperatures, and any uncertainties in the relaxation rates.

If you're working with rates as a function of field, take a look at :py:class:`HDataset <ccfit2.relaxation.HDataset>` instead.


.. code-block:: python

    import ccfit2.relaxation as rx

    # ccfit2 output files to be read in
    # Here, the first is an AC file, second is a DC file
    files = [
        'ac_generalised_debye_params.csv',
        'dc_exponential_params.csv'
    ]

    # Make TDataset from these files
    dataset = rx.TDataset.from_ccfit2_csv(files)

The two files we are using have been obtained by *i)* fitting a Generalised Debye model to a set of AC susceptibility experiments, and
*ii)*, fitting a stretched exponential model to a set of DC decay experiments.

To check that this data has been read in correctly, we can plot it

.. code-block:: python

    # Check the data was read in successfully by plotting
    # rate against temperature
    rx.plot_rates(dataset)
    # and ln(time) against 1/T
    rx.plot_times(dataset)

You can clearly see the two sets of rate data - DC at low temperature, and AC at high temperature.

Now, lets fit this relaxation data to some phenomenological models. Lets assume we can fit Orbach,
Raman, and QTM terms to this data. In fact, we'll fix the QTM term at a specific value rather than fitting it.

To do this, first make a list of the models we want to use. These models are all
based on the :py:class:`LogTauTModel <ccfit2.relaxation.LogTauTModel>` class.

.. code-block:: python

    # Select models to use
    models = [rx.LogRamanModel, rx.LogOrbachModel, rx.LogQTMModel]

Then, make lists of the model parameters we want to fit and fix, along with their values and initial guesses.
Each list contains one dictionary per model, and each dictionary contains the fitted parameter of that model.

This sounds complicated but the code is quite simple.

.. code-block:: python

    # Parameters to fit
    # Separated by model
    # ccfit2 will decide which set of parameters goes with which model
    # so the order doesn't matter here
    # empty dictionary is for QTM, where no parameters are fitted
    fit_vars = [
        {'R': -30, 'n': 5},  # Raman
        {'A': -7, 'u_eff': 1000},  # Orbach
        {}  # QTM
    ]
    # Parameters to fix, one dictionary for each model
    # in this case, we fix the QTM parameter
    fix_vars = [
        {},  # Raman
        {},  # Orbach
        {'Q': 2.447}  # QTM
    ]

The order of the dictionaries in this list doesn't matter, ``ccfit2`` will work it out.

Now, we'll combine all this information into a :py:class:`MultiLogTauTModel <ccfit2.relaxation.MultiLogTauTModel>`.

You can think of this as one big model that contains each of the models listed in ``models``. In fact, that's
exactly what it is.

.. code-block:: python

    # Create MultiLogModel as combination of individual models
    multilogmodel = rx.MultiLogTauTModel(
        models,
        fit_vars,
        fix_vars
    )

Now, we fit this to our experimental dataset

.. code-block:: python

    # and fit to experiment
    multilogmodel.fit_to(dataset)

and we can print the final fitted values and their estimated uncertainties.

To do this, we loop through the individual ``logmodels`` of the :py:class:`MultiLogTauTModel <ccfit2.relaxation.MultiLogTauTModel>`.

.. code-block:: python

    # Print final values of models (fitted and fixed) and uncertainties
    for logmodel in multilogmodel.logmodels:
        print("Values:", logmodel.final_var_values)
        print("Uncertainties:", logmodel.fit_stdev)

Then, we can plot the relaxation rates from experiment with the
model data overlayed, and plot the difference between the experimental and model rates.

.. code-block:: python

    # Plot fitted model and experiment
    rx.plot_fitted_rates(dataset, multilogmodel)
    rx.plot_fitted_times(dataset, multilogmodel)

    # Plot difference between experimental and model tau
    rx.plot_rate_residuals(dataset, multilogmodel)

and finally, save this data to file

.. code-block:: python

    # Write model parameters to file
    rx.write_model_params(
        multilogmodel,
        file_name='relaxation_orbach_raman_qtm_params.csv'
    )

    # Write model data (rate, temperature) to file
    rx.write_model_data(
        multilogmodel,
        file_name='relaxation_orbach_raman_qtm_model.csv'
    )

.. _relax_script_modeldata:

Creating model data
^^^^^^^^^^^^^^^^^^^

In this example, we'll create some hypothetical rate data and then fit it to a relaxation model.

The full python code for this example can be found at ``ccfit2/examples/relaxation/script_relax_raw.py``.

First, lets create a set of temperature values that we'll use to generate our rates. Here we'll choose 30-100 K with 100 points.

.. code-block:: python

    import ccfit2.relaxation as rx
    import numpy as np

    temperatures = np.linspace(30, 100, 100)

Now lets create some rate data. First, we'll calculate a Raman term using :py:class:`LogRamanModel <ccfit2.relaxation.LogRamanModel>`.

Here the parameter values are from :footcite:t:`goodwin_2017`.

.. code-block:: python

    # Define Raman Parameters
    parameters = {
        'R': np.log10(1.664E-6), 'n': 2.151
    }

    # Calculate rates
    rates = 10**rx.LogRamanModel.model(
        parameters, temperatures
    )

and then an Orbach term using a :py:class:`LogOrbachModel <ccfit2.relaxation.LogOrbachModel>`

.. code-block:: python

    # Define Orbach Parameters
    parameters = {
        'A': -np.log10((1.986E-11)**-1),
        'u_eff': 1223 / 0.695034800  # convert cm^-1 to K using boltzmann
    }
    # Add Orbach term to rates
    rates += 10**rx.LogOrbachModel.model(
        parameters, temperatures
    )

Then, to make this a bit more interesting, lets add some random noise to the rates

.. code-block:: python

    # Add some noise to rates
    # done in log domain for simplicity
    logrates = np.log10(rates)
    logrates += np.random.rand(len(logrates)) * 0.1
    # then convert back to linear domain
    rates = 10**logrates

And then add this data to a :py:class:`TDataset <ccfit2.relaxation.TDataset>`

.. code-block:: python

    dataset = rx.TDataset(rates, temperatures)

Now, lets see if we can fit an model consisting of Orbach and Raman terms to this data (we should be able to!).


To do this, first make a list of the models we want to use. These models are all
based on the :py:class:`LogTauTModel <ccfit2.relaxation.LogTauTModel>` class.

.. code-block:: python

    # Select models to use
    models = [rx.LogOrbachModel, rx.LogRamanModel]

Then, make lists of the model parameters we want to fit and fix, along with their values and initial guesses.
These lists contain one dictionary per model, and each dictionary contains the fitted parameter of that model.

This sounds complicated but the code is quite simple.

.. code-block:: python

    # Parameters to fit
    # Separated by model
    # ccfit2 will decide which set of parameters goes with which model
    # so the order doesn't matter here
    fit_vars = [
        {'R': -3, 'n': 3},  # Raman
        {'A': -10, 'u_eff': 2000}  # Orbach
    ]
    # Parameters to fix, one dictionary for each model
    # in this case, no parameters are fixed
    fix_vars = [
        {},  # Raman
        {}  # Orbach
    ]


The order of the dictionaries in this list doesn't matter, ``ccfit2`` will work it out.

Now, we'll combine all this information into a :py:class:`MultiLogTauTModel <ccfit2.relaxation.MultiLogTauTModel>`.

You can think of this as one big model that contains each of the models listed in ``models``. In fact, that's
exactly what it is.

.. code-block:: python

    # Create MultiLogModel as combination of individual models
    multilogmodel = rx.MultiLogTauTModel(
        models,
        fit_vars,
        fix_vars
    )

Now, we fit this to our experimental dataset

.. code-block:: python

    # and fit to hypothetical data
    multilogmodel.fit_to(dataset)

and we can print the final fitted values and their estimated uncertainties.

To do this, we loop through the individual ``logmodels`` of the :py:class:`MultiLogTauTModel <ccfit2.relaxation.MultiLogTauTModel>`.

.. code-block:: python

    # Print final values of models (fitted and fixed) and uncertainties
    print("Final Model Values:")
    for logmodel in multilogmodel.logmodels:
        print("Values:", logmodel.final_var_values)
        print("Uncertainties:", logmodel.fit_stdev)

Then, finally, we can plot our hypothetical relaxation rates with the
model data overlayed, and plot the difference between the hypothetical and model rates.

.. code-block:: python

    # Plot fitted model and hypothetical data
    rx.plot_fitted_rates(dataset, multilogmodel)
    rx.plot_fitted_times(dataset, multilogmodel)

    # Plot difference between hypothetical and model tau
    rx.plot_rate_residuals(dataset, multilogmodel)

.. _relax_script_convert:

Converting old ccfit2 rate data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Prior to ``ccfit2 5.0.0``, the ``relaxation`` subprogram required a file containing relaxation rates and temperatures/fields,
and optionally a set of ``alpha`` or ``beta`` values which define the uncertainty of the rates. In ``ccfit2 5.0.0`` this input
file format was changed in order to add support for more than just the Generalised Debye and Stretched Exponential models of AC susceptibility and DC decays.

As of ``ccfit2 5.8.0``, you can use the ``update_ac_dc_params`` command to convert old data to the new format.
Alternatively, you can follow the guide below to do this in a python script.

This scripting example shows how to convert an old ``ccfit2`` file containing a Generalised Debye model to the
format currently required by ``ccfit2``.

The specific files used in this example are available at ``ccfit2/examples/relaxation/``,
and the full python code at ``ccfit2/examples/relaxation/calc_lntau_from_params.py`` (Example 2).

We're going to use ``pandas`` and ``numpy`` to load the (old) file ``ac_generalised_debye_oldccfit2_alpha_tau.out``
as a ``csv`` file delimited by spaces.

First, we load just the headers (column names), so we know what each column contains.

.. code-block:: python

    import pandas as pd
    import numpy as np

    # Load the headers (column names)
    data = pd.read_csv(
        'ac_generalised_debye_oldccfit2_alpha_tau.out',
        comment='#',
        skipinitialspace=True,
        delimiter='  ',
        engine='python'
    )
    # Convert headers into a list
    headers = data.keys()

Then we can load the actual data using ``numpy``, and convert it to a pandas dataframe.

This combination of numpy and pandas gets around the fact that old ``ccfit2`` files
didn't have particularly uniform input/output formatting.

.. code-block:: python

    # Then load the actual values
    # this file has 3 comment rows and a header row, so skiprows=4
    values = np.loadtxt(
        'ac_generalised_debye_oldccfit2_alpha_tau.out',
        skiprows=4
    )
    data = pd.DataFrame(values, columns=headers)

Now everything we need is contained in the ``data`` dataframe, and can be accessed using the column names.

In order to avoid confusion, lets first remove the old bounds on :math:`\tau`. In ``ccfit2<5.0.0`` these
were calculated using a slightly different numerical approach. 

.. code-block:: python

    # Remove old tau bounds to avoid confusion
    old_headers = ['tau_ln_ESD-up (s)', 'tau_ln_ESD-lw (s)']
    for old in old_headers:
        if old in data.keys():
            data.pop(old)

The rest of the data is fine to reuse.

Since we know this dataset corresponds to a Generalised Debye model, we can use
``ccfit2``'s ``GeneralisedDebyeModel`` class to calculate the values of
:math:`\langle\ln(\tau)\rangle`, :math:`\sigma_{\ln(\tau)}`,
and :math:`\langle \ln(\tau)\rangle_\pm` required by ``ccfit2``.

We then add these values to our dataframe.

.. code-block:: python

    # Calculate <ln(tau)>, and add it to the dataframe with the
    # header ccfit2 expects
    data['<ln(tau)> (ln(s))'] = ac.GeneralisedDebyeModel.calc_lntau_expect(
        data['tau (s)']
    )

    # and do the same for sigma_lntau
    data['sigma_ln(tau) (ln(s))'] = ac.GeneralisedDebyeModel.calc_lntau_stdev(
        data['alpha']
    )

    # If your original file contains the standard deviation of alpha and tau,
    # then you can also compute the upper and lower bounds of <ln(tau)> based
    # on those uncertainties.

    # If not, just delete these lines
    [upper, lower] = ac.GeneralisedDebyeModel.calc_lntau_fit_ul(
        data['tau (s)'], data['tau_err (s)']
    )
    data['fit_upper_ln(tau) (ln(s))'] = upper
    data['fit_lower_ln(tau) (ln(s))'] = lower

Finally, we can save this data to a new ``.csv`` file which can be read by 
the ``relaxation`` subprogram of ``ccfit2``.

.. code-block:: python

    # Then save this data to a new csv file which can be read by
    # ccfit2 relaxation
    data = data.to_csv(
        'ac_generalised_debye_oldccfit2_with_lntau.csv', index=False
    )

Which can be used with ``ccfit2 relaxation ac_generalised_debye_oldccfit2_with_lntau.csv``.

A similar approach can be used for old DC data, and an example is available in
``ccfit2/examples/relaxation/calc_lntau_from_params.py`` (Example 4).

.. footbibliography::