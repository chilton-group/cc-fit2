.. _guide_scripting:

Scripting
=========

.. toctree::
   :maxdepth: 1
   :hidden:

    AC Susceptibilities <ac_script>
    DC Decays <dc_script>
    Relaxation Fitting <relaxation_script>

Thanks to its modular and object-oriented design, ``ccfit2`` can easily be integrated into any python program/script, for full details of the classes and functions available, see :ref:`modules`.

Within these pages we give several guided examples of using ``ccfit2`` in a python script/program.

1. Loading and processing :ref:`AC susceptibility data <ac_script>`
2. Loading and processing :ref:`DC magnetisation decay data <dc_script>`
3. Loading and fitting :ref:`relaxation rate data <relax_script_fromfile>` outputted by ``ccfit2 ac`` and ``ccfit2 dc``
4. Loading and fitting :ref:`theoretical relaxation rate data <relax_script_modeldata>`
5. :ref:`Converting old relaxation rate data <relax_script_convert>` from ``ccfit2`` versions older than ``5.0.0``