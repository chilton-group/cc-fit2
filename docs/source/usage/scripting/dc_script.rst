.. _dc_script:


DC Decays
=========

This example reads in a set of magnetometer datafiles, each containing a single magnetisation decay
experiment at a given temperature, and then fits the magnetic moment as a function of time to an exponential decay model
in order to extract the relaxation time and distribution.

The specific datafiles used in this example can be found in the ``ccfit2`` repository
at ``examples/dc/example_data``, and code detailed in this page at ``examples/dc/script_dc.py``.

First, we create a list of the files which will be read. In this case, relative paths are used,
since this python script will run in the same directory as ``example_data``

.. code-block:: python

    all_measurements = []
    fnames = [
        'example_data/VSM_2K_0Oe.dat',
        'example_data/VSM_3K_0Oe.dat',
        'example_data/VSM_4K_0Oe.dat',
        'example_data/VSM_6K_0Oe.dat',
        'example_data/VSM_9K_0Oe.dat',
        'example_data/VSM_13K_0Oe.dat',
        'example_data/VSM_16K_0Oe.dat',
        'example_data/VSM_20K_0Oe.dat',
        'example_data/VSM_23K_0Oe.dat'
    ]

We then loop over each filename, and read each file into a series of :py:class:`Measurement <ccfit2.dc.Measurement>`
objects - each measurement is a single point in a decay experiment.

.. code-block:: python

    # loop over each file
    for fname in fnames:

        # Read current file
        measurements = dc.Measurement.from_file(fname)

Then, some cleanup is required before we do anything with the data. We want to remove
measurements carried out at saturating and decaying magnetic fields. For the former,
we do this because otherwise our data is dominated by points in time. In the case of the latter
we ignore data where the magnetic field is changing, since the relaxation rate is dependent on
magnetic field and so we wish to only consider relaxation that occurs in the (constant) target field.

To do this, we calculate the (forward) difference between each magnetic field value, and then
discard any :py:class:`Measurements <ccfit2.dc.Measurement>` where the field changes by more
than this value. Put simply, if the field isn't stable for a given :py:class:`Measurement <ccfit2.dc.Measurement>`,
we discard that point.

.. code-block:: python

    # loop over each file
    for fname in fnames:

        # Read current file
        measurements = dc.Measurement.from_file(fname)

        # Trim measurements to ignore values at saturating and decaying field
        diff_thresh = 0.5
        # Forward difference of field
        field_diff = np.abs(
            np.diff([measurement.dc_field for measurement in measurements])
        )

        # Flip field diff and find first value > threshold, then index to unflipped
        n_pts = len(measurements)
        cut_at = n_pts - np.argmax(np.flip(field_diff) > diff_thresh) - 1

        # Cut to stable field values only
        measurements = measurements[cut_at:]

We then take the final list of :py:class:`Measurements <ccfit2.dc.Measurement>` read from
this file, and append them to a list of all measurements we've read from all the files.

.. code-block:: python

    # loop over each file
    for fname in fnames:

        # Read current file
        measurements = dc.Measurement.from_file(fname)

        ...

        # Add to list of all measurements
        all_measurements += measurements

We have now read in all the data from our files, but we have no idea about how the data
are connected - i.e. which set of measurements constitutes an experiment.

It might seem strange to say this, since we know which files we've read, and if we keep our files
nicely organised then we should know what's in each file. Unfortunately this often isn't true in
the real world - sometimes more than one experiment is stored in a file, or other bad habits pop up.

So then, lets figure out which measurements constitute an experiment. Luckily ``ccfit2`` does
this automatically - take a look at the :py:meth:`from_measurements <ccfit2.dc.Experiment.from_measurements>` method
for more information.

At this point, we also choose to remove data points which have moments less than 1% of the initial moment.
This is an arbitrary step that we carry out to ignore noisy data at long aquisition times.

.. code-block:: python

    # Group measurements into experiments based on temperature
    # and cut moments < 0.01 * initial moment
    all_experiments = dc.Experiment.from_measurements(
        all_measurements,
        cut_moment=0.01
    )

    # all_experiments is a list of lists of experiments with
    # first dimension as DC field, second as temperature
    # An experiment is set of datapoints with same DC field and temperature

    # This will be 1, since there is only 1 DC field strength for all measurements
    print(len(all_experiments))
    # This will be 9, since there are 9 different temperatures
    print(len(all_experiments[0]))

    # Since we only have 1 dc field strength, we can remove the first dimension
    experiments = all_experiments[0]

We now have a list of :py:class:`Experiments <ccfit2.dc.Experiment>`, each specifying
a single decay experiment at a single DC field with different temperatures.

To prove this, lets plot the decays :py:class:`Experiment <ccfit2.dc.Experiment>`

.. code-block:: python

    # Plot all decays on one plot
    dc.plot_decays(experiments)

The connectivity of the points shows that ``ccfit2`` has correctly decided which
:py:class:`Measurements <ccfit2.dc.Measurement>` form an :py:class:`Experiment <ccfit2.dc.Experiment>`.

Now, lets take each :py:class:`Experiment <ccfit2.dc.Experiment>` and fit a stretched exponential
model to it. First, lets create a list of :py:class:`ExponentialModels <ccfit2.dc.ExponentialModel>`,
one per :py:class:`Experiment <ccfit2.dc.Experiment>`, where we specify that we want to fit the
:math:`\tau^*` and :math:`\beta` terms, but fix :math:`t_\mathrm{offset}=0`, :math:`M_\mathrm{eq}=0`,
and :math:`M_0` equal to the moment of the first measured point.

.. code-block:: python

    # Fit decays to exponential function

    model_to_use = dc.ExponentialModel

    # Fit parameters
    fit_vars = {
        'tau*': 'guess',
        'beta': 'guess'
    }
    # Fixed parameters
    fix_vars = {
        't_offset': 0.,
        'm_eq': 0.,
        'm_0': 'guess'
    }

    # Create models, one per experiment
    # specifying type of model, and which parameters will
    # be fitted and which are fixed
    models = [
        model_to_use(fit_vars, fix_vars, experiment)
        for experiment in experiments
    ]

Now we can use the :py:meth:`fit_to <ccfit2.dc.Model.fit_to>` method of
:py:class:`ExponentialModel <ccfit2.dc.ExponentialModel>` to carry out the fit, and then
plot the result

.. code-block:: python

    # For each model and accompanying experiment, fit model
    for model, experiment in zip(models, experiments):
        model.fit_to(experiment)

    # Plot decay and fit - just one, final iteration of loop
    dc.plot_fitted_decay(experiment=experiment, model=model, show=True)

Finally, we can save the model decays, and the parameters used to calculate them, to file

.. code-block:: python

    # Save parameters to file
    dc.write_model_params(models, file_name='dc_params.out')

    # Save modelled data to file
    dc.write_model_data(experiments, models, file_name='dc_model.out')
