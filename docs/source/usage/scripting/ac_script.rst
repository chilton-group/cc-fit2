.. _ac_script:


AC Susceptibilities
===================

This example reads in a magnetometer datafile containing a series of AC susceptibility experiments, and
fits the in-phase and out-of-phase susceptibilities to a model in order to extract the relaxation time and distribution.

The specific datafile used in this example can be found in the ``ccfit2`` repository
at ``examples/ac/example.ac.dat``, and the code detailed in this page at ``examples/ac/script_ac.py``.

First, load the data from file and create a series of :py:class:`Experiment <ccfit2.ac.Experiment>` objects. Each :py:class:`Experiment <ccfit2.ac.Experiment>` specifies a set of
datapoints recorded at the same temperature and applied DC field, with a range of AC frequencies. You can think of an :py:class:`Experiment <ccfit2.ac.Experiment>` as a single trace on a Cole-Cole plot. 

Here we specify the mass and molecular weight of the sample, since ``ccfit2`` converts susceptibility values in emu to cm :sup:`2` mol :sup:`-1`.

.. code-block:: python

    from ccfit2 import ac

    # Load experimental data - all measurements in a single output file
    # all_experiments is a list of lists of experiments with
    # first dimension as DC field, second as temperature
    # An experiment is set of datapoints with same DC field and temperature
    all_experiments = ac.Experiment.from_file('example.ac.dat', 50, 2000)

Here, ``ccfit2`` provides the :py:class:`Experiments <ccfit2.ac.Experiment>` as a list of lists, the first dimension specifying the
DC field strength, and the second specifying the temperature. In this case, all the datapoints have a
DC field strength of 0 Oe so the first dimension of the list has size 1, and the second has size 19, since we have
19 different temperatures. You can check this by printing

.. code-block:: python

    # This will be 1, since there is only 1 DC field strength for all measurements
    print(len(all_experiments))
    # This will be 19, since there are 19 different temperatures
    print(len(all_experiments[0]))

At this point, it's nice to check that we've read in the data correctly. So, let's visualise each :py:class:`Experiment <ccfit2.ac.Experiment>`
as a Cole-Cole plot. Since we have only one field value, we can collapse the first dimension of ``all_experiments``.

.. code-block:: python

    # Cut down first dimension, since there is only 1 DC field strength
    # for all measurements
    experiments = all_experiments[0]
    # Plot raw experimental data
    ac.plot_colecole(experiments)

You should see a Cole-Cole plot with some garish colours. By examining the connectivity of the points, we can tell 
that the data has been parsed correctly.

Now let's take each experiment and fit an AC susceptibility model to it. To do this, we'll first select a model and will then define
which variables of that model we want to fit (allow to vary freely), and which we want to fix (keep the same). We'll use the same type of model
for each experiment, in this case a :py:class:`Generalised Debye Model <ccfit2.ac.GeneralisedDebyeModel>` with all variables fitted.

.. code-block:: python

    # Name of model
    chosen_model = ac.GeneralisedDebyeModel

    # Variables to fit in Model
    # in this case, all variables are fitted
    fit_vars = {
        name: 'guess'
        for name in chosen_model.PARNAMES
    }
    # Variables to fix in Model
    # in this case, none are fixed
    fix_vars = {}

    # Make list of models, one per experiment
    # Here using same fit and fit variables for each
    models = [
        chosen_model(
            fit_vars, fix_vars, experiment
        )
        for experiment in experiments
    ]

We can then use our list of models and fit one to each corresponding experiment. We'll set the
initial parameters of our first experiment as a series of predefined `guess` values implemented
in ``ccfit2``, and then use our fitted values of this model as the initial parameters for the next model/experiment and so on.

.. code-block:: python

    # Fit model to experiment
    # Update each subsequent model's guess using the previous one's best fit
    # parameters.
    # For the first model, use its own parameters
    prev_fit = models[0].fit_vars
    for model, experiment in zip(models, experiments):

        # Update this model with previous guess
        for key in fit_vars.keys():
            model.fit_vars[key] = prev_fit[key]

        # Perform fit using previous guess
        model.fit_to(
            experiment
        )

        # If fit successful, update next guess with fitted params
        if model.fit_status:
            prev_fit = model.final_var_values
        # Else keep the current guess
        else:
            prev_fit = model.fit_vars

We've now fitted a Generalised Debye Model to each :py:class:`Experiment <ccfit2.ac.Experiment>`. The values of a given model can be
accessed using the ``final_var_values`` attribute. For example

.. code-block:: python

    # Print model variables and their standard deviations if fit was successful.
    # Then print standard deviation in ln(tau) instrinsic to the Generalised Debye
    # Model
    for model in models:
        if model.fit_status:
            print('Fit:', model.final_var_values)
            print('std_dev:', model.fit_stdev)
            print('sigma(ln(tau)):', model.lntau_stdev)
            print()

To visualise the results graphically we can again create a Cole-Cole plot, but this time with the fitted model overlayed.

.. code-block:: python

    # Plot experiment with fitted model on top
    ac.plot_fitted_colecole(experiments=experiments, models=models)

and we can then save our model susceptibility data (:math:`\chi^{'}(T)` and :math:`\chi^{''}(T)`) and our model
parameters to file.

.. code-block:: python

    # Save fitted model data (chi' and chi'') for current field
    ac.write_model_data(experiments, models)

    # Save fitted model parameters for current field
    ac.write_model_params(models)