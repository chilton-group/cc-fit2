AC
==

The :py:mod:`ac <ccfit2.ac>` Module contains functions and classes for manipulating alternating current (AC) susceptibility data.

Raw AC susceptibility data can be stored and manipulated using the :py:class:`Measurement <ccfit2.ac.Measurement>` and :py:class:`Experiment <ccfit2.ac.Experiment>` classes.

The :py:class:`Measurement <ccfit2.ac.Measurement>` class stores data for a single AC Susceptibility measurement at a
given temperature, given applied dc field, and given ac frequency.

The :py:class:`Experiment <ccfit2.ac.Experiment>` class stores data for multiple AC Susceptibility measurements at a
given temperature, given applied dc field, and multiple ac frequencies, and as such
can be considered as a *set* of :py:class:`Measurements <ccfit2.ac.Measurement>`.

These classes allow for sorting and grouping of data by temperature and field, and can be passed to the :py:class:`Model <ccfit2.ac.Model>` classes
for fitting to different models of AC susceptibility.

The :py:class:`Model <ccfit2.ac.Model>` class is an abstract base class used to define a given AC susceptibility model. Currently
the following models are implemented in ``ccfit2``, all of which use the :py:class:`Model <ccfit2.ac.Model>` base class.

These are

   * :py:class:`DebyeModel <ccfit2.ac.DebyeModel>`
   * :py:class:`GeneralisedDebyeModel <ccfit2.ac.GeneralisedDebyeModel>`
   * :py:class:`ColeDavidsonModel <ccfit2.ac.ColeDavidsonModel>`
   * :py:class:`HavriliakNegamiModel <ccfit2.ac.HavriliakNegamiModel>`
   * :py:class:`DoubleGDebyeModel <ccfit2.ac.DoubleGDebyeModel>`
   * :py:class:`DoubleGDebyeEqualChiModel <ccfit2.ac.DoubleGDebyeEqualChiModel>`

Owing to the use of an abstract base class, these have exactly the same methods and general functionality and so can be
used almost interchangeably. Models can be fitted to experiments with the :py:func:`Model.fit_to() <ccfit2.ac.Model.fit_to>` method.

Additionally, there are helper functions for plotting and saving AC data to file.

* :py:func:`plot_susceptibility <ccfit2.ac.plot_susceptibility>` - Plots raw AC Susceptibilities from list of :py:class:`Experiments <ccfit2.ac.Experiment>`
* :py:func:`plot_colecole <ccfit2.ac.plot_colecole>` - Plots raw Cole-Cole plots from list of :py:class:`Experiments <ccfit2.ac.Experiment>`

* :py:func:`plot_fitted_susceptibility <ccfit2.ac.plot_fitted_susceptibility>` - Plots fitted and experimental AC Susceptibilities from list of :py:class:`Experiments <ccfit2.ac.Experiment>` and list of :py:class:`Models <ccfit2.ac.Model>`
* :py:func:`plot_fitted_colecole <ccfit2.ac.plot_fitted_colecole>` - Plots fitted and experimental Cole-Cole plots from list of :py:class:`Experiments <ccfit2.ac.Experiment>` and list of :py:class:`Models <ccfit2.ac.Model>`

* :py:func:`write_model_params <ccfit2.ac.write_model_params>` - Saves every set of :py:class:`Model <ccfit2.ac.Model>` parameters to a single file.
* :py:func:`write_model_data <ccfit2.ac.write_model_data>` - Saves χ' and χ'' obtained using fitted :py:class:`Model <ccfit2.ac.Model>` at experimental temperatures.

All of the :py:class:`Measurements <ccfit2.ac.Measurement>`, :py:class:`Experiment <ccfit2.ac.Experiment>`, and :py:class:`Model <ccfit2.ac.Model>` classes in :py:mod:`ac <ccfit2.ac>` were designed to also be used
outside of ``ccfit2`` package and command line interface. As an example, see :ref:`ac_script`.

.. automodule:: ccfit2.ac
    :members:
