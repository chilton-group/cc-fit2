DC
==

The :py:mod:`dc <ccfit2.dc>` Module contains functions and classes for manipulating direct current (DC) magnetisation decay data.

Raw DC susceptibility data can be stored and manipulated using the :py:class:`Measurement <ccfit2.dc.Measurement>` and :py:class:`Experiment <ccfit2.dc.Experiment>` classes.

* :py:class:`Measurement <ccfit2.dc.Measurement>` - Single DC decay datapoint at single temperature, applied DC field, with given magnetic moment at a *single* point in time.
* :py:class:`Experiment <ccfit2.dc.Experiment>` - Collection of DC decay datapoints at single temperature, applied DC field, with magnetic moment at *multiple* points in time.

These classes allow for sorting and grouping of data by temperature and DC field, and can be passed to the :py:class:`Model <ccfit2.dc.Model>` classes
for fitting to different models of DC magnetisation decay.

The :py:class:`Model <ccfit2.dc.Model>` class is an abstract base class used to define a given DC magnetisation decay model. Currently
the following models are implemented in ``ccfit2``, all of which use the :py:class:`Model <ccfit2.dc.Model>` base class.

These are

   * :py:class:`ExponentialModel <ccfit2.dc.ExponentialModel>` - Single exponential model of magnetisation decay, with optional stretching parameters.
   * :py:class:`DoubleExponentialModel <ccfit2.dc.DoubleExponentialModel>` - Double exponential model of magnetisation decay, with optional stretching parameters.

Owing to the use of an abstract base class, these have exactly the same methods and general functionality and so can be used almost interchangeably.
Models can be fitted to experiments with the :py:func:`Model.fit_to() <ccfit2.dc.Model.fit_to>` method.

Additionally, there are helper functions for plotting and saving DC data to file.

* :py:func:`plot_decays_and_fields <ccfit2.dc.plot_decays_and_fields>` - Plots raw decays and field values from list of :py:class:`Experiments <ccfit2.dc.Experiment>`
* :py:func:`plot_decays <ccfit2.dc.plot_decays>` - Plots raw decays from list of :py:class:`Experiments <ccfit2.dc.Experiment>`
* :py:func:`plot_fitted_decay <ccfit2.dc.plot_fitted_decay>` - Plots fitted and experimental decays from list of :py:class:`Experiments <ccfit2.dc.Experiment>` and list of corresponding :py:class:`Models <ccfit2.dc.Model>`

* :py:func:`write_model_params <ccfit2.dc.write_model_params>` - Saves every set of :py:class:`Model <ccfit2.dc.Model>` parameters to a single file.
* :py:func:`write_model_data <ccfit2.dc.write_model_data>` - Saves magnetisation vs time obtained from fitted :py:class:`Model <ccfit2.dc.Model>` at experimental time and field values.

.. automodule:: ccfit2.dc
    :members:
