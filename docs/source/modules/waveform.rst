Waveform
========


The :py:mod:`waveform <ccfit2.waveform>` Module contains functions and classes for manipulating waveform data.

Raw waveform data can be stored and manipulated using the :py:class:`Measurement <ccfit2.waveform.Measurement>` and :py:class:`Experiment <ccfit2.waveform.Experiment>` classes.

* :py:class:`Measurement <ccfit2.waveform.Measurement>` - Single waveform datapoint at single temperature, applied DC field, with given magnetic moment at a *single* point in time.
* :py:class:`Experiment <ccfit2.waveform.Experiment>` - Collection of waveform datapoints at single temperature, with magnetic moment at *multiple* points in time and same DC field frequency.

These classes allow for sorting and grouping of data by temperature and DC field frequency, and can be used to construct a :py:class:`FTResult <ccfit2.waveform.FTResult>` object, 
which holds the result of Fourier transforming the moment and field data. 

The :py:class:`FTResult <ccfit2.waveform.FTResult>` object can then be used to generate an :py:class:`ac.Experiment <ccfit2.ac.Experiment>` object which
contains the susceptibility data extracted from the waveform measurements. 

Additionally, there is a helper function :py:func:`plot_waveform_data <ccfit2.waveform.plot_waveform_data>` for plotting the Fourier transform data stored in the :py:class:`FTResult <ccfit2.waveform.FTResult>` object.

.. automodule:: ccfit2.waveform
    :members:
