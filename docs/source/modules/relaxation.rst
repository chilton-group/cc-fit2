Relaxation
==========

Relaxation data can be stored and manipulated using the :py:class:`TDataset <ccfit2.relaxation.TDataset>` and :py:class:`HDataset <ccfit2.relaxation.HDataset>` classes, depending on
which type of relaxation profile is desired. These classes can be instantiated from ``ccfit2.ac`` and ``ccfit2.dc`` model parameter files containing the relevant relaxation rates and temperatures/fields, or simply from raw data.

The :py:class:`TDataset <ccfit2.relaxation.TDataset>` class stores relaxation rate (:math:`\tau^{-1}`) data as a function of temperature, optionally containing bounds on each relaxation rate
The :py:class:`HDataset <ccfit2.relaxation.HDataset>` class stores relaxation rate data as a function of DC field, optionally containing bounds on each relaxation rate
The :py:class:`HDataset <ccfit2.relaxation.HTDataset>` class stores relaxation rate data as a function of both DC field and temperature, optionally containing bounds on each relaxation rate

The :py:class:`LogTauTModel <ccfit2.relaxation.LogTauTModel>`, :py:class:`LogTauHModel <ccfit2.relaxation.LogTauHModel>`, and :py:class:`LogTauHTModel <ccfit2.relaxation.LogTauHTModel>` classes can then be used
to fit the data to a single relaxation rate model as function of temperature, field, or temperature and field.

Each of these is an abstract base classes used to define a given relaxation rate model.

Currently the following temperature-dependent models are implemented in ``ccfit2``, all of which use the :py:class:`LogTauTModel <ccfit2.relaxation.LogTauTModel>` base class.

   * :py:class:`LogOrbachModel <ccfit2.relaxation.LogOrbachModel>`
   * :py:class:`LogRamanModel <ccfit2.relaxation.LogRamanModel>`
   * :py:class:`LogPPDRamanModel <ccfit2.relaxation.LogPPDRamanModel>`
   * :py:class:`LogQTMModel <ccfit2.relaxation.LogQTMModel>`
   * :py:class:`LogDirectModel <ccfit2.relaxation.LogDirectModel>`
   * :py:class:`LogRamanPBModel <ccfit2.relaxation.LogRamanPBModel>`
   * :py:class:`LogRamanQTMPBModel <ccfit2.relaxation.LogRamanQTMPBModel>`

The following field-dependent models are implemented in ``ccfit2``, all of which use the :py:class:`LogTauHModel <ccfit2.relaxation.LogTauHModel>` base class.

   * :py:class:`LogFDQTMModel <ccfit2.relaxation.LogFDQTMModel>` - Field-Dependent QTM Model
   * :py:class:`LogRamanIIModel <ccfit2.relaxation.LogRamanIIModel>` - Raman-II Model
   * :py:class:`LogConstant <ccfit2.relaxation.LogConstant>` - Constant rate model
   * :py:class:`LogBVVRamanIIModel <ccfit2.relaxation.LogBVVRamanIIModel>` - Brons-Van-Vleck * Raman-II Model
   * :py:class:`LogBVVConstantModel <ccfit2.relaxation.LogBVVConstantModel>`- Brons-Van-Vleck * Constant Model

Finally, the following field- and temperature-dependent models are implemented in ``ccfit2``, all of which use the :py:class:`LogTauHTModel <ccfit2.relaxation.LogTauHTModel>` base class.

   * :py:class:`LogFTDQTMModel <ccfit2.relaxation.LogFDQTMModel>` - Field Dependent QTM Model (temperature-independent, implemented for completeness)
   * :py:class:`LogFTDRamanIIDirectModel <ccfit2.relaxation.LogFTDRamanIIDirectModel>` - Either a Raman-II or Direct Model (both have the same functional form)
   * :py:class:`LogFTDOrbachModel <ccfit2.relaxation.LogFTDOrbachModel>` - Orbach Model (field-independent, implemented for completeness)
   * :py:class:`LogFTDRamanIModel <ccfit2.relaxation.LogFTDRamanIModel>`- Raman-I Model (field-independent, implemented for completeness)
   * :py:class:`LogFTDPPDRamanIModel <ccfit2.relaxation.LogFTDPPDRamanIModel>`- Phonon Pair Driven Raman-I Model (field-independent, implemented for completeness)
   * :py:class:`LogFTDBVVRamanIModel <ccfit2.relaxation.LogFTDBVVRamanIModel>` - Brons-Van-Vleck Raman-I Model

If the user wishes to fit to more than one relaxation model, then either a :py:class:`MultiLogTauTModel <ccfit2.relaxation.MultiLogTauTModel>`, :py:class:`MultiLogTauHModel <ccfit2.relaxation.MultiLogTauHModel>`,
or :py:class:`MultiLogTauHTModel <ccfit2.relaxation.MultiLogTauHTModel>` can be used to fit the data to a sum models by simply providing the models they wish to fit to.
Models can be fitted to experiments with the ``fit_to()`` methods of the various ``LogTauModel`` and ``MultiLogTauModel`` classes.

Additionally, there are helper functions for plotting and saving relaxation rate data to file.

* :py:func:`plot_rates <ccfit2.relaxation.plot_rates>` - Plots experimental relaxation rate as a function of temperature *or* field using a dataset.
* :py:func:`plot_fitted_rates <ccfit2.relaxation.plot_fitted_rates>` - Plots fitted and experimental relaxation rate as a function of temperature *or* field using a model and dataset.
* :py:func:`plot_times <ccfit2.relaxation.plot_times>` - Plots experimental relaxation times as a function of inverse temperature *or* inverse field using a dataset.
* :py:func:`plot_fitted_times <ccfit2.relaxation.plot_fitted_times>` - Plots fitted and experimental relaxation time as a function of inverse temperature *or* field using a model and dataset.
* :py:func:`plot_tau_residuals <ccfit2.relaxation.plot_tau_residuals>` - Plots difference between fitted and experimental relaxation rate as a function of temperature *or* field using a model and dataset.

* :py:func:`write_model_params <ccfit2.relaxation.write_model_params>` - Saves set of model parameters to a single file.
* :py:func:`write_model_data <ccfit2.relaxation.write_model_data>` - Saves rate obtained using fitted model at experimental temperatures *or* fields.


.. automodule:: ccfit2.relaxation
    :members:
