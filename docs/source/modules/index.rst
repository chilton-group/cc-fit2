.. _modules:

Modules
=======

The individual modules of ``ccfit2`` are documented within these pages. Click a header for more information on a given module.

.. toctree::
   :maxdepth: 2

    Command Line Interface <cli>
    AC <ac>
    DC <dc>
    Relaxation <relaxation>
    Waveform <waveform>
    Gui <gui>
    Utils <utils>
    Stats <stats>
