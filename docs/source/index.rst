Home
====

.. toctree::
   Installation <installation>
   Configuration <configuration>
   What's New <whatsnew>
   Usage <usage/index>
   Theory <theory/index>
   Modules <modules/index>
   Contributing <contributing>
   FAQ <faq>
   Bugs <bugs>
   Authors and Citation <authors>
   License <https://gitlab.com/chilton-group/cc-fit2/-/blob/master/LICENSE>
   Repository <https://gitlab.com/chilton-group/cc-fit2/>
   :maxdepth: 3
   :caption: Contents:
   :hidden:


``ccfit2`` is a python package for working with magnetometry data.

Specifically, ``ccfit2`` is able to

1. Fit experimental AC magnetic susceptibility data and DC magnetisation decay data to known models to extract characteristic relaxation times and their uncertainties
2. Extract AC susceptibility data from DC Waveform measurements
3. Fit relaxation rate data to known phenomenological models as a function of either temperature or DC field

These pages contain the documentation for the ``ccfit2`` python package, and its associated command line interface and executable.

New users
^^^^^^^^^

First visit :ref:`installation` to see how to set up ``ccfit2``, then check out the :ref:`guide` pages for instructions on how to use ``ccfit2``.

Citing ccfit2
^^^^^^^^^^^^^

See the :ref:`citation` page for details.
