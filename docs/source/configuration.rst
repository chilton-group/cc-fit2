.. _Configuration:

Configuration
-------------

.. _env_var:


Environment Variables
^^^^^^^^^^^^^^^^^^^^^

Some default settings of ``ccfit2`` can be configured using the environment variables listed
in the Table below.

Note, these options only apply when ``ccfit2`` is used through its command line interface (CLI),
and in the case of custom fonts, users must have the specified font installed on their system.

Additionally, changing some of the environment variables used for plotting may lead
to **unsupported** and unexpected behaviour - you have been warned!

+-------------------------+---------------------------------------------------------+
| Variable Name           | Description                                             |
+=========================+=========================================================+
| ``ccfit2_csvdelimiter`` | Delimiter character used for csv creation               |
|                         |                                                         |
|                         | See :ref:`csv_files` for more information               |
+-------------------------+---------------------------------------------------------+
| ``ccfit2_fontname``     | Name of font to use in plots                            |
|                         |                                                         |
|                         |  Note this font must be installed on your system        |
+-------------------------+---------------------------------------------------------+
| ``ccfit2_fontsize``     | Size of font to use in plots                            |
+-------------------------+---------------------------------------------------------+
| ``ccfit2_plotformat``   | Extension/format to use for saved plots                 |
|                         |                                                         |
|                         | e.g. .png or .pdf                                       |
+-------------------------+---------------------------------------------------------+
| ``ccfit2_numthreads``   | Number of threads to use for parallelised file loading  |
|                         |                                                         |
|                         | Default: One less than the number of cores in your      |
|                         |                                                         |
|                         | system                                                  |
+-------------------------+---------------------------------------------------------+
| ``ccfit2_termcolor``    | If set (to any value) enables colour-coded output in    |
|                         |                                                         |
|                         | windows terminal                                        |
+-------------------------+---------------------------------------------------------+

The process for setting these variables depends on your platform

Mac and Linux users:

.. code-block:: bash

    export variablename=VALUE

e.g

.. code-block:: bash
    
    export ccfit2_plotformat=.png


Windows CMD (Command Prompt) users can set variables temporarily using

.. code-block:: doscon

    set variablename=

while Windows Powershell (PS) users can use

.. code-block:: doscon

    $env:variablename=

these will persist only for the current window. To set Windows environment variables permanently, use the system environment variable window.

Terminal output
^^^^^^^^^^^^^^^

Mac and Linux users will see a colour-coded terminal output. By default this is disabled for Windows users since ``CMD`` does
not support ASCII colour codes. If you are on Windows and are using an ASCII enabled terminal (e.g. Windows Terminal) you can
enable colour-coded output by setting the ``ccfit2_termcolor`` environment variable to any value in the system environment variable window.

.. _csv_files:

CSV Files
^^^^^^^^^

By default the comma ``,`` is used as the ``.csv`` output delimiter, but this can be changed using the ``ccfit2_csvdelimiter`` environment variable.

Note that when specifying a semicolon delimiter on Linux and Mac, you must escape the semicolon character, i.e.

.. code-block:: bash

    # Semicolon ;
    export ccfit2_csvdelimiter=\;
    # Comma ,
    export ccfit2_csvdelimiter=,

Windows Users can use the system environment variable window, or the commands in the :ref:`env_var` section.
Note that unlike Linux/Mac no escape character is required for the semi-colon - it is just ;.
