.. _relax-theory:

Relaxation Models
=================


The following relaxation rate :math:`\tau^{-1}` models are available in ``ccfit2``. These are either temperature- :math:`T` or
field- :math:`H` dependent.

Note that the relaxation times obtained from the AC and DC models supported by ``ccfit2`` are provided as
:math:`\langle \ln\left(\tau\right)\rangle`. To convert those values to the relaxation rates used below, use

.. math ::

    \tau^{-1} = \exp\left(-\langle \ln\left(\tau\right)\rangle\right)


Temperature-Dependent Models
----------------------------

Orbach
^^^^^^

.. math ::

    \tau^{-1}(T) = 10^{-A} \cdot \exp\left(-U_\mathrm{eff} / T\right)


.. math ::

    U_\mathrm{eff} &: \ \ \ \mathrm{K} \\
    T &: \ \ \ \mathrm{K} \\
    A &: \ \ \log_{10}\left[\mathrm{s}\right]
    
Raman-I
^^^^^^^

.. math ::

    \tau^{-1}(T) = 10^{R} \cdot T^{n}


.. math ::

    R &: \ \ \log_{10}\left[\mathrm{s}^{-1} \ \mathrm{K}^{-n}\right] \\
    T &: \ \ \ \mathrm{K} \\
    n &: \ \ \ \mathrm{Dimensionless}

Phonon Pair Driven Raman-I
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    \tau^{-1}(T) = 10^{R}\frac{\exp\left(\omega/T\right)}{\left[\exp\left(\omega/T\right)-1\right]^{2}}


.. math ::

    R &: \ \ \log_{10}\left[\mathrm{s}^{-1}\right] \\
    T &: \ \ \ \mathrm{K} \\
    \omega &: \ \ \ \mathrm{K}

QTM
^^^

This is temperature-independent, but has been implemented to work with temperature-dependent models in ``ccfit2``.

.. math ::

    \tau^{-1} = 10^{-Q}


.. math ::

    Q : \ \ \log_{10}\left[\mathrm{s}\right]

Direct
^^^^^^

.. math ::

    \tau^{-1}(T) = 10^{D} \cdot T


.. math ::

    D &: \ \ \log_{10}\left[\mathrm{s}^{-1} \mathrm{K}^{-1}\right] \\
    T &: \ \ \ \mathrm{K} \\

Phonon-Bottlenecked Temperature-Dependent Models
------------------------------------------------

These models are only available when using ``ccfit2`` in a python script/program.

Raman-I (Phonon-Bottlenecked)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    \tau_{\mathrm{spin-lattice}} = \left(10^{R} \cdot T^{n}\right)^{-1}\\
    \tau_{\mathrm{bath}} = 10^{B} \cdot T^{-m}

    \tau^{-1}(T) = \left(\tau_{\mathrm{spin-lattice}} + \tau_{\mathrm{bath}}\right)^{-1}

.. math ::

    R &: \ \ \log_{10}\left[\mathrm{s}^{-1} \ \mathrm{K}^{-n}\right] \\
    T &: \ \ \ \mathrm{K} \\
    n &: \ \ \ \mathrm{Dimensionless}\\
    B &: \ \ \log_{10}\left[\mathrm{s} \ \mathrm{K}^{-m}\right]\\
    m &: \ \ \ \mathrm{Dimensionless}

Raman-I + QTM (Phonon-Bottlenecked)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    \tau_{\mathrm{spin-lattice}} = \left(10^{R} \cdot T^{n} + 10^{-Q}\right)^{-1}\\
    \tau_{\mathrm{bath}} = 10^{B} \cdot T^{-m}

    \tau^{-1}(T) = \left(\tau_{\mathrm{spin-lattice}} + \tau_{\mathrm{bath}}\right)^{-1}


.. math ::

    R &: \ \ \log_{10}\left[\mathrm{s}^{-1} \ \mathrm{K}^{-n}\right] \\
    T &: \ \ \ \mathrm{K} \\
    n &: \ \ \ \mathrm{Dimensionless}\\
    Q &: \ \ \log_{10}\left[\mathrm{s}\right]\\
    B &: \ \ \log_{10}\left[\mathrm{s} \ \mathrm{K}^{-m}\right]\\
    m &: \ \ \ \mathrm{Dimensionless}


Direct (Phonon-Bottlenecked)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

.. math ::

    \tau_{\mathrm{spin-lattice}} = \left(10^{D} \cdot T\right)^{-1}\\
    \tau_{\mathrm{bath}} = 10^{B} \cdot T^{-m}

    \tau^{-1}(T) = \left(\tau_{\mathrm{spin-lattice}} + \tau_{\mathrm{bath}}\right)^{-1}

.. math ::

    D &: \ \ \log_{10}\left[\mathrm{s}^{-1} \mathrm{K}^{-1}\right] \\
    T &: \ \ \ \mathrm{K} \\
    B &: \ \ \log_{10}\left[\mathrm{s} \ \mathrm{K}^{-m}\right]\\
    m &: \ \ \ \mathrm{Dimensionless}

Field-Dependent Models
----------------------

QTM
^^^

.. math ::

    \tau^{-1}(H) = \frac{10^{-Q}}{1 + 10^{-Q_\mathrm{H}} H^{p}}


.. math ::

    Q &: \ \ \log_{10}\left[\mathrm{s}\right]\\
    Q_\mathrm{H} &: \ \ \log_{10}\left[\mathrm{Oe}^{p}\right]\\
    p &: \ \ \ \mathrm{Dimensionless}\\
    H &: \ \ \ \mathrm{Oe}

Raman-II
^^^^^^^^

.. math ::

    \tau^{-1}(H) = 10^{C} \cdot H^{m}


.. math ::

    C &: \ \ \log_{10}\left[\mathrm{s}^{-1} \ \mathrm{Oe}^{-m}\right]\\
    m &: \ \ \ \mathrm{Dimensionless}\\
    H &: \ \ \ \mathrm{Oe}

Constant
^^^^^^^^

This is field-independent, but has been implemented to work with field-dependent models in ``ccfit2``.

.. math ::

    \tau^{-1} = 10^{C_\mathrm{t}}


.. math ::

    C_\mathrm{t} &: \ \ \log_{10}\left[\mathrm{s}^{-1}\right]\\

Brons-Van-Vleck and Raman-II
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    \tau^{-1}(H) = \frac{1 + 10^e H^2}{1 + 10^f H^2} \cdot 10^{C} \cdot H^{m}


.. math ::

    e &: \ \ \log_{10}\left[\mathrm{Oe}^{-2}\right]\\
    f &: \ \ \log_{10}\left[\mathrm{Oe}^{-2}\right]\\
    C &: \ \ \log_{10}\left[\mathrm{s}^{-1} \ \mathrm{Oe}^{-m}\right]\\
    m &: \ \ \ \mathrm{Dimensionless}\\
    H &: \ \ \ \mathrm{Oe}

Brons-Van-Vleck and Constant
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    \tau^{-1}(H) = \frac{1 + 10^e H^2}{1 + 10^f H^2} \cdot 10^{C_\mathrm{t}}


.. math ::

    e &: \ \ \log_{10}\left[\mathrm{Oe}^{-2}\right]\\
    f &: \ \ \log_{10}\left[\mathrm{Oe}^{-2}\right]\\
    C_\mathrm{t} &: \ \ \log_{10}\left[\mathrm{s}^{-1}\right]\\
    H &: \ \ \ \mathrm{Oe}


Field- and Temperature-Dependent Models
---------------------------------------

These models are only available when using ``ccfit2`` in a python script/program.

Raman-II/Direct
^^^^^^^^^^^^^^^

Since the functional form of field- and temperature-dependent Direct
and Raman-II terms is the same, this model can be used for either.

.. math ::

    \tau^{-1}(H,T) = 10^g \cdot H^x \cdot T^y

.. math ::

    g &: \ \ \log_{10}\left[\mathrm{Oe}^{-x} \ \mathrm{K}^{-y} \ \mathrm{s}^{-1}\right]\\
    x &: \ \ \ \mathrm{Dimensionless}\\
    y &: \ \ \ \mathrm{Dimensionless}\\
    H &: \ \ \ \mathrm{Oe}\\
    T &: \ \ \ \mathrm{K}

Brons-Van-Vleck Raman-I
^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    \tau^{-1}(H,T) = \frac{1 + 10^e H^2}{1 + 10^f H^2} \cdot 10^{R} \cdot T^{n}


.. math ::

    e &: \ \ \log_{10}\left[\mathrm{Oe}^{-2}\right]\\
    f &: \ \ \log_{10}\left[\mathrm{Oe}^{-2}\right]\\
    R &: \ \ \log_{10}\left[\mathrm{s}^{-1} \ \mathrm{K}^{-n}\right] \\
    n &: \ \ \ \mathrm{Dimensionless}\\
    H &: \ \ \ \mathrm{Oe}\\
    T &: \ \ \ \mathrm{K}


Raman-I
^^^^^^^

This is field-independent but has also been implemented
to work with simultaneously field- and temperature-dependent models in ``ccfit2``.

The definition is exactly the same as the temperature-dependent Raman-I model.

Phonon Pair Driven Raman-I
^^^^^^^^^^^^^^^^^^^^^^^^^^

This is field-independent but has also been implemented
to work with simultaneously field- and temperature-dependent models in ``ccfit2``.

The definition is exactly the same as the temperature-dependent Phonon Pair Driven Raman-I model.

Orbach
^^^^^^

This is field-independent but has also been implemented
to work with simultaneously field- and temperature-dependent models in ``ccfit2``.

The definition is exactly the same as the temperature-dependent Orbach model.

QTM
^^^

This is temperature-independent but has also been implemented
to work with simultaneously field- and temperature-dependent models in ``ccfit2``.

The definition is exactly the same as the field-dependent QTM model.

.. _relax-fit :

Fitting Procedure
-----------------

When fitting one of the above models to a set of relaxation rates, ``ccfit2`` uses the ``least_squares`` function provided
by `scipy.optimise <https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html>`_. In particular, ``ccfit`` uses the
Trust Region Reflective algorithm with bounds on each parameter specified in :py:attr:`Model.BOUNDS <ccfit2.relaxation.LogTauHModel.BOUNDS>`, and modifies each parameter such that
a vector of residuals with elements

.. math ::

    r_i =  \log\left(\tau^{-1}_{\mathrm{exp.}, i}\right) - \log\left(\tau^{-1}_{\mathrm{calc.}, i}\right)

is minimised. Where :math:`i` runs over the individual field or temperature values of the experiment, and :math:`\tau^{-1}_{\mathrm{calc.}}` is
calculated using one of the aforementioned models.

If the uncertainty in :math:`\log\left(\tau^{-1}_{\mathrm{exp.}, i}\right)` is known (as is the case when using ``ccfit2 ac`` and ``ccfit2 dc``) then this residual is modified to include a set of weights defined by the standard deviation
of :math:`\log\left(\tau^{-1}_{\mathrm{exp.}, i}\right)`

.. math ::

    r_i =  \frac{\log\left(\tau^{-1}_{\mathrm{exp.}, i}\right) - \log\left(\tau^{-1}_{\mathrm{calc.}, i}\right)}{\sigma_{\log\left(\tau^{-1}_{\mathrm{exp.}, i}\right)}}

When using the results of ``ac`` and ``dc``, we define :math:`\sigma_{\log\left(\tau^{-1}_{\mathrm{exp.}, i}\right)}` using either :math:`\sigma_{\ln\left(\tau\right)}`
obtained from the ``ac`` or ``dc`` model distribution or the upper and lower bounds of :math:`\sigma_{\log\left(\tau^{-1}_{\mathrm{exp.}, i}\right)}` caused by the uncertainty in the
parameters used in its calculation, whichever is largest. For more information, see :ref:`below <weight-definition>`.


The standard deviation of each fitted parameter of the relaxation model is calculated using by first performing a singular value decomposition (SVD)
of the Jacobian :math:`\mathbf{J}` computed at the optimum set of parameters.

.. math ::

    \mathbf{U}\mathbf{\Sigma}\mathbf{V}^\mathrm{T} = \mathbf{J}

where :math:`\mathbf{\Sigma}` is diagonal.

The covariance matrix :math:`\mathbf{K}` can then be calculated as 

.. math ::

    \mathbf{V}^\mathrm{T}\mathbf{\Sigma}^{-2}\mathbf{V} = \mathbf{K}

which is then rescaled by the reduced chi-squared

.. math ::
    
    \chi_v^2 &= \frac{\mathrm{RSS}}{N + k}\\
    &\\
    \mathbf{\tilde{K}} &=  \mathbf{K} \cdot \chi_v^2

where :math:`\mathrm{RSS}` is the residual sum of squares at the optimum set of parameters, and :math:`N` and :math:`k` are the number of data points
and (fitted) variables in the model, respectively. This scaling ensures that the standard deviation of each parameter, defined for the :math:`i^\mathrm{th}` parameter as

.. math ::
    \sigma_i = \sqrt{\tilde{K}_{ii}} \ \ ,

measures only the quality of the fit obtained by ``least_squares``.

Previous versions of ``ccfit2<5.0.0`` did not apply this rescaling, and so quality of the fit is incorrectly defined as a much larger value.

The SVD allows for the identification of parameters which have no influence on the quality of the fit, since these correspond to
zero-values on the diagonal of :math:`\mathbf{\Sigma}`. The standard deviation of these parameters is incalculable, and is instead set to zero in ``ccfit2``.

.. _weight-definition:

Weight definition
^^^^^^^^^^^^^^^^^

To obtain the weights used in the above fitting procedure, we first define the upper and lower bounds of :math:`\tau` using
:math:`\langle \ln\left(\tau\right)\rangle` and :math:`\sigma_{\ln\left(\tau\right)}`, and :math:`\langle \ln\left(\tau\right)\rangle_\pm`

.. math ::
    \tau_\mathrm{u} &= \mathrm{Max.}\left[\exp\left(\langle \ln\left(\tau\right)\rangle + \sigma_{\ln\left(\tau\right)}\right), \exp\left(\langle \ln\left(\tau\right)\rangle_+\right)\right]\\
    \tau_\mathrm{l} &= \mathrm{Min.}\left[\exp\left(\langle \ln\left(\tau\right)\rangle - \sigma_{\ln\left(\tau\right)}\right), \exp\left(\langle \ln\left(\tau\right)\rangle_-\right)\right]\\

where :math:`\langle \ln\left(\tau\right)\rangle_+` and :math:`\langle \ln\left(\tau\right)\rangle_-` are defined in
:ref:`AC<ac_lu_bounds>` and :ref:`DC<dc_lu_bounds>`.

We then use :math:`\tau_\mathrm{u}` and :math:`\tau_\mathrm{l}` to define :math:`\sigma_{\log\left(\tau^{-1}_{\mathrm{exp.}, i}\right)}`

.. math ::

    \sigma_{\log\left(\tau^{-1}_{\mathrm{exp.}, i}\right)} = 
    \mathrm{Max.} \begin{bmatrix}
        \log_{10}\left(\tau^{-1}\right) - \log_{10}\left(\tau_\mathrm{u}^{-1}\right)\\
        \log_{10}\left(\tau_\mathrm{l}^{-1}\right) - \log_{10}\left(\tau^{-1}\right)\\
    \end{bmatrix}

where :math:`\tau^{-1} = \exp\left(-\langle\ln\left(\tau\right)\rangle\right)`

.. footbibliography::
