Theory
======

These pages contain a brief description of the models implemented in ``ccfit2``, including the approach
used for the statistical analysis of various parameters.


.. toctree::
   :maxdepth: 2

    AC Susceptibility <ac_susceptibility>
    DC Decays <dc_decays>
    Relaxation models <relaxation>
