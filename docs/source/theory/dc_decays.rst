.. _dc-theory:

DC Decays
=========

Models
------

This page details the models of direct current (DC) magnetisation decay available in ``ccfit2``.

The quantities :math:`\langle \ln\left(\tau\right)\rangle` and :math:`\sigma_{\ln\left(\tau\right)}` are
defined by :footcite:t:`zorn_logarithmic_2002` as the logarithm of the characteristic relaxation time,
and the standard deviation of the logarithm of the characteristic relaxation time inherent to a given model, respectively.

:math:`\sigma_{\ln\left(\tau\right)}` can applied to a given :math:`\langle \ln\left(\tau\right)\rangle` as

.. math ::

    \langle \ln\tau \rangle \pm \sigma_{\ln\tau}

Stretched Exponential Model
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    M(t) &= M_\mathrm{eq} + \left(M_0-M_\mathrm{eq}\right) \exp\left[-\left(\frac{t - t_\mathrm{offset}}{\tau^*}\right)^\beta\right]\\
    &\\
    \langle \ln\tau \rangle &= \left(1-\frac{1}{\beta}\right)\mathrm{Eu} + \ln \tau^* \\
    &\\
    \sigma_{\ln\tau} &= \sqrt{\left(\frac{1}{\beta^2} - 1\right) \frac{\pi^2}{6}}


Where :math:`t` is time in seconds. The parameters :math:`M_\mathrm{eq}`, :math:`M_0`, :math:`t_\mathrm{offset}`, :math:`\tau^*`, and :math:`\beta` are all
variables of the model which, if fitted, will each have an associated uncertainty :math:`\sigma` that describes the quality of fit for that parameter. The value :math:`\mathrm{Eu}` is the `Euler-Gamma <https://en.wikipedia.org/wiki/Euler%27s_constant>`_ constant.

N.B. a 'normal', non-stretched, Exponential function can be obtined by fixing :math:`\beta` to unity.

The following constraints apply

.. math ::

    0 \leq &\beta \leq 1\\
    &\\
    t_\mathrm{offset} \gt 0  \ \ \ \ \ \ \ \ &  \ \ \ \ \ \ \ \ M_\mathrm{0} \gt 0\\

Double Exponential Model
^^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    M(t) = \ M_\mathrm{eq} + \ &\left(M_0 - M_\mathrm{eq}\right)\\
    &\times \left(A\cdot\exp\left[-\left(\frac{t - t_\mathrm{offset}}{\tau_1^*}\right)^{\beta_1} \right]+ \left(1-A\right)\cdot\exp\left[-\left(\frac{t - t_\mathrm{offset}}{\tau_2^*}\right)^{\beta_2}\right]\right)\\  
    &\\
    \langle \ln\tau_1 \rangle &= \left(1-\frac{1}{\beta_1}\right)\mathrm{Eu} + \ln \tau_1^* \ \ \ \ \ \ \ \  \langle \ln\tau_2 \rangle = \left(1-\frac{1}{\beta_2}\right)\mathrm{Eu} + \ln \tau_2^* \\
    &\\
    \sigma_{\ln\tau_1} &= \sqrt{\left(\frac{1}{\beta_1^2} - 1\right) \frac{\pi^2}{6}} \ \ \ \ \ \ \ \ \sqrt{\sigma_{\ln\tau_2} = \left(\frac{1}{\beta_2^2} - 1\right) \frac{\pi^2}{6}}\\
    &\\
    


Where :math:`t` is time in seconds. The parameters :math:`M_\mathrm{eq}`, :math:`M_0`, :math:`t_\mathrm{offset}`, :math:`\tau_1^*`, :math:`\tau_2^*`, :math:`\beta_1`, :math:`\beta_2` and :math:`A` are all
variables of the model which, if fitted, will each have an associated uncertainty :math:`\sigma` that describes the quality of fit for that parameter. The value :math:`\mathrm{Eu}` is the `Euler-Gamma <https://en.wikipedia.org/wiki/Euler%27s_constant>`_ constant.

N.B. One or two 'normal', non-stretched, Exponential functions can be obtined by fixing :math:`\beta_1` and/or :math:`\beta_2` to unity.

Where the following constraints apply

.. math ::

    0 \leq \beta_1 \leq 1 \ \ \ \ \ \ \ \ 0 \leq &\beta_2 \leq 1 \ \ \ \ \ \ \ \ 0 \leq A \leq 1\\
    &\\
    t_\mathrm{offset} \gt 0  \ \ \ \ \ \ \ \ &  \ \ \ \ \ \ \ \ M_\mathrm{0} \gt 0\\


Fitting Procedure
-----------------

When fitting one of the above models to experimental magnetisation decay data, ``ccfit2`` uses the ``least_squares`` function provided
by `scipy.optimise <https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html>`_. In particular, ``ccfit`` uses the
Trust Region Reflective algorithm with bounds on each parameter specified in :py:attr:`Model.BOUNDS <ccfit2.dc.Model.BOUNDS>`, and modifies each parameter such that
a vector of residuals with elements

.. math ::

    r_i =  M^\mathrm{exp.}_{i} - M^\mathrm{calc.}_{i}

is minimised. Where :math:`i` runs over the individual time values of the experiment, and :math:`M^\mathrm{calc.}` is calculated using one of the aforementioned models.

Subsequently, the standard deviation of each fitted parameter of the model is calculated using by first performing a singular value decomposition (SVD)
of the Jacobian :math:`\mathbf{J}` computed at the optimum set of parameters.

.. math ::

    \mathbf{U}\mathbf{\Sigma}\mathbf{V}^\mathrm{T} = \mathbf{J}

where :math:`\mathbf{\Sigma}` is diagonal.

The covariance matrix :math:`\mathbf{K}` can then be calculated as 

.. math ::

    \mathbf{V}^\mathrm{T}\mathbf{\Sigma}^{-2}\mathbf{V} = \mathbf{K}

which is then rescaled by the reduced chi-squared

.. math ::
    
    \chi_v^2 &= \frac{\mathrm{RSS}}{N + k}\\
    &\\
    \mathbf{\tilde{K}} &=  \mathbf{K} \cdot \chi_v^2

where :math:`\mathrm{RSS}` is the residual sum of squares at the optimum set of parameters, and :math:`N` and :math:`k` are the number of data points
and (fitted) variables in the model, respectively. This scaling ensures that the standard deviation of each parameter, defined for the :math:`i^\mathrm{th}` parameter as

.. math ::
    \sigma_i = \sqrt{\tilde{K}_{ii}} \ \ ,

measures only the quality of the fit obtained by ``least_squares``.

The SVD allows for the identification of parameters which have no influence on the quality of the fit, since these correspond to
zero-values on the diagonal of :math:`\mathbf{\Sigma}`. The standard deviation of these parameters is incalculable, and is instead set to zero in ``ccfit2``.


.. _dc_lu_bounds:

Upper and Lower bounds of :math:`\langle \ln\left(\tau\right)\rangle`
---------------------------------------------------------------------

Additionally, ``ccfit2`` calculates upper and lower bounds of :math:`\langle \ln\left(\tau\right)\rangle` caused by the uncertainty in the parameters used in its calculation.

If we denote these bounds :math:`\langle \ln\left(\tau\right)\rangle_\pm`, then for the Stretched Exponential model this is trivial

.. math ::

    \langle \ln\left(\tau\right)\rangle_\pm = \left(1-\frac{1}{\beta \mp \sigma_{\beta}}\right)\mathrm{Eu} + \ln \left(\tau^* \pm \sigma_{\tau^*}\right)

where :math:`\sigma_{\tau^*}` is the uncertainty associated with :math:`\tau^*` obtained from the least squares fit
algorithm.

For the Double Exponential model this is replicated for each species.

.. math ::

    \langle \ln\left(\tau_{1} \right)\rangle_\pm = \left(1-\frac{1}{\beta_{1} \mp \sigma_{\beta_{1}}}\right)\mathrm{Eu} + \ln \left(\tau_{1}^* \pm \sigma_{\tau_{1}^*}\right)\\
    \\
    \langle \ln\left(\tau_{2} \right)\rangle_\pm = \left(1-\frac{1}{\beta_{2} \mp \sigma_{\beta_{2}}}\right)\mathrm{Eu} + \ln \left(\tau_{2}^* \pm \sigma_{\tau_{2}^*}\right)

.. footbibliography::
