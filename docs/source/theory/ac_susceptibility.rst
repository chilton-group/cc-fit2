.. _ac-theory:

AC Susceptibility
=================

Models
------

This page details the models of alternating current (AC) magnetic susceptibility available in ``ccfit2``.

The AC susceptibility is a complex quantity

.. math ::

    \chi_\mathrm{AC} = \chi^\prime + i\chi^{\prime\prime}

where the real :math:`\chi^\prime` and imaginary :math:`\chi^{\prime\prime}` parts are named the in- and out-of-phase susceptibility respectively.

The susceptibility varies as a function of the (angular) wave frequency :math:`\omega` of the applied magnetic field.
This variation can be fitted to a given model to extract the characteristic relaxation time :math:`\tau` of the sample.

In ``ccfit2``, the logarithm of the characteristic relaxation time :math:`\langle \ln\left(\tau\right)\rangle` is used,
since for each model below, an expression for this quantity's standard deviation :math:`\sigma_{\ln\left(\tau\right)}` has been
defined by :footcite:t:`zorn_logarithmic_2002`.

:math:`\sigma_{\ln\left(\tau\right)}` can applied to a given :math:`\langle \ln\left(\tau\right)\rangle` as

.. math ::

    \langle \ln\tau \rangle \pm \sigma_{\ln\tau}

Debye Model
^^^^^^^^^^^

.. math ::

    \chi_\mathrm{AC}(\omega) &= \chi_\mathrm{S} + \frac{\chi_\mathrm{T} - \chi_\mathrm{S}}{1 + i\omega\tau_\mathrm{D}}\\
    &\\
    \langle \ln\left(\tau\right)\rangle &= \ln(\tau_\mathrm{D})\\
    &\\
    \sigma_{\ln\left(\tau\right)} &= 0

Where :math:`\omega` is angular frequency in units of Hz.
The parameters :math:`\chi_\mathrm{T}`, :math:`\chi_\mathrm{S}`, and :math:`\tau_\mathrm{D}` are variables of the model which, if fitted
will each have an associated uncertainty :math:`\sigma` that describes the quality of fit for that parameter.

Generalised Debye Model
^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    \chi_\mathrm{AC}(\omega) &= \chi_\mathrm{S} + \frac{\chi_\mathrm{T} - \chi_\mathrm{S}}{1 + (i\omega\tau_\mathrm{GD})^{1-\alpha}}\\
    &\\
    \langle \ln\left(\tau\right)\rangle &= \ln(\tau_\mathrm{GD})\\
    &\\
    \sigma_{\ln\left(\tau\right)} &= \sqrt{\left(\frac{1}{(1-\alpha)^2} - 1\right)\frac{\pi^2}{3}}

Where :math:`\omega` is angular frequency in units of Hz.
The parameters :math:`\chi_\mathrm{T}`, :math:`\chi_\mathrm{S}`, :math:`\alpha`, and :math:`\tau_\mathrm{GD}` are variables of the model which, if fitted
will each have an associated uncertainty :math:`\sigma` that describes the quality of fit for that parameter.

The :math:`\alpha` parameter satisfies

.. math ::

    0 \leq \alpha \leq 1

Cole-Davidson Model
^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    \chi_\mathrm{AC}(\omega) &= \chi_\mathrm{S} + \frac{\chi_\mathrm{T} - \chi_\mathrm{S}}{(1 + i\omega\tau_\mathrm{CD})^{1-\gamma}}\\
    &\\
    \langle \ln\left(\tau\right)\rangle &= \ln(\tau_\mathrm{CD}) + \psi(\gamma)+\mathrm{Eu} \\
    &\\
    \sigma_{\ln\left(\tau\right)} &= \sqrt{\psi'(\gamma)-\frac{\pi^2}{6}}

Where :math:`\omega` is angular frequency in units of Hz. The value :math:`\mathrm{Eu}` is the `Euler-Gamma <https://en.wikipedia.org/wiki/Euler%27s_constant>`_ constant., :math:`\psi(x)` is the digamma function, and :math:`\psi'(x)` the trigamma function.
The parameters :math:`\chi_\mathrm{T}`, :math:`\chi_\mathrm{S}`, :math:`\gamma`, and :math:`\tau_\mathrm{CD}` are variables of the model which, if fitted
will each have an associated uncertainty :math:`\sigma` that describes the quality of fit for that parameter.

The :math:`\gamma` parameter satisfies

.. math ::

    0 \leq \gamma \leq 1

Havriliak-Negami Model
^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    \chi_\mathrm{AC}(\omega) &= \chi_\mathrm{S} + \frac{\chi_\mathrm{T} - \chi_\mathrm{S}}{\left(1 + (i\omega\tau_\mathrm{HN})^{1-\alpha}\right)^\gamma}\\
    &\\
    \langle \ln\left(\tau\right)\rangle &= \ln(\tau_\mathrm{HN}) + \frac{\psi(\gamma)+\mathrm{Eu}}{1 - \alpha}\\
    &\\
    \sigma_{\ln\left(\tau\right)} &= \sqrt{\frac{\psi'(\gamma)}{(1 - \alpha)^2} + \frac{\pi^2}{6(1 - \alpha)^2} - \frac{\pi^2}{3}}

Where :math:`\omega` is angular frequency in units of Hz. The value :math:`\mathrm{Eu}` is the `Euler-Gamma <https://en.wikipedia.org/wiki/Euler%27s_constant>`_ constant., :math:`\psi(x)` is the digamma function, and :math:`\psi'(x)` the trigamma function.
The parameters :math:`\chi_\mathrm{T}`, :math:`\chi_\mathrm{S}`, :math:`\alpha`, :math:`\gamma`, and :math:`\tau_\mathrm{HN}` are variables of the model which, if fitted
will each have an associated uncertainty :math:`\sigma` that describes the quality of fit for that parameter.

The :math:`\alpha` and :math:`\gamma` parameters satisfy

.. math ::

    0 \leq &\alpha \leq 1\\
    0 \leq &\gamma \cdot \left(1-\alpha\right) \leq 1

Double Generalised Debye Model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    \chi_\mathrm{AC}(\omega) &= \chi_{\mathrm{Total}} + \frac{\Delta \chi_1}{1 + (i\omega\tau_{1\mathrm{GD}})^{1-\alpha_1}} + \frac{\Delta \chi_2}{1 + (i\omega\tau_{2\mathrm{GD}})^{1-\alpha_2}}\\
    &\\
    \langle \ln\left(\tau_1\right)\rangle &= \ln(\tau_{1\mathrm{GD}}) \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \langle \ln\left(\tau_2\right)\rangle = \ln(\tau_{2\mathrm{GD}})\\    
    &\\
    \sigma_{\ln\left(\tau_1\right)} &= \sqrt{\left(\frac{1}{(1-\alpha_{1})^2} - 1\right)\frac{\pi^2}{3}} \ \ \ \ \ \ \ \ \sigma_{\ln\left(\tau_2\right)} = \sqrt{\left(\frac{1}{(1-\alpha_{2})^2} - 1\right)\frac{\pi^2}{3}}
    

Where :math:`\omega` is angular frequency in units of Hz and 

.. math ::

    \chi_{\mathrm{Total}} = \chi_{1\mathrm{S}} + \chi_{2\mathrm{S}} \ \ \ \ \ \ \ \ \Delta \chi_{1} = \chi_{1\mathrm{T}} -\chi_{1\mathrm{S}} \ \ \ \ \ \ \ \ \Delta \chi_{2} = \chi_{2\mathrm{T}} - \chi_{2\mathrm{S}}

The parameters :math:`\chi_{\mathrm{Total}}`, :math:`\Delta \chi_1`, :math:`\Delta \chi_2`, :math:`\alpha_1`, :math:`\alpha_2`, :math:`\tau_{1\mathrm{GD}}` and :math:`\tau_{2\mathrm{GD}}` are variables of the model which, if fitted
will each have an associated uncertainty :math:`\sigma` that describes the quality of fit for that parameter.

The :math:`\alpha_1` and :math:`\alpha_2` parameters satisfy

.. math ::

    0 \leq \alpha_1 \leq 1 \ \ \ \ \ \ \ \  0 \leq \alpha_2 \leq 1


Double Generalised Debye (Equal χ)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. math ::

    \chi_\mathrm{AC}(\omega) &= \chi_{\mathrm{S}} + \frac{1}{2}\left(\frac{\chi_{\mathrm{T}} - \chi_{\mathrm{S}}}{1 + (i\omega\tau_{1\mathrm{GD}})^{1-\alpha}} + \frac{\chi_{\mathrm{T}} - \chi_{\mathrm{S}}}{1 + (i\omega\tau_{2\mathrm{GD}})^{1-\alpha}}\right)\\
    &\\
    \langle \ln\left(\tau_1\right)\rangle &= \ln(\tau_{1\mathrm{GD}}) \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \langle \ln\left(\tau_2\right)\rangle = \ln(\tau_{2\mathrm{GD}})\\    
    &\\
    \sigma_{\ln\left(\tau_1\right)} &= \sigma_{\ln\left(\tau_2\right)} = \sqrt{\left(\frac{1}{(1-\alpha)^2} - 1\right)\frac{\pi^2}{3}}
    

Where :math:`\omega` is angular frequency in units of Hz. This model differs from the Double Generalised Debye model insofar that the material has two components of equal proportion, differing only in their
characteristic relaxation times :math:`\tau_{1\mathrm{GD}}, \tau_{2\mathrm{GD}}`, i.e.

.. math ::

    \chi_{\mathrm{S}} = \chi_{1\mathrm{S}} = \chi_{2\mathrm{S}} \ \ \ \ \ \ \ \ \chi_{\mathrm{T}} &= \chi_{1\mathrm{T}} = \chi_{2\mathrm{T}} \ \ \ \ \ \ \ \ \alpha = \alpha_1 = \alpha_2\\

For more information, see :footcite:t:`reta_hidden_2020`.

The parameters :math:`\chi_{\mathrm{S}}`, :math:`\chi_{\mathrm{T}}`, :math:`\alpha`, :math:`\tau_{1\mathrm{GD}}` and :math:`\tau_{2\mathrm{GD}}` are variables of the model which, if fitted
will each have an associated uncertainty :math:`\sigma` that describes the quality of fit for that parameter.

The :math:`\alpha` parameter satisfies

.. math ::

    0 \leq \alpha \leq 1

Fitting Procedure
-----------------

When fitting one of the above models to experimental susceptibility data, ``ccfit2`` uses the ``least_squares`` function provided
by `scipy.optimise <https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html>`_. In particular, ``ccfit`` uses the
Trust Region Reflective algorithm with bounds on each parameter specified in :py:attr:`Model.BOUNDS <ccfit2.ac.Model.BOUNDS>`, and modifies each parameter such that
a vector of residuals :math:`\mathbf{r}` with elements

.. math ::

    r'_j &=  {\chi'}^\mathrm{exp.}_{j} - {\chi'}^\mathrm{calc.}_{j}\\
    r''_j &=  {\chi''}^\mathrm{exp.}_{j} - {\chi''}^\mathrm{calc.}_{j}

    \mathbf{r} = \left[\mathbf{r}', \mathbf{r}''\right]

is minimised. Where :math:`j` runs over the individual angular frequency values of the experiment, and

.. math ::

    \chi = \chi' + i\chi''

is calculated with one of the aforementioned models.

Subsequently, the standard deviation of each fitted parameter of the model is calculated using by first performing a singular value decomposition (SVD)
of the Jacobian :math:`\mathbf{J}` computed at the optimum set of parameters.

.. math ::

    \mathbf{U}\mathbf{\Sigma}\mathbf{V}^\mathrm{T} = \mathbf{J}

where :math:`\mathbf{\Sigma}` is diagonal.

The covariance matrix :math:`\mathbf{K}` can then be calculated as 

.. math ::

    \mathbf{V}^\mathrm{T}\mathbf{\Sigma}^{-2}\mathbf{V} = \mathbf{K}

which is then rescaled by the reduced chi-squared

.. math ::
    
    \chi_v^2 &= \frac{\mathrm{RSS}}{N + k}\\
    &\\
    \mathbf{\tilde{K}} &=  \mathbf{K} \cdot \chi_v^2

where :math:`\mathrm{RSS}` is the residual sum of squares at the optimum set of parameters, and :math:`N` and :math:`k` are the number of data points
and (fitted) variables in the model, respectively. This scaling ensures that the standard deviation of each parameter, defined for the :math:`j^\mathrm{th}` parameter as

.. math ::
    \sigma_j = \sqrt{\tilde{K}_{jj}} \ \ ,

measures only the quality of the fit obtained by ``least_squares``.

The SVD allows for the identification of parameters which have no influence on the quality of the fit, since these correspond to
zero-values on the diagonal of :math:`\mathbf{\Sigma}`. The standard deviation of these parameters is incalculable, and is instead set to zero in ``ccfit2``.

.. _ac_lu_bounds:

Upper and Lower bounds of :math:`\langle \ln\left(\tau\right)\rangle`
---------------------------------------------------------------------

Additionally, ``ccfit2`` calculates upper and lower bounds of :math:`\langle \ln\left(\tau\right)\rangle` caused by the uncertainty in the parameters used in its calculation.

If we denote these bounds :math:`\langle \ln\left(\tau\right)\rangle_\pm`, then for the Debye and Generalised Debye models this is trivial.

.. math ::

    \langle \ln\left(\tau\right)\rangle_\pm &= \ln(\tau_\mathrm{D} \pm \sigma_{\tau_\mathrm{D}})\\
    &\\
    \langle \ln\left(\tau\right)\rangle_\pm &= \ln(\tau_\mathrm{GD} \pm \sigma_{\tau_\mathrm{GD}})\\

where :math:`\sigma_{\tau_\mathrm{D}}` is the uncertainty associated with :math:`\tau_\mathrm{D}` obtained from the least squares fit
algorithm.

For the Havriliak-Negami model, this is more complicated, and all possible combinations of the parameters with their associated
standard deviations are computed to find the largest and smallest values of :math:`\langle \ln\left(\tau\right)\rangle`.

.. footbibliography::
