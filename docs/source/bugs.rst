Reporting Bugs
==============

Please check the :ref:`faq` page before reporting any bugs!

To report bugs with ``ccfit2`` please raise a `GitLab Issue <https://gitlab.com/chilton-group/cc-fit2/-/issues>`_ instead
of emailing individual developers. We appreciate users reporting bugs, as this makes ``ccfit2``
better for everyone.

To make life easier for the person helping you, and to save time replicating the issue you're reporting,
we ask that you use the issue template as indicated on the create issue page, and provide the following
information.

1. Your platform (Windows, Mac, Linux)
2. Versions of ``python`` and all dependencies for ``ccfit2``
3. Input Files
4. Input commands used or minimal-working-example of code
5. Any (incorrect) output of ``ccfit2``
