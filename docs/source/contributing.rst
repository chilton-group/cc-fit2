Contributing
============

We welcome contributions to ``ccfit2``, please create an issue on GitLab if you wish to add functionality.

When contributing, you **must** follow the following rules which help to standardise development.

Source Code
-----------

1. All commits must conform to the `angular style <https://gist.github.com/brianclements/841ea7bffdb01346392c>`_ with imperative, present tense.
2. Use ``flake8`` to ensure compliance with ``pep8`` standards.
3. Use an 80-character line length limit, `CamelCase` for `Classes`, and `snake_case` for everything else.
4. Use numpy style docstrings, and type-hinting for all arguments and return values.
5. Update documentation accordingly with your changes - e.g. the relevant CLI 'usage' page, and the 'What's New' page.
6. If your changes add or modify dependencies, update ``setup.py`` with explicit version numbers.
7. Check the tests in `tests`, and add new tests.

Do not!
^^^^^^^

1. Merge broken code
2. Merge code with debug print statements
3. Merge code with large amounts of commented out code

These are simple requirements, and they make the code easier to use for everyone.

Testing
-------

We use `pytest` for testing, and all tests are stored in the `tests` directory.

There is one test for each of the `ac`, `dc`, `waveform` and `relaxation` submodules.

Tests run automatically in merge requests and the output is shown on the merge request page.
Tests also run when any code is committed to `master`.

To run tests manually, navigate to the `tests` directory and run

.. code-block:: bash

    pytest *_tests.py

Documentation
-------------

Our documentation is written in `Sphinx <https://www.sphinx-doc.org/en/master/>`_ and uses
the `Read the Docs` theme. The source-code for the docs is available at ``ccfit2/docs/source``.

To build the documentation, navigate to ``ccfit2/docs`` and install the python dependencies with

.. code-block:: bash

    pip install -r requirements.txt

You will also need to install ``make`` using your preferred system package manager.

To build the documentation run

.. code-block:: bash

    make clean html

The compiled html pages will be available at ``ccfit2/docs/build/html``.

Please ensure the documentation builds successfully prior to committing/merging.

You do not need to commit compiled pages to the repository.