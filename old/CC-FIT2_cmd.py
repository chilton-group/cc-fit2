#!/usr/bin/env python3

import numpy as np
import sys
from sys import exit
if sys.platform[0:3] == "win":
    import matplotlib
    matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib.ticker as mtick
import matplotlib.cm as cm
from matplotlib import colormaps as cmaps
import matplotlib.colors as mpl_col
from matplotlib.widgets import Slider, Button, RadioButtons
from matplotlib.ticker import Locator, ScalarFormatter, LinearLocator, LogFormatter, MultipleLocator, LogLocator, StrMethodFormatter, AutoLocator, NullFormatter
from scipy.optimize import least_squares
from scipy.optimize import curve_fit
import argparse
from argparse import RawTextHelpFormatter
import os
import warnings
import requests
import math as math
import collections
# to provide multiple constructors of the Waveform class.
# from functools import singledispatchmethod
from glob import glob
from itertools import groupby
# from operator import itemgetter

warnings.filterwarnings("ignore",".*GUI is implemented.*")
warnings.filterwarnings("ignore","invalid value encountered in power")
warnings.filterwarnings("ignore",'invalid value encountered in log10')


class Read_Raw_AC_Data():
    def __init__( self, DC_field, temperature, frequency, sus_real, sus_im, AC_field ):
        self.DC_field    = round(float(DC_field), args.round_field)
        self.temperature = float(temperature)
        self.frequency   = float(frequency)
        self.sus_real    = float(sus_real)
        self.sus_im      = float(sus_im)
        self.AC_field    = float(AC_field)


class Read_Raw_DC_Data():
    def __init__( self, time, field, temperature, moment ):
        self.time        = float(time)
        self.field       = float(field)
        self.temperature = float(temperature)
        self.moment      = float(moment)


class Process_AC():

    def __init__(self):
        self.model    = None # function that will be used to fit the data
        self.model_AC = None # string that will be written to output files
        self.bounds   = None # bounds of the model

    def get_data(self, file_to_open):
            
        '''Prepares the raw SQUID data. Only positive values of the real and imaginary susceptibilities are considered.

        Parameters:
            input_file (str): SQUID ac.dat file

        Returns:
            RawDatalist (list) : Contains the raw data as a list of objects. Sorted by field and temperature.
            real_susc   (list) : Contains the real components of the susceptibility (cm^3mol-1).
            im_susc     (list) : Contains the imaginary components of the susceptibility (cm^3mol-1).
            wave_freq   (list) : Contains wave frequencies employed (Hz).
            temperature (list) : Contains the temperatures measured (K).
            DC_field    (list) : Contains the Direct Current fields employed (Oe).

            real_susc, im_susc and wave_freq are all structured as [ [ [ x nfreq ], ... x ntemps ..., [ x nfreq ] ], ... x nfields ... ]:
            [  DC field 1,  DC field 2, ... ]
                   |
                   |
                    -> [ Temperature 1, Temperature 2, ... ]
                             |
                             |
                              -> [ frequency 1, frequency 2, ... ]
                              
            print( A[i][:] ) to access the data measured at different DC fields,
            print( A[i][j] ) to access each individual list.

            temperature is structured as [ [ ... x ntemps ... ], ... x nfields ... ]

            DC_field is structured as [ ... x nfields ... ]

        Raises:
            Error: If the data contains any duplicate. Ex: two points measured with under the same DC field, temperature and wave frequency.
            
        '''

        # Open the file and store the headers.
        with open(file_to_open, 'r') as f:
            ac_headers = []
            for line in f:
                if line.find(args.data_pointer) != -1:
                    ac_headers.append(next(f).split(','))
                    break   
        
        # Get the indexes of the columns containing the data.
        wanted_headers = {
            'Field':       [ 'Field (Oe)', 'Magnetic Field (Oe)'],
            'Temperature': [ 'Temperature (K)'],
            'Frequency':   [ 'Wave Frequency (Hz)', 'AC Frequency (Hz)', 'Frequency (Hz)'],
            'RealSusc':    [ 'm\' (emu)', 'AC X\'  (emu/Oe)', 'AC X\' (emu/Oe)', 'M\' (emu)', "AC X' (emu/Oe)"],
            'ImSusc':      [ 'm" (emu)', 'AC X" (emu/Oe)', 'AC X\'\' (emu/Oe)', 'M\'\' (emu)', "AC X'' (emu/Oe)"],
            'DriveAmp':    [ 'Drive Amplitude (Oe)', 'AC Drive (Oe)', 'Amplitude (Oe)']
            }

        headers_indices = {}
        for i in ac_headers[0]:
            for key,val in wanted_headers.items():
                for item in val:
                    if i == item:
                        headers_indices[key] = ac_headers[0].index(i)

        if len(headers_indices.values()) != 6:
             exit('\n***Error***:\n Headers on the input file are not recognised.\n Refer to manual to see valid options and contact danielreta1@gmail.com if you want them implemented.')


        # Store the raw data in a list of objects, where Read_Raw_AC_Data() is a class. Store only if data exists and susceptibilities are positive.
        RawDatalist = []
        with open(file_to_open, 'r') as f:   
            #dropped = dropwhile(lambda _line: "Time" not in _line, f) # contains the headers. Start reading after "Time".
            #next(dropped,"")
            #for line in dropped:
            for line in f:
                if line.find(args.data_pointer) != -1:
                    line = next(f)
                    for line in f:
                        try:
                            if  len( line.split(',')[headers_indices['Frequency']] ) != 0 \
                            and float(line.split(',')[headers_indices['RealSusc']]) >= 0 \
                            and float(line.split(',')[headers_indices['ImSusc']]) >= 0: # reading non-empty and positive vals only
                                RawDatalist.append( 
                                    Read_Raw_AC_Data(
                                        line.split(',')[headers_indices['Field']],
                                        line.split(',')[headers_indices['Temperature']],
                                        line.split(',')[headers_indices['Frequency']],
                                        line.split(',')[headers_indices['RealSusc']],
                                        line.split(',')[headers_indices['ImSusc']],
                                        line.split(',')[headers_indices['DriveAmp']]
                                        )
                                            ) # RawDatalist is a list of objects  print( [ r.DC_field for r in RawDatalist ]  )
                        except:
                            break

        # Sort all the data by Field and Temperatures
        RawDatalist.sort(key=lambda k: (k.DC_field, k.temperature))

        # Create the lists where I will append the sorted data.
        DC_fields    = []
        real_susc    = []
        im_susc      = []
        wave_freq    = []  
        temperatures = []
       
        # Get the unique values of DC fields employed.
        for res in RawDatalist:
            if res.DC_field not in DC_fields:
                DC_fields.append(res.DC_field) 

        # Store the data recorded with the same DC field.
        for f in DC_fields:
            original_T = []
            for res in RawDatalist:
                if res.DC_field == f:
                    original_T.append(res.temperature) # collect the sorted temperatures.
                    
            # Calculate the step sizes of the temperature list. This will be used to separate the data based on a threshold value fixed as 0.1.
            T_steps = [ round(abs(original_T[i] - original_T[i+1]), args.round) for i in range(len(original_T)-1) ]
            T_steps.insert(len(T_steps),0.)            # Fill the list with 0 for the last item, to match lenghts of lists.
            chunks = [ ]                               # list containing the cumulative number of T of the same groups.
            threshold = 0.1
            for it,val in enumerate(T_steps):
                if val >= threshold:       # Different way of doing this? 
                    chunks.append(it+1)
            chunks.append(len(original_T)) # Add the size of the temperature list as the last item.

            sel_T    = [] # Creates the list with the averaged T used. This will define my final temperatures list.
            up_lim   = [] # This is the difference between the mean and the maximum temperature recorded for the set.
            down_lim = [] # This is the difference between the mean and the minimum temperature recorded for the set.
            for it,val in enumerate(chunks): 
                if it == 0: 
                    sel_T.append(    sum(original_T[0:chunks[it]])/(chunks[it]) )
                    up_lim.append(   max(original_T[0:chunks[it]])              )
                    down_lim.append( min(original_T[0:chunks[it]])              )
                else:       
                    sel_T.append(    sum(original_T[chunks[it-1]:chunks[it]])/(chunks[it]-chunks[it-1]) )
                    up_lim.append(   max(original_T[chunks[it-1]:chunks[it]])                           )
                    down_lim.append( min(original_T[chunks[it-1]:chunks[it]])                           )


            # Provide information on how the data has been treated if the user requests it.
            if args.verbose:
                print('\nVerbose mode entered.\n')
                print('Field: {} Oe'.format(f))
                print('The sorted temperatures from the {} file are:\n{}\n'.format(args.input_file, original_T))
                print('These make {} different sets, with {} cumulative elements each.\n'.format(len(chunks),chunks))

           
            # Get the frequencies used for each DC_field and temperature range, then sort it. k.frequency had been sorted by DC_field and temperature before.
            _freq = []
            _real = []
            _im   = []
            _AC =   []
            for it,t in enumerate(sel_T):
                freqs = sorted( [ k.frequency for k in RawDatalist if (down_lim[it] <= k.temperature <= up_lim[it]) and k.DC_field == f ] )
                _freq.append(freqs)

                if args.verbose:
                    print('Field: {}. Min and max temperatures in the set: {}, {}. Averaged temperature: {:04.1f}.\n{} frequencies: {}\n'.format(f, down_lim[it], up_lim[it], float(t), len(freqs), freqs))

                # Get the real and imaginary susceptibilities and AC fields for DC each field, temperature and frequency.
                _real2 = []
                _im2   = []
                _AC    = []
                for nu in freqs: 
                    _real2.append( 
                    [ res.sus_real for res in RawDatalist if ( res.DC_field == f and (down_lim[it] <= res.temperature <= up_lim[it]) and res.frequency == nu ) ] )
                    _im2.append(
                    [ res.sus_im for res in RawDatalist if ( res.DC_field == f and (down_lim[it] <= res.temperature <= up_lim[it]) and res.frequency == nu ) ] )
                    _AC.append(
                    [ res.AC_field for res in RawDatalist if ( res.DC_field == f and (down_lim[it] <= res.temperature <= up_lim[it]) and res.frequency == nu ) ] )

                _real2 = [item for sublist in _real2 for item in sublist] # Flatten the list: from [ [], [], ... ] to [ ... ]
                _im2   = [item for sublist in _im2   for item in sublist]
                _AC    = [item for sublist in _AC    for item in sublist]

                # Change of units from input to cm^3mol^(-1)
                # Input data in (emu/Oe): apply MW/(mass/1000) conversion factor.
                if 'AC X" (emu/Oe)' in ac_headers[0] or "AC X'' (emu/Oe)" in ac_headers[0]:
                    _real2 = [ a*args.MW/(args.mass/1000.) for a in _real2 ] 
                    _im2   = [ a*args.MW/(args.mass/1000.) for a in _im2   ]
                # Input data in (emu): apply MW/(AC_field*mass/1000) conversion factor.
                else:
                    _real2 = [ a*args.MW/(b*args.mass/1000.) for a,b in zip( _real2,_AC )] 
                    _im2   = [ a*args.MW/(b*args.mass/1000.) for a,b in zip( _im2,  _AC )]
                
                _real.append(_real2)
                _im.append(_im2)          

            real_susc.append(_real)
            im_susc.append(_im)
            wave_freq.append(_freq) 
            temperatures.append(sel_T)


        # Check that the data does not contain duplicates.
        dim_real_susc = []
        dim_im_susc   = []
        dim_wave_freq = []
        for it,field in enumerate(DC_fields):
            dim_real_susc.append( [ len(real_susc[it][j]) for j in range(len(temperatures[it])) ] )
            dim_im_susc.append(   [ len(im_susc[it][j])   for j in range(len(temperatures[it])) ] )
            dim_wave_freq.append( [ len(wave_freq[it][j]) for j in range(len(temperatures[it])) ] )
        if dim_real_susc == dim_wave_freq or dim_im_susc == dim_wave_freq:
            pass
        else: # This will not work when there are more than one field !! Correct
            check = np.array(dim_real_susc) == np.array(dim_wave_freq)
            for i in range(len(DC_fields)):
                _a =  [ it for it,val in enumerate(check[i]) if val==False] 
                exit('\n***Error***.\n The program cannot continue as there is a duplicate measured at {:3.2f} K and {:3.2f} Oe.\n Please edit the file and decide which one to discard.'.format(temperatures[i][_a[i]], DC_fields[i] ))

        return RawDatalist, real_susc, im_susc, wave_freq, temperatures, DC_fields

    def only_plot(self, exp_real, exp_im, wave_freq, range_T, DC_field):
        """
        Plot the raw data (not fitted).

        Parameters
        ----------
            exp_real: list
                Contains the real components of the susceptibility (cm^3mol-1).
            exp_im: list
                Contains the imaginary components of the susceptibility
                (cm^3mol-1).
            wave_freq: list
                Contains wave frequencies employed (Hz).
            range_T: list
                Contains the tuples with orginal index and corresponding
                temperature values measured (K). [(), (), ...]
            DC_field: list
                Contains the Direct Current field employed (Oe).
        """

        # Define the figures.
        susc_comp, (ax1, ax2) = plt.subplots(
            2, 1, sharex=True, sharey=False, figsize=(6.5, 4.5),
            num='AC susceptibility raw data'
        )  # 8.27, 11.69 A4
        cole_cole, (ax3) = plt.subplots(1, 1, figsize=(5.1, 4.8), num='Cole-Cole raw data')

        for it, T in range_T:
            ax1.semilogx(
                wave_freq[it], exp_real[it], label='{} K'.format(round(T, 1)),
                marker='o', markeredgewidth=1, lw=0.85,
                markerfacecolor='w', markersize=5
                )
            ax2.semilogx(
                wave_freq[it], exp_im[it],
                marker='o', markeredgewidth=1, lw=0.85,
                markerfacecolor='w', markersize=5
                )
            ax3.plot(
                exp_real[it], exp_im[it], label='{} K'.format(round(T, 1)),
                marker='o', markeredgewidth=1, lw=0.85,
                markerfacecolor='w', markersize=5
                )

        # Set decimal places for wave frequency.
        minfreq = np.min(self.flatten_recursive(wave_freq))
        if minfreq >= 0.1:
            decimals = '%.1f'
        elif 0.001 < minfreq < 0.01:
            decimals = '%.2f'
        elif minfreq <= 0.001:
            decimals = '%.3f'
        # Sets float formatting.
        for axis in [ax1.xaxis, ax1.yaxis,
                     ax2.yaxis,
                     ax3.xaxis, ax3.yaxis]:
            axis.set_major_formatter(mtick.FormatStrFormatter('%.1f'))
        ax2.xaxis.set_major_formatter(mtick.FormatStrFormatter(decimals))
        # Get rid of the frames of susceptibility plots
        for axis in [ax1, ax2, ax3]:
            axis.spines["top"].set_visible(False)
            axis.spines["right"].set_visible(False)
        # Set labels for the axes
        ax2.set_xlabel('Wave Frequency (Hz)')
        ax1.set_ylabel(r'$\chi^{,}$  (cm$^{3}$mol$^{-1}$)')
        ax2.set_ylabel(r'$\chi^{,,}$ (cm$^{3}$mol$^{-1}$)')
        ax3.set_xlabel(r'$\chi^{,}$  (cm$^{3}$mol$^{-1}$)')
        ax3.set_ylabel(r'$\chi^{,,}$ (cm$^{3}$mol$^{-1}$)')

        # Set the legend.
        for axis in [ax1, ax3]:
            axis.legend(
                loc=0, fontsize='x-small', markerscale=0.7,
                handlelength=0.5, labelspacing=0.2, handletextpad=0.6,
                numpoints=1, ncol=int(len(range_T)/3), frameon=False
                )

        susc_comp.savefig(
            os.path.join(os.getcwd(), 'susc_comp_plot.png'), dpi=300
            )
        cole_cole.savefig(
            os.path.join(os.getcwd(), 'Cole_Cole_plot.png'), dpi=300
            )
        plt.show()
        plt.close('all')

        print(
            '\n    Plot of susceptibility components (without fits) saved as\n\
                {}'.format(os.path.join(os.getcwd(), 'susc_comp_plot.png'))
            )
        print(
            '    Cole-Cole plot (without fits) saved as\n\
                {}'.format(os.path.join(os.getcwd(), 'Cole_Cole_plot.png'))
            )

        return

    def flatten_recursive(self, S):
        # Flatten a list of lists recursively.
        if S == []:
            return S
        if isinstance(S[0], list):
            return self.flatten_recursive(S[0]) + self.flatten_recursive(S[1:])
        return S[:1] + self.flatten_recursive(S[1:])

    def select_and_estimate(self, exp_real, exp_im, wave_freq, range_T, DC_field):
            
        '''Decide what Debye model function to use and estimate initial guess for the coldest temperature.

        Parameters:
            real_susc   (list): Contains the real components of the susceptibility (cm^3mol-1).
            im_susc     (list): Contains the imaginary components of the susceptibility (cm^3mol-1).
            wave_freq   (list): Contains wave frequencies employed (Hz).
            temperature (list): Contains the temperatures measured (K).
            DC_field    (list): Contains the Direct Current field employed (Oe).

        Returns:
            model_function (list): Contains the model function to be employed for each DC field.
            param          (list): Contains the initial guess to be used in the indicated Debye model.
            selected_T     (list): Contains the indexes and values of the selected temperatures. If --select_T is active, it is defined by the user; otherwise defined by the temperature (list).

        Raises:
            Error: If the temperatures indicated by the user do not match those measured. Relevant only when --select_T is active.      
        '''

        # If --select_T is indicated, ask the user to provide the temperature points_to_fit.
        if args.select_T:
            print('\n    Entering manual selection of temperatures. Select only the temperatures with a clear peak.')
            #points_to_fit = []

            # Show each data set individually for identification of peaks.
            fig, axs = plt.subplots( (int((len(range_T)-1)/4)+1), 4, sharex='none', sharey='none', figsize=(7,7))
            fig.subplots_adjust(hspace = .4, wspace=.08)
            axs = axs.ravel()
            plt.suptitle(r'$\chi^{{,,}}$ vs wave frequency under {:2.1f} Oe field'.format(DC_field), fontsize=11)
            for it, T in enumerate(range_T):
                rounded_T = [ round(x,2) for x in range_T ]
                #axs[it].set_title((str(rounded_T[it])+' K'), fontsize=10)
                axs[it].semilogx( 
                    wave_freq[it], exp_im[it], marker='o', markeredgewidth=1, label=(str(rounded_T[it])+' K'),
                        markeredgecolor='b', markerfacecolor='w', markersize=5, c='b', lw=.5 
                    )
                axs[it].set_yticklabels([])
                axs[it].set_xticklabels([])
                axs[it].legend(loc=0, fontsize='small', markerscale=0, handlelength=0, numpoints=1, ncol=1, frameon=False)
            # Remove empty axes
            for ait in range(len(range_T), len(axs)):
                fig.delaxes(axs[ait])
            plt.draw()
            plt.pause(0.05)

            # Get the working python version to use either raw_input() or input() functions.            
            if sys.version_info[0] == 2:
                user_input = raw_input(
                    '\n    Select the temperature points to fit, COLDER -> WARMER separated by comma.\n    All is a valid argument.\n    Available temperatures: {}\n\n    Selected temperatures: '.format(rounded_T)
                    ) # , range_T))
            elif sys.version_info[0] == 3:
                user_input = input(
                    '\n    Select the temperature points to fit, COLDER -> WARMER separated by comma.\n    All is a valid argument.\n    Available temperatures: {}\n\n    Selected temperatures: '.format(rounded_T)
                    ) # , range_T))
            
            # Collect the temperatures indicated by the user.
            if user_input.lower() == 'all':
                points_to_fit = range_T
            else: 
                points_to_fit = [ float(value) for value in user_input.split(',') ] 
                if any(item not in rounded_T for item in points_to_fit):
                    exit('***Error***: Indicated temperature not available.')
                        
            plt.close('all')

        # If --select_T is not indicated, points_to_fit contains the sorted lists of temperatures to be fitted.
        else:
            points_to_fit = range_T 


        # List of tuples containing the index and temperature of the selected temperatures.
        selected_T = [] 
        for index_all_temperatures, all_temp in enumerate(range_T):
            for selected_temp in points_to_fit:
                if round(all_temp,2) == round(selected_temp,2):
                    selected_T.append( ( index_all_temperatures, selected_temp) )

        # If the user wants to plot the selected temeperatures.
        # Note it will only work with dat files measured at one field, because
        # this is badly written.
        if args.process == 'plot':
            process_AC.only_plot(
                exp_real,
                exp_im,
                wave_freq,
                selected_T,
                DC_field
                )
            exit('\n    ***Exiting***\n')


        # Define the model function to use and the initial parameters guess of the coldest temperature to be passed to the fitting routine.
        param = []
        parameters = []

        colors = cmaps['coolwarm'].resampled(len(selected_T))
        count = 0
        fig, (ax1) = plt.subplots( 1, 1, sharex='none', sharey='none', figsize=(7.5,6), num='Select Debye model')
        plt.suptitle('Cole-Cole plot at {:4.1f} Oe.\nSelect model by clicking the circle -->'.format(DC_field), fontsize=10)
        ratio = np.linspace(0, 1, len(selected_T))

        for it_temp, temp in selected_T:
            ax1.plot( exp_real[it_temp], exp_im[it_temp],'o', color=colors(ratio[it_temp]), 
               label=(str(round(temp,2))+' K' if ( it_temp == 0 or it_temp == (len(selected_T)-1) or it_temp%4 == 0 ) else '' ))
            count += 1

        ax1.yaxis.set_major_formatter(StrMethodFormatter('{x:.2f}'))
        ax1.legend(loc=0, fontsize='small', numpoints = 1, ncol=2, frameon=False)
        ax1.set_xlabel(r'$\chi^{,}$  (cm$^{3}$mol$^{-1}$)')
        ax1.set_ylabel(r'$\chi^{,,}$ (cm$^{3}$mol$^{-1}$)')

        print('\n    Select the model function:\n    Debye             -> For a single relaxation process.\n    Generalised Debye -> For a single relaxation process with a distribution.\n    Two processes     -> For two distinct relaxation processes, each with its own distribution\n')

        rax = plt.axes([0.63, 0.878, 0.3, 0.13], facecolor='w',  frameon=False, aspect='equal')
        radio = RadioButtons(
            rax,
            ('Debye', 'Generalised Debye', 'Two processes'),
            radio_props={
                's': 100,
                'facecolor': 'blue'
            }
        )

        fc = [
            [0, 0, 0, 0] for _ in range(3)
        ]

        radio._buttons.set_facecolor(
            fc
        )

        def model_radiobutton(label):
            print('    Model function {} has been selected.'.format(radio.value_selected))
            if radio.value_selected == 'Debye':
                self.model = self.model_1
                self.model_AC = 'Debye'
            elif radio.value_selected == 'Generalised Debye': 
                self.model = self.model_2
                self.model_AC = 'Generalised'
            elif radio.value_selected == 'Two processes':
                self.model = self.model_3
                self.model_AC = 'Bimodal'
            plt.pause(0.05)
            plt.close('all')

        radio.on_clicked(model_radiobutton)
        plt.show()
        if self.model == None: exit()

        # Define initial guess using the coldest temperature selected (selected_T[it_field][0][0]) -> first element
        coldest_T = selected_T[0][1]
        index_coldest_T = selected_T[0][0]
        freq_selected = wave_freq[index_coldest_T]


        # Calculate the mean between the two largest values. np.argsort(exp_im[][])[-1]] gives the index 
        # of largest exp_im value; np.argsort(exp_im[][])[-2] gives the index of the second largest exp_im value
        if self.model_AC == 'Debye': # param = tau, chi_S, chi_T
            max_freq = (freq_selected[np.argsort(exp_im[index_coldest_T])[-1]] +
                        freq_selected[np.argsort(exp_im[index_coldest_T])[-2]] ) /2.
            parameters = [1./(2.*np.pi*max_freq)] +           \
                         [np.min(exp_real[index_coldest_T])] +\
                         [np.max(exp_real[index_coldest_T])-np.min(exp_real[index_coldest_T])]
            self.bounds = ([0., 0., 0.],[np.inf, np.inf, np.inf])

        elif self.model_AC == 'Generalised': # param = tau, chi_S, chi_T, alpha
            max_freq = (freq_selected[np.argsort(exp_im[index_coldest_T])[-1]] +
                        freq_selected[np.argsort(exp_im[index_coldest_T])[-2]] ) /2.
            parameters = [1./(2.*np.pi*max_freq)] +                                             \
                         [np.min(exp_real[index_coldest_T])] +                                  \
                         [np.max(exp_real[index_coldest_T])-np.min(exp_real[index_coldest_T])] +\
                         [0.1]
            self.bounds = ([0., 0., 0., 0.],[np.inf, np.inf, np.inf, 1.0])

        elif self.model_AC == 'Bimodal': # param = tau1, delta_chi1, alpha1, tau2, delta_chi2, alpha2, chi_total
            # Calculate gradient of chi imaginary and retrieve indexes where there is a change of sign.
            der_freq = np.gradient( exp_im[index_coldest_T] )
            zero_crossings_freq = np.where(np.diff(np.sign(der_freq)))[0]

            # Use those to extract the values of the maxima and sort them. Retrieve the indexes of the two largest maxima values to access the frequency values associated
            exp_im_at_crossings = [ exp_im[index_coldest_T][int(x)] for x in zero_crossings_freq ]
            index_freq_1 = int( np.where( np.array(exp_im[index_coldest_T]) == ( np.sort( exp_im_at_crossings )[-1] ) )[0] )
            index_freq_2 = int( np.where( np.array(exp_im[index_coldest_T]) == ( np.sort( exp_im_at_crossings )[-2] ) )[0] )
            if freq_selected[index_freq_1] < freq_selected[index_freq_2]:
                max_freq_1 = freq_selected[index_freq_1]
                max_freq_2 = freq_selected[index_freq_2]
            else:
                max_freq_1 = freq_selected[index_freq_2]
                max_freq_2 = freq_selected[index_freq_1]

            range_real = np.max(exp_real[index_coldest_T])-np.min(exp_real[index_coldest_T])
            parameters = [ 1./(2.*np.pi*max_freq_1) ] + \
                         [float(2.*range_real/3.)] +    \
                         [0.1] +                        \
                         [ 1./(2.*np.pi*max_freq_2) ] + \
                         [float(range_real/3.)] +       \
                         [0.01] +                       \
                         [float(np.min(exp_real[index_coldest_T]))]
            self.bounds = ([0., 0., 0., 0., 0., 0., 0.],[np.inf, np.inf, 1.0, np.inf, np.inf, 1.0, np.inf,])

        if args.verbose:
            if self.model_AC == 'Debye':
                print(
                    '\nFor model {}, the initial guess at {} K are:\ntau = {}\nchi_S = {}\nchi_T = {}\n'.format(
                        self.model_AC, str(round(coldest_T,2)), parameters[0], parameters[1], parameters[2] 
                        )
                    )

            elif self.model_AC == 'Generalised':
                print(
                    '\nFor model {}, the initial guess at {} K are:\ntau = {}\nchi_S = {}\nchi_T = {}\nalpha = {}\n'.format(
                        self.model_AC, str(round(coldest_T,2)), parameters[0], parameters[1], parameters[2], parameters[3] 
                        )
                    )

            elif self.model_AC == 'Bimodal':
                print('\nFor model {}, the initial guess at {} K are:\ntau_1 = {}\ndelta_chi_1 = {}\nalpha_1 = {}\ntau_2 = {}\ndelta_chi_2 = {}\nalpha_2 = {}\nchi_total = {}'.format(
                    self.model_AC, str(round(coldest_T,2)),
                        parameters[0], parameters[1], parameters[2], parameters[3], parameters[4], parameters[5], parameters[6] 
                        ) 
                )

        return self.model_AC, parameters, selected_T

    def Debye_model(self, exp_real, exp_im, wave_freq, DC_field, model_function, param, selected_T):
            
        '''Fit the data to the Debye model.

        Parameters:
            real_susc      (list): Contains the real components of the susceptibility (cm^3mol-1).
            im_susc        (list): Contains the imaginary components of the susceptibility (cm^3mol-1).
            wave_freq      (list): Contains wave frequencies employed (linear frequency, Hz).
            DC_field       (list): Contains the Direct Current field employed (Oe).
            model_function (list): Contains the model function to be employed.
            param          (list): Contains the initial guess to be used in the indicated Debye model.
            selected_T     (list): Contains the selected temperatures (K).

        Returns:        
            Plots          (fig) : Frequency dependent plots of the real and imaginary susceptibility components; Cole-Cole plot;
            Files          (.dat): For each temperature, the best parameters to the model function and the values of a continuos fit are saved in two separate files.        
            fitted_data    (dict): Dictionary with keys as DC field values and values the lists corresponding to fitted_T.

        Raises:
            Point discarded: For those temperature points for which the fit is incorrect.


                least_squares outputs the jacobian, which can be used to estimate the hessian matrix as
                np.linalg.inv(J.T.dot(J)) where J is the jacobian from the least squares routine.
                However, if one wants to compute the errors on the parameters as curve_fit does,
                this covariance needs to be multiplied by the reduced chi squared (also called residual variance)
                -> cov * R_chisq, with R_chisq defined as (func(popt, args)**2).sum()/(len(ydata)-len(p0)).
                That is the chi squared but considering also the amount of parameters in the model function via the denominator.
                chisq can be defines as (best_param.fun**2).mean()

                best_param.fun returns the residuals, because fun is residuals. Check with
                print(residuals(best_param.x, val_real, val_im), best_param.fun)

                then, (best_param.fun**2).sum() is the residual sum of squares (RSS) at best fit point.
                so (best_param.fun**2).mean() is the RSS divided by the number of data points.
            
        '''

        # Create an empty dictionary with keys as DC field values and values the lists corresponding to fitted_T
        fitted_data = {} # needs to be created out of this function.

        # Initialise empty list to store the data of the correct fits only; the bad fits are filtered out (if not args.discard).
        guess      = param # First guess given by coldest T, it then gets updated with best.param.x
        fitted_T   = []    # list of tuples, 1st and 2nd elements are iterator and actual temperature of correct fits.
        fittings   = []    # list of tuples, 1st and 2nd elements are the fitted real and imaginary susceptibilities.
        final_susc = []    # list of tuples, 1st, 2nd and 3rd elements are val_x (linear freq), val_real, val_im.
        final_lims_model = [] # list of arrays containing the x-vals for each T.
     

        # Loop over all temperatures, fit the data, check it is correct.
        for it_T,temp in selected_T:
                            
            # Get each list corresponding to all temperatures points to be fitted.
            val_real = np.array( exp_real[it_T] ) 
            val_im   = np.array( exp_im[it_T] ) 
            val_x    = np.array( wave_freq[it_T] ) # linear freq (Hz).

            # Fit each of the data sets
            # SQUID data is nu (linear frequency, Hz), but the equations need angular frequency (seconds). w = 2*pi*nu
            # model functions convert linear to angular internally.
            best_param = least_squares(self.residuals, guess, args=(val_x, val_real, val_im, self.model), bounds=self.bounds)#, loss='cauchy', f_scale=0.1, bounds=bounds)
            best_param.x = abs(best_param.x)

            # filter out bad fits.
            if not args.discard_off:
                # Check if the fitted tau is within the limits of the employed frequency. If not, discard fit.
                if (model_function == 'Debye' or model_function == 'Generalised'):
                    if ( 1./(best_param.x[0]) < 2.*np.pi*val_x[0] or 1./(best_param.x[0]) > 2.*np.pi*val_x[-1] ):
                        print('    At {: 6.1f} Oe and {: 6.2f} K, no peak measured -> point discarded.'.format(DC_field, temp))
                        continue
                    if args.filter_flats:
                        print('    At {: 6.1f} Oe and {: 6.2f} K, the error in the linear fit is {}.'.format( DC_field, temp, self.error_linear(2.*np.pi*val_x, val_im) ) )
                        if self.error_linear(2.*np.pi*val_x, val_im) < args.error_fit:
                            print('    At {: 6.1f} Oe and {: 6.2f} K, flat line -> point discarded.'.format(DC_field, temp))
                            continue
                elif model_function == 'Bimodal':
                    if (1./(best_param.x[0]) < 2.*np.pi*val_x[0] or 1./(best_param.x[3]) < 2.*np.pi*val_x[0] or 1./(best_param.x[0]) > 2.*np.pi*val_x[-1] or 1./(best_param.x[3]) > 2.*np.pi*val_x[-1] or self.error_linear(2.*np.pi*val_x, val_im) < args.error_fit):
                        print('    At {: 6.1f} Oe and {: 6.2f} K, no peak measured -> point discarded.'.format(DC_field, temp))
                        continue
                if best_param.status == 0:
                    print('    The fitting at {} Oe and {} K failed. Too many iterations.'.format(DC_field, temp))
                    continue
                            
            # Get the fitted parameters and the associated temperature of the correct fits.
            guess = best_param.x
            fitted_T.append((it_T,temp))
            final_susc.append((val_x, val_real, val_im)) # linear frequency -> used for the plots.

            # Calculate standard deviation errors
            J    = best_param.jac
            cov  = np.linalg.inv(J.T.dot(J)) * (best_param.fun**2).sum()/(len(best_param.fun)-len(best_param.x))
            perr = np.sqrt(np.diag(cov)) 

            # Write the Debye params to file.
            # where tau is in angular frequency.
            self._write_results(Debye_params_result, model_function, temp, best_param.x, _perr=perr)

            # Define an array of temperatures to calculate the model function.
            # linear frequency Hz -> used for the plots.
            lims_model = np.logspace(np.log10(val_x[0]), np.log10(val_x[-1]), 100)
            final_lims_model.append(lims_model)

            # Store the results from the model function (tuple, real and imaginary fits as 1st and 2nd components).
            results = self.model(lims_model, best_param.x)
            fittings.append(results)
            
            # Write the results of the fitting in linear (Hz) frequency.
            self._write_results(Debye_fit, model_function, temp, best_param.x,
                _results=results, _lims_model=lims_model, 
                _val_x=val_x, _val_real=val_real, _val_im=val_im,
                )
                        
        Debye_params_result.close()
        Debye_fit.close()
        print('\n    Parameters written to\n     {}'.format(Debye_filename+'_params.out'))
        print('    Fit written to\n     {}'.format(Debye_filename+'_fit.out'))

        if len(fitted_T) == 0:
            exit('\n    ***Error***:\nAt {} Oe, no peak was measured.'.format(field))


        self._plot_results( fitted_T, final_susc, fittings, final_lims_model, Debye_filename)
        
        fitted_data[DC_field] = fitted_T

        return fitted_data

    def model_1(self, val_x, guess):
        tau, chi_S, chi_T = guess
        val_x = 2.*np.pi*val_x
        func_real = abs(chi_S)+((abs(chi_T)-abs(chi_S))/(1.+(val_x**2)*(abs(tau)**2)))
        func_im   = ((val_x*abs(tau)*(abs(chi_T)-abs(chi_S)))/(1.+(val_x**2)*(abs(tau)**2)) )
        return func_real, func_im

    def model_2(self, val_x, guess):
        tau, chi_S, chi_T, alpha = guess
        val_x = 2.*np.pi*val_x
        func_real = abs(chi_S)+(abs(chi_T)-abs(chi_S))*(1.+np.power((val_x*abs(tau)),(1.-abs(alpha)))*np.sin(np.pi*abs(alpha)/2.))/(1.+2.*np.power((val_x*abs(tau)),(1.-abs(alpha)))*np.sin(np.pi*abs(alpha)/2.)+np.power((val_x*abs(tau)),(2.-2*abs(alpha)))) 
        func_im = (abs(chi_T)-abs(chi_S))*((np.power((val_x*abs(tau)),(1.-abs(alpha)))*np.cos(np.pi*abs(alpha)/2.)))/(1.+2.*np.power((val_x*abs(tau)),(1.-abs(alpha)))*np.sin(np.pi*abs(alpha)/2.)+np.power((val_x*abs(tau)),(2.-2*abs(alpha))))  
        return func_real, func_im

    def model_3(self, val_x, guess):
        tau1, delta_chi1, alpha1, tau2, delta_chi2, alpha2, chi_total = guess
        val_x = 2.*np.pi*val_x
        func = abs(chi_total) + abs(delta_chi1)/(1+np.power((val_x*abs(tau1)*1j),(1.-abs(alpha1)))) + abs(delta_chi2)/(1+np.power((val_x*abs(tau2)*1j),(1.-abs(alpha2))))
        return np.real(func), np.abs(np.imag(func))

    def residuals(self, guess, val_x, val_real, val_im, model):
        residuals_real = self.model(val_x, guess)[0] - val_real
        residuals_im   = self.model(val_x, guess)[1] - val_im   
        return np.concatenate((residuals_real, residuals_im))

    def linear(self, val_x, a, b):
        return  a*val_x+b 

    def error_linear(self, val_x, val_y):
        linear_popt, linear_pcov = curve_fit( self.linear, val_x, val_y )
        error = np.square( self.linear(val_x, *linear_popt) -  val_y )
        return np.sqrt( np.sum( error ) )

    def r_squared(self, val_x, val_y):
        linear_popt, linear_pcov = curve_fit( linear, val_x, val_y )
        residuals = val_y - linear(val_x, *linear_popt) 
        ss_res = np.sum(residuals**2)
        ss_tot = np.sum( ( val_y - np.mean(val_y) )**2 )
        r_sq = 1. - (ss_res/ss_tot)
        return r_sq

    def error(self, guess, val_real, val_im):
        error_real = np.square( model(val_x, guess)[0] - val_real )
        error_im   = np.square( model(val_x, guess)[1] - val_im )
        return np.sum( np.concatenate((error_real, error_im)) )

    def log_normal(self, tau, alpha, sigma):
        '''
        In log10 space, the positive and negative deviations from tau are equal -> I calculate only the positive.
        '''
        return tau*np.exp( (1.82*sigma*np.sqrt(alpha))/(1-alpha) )

    def Fuoss_Kirkwood(self, tau, alpha, sigma): # Not used
        if args.sigma == 1: 
            return np.log10(tau*np.exp(-(2.57*np.tang(alpha*np.pi/2))/(alpha**0.096))),\
                   np.log10(tau*np.exp((2.57*np.tang(alpha*np.pi/2))/(alpha**0.096)))
        if args.sigma == 2:
            return np.log10(tau*np.exp(-(5.52*np.tang(alpha*np.pi/2))/(alpha**0.30))),\
                   np.log10(tau*np.exp((5.52*np.tang(alpha*np.pi/2))/(alpha**0.30))) 
        else: print('The uncertainties cannot be estimated with a confidence interval \
                     larger than two sigma using Fuoss_Kirkwood.\nExiting program.')

    def _write_results(self, outputfile, model, _temp, _popt, **kwargs):

        if outputfile == Debye_params_result:

            # Write the result to Debye_params_result
            outputfile.write('{: 8.6f}   '.format(_temp))
            outputfile.write('{: 12.8E}   '.format(_popt[0]))

            if model == 'Generalised': 
                outputfile.write(
                    '{: 12.8E}   {: 12.8E}   '.format( 
                        self.log_normal(_popt[0], _popt[3],  args.sigma),
                        self.log_normal(_popt[0], _popt[3], -args.sigma) 
                ) )
            outputfile.write('{: 12.8E}'.format(kwargs['_perr'][0]))
            for j in range(1,len(_popt)):
                outputfile.write('   {: 12.8E}   {: 12.8E}'.format( _popt[j], kwargs['_perr'][j] ))
            outputfile.write('\n')

        elif outputfile == Debye_fit:

            # Write the result to Debye_fit
            outputfile.write('{} {: 8.6f} {}'.format('T =', _temp, 'K')+'\n')

            # Write the fit.
            outputfile.write('{:^11} {:^11} {:^11}'.format(
                'Wave_freq (Hz)', 'fit_chi* (cm^{3}mol^{-1})', 'fit_chi** (cm^{3}mol^{-1})')+'\n'
                )
            for line,val_frq in enumerate(kwargs['_lims_model']):
                outputfile.write('{: 12.8E} {: 12.8E} {: 12.8E}'.format( 
                    val_frq, kwargs['_results'][0][line], kwargs['_results'][1][line])+'\n'
                    )
            # Write the original data
            outputfile.write('{:^11} {:^11} {:^11}'.format(
                'Wave_freq (Hz)', 'chi* (cm^{3}mol^{-1})', 'chi** (cm^{3}mol^{-1})')+'\n'
                )
            for line,val_frq in enumerate(kwargs['_val_x']):
                outputfile.write('{: 12.8E} {: 12.8E} {: 12.8E}'.format( 
                    val_frq, kwargs['_val_real'][line], kwargs['_val_im'][line])+'\n'
                    )

        return


    def _plot_results(self, _fitted_T, _final_susc, _fittings, _lims_model, _filename ):

        susc_comp = plt.figure(
            figsize=(6.5, 4.5),
            num='AC susceptibility fit'
        )
        spec = susc_comp.add_gridspec(3, 1, height_ratios=[0.05, 1, 1])
        ax1 = susc_comp.add_subplot(spec[0, 0])
        ax2 = susc_comp.add_subplot(spec[1, 0])
        ax4 = susc_comp.add_subplot(spec[2, 0], sharex=ax2)
        susc_comp.subplots_adjust(hspace=.05, wspace=.02)

        cole_cole, (ax5,ax3) = plt.subplots( 2, 1, sharex='none', sharey='none', figsize=(5.1,4.8), gridspec_kw={"height_ratios":[0.03, 1]}, num='Fitted Cole-Cole Data')
        cole_cole.subplots_adjust(hspace=.02, wspace=.02)
        
        # Define the colors.
        colors = cmaps['coolwarm'].resampled(len(_fitted_T))
        ratio = np.linspace(0,1,len(_fitted_T)) # controls which color is plotted
        for it in range(len(_fitted_T)): # fitted_T contains the indices and values of the temperatures that are correctly fitted.
            
            # Plot the real and imaginary components
            ax2.semilogx( _final_susc[it][0], _final_susc[it][1], 'o', color=colors(ratio[it]), markersize=4, fillstyle='none' )
            ax2.semilogx( _lims_model[it],    _fittings[it][0],   '-', color=colors(ratio[it]), lw=1 )
            ax4.semilogx( _final_susc[it][0], _final_susc[it][2], 'o', color=colors(ratio[it]), markersize=4, fillstyle='none' )
            ax4.semilogx( _lims_model[it],    _fittings[it][1],   '-', color=colors(ratio[it]), lw=1 )
            # Plot Cole-Cole
            ax3.plot( _final_susc[it][1], _final_susc[it][2], 'o', color=colors(ratio[it]), markersize=4, fillstyle='none' )
            ax3.plot( _fittings[it][0],   _fittings[it][1],   '-', color=colors(ratio[it]), lw=1 )

        # Make colourbar
        # norm creates an indexing for the colourmap values - i.e. what do the start and end correspond to
        # I added another element to have the same lenght as colors and summed 1 to access the last shifted tick (_fitted_T[-1][1]+.5 in cbticks).
        norm = mpl_col.BoundaryNorm( [ i[1] for i in _fitted_T ]+[_fitted_T[-1][1]+1], ncolors=colors.N) 
        
        # Scalar mappable converts colourmap numbers into an image of colours
        sm = plt.cm.ScalarMappable(cmap=colors, norm=norm)
        
        # Plot colourbar and attach to axes
        cbar_susc = susc_comp.colorbar(sm, orientation='horizontal', format='%.1f', cax=ax1)
        cbar_cole = cole_cole.colorbar(sm, orientation='horizontal', format='%.1f', cax=ax5)
        
        #Set colourbar label - technically title - above bar
        cbar_susc.ax.set_title('T (K)', fontsize='smaller')
        cbar_cole.ax.set_title('T (K)', fontsize='smaller')
        
        # Put the x-labels of the colourbar on top
        for axis in [ax1,ax5]:
            axis.xaxis.set_ticks_position('top')

        # Calculate the steps between each of the T points to shift the colorbar ticks.
        if len(_fitted_T) > 1:
            deltat = [ 
                ( _fitted_T[i+1][1] - x )/2. 
                if i < len(_fitted_T)-1 
                else ( _fitted_T[-1][1] - _fitted_T[-2][1] )/2. 
                for i, x in enumerate(np.array(_fitted_T)[:,1]) 
                ]
        elif len(_fitted_T) == 1:
            deltat = [ _fitted_T[0][1] + 0.5
                ]

        # Set the colourbar ticks and ticks_labels.
        cbticks = [ 
            float(val)+deltat[it] 
            if it < len(_fitted_T)-1 
            else _fitted_T[-1][1]+.5 
            for it, val in enumerate(np.array(_fitted_T)[:,1]) ]
        cbticklabels = [ 
            ('{:4.1f}').format(val) 
            if it < len(_fitted_T)-1 
            else ('{:4.1f}').format(_fitted_T[-1][1]) 
            for it, val in enumerate(np.array(_fitted_T)[:,1]) ]

        # To avoid labels overlapping, get every second one and rotate if many points.
        if len(_fitted_T) > 12:
            cbticks = cbticks[::2]
            cbticklabels = cbticklabels[::2]
            plt.setp(cbar_susc.ax.get_xticklabels(),rotation=45,fontsize=8.5)
            plt.setp(cbar_cole.ax.get_xticklabels(),rotation=45,fontsize=8.5)

        cbar_susc.set_ticks( cbticks )
        cbar_cole.set_ticks( cbticks )
        cbar_susc.set_ticklabels( cbticklabels )
        cbar_cole.set_ticklabels( cbticklabels )

        # Unpack frequencies.
        freqs = [list(i[0]) for i in _final_susc]

        # Set decimal places for wave frequency.
        minfreq = np.min(self.flatten_recursive(freqs))
        if minfreq >= 0.1:
            decimals = '%.1f'
        elif 0.001 < minfreq < 0.01:
            decimals = '%.2f'
        elif minfreq <= 0.001:
            decimals = '%.3f'
        # Sets float formatting.
        for axis in [ax2.xaxis, ax2.yaxis,
                     ax3.xaxis, ax3.yaxis,
                     ax4.yaxis]:
            axis.set_major_formatter(mtick.FormatStrFormatter('%.1f'))
        ax4.xaxis.set_major_formatter(mtick.FormatStrFormatter(decimals))

        # Get rid of the frames of susceptibility plots
        for axis in [ax2, ax4, ax3]:
            axis.spines["top"].set_visible(False)
            axis.spines["right"].set_visible(False)
        # Set labels for the axes
        ax4.set_xlabel('Wave Frequency (Hz)')
        ax2.set_ylabel(r'$\chi^{,}$  (cm$^{3}$mol$^{-1}$)')
        ax4.set_ylabel(r'$\chi^{,,}$ (cm$^{3}$mol$^{-1}$)')
        ax3.set_xlabel(r'$\chi^{,}$  (cm$^{3}$mol$^{-1}$)')
        ax3.set_ylabel(r'$\chi^{,,}$ (cm$^{3}$mol$^{-1}$)')
        
        susc_comp.savefig(_filename+'_susc_comp.png', dpi=300)
        cole_cole.savefig(_filename+'_Cole_Cole.png', dpi=300)
        plt.show()
        plt.close('all')

        print('\n    Plot of susceptibility components saved as\n     {}'.format(_filename+'_susc_comp.png'))
        print('    Cole-Cole plot saved as\n     {}'.format(_filename+'_Cole_Cole.png'))

        return


    def susc_vs_T(self, RawDatalist, DC_field, fitted_data, _filename):

        '''Plot susceptibility components versus T for the selected temperatures.

        Parameters:
            RawDatalist    (list): Contains the raw data as a list of objects. Sorted by field and temperature.
            DC_field       (list): Contains the Direct Current field employed (Oe).
            fitted_data    (dict): Contains the final selected temperatures (K).

        Returns:        
            Plots          (fig) : Temperature dependent plots of the real and imaginary susceptibility components.
            
        '''

        # Sort the raw data by frequency, temperature and field.
        RawDatalist.sort(key=lambda k: ( k.frequency, k.temperature, k.DC_field))

        # Define the figure    
        fig, (ax1,ax2) = plt.subplots( 2, 1, sharex=True, sharey=False, figsize=(7,5), num='Temperature dependence of susceptibilities') # 8.27, 11.69 A4
        fig.subplots_adjust(hspace=.1, wspace=.02)


        # Create the list where I will append the unique frequencies used for the current DC field.
        wave_freq = []  
        
        # Get the unique values of frequencies employed.
        for res in RawDatalist:
            if res.frequency not in wave_freq and res.DC_field == DC_field:
                wave_freq.append(res.frequency) 

        # Retrieve the temperatures that have been successfully fitted for each DC field.
        # because it might happen that the raw data for susceptibilities has fewer points than T because they were negative, 
        # val_x needs to be recalculated each time
        temps = np.array( fitted_data[DC_field] )[:,1] # field is the key of the fitted_data dictionary. Its values is a list of tuples, where T is the second entry of the tuple.

        for it,freq in enumerate(wave_freq):

            val_real = np.array( [ r.sus_real    for r in RawDatalist if r.DC_field == DC_field and r.frequency == freq and round(r.temperature,1) in np.round(temps,1) ] )
            val_im   = np.array( [ r.sus_im      for r in RawDatalist if r.DC_field == DC_field and r.frequency == freq and round(r.temperature,1) in np.round(temps,1) ] )
            val_x    = np.array( [ r.temperature for r in RawDatalist if r.DC_field == DC_field and r.frequency == freq and round(r.temperature,1) in np.round(temps,1) ] )
            
            ax1.plot( val_x, val_real, '-o', markersize=4, markerfacecolor='None', lw=0.75, label=str(round(freq,2))+'(Hz)' if it==0 or it==len(wave_freq) or it%4==0 else '')
            ax2.plot( val_x, val_im,   '-o', markersize=4, markerfacecolor='None', lw=0.75)#, label=str(round(freq,2))+'(Hz)' if it==0 or it==len(wave_freq) or it%4==0 else '')

        for axis in [ax1.yaxis, ax2.yaxis, ]: # Sets float formatting with two decimal numbers
            axis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
        for axis in [ax1, ax2]:
            axis.spines["top"].set_visible(False)
            axis.spines["right"].set_visible(False)
        ax1.legend(loc=0, fontsize='small', numpoints=1, ncol=5, frameon=False)
        ax1.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.1f')) 
        ax2.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.1f')) 
        ax2.set_xlabel('Temperature (K)')
        ax1.set_ylabel(r'$\chi^{,}$  (emu)')
        ax2.set_ylabel(r'$\chi^{,,}$ (emu)')
        
        #fig.tight_layout()
        fig.savefig(_filename+'_suscp_vs_T.png', dpi=300)
        plt.show()
        plt.close('all')

        print('    Temperature dependence of susceptibilities plot saved as\n     {}'.format(_filename+'_suscp_vs_T.png'))

        return


class Relaxation_profile():

    def __init__(self):
        self._guess      = None
        self.RP_function = None # Name (string) of the selected option.
        self.RP_model    = None # Actual method used in the fit.
        self.yerr_plt    = None
        self.yerr_fit    = None
        self.bounds      = None
        self.field       = None
        self.process     = None
        self.ZFQTM_fixed = None # In case constrained functions for tau_vs_H are requested.
        self.Ct_fixed    = None # 

    def read_rates(self, File, exe_mode, AC_model=None, DC_model=None): # AC_model=[Debye, Generalised], DC_model=[Single, Stretched]
        '''
        Reads-in the data and defines self.yerr_plt, self.yerr_fit.
        '''

        if exe_mode == 'AC':

            # Read in the file        
            data = np.loadtxt(File, skiprows=1)

            if AC_model == 'Debye':

                xvals   = data[ :,0 ]
                tau     = data[ :,1 ]
                tau_err = data[ :,2 ]
                tau     = np.array( [ x for _,x in sorted(zip(xvals,tau)) ] )
                tau_err = np.array( [ x for _,x in sorted(zip(xvals,tau_err)) ] )
                xvals.sort()
                self.yerr_plt, self.yerr_fit = self.calculate_yerr(tau, tau_err )

            elif AC_model == 'Generalised':

                xvals   = data[ :,0 ]
                tau     = data[ :,1 ]
                tau_err = data[ :,4 ]
                alpha   = data[ :,9 ]
                tau     = np.array( [ x for _,x in sorted(zip(xvals,tau)) ] )
                tau_err = np.array( [ x for _,x in sorted(zip(xvals,tau_err)) ] )
                alpha   = np.array( [ x for _,x in sorted(zip(xvals,alpha)) ] )            
                xvals.sort()
                self.yerr_plt, self.yerr_fit = self.calculate_yerr(tau, tau_err, _alpha=alpha, _sigma=args.sigma )


        elif exe_mode == 'DC':
            
            # Read in the file        
            data = np.loadtxt(File, skiprows=1)

            if DC_model == 'single':

                xvals   = data[ :,0 ]
                field   = data[ :,1 ]
                tau     = data[ :,4 ]
                tau_err = data[ :,5 ]
                tau     = np.array( [ x for _,x in sorted( zip(xvals,tau)     ) ] )
                tau_err = np.array( [ x for _,x in sorted( zip(xvals,tau_err) ) ] )
                xvals.sort()
                self.yerr_plt, self.yerr_fit = self.calculate_yerr(tau, tau_err )

            elif DC_model == 'stretched':

                xvals   = data[ :,0 ]
                field   = data[ :,1 ]
                tau     = data[ :,4 ]
                tau_err = data[ :,5 ]
                beta    = data[ :,6 ]
                tau     = np.array( [ x for _,x in sorted( zip(xvals,tau)     ) ] )
                tau_err = np.array( [ x for _,x in sorted( zip(xvals,tau_err) ) ] )
                beta    = np.array( [ x for _,x in sorted( zip(xvals,beta)    ) ] )            
                xvals.sort()
                self.yerr_plt, self.yerr_fit = self.calculate_yerr(tau, tau_err, _beta=beta )            


        else: # RelaxationProfile

            AC, DC = False, False
            xvals_AC, xvals_DC, tau_AC, tau_DC, tau_err_AC, tau_err_DC, alpha, beta = [], [], [], [], [], [], [], []

            with open(File, 'r') as f:
                for line in f:
                    if line.find('AC') != -1:
                        AC = True
                        line = next(f) # skip the headers. To avoid problems with spaces, I only care about the length of the data

                        for line in f:

                            try:
                            
                                xvals_AC.append(   float(line.split()[0]) )
                                tau_AC.append(     float(line.split()[1]) )
                                tau_err_AC.append( float(line.split()[2]) )
                                if len(line.split()) == 4: # contains alpha
                                    if float(line.split()[3]) == 0.: 
                                        alpha.append( 0.001 )
                                    else:
                                        alpha.append(  float(line.split()[3]) )

                            except:
                                break

            with open(File, 'r') as f:
                for line in f:
                    if line.find('DC') != -1:
                        DC = True
                        line = next(f) # skip the headers. To avoid problems with spaces, I only care about the length of the data

                        for line in f:

                            try:
                                
                                xvals_DC.append(   float(line.split()[0]) )
                                tau_DC.append(     float(line.split()[1]) )
                                tau_err_DC.append( float(line.split()[2]) )
                                if len(line.split()) == 4: # contains alpha
                                    if float(line.split()[3]) == 1.:
                                        beta.append( 0.999 )
                                    else:
                                        beta.append(  float(line.split()[3]) )
                            except:
                                break

            if AC and not DC:

                tau_AC     = np.array( [ x for _,x in sorted( zip(xvals_AC,tau_AC)     ) ] )
                tau_err_AC = np.array( [ x for _,x in sorted( zip(xvals_AC,tau_err_AC) ) ] )

                if len(alpha) != 0:
                    alpha = np.array( [ x for _,x in sorted( zip(xvals_AC,alpha)     ) ] )
                else: alpha = None

                xvals_AC.sort()

                self.yerr_plt, self.yerr_fit = self.calculate_yerr(tau_AC, tau_err_AC, _alpha=alpha, _sigma=args.sigma )

                xvals, tau = xvals_AC, tau_AC

            elif DC and not AC:

                tau_DC     = np.array( [ x for _,x in sorted( zip(xvals_DC,tau_DC)     ) ] )
                tau_err_DC = np.array( [ x for _,x in sorted( zip(xvals_DC,tau_err_DC) ) ] )

                if len(beta) != 0:
                    beta = np.array( [ x for _,x in sorted( zip(xvals_DC,beta)     ) ] )
                else: beta = None


                xvals_DC.sort()

                self.yerr_plt, self.yerr_fit = self.calculate_yerr(tau_DC, tau_err_DC, _beta=beta )

                xvals, tau = xvals_DC, tau_DC

            elif AC and DC:

                tau_AC     = np.array( [ x for _,x in sorted( zip(xvals_AC,tau_AC)     ) ] )
                tau_err_AC = np.array( [ x for _,x in sorted( zip(xvals_AC,tau_err_AC) ) ] )

                if len(alpha) != 0:
                    alpha = np.array( [ x for _,x in sorted( zip(xvals_AC,alpha)     ) ] )
                else: alpha = None

                xvals_AC.sort()

                tau_DC     = np.array( [ x for _,x in sorted( zip(xvals_DC,tau_DC)     ) ] )
                tau_err_DC = np.array( [ x for _,x in sorted( zip(xvals_DC,tau_err_DC) ) ] )

                if len(beta) != 0:
                    beta = np.array( [ x for _,x in sorted( zip(xvals_DC,beta)     ) ] )
                else: beta = None

                xvals_DC.sort()

                yerr_plt_AC, yerr_fit_AC = self.calculate_yerr(tau_AC, tau_err_AC, _alpha=alpha, _sigma=args.sigma )
                yerr_plt_DC, yerr_fit_DC = self.calculate_yerr(tau_DC, tau_err_DC, _beta=beta )

                # Stack them back together
                self.yerr_plt = np.hstack( (yerr_plt_AC, yerr_plt_DC) )
                self.yerr_fit = np.hstack( (yerr_fit_AC, yerr_fit_DC) )

                xvals = np.hstack((xvals_AC,xvals_DC))
                tau = np.hstack((tau_AC, tau_DC))

                # Sort all the arrays by xvals. For this xvals has to be sorted last
                tau = tau[np.argsort(xvals)]

                for i in range(2):
                    self.yerr_plt[i] = self.yerr_plt[i][np.argsort(xvals)]
                self.yerr_fit = self.yerr_fit[np.argsort(xvals)]

                xvals = np.sort(xvals)

        return np.array(xvals), np.array(tau)
        
    def calculate_yerr(self, tau, tau_err, _alpha=None, _sigma=None, _beta=None): 

        if _alpha is None and _beta is None:

            yerr_plt    = np.vstack( ( ( 1./tau - 1./(tau + tau_err) ), ( 1./(tau - tau_err) - 1./tau ) ) ) #First row contains the lower errors, the second row contains the upper errors.
            yerr_fit    = np.amax( (abs( np.log10(1./tau) - np.log10(1./(tau + tau_err) ) ), abs( np.log10(1./tau) - np.log10(1./(tau - tau_err) ) ) ), axis=0) # this error is passed to the fitting function that returns the log10 of the data
            
            return yerr_plt, yerr_fit

        else:

            N_err_lower = 1./(tau+tau_err)
            N_err_upper = 1./(tau-tau_err)
            
            if _alpha is not None:

                # Recalculate tau_lognormal_err in case the user provides a different sigma
                _err_lower = 1./self.log_normal( tau, _alpha,  _sigma )
                _err_upper = 1./self.log_normal( tau, _alpha, -_sigma )


            if _beta is not None:

                tau_upper, tau_lower = self.Johnston( tau, _beta )

                _err_lower = 1./tau_upper
                _err_upper = 1./tau_lower


            yerr_fit = np.amax(
                            (
                            abs( np.log10(1./tau) - np.log10(_err_lower) ),
                            abs( np.log10(1./tau) - np.log10(_err_upper) ),
                            abs( np.log10(1./tau) - np.log10(N_err_lower)  ),
                            abs( np.log10(1./tau) - np.log10(N_err_upper)  )
                            ),
                                axis=0 )

            _ind = np.argmax(
                            (
                            abs( np.log10(1./tau) - np.log10(_err_lower) ),
                            abs( np.log10(1./tau) - np.log10(_err_upper) ),
                            abs( np.log10(1./tau) - np.log10(N_err_lower)  ),
                            abs( np.log10(1./tau) - np.log10(N_err_upper)  )
                            ),
                                axis=0 )
                
            yerr_plt = np.zeros((2,len(_ind)))
            for it,val in enumerate(_ind):
                if   ( val == 0 or val == 1):
                    yerr_plt[0,it], yerr_plt[1,it] = (1./tau[it] - _err_lower[it]), (_err_upper[it] - 1./tau[it])
                elif ( val == 2 or val == 3):
                    yerr_plt[0,it], yerr_plt[1,it] = (1./tau[it] - N_err_lower[it] ), (N_err_upper[it] - 1./tau[it] )

            if args.verbose:
                print('Sigma used: {}'.format(_sigma))
                print('\nThe rates are:\n{}'.format(1./tau))
                print('The lower and upper errors in the rates are:\n{}\n'.format(yerr_plt))

            return yerr_plt, yerr_fit

    def log_normal(self, tau, alpha, sigma):
        '''
        In log10 space, the positive and negative deviations from tau are equal -> I calculate only the positive.
        '''
        return tau*np.exp( (1.82*sigma*np.sqrt(alpha))/(1-alpha) )

    def Johnston(self, tau, beta):

        tau_upper = tau*np.exp(   ( 1.64*np.tan( np.pi*(1-beta)/2. ) ) / ((1-beta)**0.141) )
        tau_lower = tau*np.exp( - ( 1.64*np.tan( np.pi*(1-beta)/2. ) ) / ((1-beta)**0.141) )

        return tau_upper, tau_lower

    # tau_vs_T under zero-field.
    def Orbach(self, temperature, pre_1, U_1):
        return np.log10( 10**(-pre_1)*np.exp(-U_1/(temperature)) )

    def Raman(self, temperature, C, n):
        return np.log10( 10**C*(temperature**n) )

    def QTM(self, temperature, QTM):
        return np.log10( (10**(-QTM)*temperature/temperature) )

    def Orbach_QTM(self, temperature, pre_1, U_1, QTM):
        return np.log10( 10**(-pre_1)*np.exp(-U_1/(temperature)) + 10**(-QTM) )
        #return np.log10( 10**self.Orbach(temperature, pre_1, U_1) + 10**self.QTM(temperature, QTM) )

    def Orbach_Raman(self, temperature, pre_1, U_1, C, n):
        return np.log10( 10**(-pre_1)*np.exp(-U_1/(temperature)) + 10**C*(temperature**n) )
        #return np.log10( 10**self.Orbach(temperature, pre_1, U_1) + 10**self.Raman(temperature, C, n) )

    def Orbach_Raman_QTM(self, temperature, pre_1, U_1, C, n, QTM):
        return np.log10( 10**(-pre_1)*np.exp(-U_1/(temperature)) + 10**C*(temperature**n) + 10**(-QTM) )
        #return np.log10( 10**self.Orbach(temperature, pre_1, U_1) + 10**self.Raman(temperature, C, n) + 10**self.QTM(temperature, QTM) )

    def Raman_QTM(self, temperature, C, n, QTM):
        return np.log10( 10**C*(temperature**n) + 10**(-QTM) )
        #return np.log10( 10**self.Raman(temperature, C, n) + 10**self.QTM(temperature, QTM) )

    def Two_Orbach(self, temperature, pre_1, U_1, pre_2, U_2):
        return np.log10( 10**(-pre_1)*np.exp(-U_1/(temperature)) + 10**(-pre_2)*np.exp(-U_2/(temperature)) )
        #return np.log10( 10**self.Orbach(temperature, pre_1, U_1) + 10**self.Orbach(temperature, pre_2, U_2) )

    # tau_vs_T in-field (> 0.5 T).
    def Direct(self, temperature, A):
        return np.log10(10**A*temperature)

    def Orbach_Direct(self, temperature, pre_1, U_1, A):
        return np.log10( 10**(-pre_1)*np.exp(-U_1/(temperature)) + 10**A*temperature )
        #return np.log10( 10**self.Orbach(temperature, pre_1, U_1) + 10**self.Direct(temperature, A) )

    def Raman_Direct(self, temperature, C, n, A):
        return np.log10( 10**C*(temperature**n) + 10**A*temperature )
        #return np.log10( 10**self.Raman(temperature, C, n) + 10**self.Direct(temperature, A) )

    def Orbach_Raman_Direct(self, temperature, pre_1, U_1, C, n, A):
        return np.log10( 10**(-pre_1)*np.exp(-U_1/(temperature)) + 10**C*(temperature**n) + 10**A*temperature )
        #return np.log10( 10**self.Orbach(temperature, pre_1, U_1) + 10**self.Raman(temperature, C, n) + 10**self.Direct(temperature, A) )


    # tau_vs_H.
    def FieldQTM(self, field, ZFQTM, FQTM, p): # ZFQTM, FQTM are the zero-field and in-field QTM params.
        return np.log10( 10**(-ZFQTM)/(1 + (10**(-FQTM))*(field**p)) )

    def RamanII(self, field, CII, mII):
        return np.log10( (10**CII)*(field**mII ) )

    def Ct(self, field, Cte):
        return np.log10( (10**Cte*(field/field)) )

    def FieldQTM_RamanII_Ct(self, field, ZFQTM, FQTM, p, CII, mII, Cte):
        return np.log10( 10**(-ZFQTM)/(1 + (10**(-FQTM))*(field**p)) + (10**CII)*(field**mII) + 10**Cte )

    def FieldQTM_RamanII_Ct_constrained(self, field, FQTM, p, CII, mII):
        return np.log10( 10**(-self.ZFQTM_fixed)/(1 + (10**(-FQTM))*(field**p)) + (10**CII)*(field**mII) + 10**self.Ct_fixed )

    def BronsVanVleck_RamanII(self, field, e, f,  CII, mII):
        return np.log10( ((1 + (10**e)*(field**2))/(1 + (10**f)*(field**2))) * (10**CII)*(field**mII) )

    def BronsVanVleck_Cte(self, field, e, f, Cte):
        return np.log10( ((1 + (10**e)*(field**2))/(1 + (10**f)*(field**2))) * 10**Cte )

    def BronsVanVleck_Cte_constrained(self, field, e, f):
        return np.log10( ((1 + (10**e)*(field**2))/(1 + (10**f)*(field**2))) * 10**self.Ct_fixed )


    def fitting_params(self, RP_function):

        # Define the model function passed to curve_fit.

        # tau_vs_T under zero-field.
        if RP_function == 'Orbach':               
            self.RP_model = self.Orbach
            self.bounds = np.array(([np.log10(1E-30), 0.0],[np.inf, np.inf])) # tau0, U
        elif RP_function == 'Raman':                
            self.RP_model = self.Raman
            self.bounds = np.array(([np.log10(1E-30), 0.0],[np.inf, np.inf])) # C, n
        elif RP_function == 'QTM':                  
            self.RP_model = self.QTM
            self.bounds = np.array(([np.log10(1E-30)],[np.inf])) # tau_0_QTM
        elif RP_function == 'Orbach + QTM':         
            self.RP_model = self.Orbach_QTM
            self.bounds = np.array(([np.log10(1E-30), 0.0, np.log10(1E-30)],[np.inf, np.inf, np.inf])) # tau0, U, tau_0_QTM
        elif RP_function == 'Raman + QTM':          
            self.RP_model = self.Raman_QTM
            self.bounds = np.array(([np.log10(1E-30), 0.0, np.log10(1E-30)],[np.inf, np.inf, np.inf])) # C, n, tau_0_QTM
        elif RP_function == 'Orbach + Raman':       
            self.RP_model = self.Orbach_Raman
            self.bounds = np.array(([np.log10(1E-30), 0.0, np.log10(1E-30), 0.0],[np.inf, np.inf, np.inf, np.inf])) # tau0, U, C, n
        elif RP_function == 'Orbach + Raman + QTM': 
            self.RP_model = self.Orbach_Raman_QTM
            self.bounds = np.array(([np.log10(1E-30), 0.0, np.log10(1E-30), 0.0, np.log10(1E-30)],[np.inf, np.inf, np.inf, np.inf, np.inf])) # tau0, U, C, n, tau_0_QTM
        # tau_vs_T in-field (> 0.5 T).
        elif RP_function == 'Orbach + Direct':
            self.RP_model = self.Orbach_Direct
            self.bounds = np.array(([np.log10(1E-30), 0.0, np.log10(1E-30)],[np.inf, np.inf, np.inf])) # tau0, U, A
        elif RP_function == 'Raman + Direct':
            self.RP_model = self.Raman_Direct
            self.bounds = np.array(([np.log10(1E-30), 0.0, np.log10(1E-30)],[np.inf, np.inf, np.inf])) # C, n,  A
        elif RP_function == 'Orbach + Raman + Direct':
            self.RP_model = self.Orbach_Raman_Direct
            self.bounds = np.array(([np.log10(1E-30), 0.0, np.log10(1E-30), 0.0, np.log10(1E-30)],[np.inf, np.inf, np.inf, np.inf, np.inf])) # tau0, U, C, n,  A
        # tau_vs_H.
        elif RP_function == 'Brons-Van-Vleck & Ct':
            self.RP_model = self.BronsVanVleck_Cte
            self.bounds = np.array(([np.log10(1E-30), np.log10(1E-30), np.log10(1E-30)],[np.inf, np.inf, np.inf])) # e, f, Ct
        elif RP_function == 'Brons-Van-Vleck & Ct - constrained':
            self.RP_model = self.BronsVanVleck_Cte_constrained
            self.bounds = np.array(([np.log10(1E-30), np.log10(1E-30)],[np.inf, np.inf])) # e, f
        elif RP_function == 'Brons-Van-Vleck & Raman-II':
            self.RP_model = self.BronsVanVleck_RamanII
            self.bounds = np.array(([np.log10(1E-30), np.log10(1E-30), np.log10(1E-30), 0.0],[np.inf, np.inf, np.inf, 10.])) # e, f, C, m
        elif RP_function == 'Raman-II':
            self.RP_model = self.RamanII
            self.bounds = np.array(([np.log10(1E-30), 0.0],[np.inf, 10.])) # C, m
        elif RP_function == 'QTM(H)':
            self.RP_model = self.FieldQTM
            self.bounds = np.array(([np.log10(1E-30), np.log10(1E-30), 0.0 ],[np.inf, np.inf, 10.])) # ZFQTM, FQTM, p
        elif RP_function == 'QTM(H) + Raman-II + Ct':
            self.RP_model = self.FieldQTM_RamanII_Ct
            self.bounds = np.array(([np.log10(1E-30), np.log10(1E-30), 0.0, np.log10(1E-30), 0.0, np.log10(1E-30)],[np.inf, np.inf, 10., np.inf, np.inf, 10.])) # ZFQTM, FQTM, p, C, m, Ct
        elif RP_function == 'QTM(H) + Raman-II + Ct - constrained':
            self.RP_model = self.FieldQTM_RamanII_Ct_constrained
            self.bounds = np.array(([np.log10(1E-30), 0.0, np.log10(1E-30), 0.0],[np.inf, 10., np.inf, 10.])) # FQTM, p, C, m


        return 

    def select_model_rates_plot(self, xvals, rate, process, RadioButtonsOptions):

        # Reset the value in case the plot is closed after having pressed "Change Model" button.
        self.RP_function == None

        fig1, ax = plt.subplots(figsize=(6.0,6.0), num='Select relaxation model')
        ax.set_xscale('log')
        ax.set_yscale('log')

        if process == 'tau_vs_T': 
            plt.suptitle('Temperature dependence of relaxation rates.\nSelect type of process.', fontsize=10)
            # Set x and y limits
            set_rate_xy_lims(ax, rate, self.yerr_plt, xvals)
            ax.set_xlabel(r'Temperature (K)')
            # set the position of the buttons.
            button_position = [0.1, 0.35, 0.3, 0.75]

        elif process == 'tau_vs_H':
            plt.suptitle('Field dependence of relaxation rates.\nSelect type of process.', fontsize=10)
            ax.set_xlabel(r'Field (Oe)')
            # set the position of the buttons.
            button_position = [0.35, 0.63, 0.25, 0.3] # left, bottom, width, height.

        ax.errorbar(xvals, rate, yerr=self.yerr_plt, marker='o', ls='none', fillstyle='none', color='r' )

        rax = plt.axes(button_position, facecolor='w',  frameon=False, aspect='equal')
        radio = RadioButtons(
            rax,
            RadioButtonsOptions,
            radio_props={
                's': 100,
                'facecolor': 'blue'
            }
        )

        fc = [
            [0, 0, 0, 0] for _ in range(len(RadioButtonsOptions))
        ]

        radio._buttons.set_facecolor(
            fc
        )

        def model_radiobutton(label):
            print('    Model function {} has been selected.'.format(radio.value_selected))
            self.RP_function = radio.value_selected
            plt.pause(0.05)
            plt.close(fig1)

        radio.on_clicked(model_radiobutton)

        ax.set_ylabel(r'$\tau^{-1}$ (s$^{-1}$)')

        plt.show()
        plt.close(fig1)

        if self.RP_function == None: exit()

        return

    def select_model_rates(self, xvals, rate, field, _process=None):

        # Defined as an attribute so change_model_rates() can access it.
        self.field = field
        self.process = _process

        if _process == 'tau_vs_T':

            if self.field:
                options = ['Orbach + Direct', 'Raman + Direct', 'Orbach + Raman + Direct']
            else:
                options = ['Orbach', 'Raman', 'QTM', 'Orbach + Raman', 'Orbach + QTM', 'Raman + QTM', 'Orbach + Raman + QTM']

        elif _process == 'tau_vs_H':

            # If there is a data point measured at 0 field, raise an error.
            if float(0) in xvals:
                exit('\n***Error***:\n Zero-field data point present in the data.\n Delete that point and try again.')

            options  = [
                'QTM(H) + Raman-II + Ct', 'QTM(H) + Raman-II + Ct - constrained', 
                'Brons-Van-Vleck & Raman-II', 
                'Brons-Van-Vleck & Ct', 'Brons-Van-Vleck & Ct - constrained'
                ]

        self.select_model_rates_plot(xvals, rate, _process, options)

        # Define self.RP_model and self.bounds.
        self.fitting_params(self.RP_function)

        # Fit the relaxation profile.
        self.guess_fit_rates(
            xvals, rate
                )

        return

    def change_model_rates(self, event, xvals, rate, field): # I had to define 2 different ones since this one needs an event trigger.

        plt.close('all')

        if self.process == 'tau_vs_T':

            if self.field:
                options = ['Orbach + Direct', 'Raman + Direct', 'Orbach + Raman + Direct']
            else:
                options = ['Orbach', 'Raman', 'QTM', 'Orbach + Raman', 'Orbach + QTM', 'Raman + QTM', 'Orbach + Raman + QTM']

        elif self.process == 'tau_vs_H':

            options  = [
                'QTM(H) + Raman-II + Ct', 'QTM(H) + Raman-II + Ct - constrained', 
                'Brons-Van-Vleck & Raman-II', 
                'Brons-Van-Vleck & Ct', 'Brons-Van-Vleck & Ct - constrained'
                ]
            

        self.select_model_rates_plot(xvals, rate, self.process, options)

        # Define self.RP_model and self.bounds.
        self.fitting_params(self.RP_function)

        # Fit the relaxation profile.
        self.guess_fit_rates(
            xvals, rate
                )

        return


    def guess_fit_rates(self, xvals, rate):

        fig, ax = plt.subplots(figsize=(7,6), num='Choose starting guess')
        plt.subplots_adjust(left=0.15, bottom=0.15)
        ax.errorbar(xvals, rate, yerr=self.yerr_plt, marker='o', ls='none', fillstyle='none', color='r' )
        ax.set_xscale('log')
        ax.set_yscale('log')


        axcolor = 'lightgoldenrodyellow'
        #colors = {
        #'Orbach': 'indianred',
        #'Raman':  'cornflowerblue',
        #}

        # Initial guess for all possible params. The actual value is defined as log because it is passed to the 10** functions.
        _init = {
            'Orbach' :       ([np.log10(1E-5), 1E-13, 1E-1], [500, 5, 2000]),                # ([tau0, min, max], [Ueff, min, max])
            'Raman'  :       ([np.log10(1E-4), 1E-9,  1E-1], [9, 1, 12]),                    # ([C, min, max], [n, min, max]) 
            'QTM'    :       ([np.log10(1E-2), 1E-6,  1000], []),                            # ([tau0QTM, min, max])
            'Direct' :       ([np.log10(1E-5), 1E-10, 1],    []),                            # ([A, min, max])
            'BronsVanVleck': ([np.log10(1E-5), np.log10(1E-5)],    [1E-10, 1],    [1E-10, 1]),               # ([e, f], [min, max], [min, max])
            'RamanII':       ([np.log10(1E-4), 4],                 [1E-30, 1E-1], [0.5, 8]),                 # ([CII, mII],, [minCII, maxCII] [minmII, maxmII]) 
            'FieldQTM':      ([np.log10(1E-2), np.log10(1E-2), 2], [1E-30, 1000], [1E-30, 1000], [0.5, 6.]), # ([ZFQTM, FQTM, p], [min, max], [min, max], [min, max])
            'Cte':           ([np.log10(1E-4)],                    [1E-14, 1E4])                             # ([Ct], [min, max])
            }

        # Positions of buttons in the plot. [left, bottom, width, height]
        _buttons = {
            'Reset':  [0.91, 0.83, 0.08, 0.05],
            'Fit':    [0.91, 0.75, 0.06, 0.05],
            'Change': [0.91, 0.62, 0.08, 0.08],
            'tau0':   [0.24, 0.890, 0.20, 0.02], # Orbach
            'U':      [0.64, 0.890, 0.20, 0.02], # Orbach
            'C':      [0.24, 0.925, 0.20, 0.02], # Raman
            'n':      [0.64, 0.925, 0.20, 0.02], # Raman
            'qtm':    [0.44, 0.965, 0.20, 0.02], # QTM
            'A':      [0.44, 0.965, 0.20, 0.02], # Direct
            'e':      [0.22, 0.890, 0.20, 0.02], # Brons-Van-Vleck
            'f':      [0.22, 0.925, 0.20, 0.02], # Brons-Van-Vleck
            'CII':    [0.68, 0.890, 0.20, 0.02], # RamanII
            'mII':    [0.68, 0.925, 0.20, 0.02], # RamanII
            'Cte':    [0.68, 0.965, 0.20, 0.02], # Raman (isothermal)
            'ZFQTM':  [0.22, 0.890, 0.20, 0.02], # QTM(H)
            'FQTM':   [0.22, 0.925, 0.20, 0.02], # QTM(H)
            'p':      [0.22, 0.965, 0.20, 0.02]  # QTM(H)
            } # options  = ['QTM(H) + Raman-II + Cte', 'Brons-Van-Vleck & Raman-II', 'Brons-Van-Vleck & Cte']

        # Define the buttons.
        resetax       = plt.axes(_buttons['Reset'])
        fitax         = plt.axes(_buttons['Fit'])
        changemodelax = plt.axes(_buttons['Change'])
        button        = Button( resetax,       'Reset',         color='lightgoldenrodyellow', hovercolor='0.975' )
        button_fit    = Button( fitax,         'Fit',           color='lightblue',            hovercolor='0.975' )
        button_change = Button( changemodelax, 'Change\nmodel', color='lightcoral',           hovercolor='0.975' )


        # Attempt a first fit of a reduced set of points to get reasonable parameters estimates.
        if self.RP_function == 'Orbach':
            try:
                popt, pcov  = curve_fit(self.Orbach, xvals[-3:], np.log10(rate[-3:]), bounds=self.bounds)
                init_pre, init_U = popt
                self._guess = popt
                minpre, maxpre, minU, maxU = 10**popt[0]*0.1, 10**popt[0]*10, popt[1]-popt[1]/2., popt[1]+popt[1]/2.
            except: # (RuntimeError, OptimizeWarning):
                print('    Could not estimate initial guess.\n    Use sliders to sample the parameter space.')
                init_pre, init_U, minpre, maxpre, minU, maxU = _init['Orbach'][0][0], _init['Orbach'][1][0], _init['Orbach'][0][1], _init['Orbach'][0][2], _init['Orbach'][1][1], _init['Orbach'][1][2]

            l,     = ax.loglog( xvals, np.power(10., self.Orbach(xvals, init_pre, init_U)), '-', label=self.RP_function )
            axpre  = plt.axes( _buttons['tau0'], facecolor=axcolor )
            axU_1  = plt.axes( _buttons['U'],    facecolor=axcolor )
            spre_1 = Slider(axpre, r'$\tau_0$ (s)',           np.log10(minpre), np.log10(maxpre), np.log10(10**init_pre), valfmt='%4.2E' )
            sU_1   = Slider(axU_1, r'$U_{\mathrm{eff}}$ (K)', minU,             maxU,             init_U,                 valfmt='%3.2f' )
            spre_1.valtext.set_text('{:4.2E}'.format(10**spre_1.val)) # Slider is in log10 scale to appear linear, so I need to convert back. The stored value goes as it is to the function as the model function uses 10**value
            def update(val): 
                fig.canvas.draw_idle()
                prefac, U_Eff = [spre_1.val, sU_1.val]
                self._guess   = [spre_1.val, sU_1.val]
                spre_1.valtext.set_text('{:4.2E}'.format(10**prefac))
                l.set_ydata(np.power(10., self.Orbach(xvals, prefac, U_Eff)))           

            def reset(event):
                sU_1.reset()
                spre_1.reset()
            spre_1.on_changed(update)
            sU_1.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'Raman': 
            try:
                popt, pcov     = curve_fit(self.Raman, xvals[-3:], np.log10(rate[-3:]), bounds=self.bounds)
                init_C, init_n = popt
                self._guess    = [init_C, init_n]
                minC, maxC, minn, maxn = 10**init_C*0.1, 10**init_C*10, init_n-init_n/2., init_n+init_n/2.
            except: 
                print('    Could not estimate initial guess.\n    Use sliders to sample the parameter space.')
                init_C, init_n, minC, maxC, minn, maxn = _init['Raman'][0][0], _init['Raman'][1][0], _init['Raman'][0][1], _init['Raman'][0][2], _init['Raman'][1][1], _init['Raman'][1][2]

            l,  = ax.loglog(xvals, np.power(10., self.Raman(xvals, init_C, init_n)), '-', label=self.RP_function )
            axC = plt.axes(_buttons['C'], facecolor=axcolor)
            axn = plt.axes(_buttons['n'], facecolor=axcolor)
            sC  = Slider(axC, r'$C$ (s$^{-1}$ K$^{-n}$)',  np.log10(minC), np.log10(maxC), np.log10(10**init_C), valfmt='%4.2E' )
            sn  = Slider(axn,  '$n$',                      minn,           maxn,           init_n,               valfmt='%3.2f' )
            sC.valtext.set_text('{:4.2E}'.format(10**sC.val))
            def update(val):
                fig.canvas.draw_idle()
                C, n        = [sC.val, sn.val]
                self._guess = [sC.val, sn.val]
                sC.valtext.set_text('{:4.2E}'.format(10**C))
                l.set_ydata(np.power(10.,self.Raman(xvals, C, n)))
            def reset(event):
                sC.reset()
                sn.reset()
            sC.on_changed(update)
            sn.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'QTM': 
            try:
                popt, pcov  = curve_fit(self.QTM, xvals[-3:], np.log10(rate[-3:]), bounds=self.bounds)
                init_QTM    = popt[0]
                self._guess = init_QTM
                minQTM, maxQTM = 10**init_QTM*0.1, 10**init_QTM*10
            except: #  (RuntimeError, OptimizeWarning):
                print('    Could not estimate initial guess.\n    Use sliders to sample the parameter space.')
                init_QTM, minQTM, maxQTM = _init['QTM'][0][0], _init['QTM'][0][1], _init['QTM'][0][2]

            l,    = ax.loglog(xvals, np.power(10., self.QTM(xvals, init_QTM)), '-', label=self.RP_function )
            axqtm = plt.axes(_buttons['qtm'], facecolor=axcolor)
            sqtm  = Slider(axqtm, r'$\tau_{\mathrm{QTM}}$ (s)', np.log10(minQTM), np.log10(maxQTM), np.log10(10**init_QTM), valfmt='%4.2E' )
            sqtm.valtext.set_text('{:4.2E}'.format(10**sqtm.val))
            def update(val):
                fig.canvas.draw_idle()
                qtm         = sqtm.val
                self._guess = [sqtm.val]
                sqtm.valtext.set_text('{:4.2E}'.format(10**sqtm.val))
                l.set_ydata(np.power(10.,self.QTM(xvals, qtm)))            
            def reset(event):
                sqtm.reset()
            sqtm.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'Orbach + Raman':
            try:
                popt, pcov       = curve_fit(self.Orbach, xvals[-3:], np.log10(rate[-3:]), bounds=self.bounds[:,0:2])
                init_pre, init_U = popt
                popt, pcov       = curve_fit(self.Raman, xvals[:3], np.log10(rate[:3]), bounds=self.bounds[:,2::])
                init_C, init_n   = popt
                self._guess      = [init_pre, init_U, init_C, init_n]
                minpre, maxpre, minU, maxU, minC, maxC, minn, maxn = 10**init_pre*0.1, 10**init_pre*10, init_U-init_U/2., init_U+init_U/2.,\
                 10**init_C*0.1, 10**init_C*10, init_n-init_n/2., init_n+init_n/2.

            except: # (RuntimeError, OptimizeWarning):
                print('    Could not estimate initial guess.\n    Use sliders to sample the parameter space.')
                init_pre, init_U, minpre, maxpre, minU, maxU = _init['Orbach'][0][0], _init['Orbach'][1][0], _init['Orbach'][0][1], _init['Orbach'][0][2], _init['Orbach'][1][1], _init['Orbach'][1][2]
                init_C, init_n, minC, maxC, minn, maxn = _init['Raman'][0][0], _init['Raman'][1][0], _init['Raman'][0][1], _init['Raman'][0][2], _init['Raman'][1][1], _init['Raman'][1][2]


            l,     = ax.loglog(xvals, np.power(10., self.Orbach_Raman(xvals, init_pre, init_U, init_C, init_n)), lw=1.5, label=self.RP_function,  ls='-' )
            k,     = ax.loglog(xvals, np.power(10.,       self.Orbach(xvals, init_pre, init_U)                ), lw=1.3, label='Orbach', ls=':'  )
            j,     = ax.loglog(xvals, np.power(10.,        self.Raman(xvals, init_C,   init_n)                ), lw=1.3, label='Raman',  ls='--' )
            axpre  = plt.axes(_buttons['tau0'], facecolor=axcolor)
            axU_1  = plt.axes(_buttons['U'],    facecolor=axcolor)
            axC    = plt.axes(_buttons['C'],    facecolor=axcolor)
            axn    = plt.axes(_buttons['n'],    facecolor=axcolor)
            spre_1 = Slider( axpre, r'$\tau_0$ (s)',            np.log10(minpre), np.log10(maxpre), valinit=np.log10(10**(init_pre)), valfmt='%4.2E' )
            sU_1   = Slider( axU_1, r'$U_{\mathrm{eff}}$ (K)',  minU,             maxU,             valinit=init_U,                   valfmt='%3.2f' ) 
            sC     = Slider( axC,   r'$C$ (s$^{-1}$ K$^{-n}$)', np.log10(minC),   np.log10(maxC),   valinit=np.log10(10**(init_C)),   valfmt='%4.2E' )
            sn     = Slider( axn,   '$n$',                      minn,             maxn,             valinit=init_n,                   valfmt='%3.2f' )
            spre_1.valtext.set_text('{:4.2E}'.format(10**(spre_1.val))) 
            sC.valtext.set_text(    '{:4.2E}'.format(10**(sC.val)    ))

            def update(val):
                fig.canvas.draw_idle()
                prefac, U_Eff, C, n = [spre_1.val, sU_1.val, sC.val, sn.val]
                self._guess         = [spre_1.val, sU_1.val, sC.val, sn.val]
                spre_1.valtext.set_text('{:4.2E}'.format(10**prefac))
                sC.valtext.set_text(    '{:4.2E}'.format(10**C)     )
                l.set_ydata(np.power(10., self.Orbach_Raman(xvals, prefac, U_Eff, C, n)))
                k.set_ydata(np.power(10.,       self.Orbach(xvals, prefac, U_Eff)      ))
                j.set_ydata(np.power(10.,       self.Raman( xvals, C,      n)          ))
            def reset(event):
                spre_1.reset()
                sU_1.reset()
                sC.reset()
                sn.reset()
            spre_1.on_changed(update)
            sU_1.on_changed(update)
            sC.on_changed(update)
            sn.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'Orbach + QTM':
            try:
                popt, pcov       = curve_fit(self.Orbach, xvals[-3:], np.log10(rate[-3:]), bounds=self.bounds[:,0:2])
                init_pre, init_U = popt
                popt, pcov       = curve_fit(self.QTM, xvals[:3], np.log10(rate[:3]), bounds=self.bounds[:,2::])
                init_QTM         = popt[0]
                self._guess      = [init_pre, init_U, init_QTM]
                minpre, maxpre, minU, maxU, minQTM, maxQTM = 10**init_pre*0.1, 10**init_pre*10, init_U-init_U/2., init_U+init_U/2., 10**init_QTM*0.1, 10**init_QTM*10 
            except: # (RuntimeError, OptimizeWarning):
                print('    Could not estimate initial guess.\n    Use sliders to sample the parameter space.')
                init_pre, init_U, minpre, maxpre, minU, maxU = _init['Orbach'][0][0], _init['Orbach'][1][0], _init['Orbach'][0][1], _init['Orbach'][0][2], _init['Orbach'][1][1], _init['Orbach'][1][2]
                init_QTM, minQTM, maxQTM = _init['QTM'][0][0], _init['QTM'][0][1], _init['QTM'][0][2]

            l,     = ax.loglog(xvals, np.power(10., self.Orbach_QTM(xvals, init_pre, init_U, init_QTM) ), lw=1.5, label=self.RP_function,  ls='-' )
            k,     = ax.loglog(xvals, np.power(10.,     self.Orbach(xvals, init_pre, init_U)           ), lw=1.3, label='Orbach', ls=':'  )
            m,     = ax.loglog(xvals, np.power(10.,        self.QTM(xvals, init_QTM)                   ), lw=1.3, label='QTM',    ls='-.' )
            axpre  = plt.axes(_buttons['tau0'], facecolor=axcolor)
            axU_1  = plt.axes(_buttons['U'], facecolor=axcolor)
            axqtm  = plt.axes(_buttons['qtm'], facecolor=axcolor)
            spre_1 = Slider(axpre, r'$\tau_0$ (s)',              np.log10(minpre), np.log10(maxpre), valinit=np.log10(10**init_pre), valfmt='%4.2E' )
            sU_1   = Slider(axU_1, r'$U_{\mathrm{eff}}$ (K)',    minU,             maxU,             valinit=init_U,                 valfmt='%3.2f' )
            sqtm   = Slider(axqtm, r'$\tau_{\mathrm{QTM}}$ (s)', np.log10(minQTM), np.log10(maxQTM), valinit=np.log10(10**init_QTM), valfmt='%4.2E' )
            spre_1.valtext.set_text('{:4.2E}'.format(10**spre_1.val))
            sqtm.valtext.set_text(  '{:4.2E}'.format(10**sqtm.val)  )
            def update(val):
                fig.canvas.draw_idle()
                prefac, U_Eff, qtm = [spre_1.val, sU_1.val, sqtm.val]
                self._guess        = [spre_1.val, sU_1.val, sqtm.val]
                spre_1.valtext.set_text('{:4.2E}'.format(10**prefac)  )
                sqtm.valtext.set_text(  '{:4.2E}'.format(10**sqtm.val))
                l.set_ydata(np.power(10., self.Orbach_QTM(xvals, prefac, U_Eff, qtm)))
                k.set_ydata(np.power(10.,     self.Orbach(xvals, prefac, U_Eff)     ))
                m.set_ydata(np.power(10.,        self.QTM(xvals, qtm)               ))
            def reset(event):
                spre_1.reset()
                sU_1.reset()
                sqtm.reset()
            spre_1.on_changed(update)
            sU_1.on_changed(update)
            sqtm.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'Raman + QTM':
            try:
                popt, pcov     = curve_fit(self.Raman, xvals[-3:], np.log10(rate[-3:]), bounds=self.bounds[:,0:2])
                init_C, init_n = popt
                popt, pcov     = curve_fit(self.QTM, xvals[:3], np.log10(rate[:3]), bounds=self.bounds[:,2::])
                init_QTM       = popt[0]
                self._guess    = [init_C, init_n, init_QTM]
                minC, maxC, minn, maxn, minQTM, maxQTM = 10**init_C*0.1, 10**init_C*10, init_n-init_n/2., init_n+init_n/2.,\
                 10**init_QTM*0.1, 10**init_QTM*10
            except: # (RuntimeError, OptimizeWarning):
                print('    Could not estimate initial guess.\n    Use sliders to sample the parameter space.')
                init_C, init_n, minC, maxC, minn, maxn = _init['Raman'][0][0], _init['Raman'][1][0], _init['Raman'][0][1], _init['Raman'][0][2], _init['Raman'][1][1], _init['Raman'][1][2]
                init_QTM, minQTM, maxQTM = _init['QTM'][0][0], _init['QTM'][0][1], _init['QTM'][0][2]

            l,    = ax.loglog(xvals, np.power(10., self.Raman_QTM(xvals, init_C, init_n, init_QTM) ), lw=1.5, label=self.RP_function,  ls='-' )
            j,    = ax.loglog(xvals, np.power(10.,     self.Raman(xvals, init_C, init_n)           ), lw=1.3, label='Raman',  ls='--' )
            m,    = ax.loglog(xvals, np.power(10.,       self.QTM(xvals, init_QTM)                 ), lw=1.3, label='QTM',    ls='-.' )
            axC   = plt.axes(_buttons['C'], facecolor=axcolor)
            axn   = plt.axes(_buttons['n'], facecolor=axcolor)
            axqtm = plt.axes(_buttons['qtm'], facecolor=axcolor)
            sC    = Slider(axC,   r'$C$ (s$^{-1}$ K$^{-n}$)',   np.log10(minC),   np.log10(maxC),   np.log10(10**init_C),   valfmt='%4.2E' )
            sn    = Slider(axn,    '$n$',                       minn,             maxn,             init_n,                 valfmt='%2.3f' )
            sqtm  = Slider(axqtm, r'$\tau_{\mathrm{QTM}}$ (s)', np.log10(minQTM), np.log10(maxQTM), np.log10(10**init_QTM), valfmt='%4.2E' )
            sC.valtext.set_text(  '{:4.2E}'.format(10**sC.val)  )
            sqtm.valtext.set_text('{:4.2E}'.format(10**sqtm.val))
            def update(val):
                fig.canvas.draw_idle()
                C, n, qtm   = [sC.val, sn.val, sqtm.val]
                self._guess = [sC.val, sn.val, sqtm.val]
                sC.valtext.set_text(  '{:4.2E}'.format(10**C))
                sqtm.valtext.set_text('{:4.2E}'.format(10**sqtm.val))
                l.set_ydata(np.power(10., self.Raman_QTM(xvals, C, n, qtm)))
                j.set_ydata(np.power(10.,     self.Raman(xvals, C, n)     ))
                m.set_ydata(np.power(10.,       self.QTM(xvals, qtm)      ))
            def reset(event):
                sC.reset()
                sn.reset()
                sqtm.reset()
            sC.on_changed(update)
            sn.on_changed(update)
            sqtm.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'Orbach + Raman + QTM':
            try:
                popt, pcov       = curve_fit(self.Orbach, xvals[-3:], np.log10(rate[-3:]), bounds=self.bounds[:,0:2])
                init_pre, init_U = popt
                popt, pcov       = curve_fit(self.Raman, xvals[5:9], np.log10(rate[5:9]), bounds=self.bounds[:,2:4])
                init_C, init_n   = popt
                popt, pcov       = curve_fit(self.QTM, xvals[:3], np.log10(rate[:3]), bounds=self.bounds[:,4::])
                init_QTM         = popt[0]
                self._guess      = [init_pre, init_U, init_C, init_n, init_QTM]
                minpre, maxpre, minU, maxU, minC, maxC, minn, maxn, minQTM, maxQTM = \
                 10**init_pre*0.1, 10**init_pre*10, init_U-init_U/2., init_U+init_U/2.,\
                 10**init_C*0.1,   10**init_C*10,   init_n-init_n/2., init_n+init_n/2.,\
                 10**init_QTM*0.1, 10**init_QTM*10

            except: # (RuntimeError, OptimizeWarning):
                print('    Could not estimate initial guess.\n    Use sliders to sample the parameter space.')
                init_pre, init_U, minpre, maxpre, minU, maxU = _init['Orbach'][0][0], _init['Orbach'][1][0], _init['Orbach'][0][1], _init['Orbach'][0][2], _init['Orbach'][1][1], _init['Orbach'][1][2]
                init_C, init_n, minC, maxC, minn, maxn = _init['Raman'][0][0], _init['Raman'][1][0], _init['Raman'][0][1], _init['Raman'][0][2], _init['Raman'][1][1], _init['Raman'][1][2]
                init_QTM, minQTM, maxQTM = _init['QTM'][0][0], _init['QTM'][0][1], _init['QTM'][0][2]

            l,     = ax.loglog(xvals, np.power(10., self.Orbach_Raman_QTM(xvals, init_pre, init_U, init_C, init_n, init_QTM) ), lw=1.5, label=self.RP_function,  ls='-' )
            k,     = ax.loglog(xvals, np.power(10.,           self.Orbach(xvals, init_pre, init_U)                           ), lw=1.3, label='Orbach', ls=':'  )
            j,     = ax.loglog(xvals, np.power(10.,            self.Raman(xvals, init_C, init_n)                             ), lw=1.3, label='Raman',  ls='--' )
            m,     = ax.loglog(xvals, np.power(10.,              self.QTM(xvals, init_QTM)                                   ), lw=1.3, label='QTM',    ls='-.' )
            axpre  = plt.axes(_buttons['tau0'], facecolor=axcolor)
            axU_1  = plt.axes(_buttons['U'], facecolor=axcolor)
            axC    = plt.axes(_buttons['C'], facecolor=axcolor)
            axn    = plt.axes(_buttons['n'], facecolor=axcolor)
            axqtm  = plt.axes(_buttons['qtm'], facecolor=axcolor)
            spre_1 = Slider(axpre, r'$\tau_0$ (s)',                  np.log10(minpre), np.log10(maxpre), np.log10(10**init_pre), valfmt='%4.2E' )
            sU_1   = Slider(axU_1, r'$U_{\mathrm{eff}}$ (K)',        minU,             maxU,             init_U,                 valfmt='%3.2f' )
            sC     = Slider(axC,   r'$C$ (s$^{-1}$ K$^{-n}$)',       np.log10(minC),   np.log10(maxC),   np.log10(10**init_C),   valfmt='%4.2E' )
            sn     = Slider(axn,   r'$n$',                           minn,             maxn,             init_n,                 valfmt='%3.2f' )
            sqtm   = Slider(axqtm, r'$\tau_{\mathrm{QTM}}$ (s)',     np.log10(minQTM), np.log10(maxQTM), np.log10(10**init_QTM), valfmt='%4.2E' )
            sC.valtext.set_text(    '{:4.2E}'.format(10**sC.val)    )
            spre_1.valtext.set_text('{:4.2E}'.format(10**spre_1.val))
            sqtm.valtext.set_text(  '{:4.2E}'.format(10**sqtm.val)  )
            def update(val):
                fig.canvas.draw_idle()
                prefac, U_Eff, C, n, qtm = [spre_1.val, sU_1.val, sC.val, sn.val, sqtm.val]
                self._guess              = [spre_1.val, sU_1.val, sC.val, sn.val, sqtm.val]
                spre_1.valtext.set_text('{:4.2E}'.format(10**prefac)  )
                sC.valtext.set_text(    '{:4.2E}'.format(10**C)       )
                sqtm.valtext.set_text(  '{:4.2E}'.format(10**sqtm.val))
                l.set_ydata(np.power(10., self.Orbach_Raman_QTM(xvals, prefac, U_Eff, C, n, qtm)))
                k.set_ydata(np.power(10.,           self.Orbach(xvals, prefac, U_Eff)           ))
                j.set_ydata(np.power(10.,            self.Raman(xvals, C, n)                    ))
                m.set_ydata(np.power(10.,              self.QTM(xvals, qtm)                     ))
            def reset(event):
                spre_1.reset()
                sU_1.reset()
                sC.reset()
                sn.reset()
                sqtm.reset()
            spre_1.on_changed(update)
            sU_1.on_changed(update)
            sC.on_changed(update)
            sn.on_changed(update)
            sqtm.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'Orbach + Direct':
            try:
                popt, pcov  = curve_fit(self.Orbach, xvals[3:], np.log10(rate[3:]), bounds=self.bounds)
                init_pre, init_U = popt
                popt, pcov  = curve_fit(self.Direct, xvals[-3:], np.log10(rate[-3:]), bounds=self.bounds)
                init_A = popt
                self._guess = [init_pre, init_U, init_A]
                minpre, maxpre, minU, maxU, minA, maxA = 10**init_pre*0.1, 10**init_pre*10, init_U-init_U/2., init_U+init_U/2., 10**init_A*0.1, 10**init_A*10
            except: # (RuntimeError, OptimizeWarning):
                print('    Could not estimate initial guess.\n    Use sliders to sample the parameter space.')
                init_pre, init_U, init_A, minpre, maxpre, minU, maxU, minA, maxA = _init['Orbach'][0][0], _init['Orbach'][1][0], _init['Direct'][0][0], _init['Orbach'][0][1], _init['Orbach'][0][2], _init['Orbach'][1][1], _init['Orbach'][1][2], _init['Direct'][0][1], _init['Direct'][0][2]

            _l,     = ax.loglog( xvals, np.power(10., self.Orbach_Direct(xvals, init_pre, init_U, init_A)), '-', label=self.RP_function )
            _k,     = ax.loglog( xvals, np.power(10., self.Orbach(xvals, init_pre, init_U)), '--', label='Orbach' )
            _q,     = ax.loglog( xvals, np.power(10., self.Direct(xvals, init_A)), ':', label='Direct' )
            axpre  = plt.axes( _buttons['tau0'], facecolor=axcolor )
            axU_1  = plt.axes( _buttons['U'],    facecolor=axcolor )
            axA    = plt.axes( _buttons['A'], facecolor=axcolor )
            spre_1 = Slider(axpre, r'$\tau_0$ (s)',           np.log10(minpre), np.log10(maxpre), np.log10(10**init_pre), valfmt='%4.2E' )
            sU_1   = Slider(axU_1, r'$U_{\mathrm{eff}}$ (K)', minU,             maxU,             init_U,                 valfmt='%3.2f' )
            sA     = Slider(axA,   r'A (K$^{-1}$s$^{-1}$)',   np.log10(minA),   np.log10(maxA),   np.log10(10**init_A),   valfmt='%4.2E' )
            spre_1.valtext.set_text('{:4.2E}'.format(10**spre_1.val)) # Slider is in log10 scale to appear linear, so I need to convert back. The stored value goes as it is to the function as the model function uses 10**value
            sA.valtext.set_text('{:4.2E}'.format(10**sA.val)) 

            def update(val): 
                fig.canvas.draw_idle()
                prefac, U_Eff, A = [spre_1.val, sU_1.val, sA.val]
                self._guess      = [spre_1.val, sU_1.val, sA.val]
                spre_1.valtext.set_text('{:4.2E}'.format(10**prefac))
                sA.valtext.set_text('{:4.2E}'.format(10**A))
                _l.set_ydata(np.power(10., self.Orbach_Direct(xvals, prefac, U_Eff, A)))
                _k.set_ydata(np.power(10., self.Orbach(xvals, prefac, U_Eff)))
                _q.set_ydata(np.power(10., self.Direct(xvals, A)))         

            def reset(event):
                sU_1.reset()
                spre_1.reset()
                sA.reset()
            spre_1.on_changed(update)
            sU_1.on_changed(update)
            sA.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'Raman + Direct':
            try:
                popt, pcov  = curve_fit(self.Raman, xvals[3:], np.log10(rate[3:]), bounds=self.bounds)
                init_C, init_n = popt
                popt, pcov  = curve_fit(self.Direct, xvals[-3:], np.log10(rate[-3:]), bounds=self.bounds)
                init_A = popt
                self._guess = [init_C, init_n, init_A]
                minC, maxC, minn, maxn, minA, maxA = 10**init_C*0.1, 10**init_C*10, init_n-init_n/2., init_n+init_n/2., 10**init_A*0.1, 10**init_A*10
            except: # (RuntimeError, OptimizeWarning):
                print('    Could not estimate initial guess.\n    Use sliders to sample the parameter space.')
                init_C, init_n, init_A, minC, maxC, minn, maxn, minA, maxA = _init['Raman'][0][0], _init['Raman'][1][0], _init['Direct'][0][0], _init['Raman'][0][1], _init['Raman'][0][2], _init['Raman'][1][1], _init['Raman'][1][2], _init['Direct'][0][1], _init['Direct'][0][2]

            _l,     = ax.loglog( xvals, np.power(10., self.Raman_Direct(xvals, init_C, init_n, init_A)), '-', label=self.RP_function )
            _j,     = ax.loglog( xvals, np.power(10., self.Raman(xvals, init_C, init_n)), '--', label='Raman' )
            _q,     = ax.loglog( xvals, np.power(10., self.Direct(xvals, init_A)), ':', label='Direct' )
            axC    = plt.axes( _buttons['C'], facecolor=axcolor )
            axn    = plt.axes( _buttons['n'], facecolor=axcolor )
            axA    = plt.axes( _buttons['A'], facecolor=axcolor )
            sC     = Slider(axC,   r'$C$ (s$^{-1}$ K$^{-n}$)',       np.log10(minC),   np.log10(maxC),   np.log10(10**init_C),   valfmt='%4.2E' )
            sn     = Slider(axn,   r'$n$',                           minn,             maxn,             init_n,                 valfmt='%3.2f' )
            sA     = Slider(axA,   r'A (K$^{-1}$s$^{-1}$)',          np.log10(minA),   np.log10(maxA),   np.log10(10**init_A),   valfmt='%4.2E' )
            sC.valtext.set_text('{:4.2E}'.format(10**sC.val)) # Slider is in log10 scale to appear linear, so I need to convert back. The stored value goes as it is to the function as the model function uses 10**value
            sA.valtext.set_text('{:4.2E}'.format(10**sA.val)) 

            def update(val): 
                fig.canvas.draw_idle()
                C, n, A     = [sC.val, sn.val, sA.val]
                self._guess = [sC.val, sn.val, sA.val]
                sC.valtext.set_text('{:4.2E}'.format(10**C))
                sA.valtext.set_text('{:4.2E}'.format(10**A))
                _l.set_ydata(np.power(10., self.Raman_Direct(xvals, C, n, A)))
                _j.set_ydata(np.power(10., self.Raman(xvals, C, n)))
                _q.set_ydata(np.power(10., self.Direct(xvals, A)))

            def reset(event):
                sC.reset()
                sn.reset()
                sA.reset()
            sC.on_changed(update)
            sn.on_changed(update)
            sA.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'Orbach + Raman + Direct':
            try:
                popt, pcov  = curve_fit(self.Orbach, xvals[-3:], np.log10(rate[-3:]), bounds=self.bounds)
                init_pre, init_U = popt
                popt, pcov  = curve_fit(self.Raman, xvals[3:], np.log10(rate[3:]), bounds=self.bounds)
                init_C, init_n = popt
                popt, pcov  = curve_fit(self.Direct, xvals[4:7], np.log10(rate[4:7]), bounds=self.bounds)
                init_A = popt
                self._guess = [init_pre, init_U, init_C, init_n, init_A]
                minpre, maxpre, minU, maxU, minC, maxC, minn, maxn, minA, maxA = 10**init_pre*0.1, 10**init_pre*10, init_U-init_U/2., init_U+init_U/2., 10**init_C*0.1, 10**init_C*10, init_n-init_n/2., init_n+init_n/2., 10**init_A*0.1, 10**init_A*10
            except: # (RuntimeError, OptimizeWarning):
                print('    Could not estimate initial guess.\n    Use sliders to sample the parameter space.')
                init_pre, init_U, init_C, init_n, init_A, minpre, maxpre, minU, maxU, minC, maxC, minn, maxn, minA, maxA = _init['Orbach'][0][0], _init['Orbach'][1][0], _init['Raman'][0][0], _init['Raman'][1][0], _init['Direct'][0][0], _init['Orbach'][0][1], _init['Orbach'][0][2], _init['Orbach'][1][1], _init['Orbach'][1][2], _init['Raman'][0][1], _init['Raman'][0][2], _init['Raman'][1][1], _init['Raman'][1][2], _init['Direct'][0][1], _init['Direct'][0][2]

            _l,     = ax.loglog( xvals, np.power(10., self.Orbach_Raman_Direct(xvals, init_pre, init_U, init_C, init_n, init_A)), '-', label=self.RP_function )
            _k,     = ax.loglog( xvals, np.power(10., self.Orbach(xvals, init_pre, init_U)), '--', label='Orbach' )
            _j,     = ax.loglog( xvals, np.power(10., self.Raman(xvals, init_C, init_n)), ':', label='Raman' )
            _q,     = ax.loglog( xvals, np.power(10., self.Direct(xvals, init_A)), '-.', label='Direct' )
            axpre  = plt.axes( _buttons['tau0'], facecolor=axcolor )
            axU_1  = plt.axes( _buttons['U'],    facecolor=axcolor )
            axC    = plt.axes( _buttons['C'],    facecolor=axcolor )
            axn    = plt.axes( _buttons['n'],    facecolor=axcolor )
            axA    = plt.axes( _buttons['A'],    facecolor=axcolor )
            spre_1 = Slider(axpre, r'$\tau_0$ (s)',           np.log10(minpre), np.log10(maxpre), np.log10(10**init_pre), valfmt='%4.2E' )
            sU_1   = Slider(axU_1, r'$U_{\mathrm{eff}}$ (K)', minU,             maxU,             init_U,                 valfmt='%3.2f' )
            sC     = Slider(axC,   r'$C$ (s$^{-1}$ K$^{-n}$)',       np.log10(minC),   np.log10(maxC),   np.log10(10**init_C),   valfmt='%4.2E' )
            sn     = Slider(axn,   r'$n$',                           minn,             maxn,             init_n,                 valfmt='%3.2f' )
            sA     = Slider(axA,   r'A (K$^{-1}$s$^{-1}$)', np.log10(minA), np.log10(maxA), np.log10(10**init_A), valfmt='%4.2E' )
            spre_1.valtext.set_text('{:4.2E}'.format(10**spre_1.val)) # Slider is in log10 scale to appear linear, so I need to convert back. The stored value goes as it is to the function as the model function uses 10**value
            sC.valtext.set_text('{:4.2E}'.format(10**sC.val))
            sA.valtext.set_text('{:4.2E}'.format(10**sA.val)) 

            def update(val): 
                fig.canvas.draw_idle()
                prefac, U_Eff, C, n, A = [spre_1.val, sU_1.val, sC.val, sn.val, sA.val]
                self._guess      = [spre_1.val, sU_1.val, sC.val, sn.val, sA.val]
                spre_1.valtext.set_text('{:4.2E}'.format(10**prefac))
                sC.valtext.set_text('{:4.2E}'.format(10**sC.val))
                sA.valtext.set_text('{:4.2E}'.format(10**A))
                _l.set_ydata(np.power(10., self.Orbach_Raman_Direct(xvals, prefac, U_Eff, C, n, A)))
                _k.set_ydata(np.power(10., self.Orbach(xvals, prefac, U_Eff)))
                _j.set_ydata(np.power(10., self.Raman(xvals, C, n)))
                _q.set_ydata(np.power(10., self.Direct(xvals, A)))         

            def reset(event):
                sU_1.reset()
                spre_1.reset()
                sC.reset()
                sn.reset()
                sA.reset()
            spre_1.on_changed(update)
            sU_1.on_changed(update)
            sC.on_changed(update)
            sn.on_changed(update)
            sA.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'Brons-Van-Vleck & Ct' or self.RP_function == 'Brons-Van-Vleck & Ct - constrained':

            l,    = ax.loglog( xvals, np.power(10., self.BronsVanVleck_Cte(xvals, *(_init['BronsVanVleck'][0] + _init['Cte'][0])) ), lw=1.5, label=self.RP_function, ls='-' )
            
            axe   = plt.axes( _buttons['e'],   facecolor=axcolor )
            axf   = plt.axes( _buttons['f'],   facecolor=axcolor )
            axcte = plt.axes( _buttons['Cte'], facecolor=axcolor )
            
            se   = Slider(axe,   r'$e$ (Oe$^{-2}$)', np.log10(_init['BronsVanVleck'][1][0]), np.log10(_init['BronsVanVleck'][1][1]), np.log10(10**_init['BronsVanVleck'][0][0]), valfmt='%4.2E' )
            sf   = Slider(axf,   r'$f$ (Oe$^{-2}$)', np.log10(_init['BronsVanVleck'][2][0]), np.log10(_init['BronsVanVleck'][2][1]), np.log10(10**_init['BronsVanVleck'][0][1]), valfmt='%4.2E' )
            scte = Slider(axcte, r'$Ct$',            np.log10(_init['Cte'][1][0]),           np.log10(_init['Cte'][1][1]),           np.log10(10**_init['Cte'][0][0]),           valfmt='%4.2E' )

            se.valtext.set_text(    '{:4.2E}'.format(10**se.val)  ) # Slider is in log10 scale to appear linear, so I need to convert back. The stored value goes as it is to the function as the model function uses 10**value
            sf.valtext.set_text(    '{:4.2E}'.format(10**sf.val)  )
            scte.valtext.set_text(  '{:4.2E}'.format(10**scte.val))

            def update(val): 
                fig.canvas.draw_idle()
                if self.RP_function == 'Brons-Van-Vleck & Ct':
                    self._guess   = [se.val, sf.val, scte.val]
                elif self.RP_function == 'Brons-Van-Vleck & Ct - constrained':
                    self._guess   = [se.val, sf.val]
                    self.Ct_fixed = scte.val
                se.valtext.set_text(    '{:4.2E}'.format(10**se.val)  )
                sf.valtext.set_text(    '{:4.2E}'.format(10**sf.val)  )
                scte.valtext.set_text(  '{:4.2E}'.format(10**scte.val))
                l.set_ydata(np.power(10., self.BronsVanVleck_Cte(xvals, *[se.val, sf.val, scte.val])))           

            def reset(event):
                se.reset()
                sf.reset()
                scte.reset()

            se.on_changed(update)
            sf.on_changed(update)
            scte.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'Brons-Van-Vleck & Raman-II':

            l,    = ax.loglog( xvals, np.power(10., self.BronsVanVleck_RamanII(xvals, *(_init['BronsVanVleck'][0] + _init['RamanII'][0])) ), lw=1.5, label=self.RP_function, ls='-' )
            
            axe   = plt.axes( _buttons['e'],   facecolor=axcolor )
            axf   = plt.axes( _buttons['f'],   facecolor=axcolor )
            axCII = plt.axes( _buttons['CII'], facecolor=axcolor )
            axmII = plt.axes( _buttons['mII'], facecolor=axcolor )
            
            se   = Slider(axe,   r'$e$ (Oe$^{-2}$)',          np.log10(_init['BronsVanVleck'][1][0]), np.log10(_init['BronsVanVleck'][1][1]), np.log10(10**_init['BronsVanVleck'][0][0]), valfmt='%4.2E' )
            sf   = Slider(axf,   r'$f$ (Oe$^{-2}$)',          np.log10(_init['BronsVanVleck'][2][0]), np.log10(_init['BronsVanVleck'][2][1]), np.log10(10**_init['BronsVanVleck'][0][1]), valfmt='%4.2E' )
            sCII = Slider(axCII, r'$C$ (s$^{-1}$ Oe$^{-m}$)', np.log10(_init['RamanII'][1][0]),       np.log10(_init['RamanII'][1][1]),       np.log10(10**_init['RamanII'][0][0]),       valfmt='%4.2E' )
            smII = Slider(axmII, r'$m$',                               _init['RamanII'][2][0],                 _init['RamanII'][2][1],                     _init['RamanII'][0][1],        valfmt='%3.2f' )

            se.valtext.set_text(    '{:4.2E}'.format(10**se.val)  ) # Slider is in log10 scale to appear linear, so I need to convert back. The stored value goes as it is to the function as the model function uses 10**value
            sf.valtext.set_text(    '{:4.2E}'.format(10**sf.val)  )
            sCII.valtext.set_text(  '{:4.2E}'.format(10**sCII.val))

            def update(val): 
                fig.canvas.draw_idle()
                self._guess   = [se.val, sf.val, sCII.val, smII.val]
                se.valtext.set_text(    '{:4.2E}'.format(10**se.val)  )
                sf.valtext.set_text(    '{:4.2E}'.format(10**sf.val)  )
                sCII.valtext.set_text(  '{:4.2E}'.format(10**sCII.val))
                l.set_ydata(np.power(10., self.BronsVanVleck_RamanII(xvals, *[se.val, sf.val, sCII.val, smII.val])))           

            def reset(event):
                se.reset()
                sf.reset()
                sCII.reset()

            se.on_changed(update)
            sf.on_changed(update)
            sCII.on_changed(update)
            button.on_clicked(reset)

        elif self.RP_function == 'QTM(H) + Raman-II + Ct' or self.RP_function == 'QTM(H) + Raman-II + Ct - constrained':

            l,     = ax.loglog(xvals, np.power(10., self.FieldQTM_RamanII_Ct(xvals, *(_init['FieldQTM'][0] + _init['RamanII'][0] + _init['Cte'][0])) ), lw=1.5, label=self.RP_function,  ls='-' )
            k,     = ax.loglog(xvals, np.power(10.,            self.FieldQTM(xvals, *_init['FieldQTM'][0])                                           ), lw=1.3, label='QTM(H)',          ls=':'  )
            j,     = ax.loglog(xvals, np.power(10.,             self.RamanII(xvals, *_init['RamanII'][0])                                            ), lw=1.3, label='Raman-II',        ls='--' )
            m,     = ax.loglog(xvals, np.power(10.,                  self.Ct(xvals, *_init['Cte'][0])                                                ), lw=1.3, label='Ct',              ls='-.' )

            axzfqtm = plt.axes(_buttons['ZFQTM'], facecolor=axcolor)
            axfqtm  = plt.axes(_buttons['FQTM'],  facecolor=axcolor)
            axp     = plt.axes(_buttons['p'],     facecolor=axcolor)
            axCII   = plt.axes(_buttons['CII'],   facecolor=axcolor)
            axmII   = plt.axes(_buttons['mII'],   facecolor=axcolor)
            axcte   = plt.axes(_buttons['Cte'],   facecolor=axcolor)
            
            szfqtm  = Slider(axzfqtm, r'$\tau_{\mathrm{QTM}}$ (s)',            np.log10(_init['FieldQTM'][1][0]), np.log10(_init['FieldQTM'][1][1]), np.log10(10**_init['FieldQTM'][0][0]),  valfmt='%4.2E' )
            sfqtm   = Slider(axfqtm,  r'$\tau_{\mathrm{QTM(H)}}$ (Oe$^{-p}$)', np.log10(_init['FieldQTM'][2][0]), np.log10(_init['FieldQTM'][2][1]), np.log10(10**_init['FieldQTM'][0][1]),  valfmt='%4.2E' )
            sp      = Slider(axp,     r'$p$',                                           _init['FieldQTM'][3][0],           _init['FieldQTM'][3][1],               _init['FieldQTM'][0][2],   valfmt='%3.2f' )
            sCII    = Slider(axCII,   r'$C$ (s$^{-1}$ Oe$^{-m}$)',             np.log10(_init['RamanII'][1][0]),  np.log10(_init['RamanII'][1][1]),  np.log10(10**_init['RamanII'][0][0]),   valfmt='%4.2E' )
            smII    = Slider(axmII,   r'$m$',                                           _init['RamanII'][2][0],            _init['RamanII'][2][1],                _init['RamanII'][0][1],    valfmt='%3.2f' )
            scte    = Slider(axcte,   r'$Ct$',                                 np.log10(_init['Cte'][1][0]),      np.log10(_init['Cte'][1][1]),      np.log10(10**_init['Cte'][0][0]),       valfmt='%4.2E' )

            szfqtm.valtext.set_text('{:4.2E}'.format(10**szfqtm.val) )
            sfqtm.valtext.set_text( '{:4.2E}'.format(10**sfqtm.val)  )
            sCII.valtext.set_text(  '{:4.2E}'.format(10**sCII.val)   )
            scte.valtext.set_text(  '{:4.2E}'.format(10**scte.val)   )

            def update(val):
                fig.canvas.draw_idle()
                if self.RP_function == 'QTM(H) + Raman-II + Ct':
                    self._guess = [szfqtm.val, sfqtm.val, sp.val, sCII.val, smII.val, scte.val]
                elif self.RP_function == 'QTM(H) + Raman-II + Ct - constrained':
                    self._guess = [sfqtm.val, sp.val, sCII.val, smII.val]
                    self.ZFQTM_fixed  = szfqtm.val
                    self.Ct_fixed     = scte.val
                szfqtm.valtext.set_text('{:4.2E}'.format(10**szfqtm.val) )
                sfqtm.valtext.set_text( '{:4.2E}'.format(10**sfqtm.val)  )
                sCII.valtext.set_text(  '{:4.2E}'.format(10**sCII.val)   )
                scte.valtext.set_text(  '{:4.2E}'.format(10**scte.val)   )
                l.set_ydata(np.power(10., self.FieldQTM_RamanII_Ct(xvals, *[szfqtm.val, sfqtm.val, sp.val, sCII.val, smII.val, scte.val])))
                k.set_ydata(np.power(10.,            self.FieldQTM(xvals, *[szfqtm.val, sfqtm.val, sp.val])                             ))
                j.set_ydata(np.power(10.,             self.RamanII(xvals, *[sCII.val, smII.val])                                        ))
                m.set_ydata(np.power(10.,                  self.Ct(xvals, *[scte.val])                                                  ))

            def reset(event):
                szfqtm.reset()
                sfqtm.reset()
                sp.reset()
                sCII.reset()
                smII.reset()
                scte.reset()
            szfqtm.on_changed(update)
            sfqtm.on_changed(update)
            sp.on_changed(update)
            sCII.on_changed(update)
            smII.on_changed(update)
            scte.on_changed(update)
            button.on_clicked(reset)

        if   self.process == 'tau_vs_T': 
            # Set x and y limits
            set_rate_xy_lims(ax, rate, self.yerr_plt, xvals)
            # Reset x limits and formatter
            set_rate_x_lims(ax, xvals)
            # Set xlabel
            ax.set_xlabel(r'Temperature (K)')
        elif self.process == 'tau_vs_H':
            # Set xlabel
            ax.set_xlabel(r'Field (Oe)')

        ax.set_ylabel(r'$\tau^{-1}$ (s$^{-1}$)')
        ax.legend(loc=0,fontsize='10', numpoints = 1, ncol=2, frameon=False)
        
        # When clicking the Fit button.
        button_fit.on_clicked(
            lambda event: self.fit_rates(event, xvals, rate, self._guess)
            )
        # When clicking the Change Model button.
        button_change.on_clicked(
            lambda event: self.change_model_rates(event, xvals, rate, self.field )
            )

        plt.show()
        plt.close(fig)

        return 

    def fit_rates(self, event, xvals, rate, guess):

        # Close any old plot objects
        plt.close('all') 

        try:
            # Fit the data and calculate the error on total fit for each parameter.
            popt, pcov    = curve_fit(self.RP_model, xvals, np.log10(rate), p0=guess, sigma=self.yerr_fit, absolute_sigma=True, bounds=self.bounds)
            perr = (np.sqrt(np.diag(pcov))) 

        # If the fitting is not successful, repeat.
        except (ValueError): 
            print('\n    The indicated model function {} cannot fit the data.\n    Try again.'.format(self.RP_function))
            self.guess_fit_rates(
                    xvals, rate
                    )

        # Define the output files.
        # It needs to be here as self.RP_function gets updated by the user and this is the last instance.
        if args.exe_mode != 'RelaxationProfile':
            RP_filename, RP_params, RP_fit = create_output_files(
                args, _field=field, model_RP=self.RP_function,  _output='all'
                )
        else: 
            RP_filename, RP_params, RP_fit = create_output_files(
                args, model_RP=self.RP_function,
                ) 


        # Create figure and axis
        fig2, ax = plt.subplots(figsize=(6,6), num='Relaxation rate fit')
        
        # Plot error bar from cole-cole fit
        ax.errorbar(xvals, rate, yerr=self.yerr_plt, marker='o', ls='none', fillstyle='none', color='r' )

        # Set log scale
        ax.set_xscale('log')
        ax.set_yscale('log')

        if   self.process == 'tau_vs_T': 
            # Set x and y limits
            set_rate_xy_lims(ax, rate, self.yerr_plt, xvals)
            # Reset x limits and formatter
            set_rate_x_lims(ax, xvals)
            # Set xlabel
            ax.set_xlabel(r'Temperature (K)')
        elif self.process == 'tau_vs_H':
            # Set xlabel
            ax.set_xlabel(r'Field (Oe)')


        # xvals for fitted eqn to be plotted at
        xvals_fit = np.linspace(xvals[0],xvals[-1], len(xvals)*10)

        ax.plot(xvals_fit, np.power(10, self.RP_model(xvals_fit, *popt )), lw=1.5, label='Fit')

        print('    For {} process, the fitted parameters and 1-sigma estimates are are:'.format(self.RP_function))

        if self.RP_function == 'Orbach':
            print('    log_10[t_0] = {} +- {} log_10[s]'.format(popt[0], perr[0]))
            print('    U_eff = {} +- {} K'.format(popt[1], perr[1]))
 
        elif self.RP_function ==  'Raman':
            print('    log_10[C] = {} +- {} log_10[s^-1 K^-n]'.format(popt[0], perr[0]))
            print('    n = {} +- {}'.format(popt[1], perr[1]))

        elif self.RP_function ==  'QTM':
            print('    log_10[t_QTM] = {} +- {} log_10[s]'.format(popt[0], perr[0]))

        elif self.RP_function ==  'Orbach + Raman':
                
            print('    log_10[t_0] = {} +- {} log_10[s]'.format(popt[0], perr[0]))
            print('    U_eff = {} +- {} K'.format(popt[1], perr[1]))
            print('    log_10[C] = {} +- {} log_10[s^-1 K^-n]'.format(popt[2], perr[2]))
            print('    n = {} +- {}'.format(popt[3], perr[3]))

            ax.plot(xvals_fit, np.power(10, self.Orbach(xvals_fit, popt[0], popt[1]) ), lw=1.3, label='Orbach', ls=':'  )
            ax.plot(xvals_fit, np.power(10, self.Raman(xvals_fit, popt[2], popt[3]) ),  lw=1.3, label='Raman',  ls='--' )

        elif self.RP_function ==  'Orbach + QTM':

            print('    log_10[t_0] = {} +- {} log_10[s]'.format(popt[0], perr[0]))
            print('    U_eff = {} +- {} K'.format(popt[1], perr[1]))
            print('    log_10[t_QTM] = {} +- {} log_10[s]'.format(popt[2], perr[2]))

            ax.plot(xvals_fit, np.power(10, self.Orbach(xvals_fit, popt[0], popt[1]) ), lw=1.3, label='Orbach', ls=':'  )
            ax.plot(xvals_fit, np.power(10, self.QTM(xvals_fit, popt[2]) ),             lw=1.3, label='QTM',    ls='-.' )

        elif self.RP_function ==  'Raman + QTM':

            print('    log_10[C] = {} +- {} log_10[s^-1 K^-n]'.format(popt[0], perr[0]))
            print('    n = {} +- {}'.format(popt[1], perr[1]))
            print('    log_10[t_QTM] = {} +- {} log_10[s]'.format(popt[2], perr[2]))

            ax.plot(xvals_fit, np.power(10, self.Raman(xvals_fit, popt[0],popt[1]) ), lw=1.3, label='Raman', ls='--' )
            ax.plot(xvals_fit, np.power(10, self.QTM(xvals_fit, popt[2]) ),           lw=1.3, label='QTM',   ls='-.' )

        elif self.RP_function ==  'Orbach + Raman + QTM':

            print('    log_10[t_0] = {} +- {} log_10[s]'.format(popt[0], perr[0]))
            print('    U_eff = {} +- {} K'.format(popt[1], perr[1]))
            print('    log_10[C] = {} +- {} log_10[s^-1 K^-n]'.format(popt[2], perr[2]))
            print('    n = {} +- {}'.format(popt[3], perr[3]))
            print('    log_10[t_QTM] = {} +- {} log_10[s]'.format(popt[4], perr[4]))

            ax.plot(xvals_fit, np.power(10, self.Orbach(xvals_fit, popt[0],popt[1]) ), lw=1.3, label='Orbach', ls=':'  )
            ax.plot(xvals_fit, np.power(10, self.Raman(xvals_fit, popt[2],popt[3]) ),  lw=1.3, label='Raman',  ls='--' )
            ax.plot(xvals_fit, np.power(10, self.QTM(xvals_fit, popt[4]) ),            lw=1.3, label='QTM',    ls='-.' )

        elif self.RP_function ==  'Orbach + Direct':

            print('    log_10[t_0] = {} +- {} log_10[s]'.format(popt[0], perr[0]))
            print('    U_eff = {} +- {} K'.format(popt[1], perr[1]))
            print('    log_10[A] = {} +- {} log_10[s^-1 K^-1]'.format(popt[2], perr[2]))

            ax.plot(xvals_fit, np.power(10, self.Orbach(xvals_fit, popt[0],popt[1]) ), lw=1.3, label='Orbach', ls=':'  )
            ax.plot(xvals_fit, np.power(10, self.Direct(xvals_fit, popt[2]) ),         lw=1.3, label='Direct', ls='--' )

        elif self.RP_function ==  'Raman + Direct':

            print('    log_10[C] = {} +- {} log_10[s^-1 K^-n]'.format(popt[0], perr[0]))
            print('    n = {} +- {}'.format(popt[1], perr[1]))
            print('    log_10[A] = {} +- {} log_10[s^-1 K^-1]'.format(popt[2], perr[2]))

            ax.plot(xvals_fit, np.power(10, self.Raman(xvals_fit, popt[0],popt[1]) ), lw=1.3, label='Raman', ls=':'  )
            ax.plot(xvals_fit, np.power(10, self.Direct(xvals_fit, popt[2]) ),        lw=1.3, label='Direct', ls='--' )

        elif self.RP_function ==  'Orbach + Raman + Direct':

            print('    log_10[t_0] = {} +- {} log_10[s]'.format(popt[0], perr[0]))
            print('    U_eff = {} +- {} K'.format(popt[1], perr[1]))
            print('    log_10[C] = {} +- {} log_10[s^-1 K^-n]'.format(popt[2], perr[2]))
            print('    n = {} +- {}'.format(popt[3], perr[3]))
            print('    log_10[A] = {} +- {} log_10[s^-1 K^-1]'.format(popt[4], perr[4]))

            ax.plot(xvals_fit, np.power(10, self.Orbach(xvals_fit, popt[0],popt[1]) ), lw=1.3, label='Orbach', ls=':'  )
            ax.plot(xvals_fit, np.power(10, self.Raman(xvals_fit, popt[2],popt[3]) ),  lw=1.3, label='Raman',  ls='--' )
            ax.plot(xvals_fit, np.power(10, self.Direct(xvals_fit, popt[4]) ),         lw=1.3, label='Direct', ls='-.' )

        elif self.RP_function == 'QTM(H) + Raman-II + Ct':

            print('    log_10[t_QTM] = {} +- {} log_10[s]'.format(popt[0], perr[0]))
            print('    log_10[t_QTM(H)] = {} +- {} log_10[Oe^-p]'.format(popt[1], perr[1]))
            print('    p = {} +- {} '.format(popt[2], perr[2]))
            print('    log_10[CII] = {} +- {} log_10[s^-1 Oe^-m]'.format(popt[3], perr[3]))
            print('    m = {} +- {}'.format(popt[4], perr[4]))
            print('    log_10[Ct] = {} +- {} log_10[s^-1]'.format(popt[5], perr[5]))

            ax.plot(xvals_fit, np.power(10, self.FieldQTM(xvals_fit, popt[0], popt[1], popt[2]) ), lw=1.3, label='QTM(H)',  ls=':'  )
            ax.plot(xvals_fit, np.power(10, self.RamanII(xvals_fit, popt[3], popt[4]) ),           lw=1.3, label='RamanII', ls='--' )
            ax.plot(xvals_fit, np.power(10, self.Ct(xvals_fit, popt[5]) ),                         lw=1.3, label='Ct',      ls='-.' )

        elif self.RP_function == 'QTM(H) + Raman-II + Ct - constrained':

            print('    log_10[t_QTM] = {} log_10[s]'.format(self.ZFQTM_fixed))
            print('    log_10[t_QTM(H)] = {} +- {} log_10[Oe^-p]'.format(popt[0], perr[0]))
            print('    p = {} +- {} '.format(popt[1], perr[1]))
            print('    log_10[CII] = {} +- {} log_10[s^-1 Oe^-m]'.format(popt[2], perr[2]))
            print('    m = {} +- {}'.format(popt[3], perr[3]))
            print('    log_10[Ct] = {} log_10[s^-1]'.format(self.Ct_fixed))

            ax.plot(xvals_fit, np.power(10, self.FieldQTM(xvals_fit, self.ZFQTM_fixed, popt[0], popt[1]) ), lw=1.3, label='QTM(H)',  ls=':'  )
            ax.plot(xvals_fit, np.power(10, self.RamanII(xvals_fit, popt[2], popt[3]) ),                    lw=1.3, label='RamanII', ls='--' )
            ax.plot(xvals_fit, np.power(10, self.Ct(xvals_fit, self.Ct_fixed) ),                            lw=1.3, label='Ct',      ls='-.' )

        elif self.RP_function == 'Brons-Van-Vleck & Raman-II':

            print('    log_10[e] = {} +- {} log_10[(Oe^-2)]'.format(popt[0], perr[0]))
            print('    log_10[f] = {} +- {} log_10[(Oe^-2)]'.format(popt[1], perr[1]))
            print('    log_10[CII] = {} +- {} log_10[s^-1 Oe^-m]'.format(popt[2], perr[2]))
            print('    m = {} +- {}'.format(popt[3], perr[3]))

        elif self.RP_function == 'Brons-Van-Vleck & Ct':

            print('    log_10[e] = {} +- {} log_10[(Oe^-2)]'.format(popt[0], perr[0]))
            print('    log_10[f] = {} +- {} log_10[(Oe^-2)]'.format(popt[1], perr[1]))
            print('    log_10[Ct] = {} +- {} log_10[s^-1]'.format(popt[2], perr[2]))

        elif self.RP_function == 'Brons-Van-Vleck & Ct - constrained':

            print('    log_10[e] = {} +- {} log_10[(Oe^-2)]'.format(popt[0], perr[0]))
            print('    log_10[f] = {} +- {} log_10[(Oe^-2)]'.format(popt[1], perr[1]))
            print('    log_10[Ct] = {} log_10[s^-1]'.format(self.Ct_fixed))

        # Create the final expression to be written into the plot.
        expression = self.final_expression(popt, perr)
        ax.text( 0.0, 1.02, s=expression, fontsize=10, transform=ax.transAxes)

        ax.legend(loc=0, fontsize='medium', numpoints=1, ncol=2, frameon=False)
        ax.set_ylabel(r'$\tau^{-1}$ (s$^{-1}$)')

        plt.savefig(RP_filename+'_fit.png', dpi=300)
        plt.show()
        plt.close(fig2)
        print('\n    Plot of relaxation profile fit saved as\n     {}'.format(RP_filename+'_fit.png'))

        self.plot_residuals(xvals, rate, popt, self.RP_model, RP_filename)

        self._write_results( 
            xvals_fit, 10**self.RP_model(xvals_fit, *popt), popt, perr, RP_filename, RP_params, RP_fit
            )

        if args.verbose:
            print('\nThe covariance matrix is: \n', pcov)
            pcor = np.ones((len(pcov),len(pcov)))
            for i in range(len(pcov)):
                for j in range(len(pcov)):
                    if i == j: continue
                    pcor[i,j] = pcov[i,j]/(np.sqrt( np.diag(pcov)[i]*np.diag(pcov)[j] ) )                
            print('\nThe correlation matrix is:\n{}\n'.format(pcor))
            print('\nChi squared value is: ', sum( ( np.log10(rate)-self.RP_model(xvals,*popt) )/self.yerr_fit )**2 )

        return

    def final_expression(self, popt, perr):

        opt_list = []
        err_list = []
        for opt,err in zip(popt,perr):
            if int('{:1.0e}'.format( err ).split('e')[1] ) < 0:
                err_list.append('{:1.0e}'.format( err ).split('e')[0])
                opt_list.append( round( opt, abs( int('{:1.0e}'.format( err ).split('e')[1]) ) ) )
            else:
                err_list.append( str( round( err, -len( str(err).split('.')[0] )+1 ) ).split('.')[0] )
                opt_list.append( str( round( opt, -len( str(err).split('.')[0] )+1 ) ).split('.')[0] )


        if   self.RP_function ==  'Orbach':
            expression = r'$U_{{\mathrm{{eff}}}}$ = {} ({}) K, $\tau_0$ = 10$^{{{} ({})}}$ s'.format(opt_list[1], err_list[1], opt_list[0], err_list[0])
        
        elif self.RP_function ==  'Raman':
            expression = r'$C$ = 10$^{{ {}({})}}$ s$^{{-1}}$ K$^{{-n}}$, $n$ = {} ({})'.format(opt_list[0],err_list[0], opt_list[1], err_list[1])
        
        elif self.RP_function ==  'QTM':
            expression = r'$\tau_{{\mathrm{{QTM}}}}$ = 10$^{{{} ({})}}$ s'.format(opt_list[0], err_list[0])
        
        elif self.RP_function ==  'Orbach + Raman':
            expression = r'$U_{{\mathrm{{eff}}}}$ = {} ({}) K, $\tau_0$ = 10$^{{{} ({})}}$ s'.format(opt_list[1], err_list[1], opt_list[0], err_list[0])
            expression +='\n'
            expression +=r'$C$ = 10$^{{ {}({})}}$ s$^{{-1}}$ K$^{{-n}}$, $n$ = {} ({})'.format(opt_list[2], err_list[2], opt_list[3], err_list[3])
        
        elif self.RP_function ==  'Orbach + QTM':
            expression = r'$U_{{\mathrm{{eff}}}}$ = {} ({}) K, $\tau_0$ = 10$^{{{} ({})}}$ s, $\tau_{{\mathrm{{QTM}}}}$ = 10$^{{{} ({})}}$ s'.format(opt_list[1], err_list[1], opt_list[0], err_list[0], opt_list[2], err_list[2])
        
        elif self.RP_function ==  'Raman + QTM':
            expression = r'$C$ = 10$^{{ {}({})}}$ s$^{{-1}}$ K$^{{-n}}$, $n$ = {} ({}), $\tau_{{\mathrm{{QTM}}}}$ = 10$^{{{} ({})}}$ s'.format(opt_list[0],err_list[0], opt_list[1], err_list[1], opt_list[2], err_list[2])
        
        elif self.RP_function ==  'Orbach + Raman + QTM':
            expression = r'$U_{{\mathrm{{eff}}}}$ = {} ({}) K, $\tau_0$ = 10$^{{{} ({})}}$ s'.format(opt_list[1], err_list[1], opt_list[0], err_list[0])
            expression +='\n'
            expression +=r'$C$ = 10$^{{ {}({})}}$ s$^{{-1}}$ K$^{{-n}}$, $n$ = {} ({}), $\tau_{{\mathrm{{QTM}}}}$ = 10$^{{{} ({})}}$ s'.format(opt_list[2],err_list[2], opt_list[3], err_list[3], opt_list[4], err_list[4])

        elif self.RP_function == 'Orbach + Direct':
            expression = r'$U_{{\mathrm{{eff}}}}$ = {} ({}) K, $\tau_0$ = 10$^{{{} ({})}}$ s, $A$ = 10$^{{{} ({})}}$ s$^{{-1}}$ K$^{{-1}}$'.format(
                opt_list[1], err_list[1],
                opt_list[0], err_list[0],
                opt_list[2], err_list[2])

        elif self.RP_function == 'Raman + Direct':
            expression = r'$C$ = 10$^{{ {}({})}}$ s$^{{-1}}$ K$^{{-n}}$, $n$ = {} ({}), $A$ = 10$^{{{} ({})}}$ s$^{{-1}}$ K$^{{-1}}$'.format(
                opt_list[0], err_list[0],
                opt_list[1], err_list[1],
                opt_list[2], err_list[2])

        elif self.RP_function == 'Orbach + Raman + Direct':
            expression = r'$U_{{\mathrm{{eff}}}}$ = {} ({}) K, $\tau_0$ = 10$^{{{} ({})}}$ s'.format(
                opt_list[1], err_list[1],
                opt_list[0], err_list[0])
            expression += '\n'
            expression += r'$C$ = 10$^{{ {}({})}}$ s$^{{-1}}$ K$^{{-n}}$, $n$ = {} ({}), $A$ = 10$^{{{} ({})}}$ s$^{{-1}}$ K$^{{-1}}$'.format(
                opt_list[2], err_list[2],
                opt_list[3], err_list[3],
                opt_list[4], err_list[4])

        elif self.RP_function == 'QTM(H) + Raman-II + Ct':
            expression = r'$\tau_{{\mathrm{{QTM}}}}$ = 10$^{{{} ({})}}$ s, $\tau_{{\mathrm{{QTM(H)}}}}$ = 10$^{{{} ({})}}$ Oe$^{{-p}}$, $p$ = {} ({})'.format(
                opt_list[0], err_list[0],
                opt_list[1], err_list[1],
                opt_list[2], err_list[2])
            expression += '\n'
            expression += r'$CII$ = 10$^{{ {}({})}}$ s$^{{-1}}$ Oe$^{{-m}}$, $m$ = {} ({}), $Ct$ = 10$^{{{} ({})}}$ s $^{{-1}}$'.format(
                opt_list[3], err_list[3],
                opt_list[4], err_list[4],
                opt_list[5], err_list[5])

        elif self.RP_function == 'QTM(H) + Raman-II + Ct - constrained':
            expression = r'$\tau_{{\mathrm{{QTM}}}}$ = 10$^{{{:3.2f}}}$ s, $\tau_{{\mathrm{{QTM(H)}}}}$ = 10$^{{{} ({})}}$ Oe$^{{-p}}$, $p$ = {} ({})'.format(
                self.ZFQTM_fixed,
                opt_list[0], err_list[0],
                opt_list[1], err_list[1])
            expression += '\n'
            expression += r'$CII$ = 10$^{{ {}({})}}$ s$^{{-1}}$ Oe$^{{-m}}$, $m$ = {} ({}), $Ct$ = 10$^{{{:3.2f}}}$ s $^{{-1}}$'.format(
                opt_list[2], err_list[2],
                opt_list[3], err_list[3],
                self.Ct_fixed)

        elif self.RP_function ==  'Brons-Van-Vleck & Raman-II':
            expression = r'$e$ = 10$^{{{} ({})}}$ Oe$^{{-2}}$, $f$ = 10$^{{{} ({})}}$ Oe$^{{-2}}$'.format(opt_list[0], err_list[0], opt_list[1], err_list[1])
            expression +='\n'
            expression +=r'$CII$ = 10$^{{{} ({})}}$ s$^{{-1}}$ Oe$^{{-m}}$, $m$ = {} ({})'.format(opt_list[2],err_list[2], opt_list[3], err_list[3])

        elif self.RP_function ==  'Brons-Van-Vleck & Ct':
            expression = r'$e$ = 10$^{{{} ({})}}$ Oe$^{{-2}}$, $f$ = 10$^{{{} ({})}}$ Oe$^{{-2}}$'.format(opt_list[0], err_list[0], opt_list[1], err_list[1])
            expression +='\n'
            expression +=r'$Ct$ = 10$^{{{} ({})}}$ s $^{{-1}}$'.format(opt_list[2],err_list[2])

        elif self.RP_function ==  'Brons-Van-Vleck & Ct - constrained':
            expression = r'$e$ = 10$^{{{} ({})}}$ Oe$^{{-2}}$, $f$ = 10$^{{{} ({})}}$ Oe$^{{-2}}$'.format(opt_list[0], err_list[0], opt_list[1], err_list[1])
            expression +='\n'
            expression +=r'$Ct$ = 10$^{{{:3.2f}}}$ s $^{{-1}}$'.format(self.Ct_fixed)

        return expression 

    def plot_residuals(self, xvals, rate, popt, model_func, RP_filename):
        
        # Create figure and axes
        residual_plot, ax1 = plt.subplots(1,1, figsize = (6,6), num = 'Residuals')

        # Add additional set of axes to create "zero" line
        ax2 = ax1.twiny()
        ax2.get_yaxis().set_visible(False)
        ax2.set_yticks([])
        ax2.set_xticks([])
        ax2.spines["bottom"].set_position(("zero"))
        
        # Plot residuals
        ax1.errorbar(xvals,
                     np.log10(rate) - model_func(xvals, *popt),
                     self.yerr_fit, fmt='b.')

        # Set log scale on x axis
        ax1.set_xscale('log')

        # Upper and lower xvals
        tmaxmin = [xvals[0],xvals[-1]]

        # Set formatting for y axis ticks
        ax1.yaxis.set_major_formatter(ScalarFormatter())

        # Symmetrise y axis limits based on max abs error
        all_data = [np.log10(rate) - model_func(xvals, *popt) + self.yerr_fit] + [np.log10(rate) - model_func(xvals, *popt) - self.yerr_fit]
        ticks, maxval = min_max_ticks_with_zero(all_data, 5)
        ax1.set_yticks(ticks)
        ax1.set_ylim([-maxval*1.1, +maxval*1.1])

        # Axis labels
        ax1.set_ylabel(r'log$_{10}[\tau^{-1}_{\mathrm{exp}}]$ - log$_{10}[\tau^{-1}_{\mathrm{fit}}]$')
        if   self.process == 'tau_vs_T': 
            ax1.set_xlabel(r'Temperature (K)')
            # Set x limits
            set_rate_x_lims(ax1, xvals)
        elif self.process == 'tau_vs_H': 
            ax1.set_xlabel(r'Field (Oe)')

        # Adjust to fit y axis label
        residual_plot.subplots_adjust(left = 0.175)

        # Save plot
        plt.savefig(RP_filename+'fit_residuals.png', dpi=300)

        # Show
        #plt.show()
        plt.close(residual_plot)

        print('\n    Plot of fit residuals saved as\n     {}'.format(RP_filename+'fit_residuals.png'))

        return

    def _write_results(self, _temp, _fit, _popt, _perr, RP_filename, RP_params, RP_fit):

        # Write the result to RP_params.
        if self.RP_function ==  'QTM(H) + Raman-II + Ct - constrained':
            RP_params.write('{: 12.8E} {:^12}'.format(self.ZFQTM_fixed, '-'))
            for (i,j) in zip(_popt, _perr):
                RP_params.write('{: 12.8E} {: 12.8E}'.format(i,j))
            RP_params.write('{: 12.8E} {:^12}'.format(self.Ct_fixed,    '-'))

        elif self.RP_function ==  'Brons-Van-Vleck & Ct - constrained':
            RP_params.write('{: 12.8E} {:^12}'.format(self.Ct_fixed,    '-'))
            for (i,j) in zip(_popt, _perr):
                RP_params.write('{: 12.8E} {: 12.8E}'.format(i,j))

        else:
            for (i,j) in zip(_popt, _perr):
                RP_params.write('{: 12.8E} {: 12.8E}'.format(i,j))


        # Write the result to RP_fit
        for (i,j) in zip(_temp,_fit):
            RP_fit.write('{: 8.6f} {: 12.8E}'.format(i,j)+'\n')

        print('\n    Parameters written to\n     {}'.format(RP_filename+'_params.out'))
        print('    Fit written to\n     {}'.format(RP_filename+'_fit.out'))

        return


class Process_DC():

    def __init__(self, user_args):
        self.guess = user_args.guess


    def read_input(self, mag_decays_file, points_to_discard):
        '''
        Returns a dictionary ordered by field.
        '''

        data = {} # Data will be stored in a dictionary where the keys and values will be the fields and (temperature, time, moment), respectively.

        with open(mag_decays_file, 'r') as f:

            temp, time, moment = None, None, None # Needed to avoid UnboundLocalError: local variable 'time' referenced before assignment
                
            for line in f:

                if line.find('Field =') != -1:

                    if time is not None:
                        # Save previous dataset
                        data[key][1].append(time)
                        data[key][2].append(moment)
                        # Reset
                        temp, time, moment = None, None, None 

                    # initialise each value of the field (key) as a tuple of 3 empty lists (temperature, time, moment) that I will populate.
                    key = float(line.split()[2])
                    data[key] = ([], [], [])
                    

                elif 'T' in line:
                    data[key][0].append( float(line.split()[2]) )

                    # reset lists for the current dataset
                    temp   = []

                elif 'time' in line:

                    # skip the points measured with a saturating field.
                    for i in range(points_to_discard):
                        next(f)

                    # add completed lists from previous dataset to dictionary
                    if temp is not None and time is not None and moment is not None:
                        data[key][1].append(time)
                        data[key][2].append(moment)

                    # reset lists for the current dataset
                    time   = []
                    moment = []

                else:

                    time.append(   float(line.split()[0]) )
                    moment.append( float(line.split()[1]) )


        # To collect the last data set for which there is no next(f)
        data[key][1].append(time)
        data[key][2].append(moment)

        #for key,vals in data.items():
        #    print(key, len(vals[0]), len(vals[1][0]))

                    
        if len(data) == 0: 
            exit('\n***Error***:\n The program could not find information on the temperature. Make sure the employed format is: T = X K.')

        return collections.OrderedDict(sorted(data.items()))


    def define_DC_model(self, user_args):

        if user_args.model == 'single':

            if user_args.M_eq != 'free':
                self.model_function = self.single_exponential
                self.bounds = ([0.0],[np.inf])                     # tau

            else:
                self.model_function = self.single_exponential_2
                self.bounds = ([0.0, -np.inf ],[np.inf, np.inf]) # tau, Meq

        elif user_args.model == 'stretched':

            if user_args.M_eq != 'free':
                self.model_function = self.stretched_exponential
                self.bounds = ([0.0, 0.0],[np.inf, 1.0])                  # tau, beta

            else:
                self.model_function = self.stretched_exponential_2
                self.bounds = ([0.0, 0.0, -np.inf],[np.inf, 1.0, np.inf]) # tau, beta, Meq


    def single_exponential(self, time, tau): # M(t) = Meq + (M0 - Meq) * exp( -t / tau)
        return self.Meq + (self.M0 - self.Meq)*np.exp(-time/tau)

    def single_exponential_2(self, time, tau, Meq): # M(t) = Meq + (M0 - Meq) * exp( -t / tau)
        return Meq + (self.M0 - Meq)*np.exp(-time/tau)

    def stretched_exponential(self, time, tau, beta): # M(t) = Meq + (M0 - Meq) * exp( -t / tau)**beta
        return self.Meq + (self.M0 - self.Meq)*np.exp(-(time/tau)**beta)

    def stretched_exponential_2(self, time, tau, beta, Meq): # M(t) = Meq + (M0 - Meq) * exp( -t / tau)**beta
        return Meq + (self.M0 - Meq)*np.exp(-(time/tau)**beta)


    def fit_decays(self, field, decays, DC_filename, params_fit, vals_fit):

        for it,temp in enumerate(decays[0]):

            time   = decays[1][it]
            moment = decays[2][it]

            fit_failed = None
            
            # Define the values.
            time   = [ i - time[0] for i in time ] # set initial measurement to t = 0.
            self.M0 = moment[0]
            if args.M_eq != 'free' and args.M_eq != 'exp' and args.M_eq != 'multiple':
                self.Meq = float(args.M_eq)
                if self.M0 < self.Meq: 
                    exit('\n***Error***: At {} K and {} Oe, the provided M_eq = {} is smaller than M0 = {}.\nPlease revise.\n'.format(temp, field, self.M_eq, self.M0))
            elif args.M_eq == 'multiple':
                # check how many lines to decide how to index it.
                if np.ndim(meq) == 1: self.Meq = meq[1]
                else: self.Meq = meq[it,1]
            else:
                self.Meq = moment[-1] 

            if it == 0: guess = self.guess

            # Fit the curve and update the guess.
            try: # I should try to wrap curve_fit into a class and adapt model_function(), write_params_fit(), write_vals_fit() and plot() so I pass them the class and I dont have to worry about the logic with fit_failed
                popt, pcov = curve_fit(self.model_function, time, moment, p0=guess, bounds=self.bounds)
            except RuntimeError:
                fit_failed = True

            if not fit_failed:
                perr = np.sqrt(np.diag(pcov)) # compute one standard deviation errors on the parameters 
                guess = popt
                #print('    T =  {:04.2f} K; Field = {:06.2f} Oe: DONE. '.format( temp, field ), end='\r', flush=True )

                # Calculate the model function with the optimised parameters.
                model = self.model_function(np.array(time), *popt)

                # Write the parameters of the fit.
                self.write_params_fit(params_fit, temp, field, self.M0, self.Meq, popt, perr )

                # Write the values of the fit.
                self.write_vals_fit(vals_fit, temp, time, field, moment, model )

                # Plot the results.
                self.plot(temp, field, time, moment, model, popt)

            else:
                #print('    T =  {:04.2f} K; Field = {:06.2f} Oe: FAILED. '.format( temp, field ), end='\r', flush=True )

                # Write the parameters of the fit.
                params_fit.write('{: 06.2f}  {: 06.2f} {:^10}'.format(temp, field, 'Fit failed')+'\n')

                # Write the values of the fit.
                vals_fit.write( '{: 06.2f}  {: 06.2f} {:^10}'.format(temp, field, 'Fit failed')+'\n')

                pass


        params_fit.close()
        vals_fit.close()
        print('\n\n    Parameters written to\n     {}'.format(DC_filename+'_params.out'))
        print('    Fit written to\n     {}'.format(DC_filename+'_fit.out'))

        return 


    def write_params_fit(self, filename, temperature, field, M0, Meq, popt, perr):

        # Calculate the ESD on tau associated with beta.
        if args.model == 'stretched':
            tau_ESD = self.Johnston(popt[0], popt[1]) # tau, beta

        # Write the parameters of the fit.
        if args.M_eq == 'free':
            filename.write(' {: 06.2f}  {: 06.2f}  {: 08.4E}'.format( temperature, field, M0 ) )
            filename.write(' {: 08.4E} '.format( popt[-1] ) ) # Meq is the last optimised parameter.
            for i, j in zip( popt[0:-1], perr[0:-1] ): # exclude the last item corresponding to Meq
                filename.write(' {: 08.4E} {: 08.4E} '.format( i, j ) )

        else:
            filename.write(' {: 06.2f}  {: 06.2f}  {: 08.4E}  {: 08.4E} '.format( temperature, field, M0, Meq ) ) 
            for i, j in zip( popt, perr ):
                filename.write(' {: 08.4E} {: 08.4E} '.format( i, j ) )

        if args.model == 'stretched':
            filename.write(' {: 08.4E} {: 08.4E}'.format( tau_ESD[0], tau_ESD[1] ) )
        filename.write('\n')

        return


    def Johnston(self, tau, beta):

        J_err_upper = tau*np.exp(   ( 1.64*np.tan( np.pi*(1-beta)/2. ) ) / ((1-beta)**0.141) )
        J_err_lower = tau*np.exp( - ( 1.64*np.tan( np.pi*(1-beta)/2. ) ) / ((1-beta)**0.141) )
            
        return J_err_upper, J_err_lower


    def write_vals_fit(self, filename, temperature, time, field, moment, model):

        # Write the values of the fit.
        filename.write('T = {:04.2f} K, field = {:04.2f} Oe'.format( temperature, field )+'\n' )
        filename.write(' {:^10} {:^10} {:^10} '.format( 'time (s)', 'moment', 'fit'  )+'\n' ) 
        for i,j,k in zip( time, moment, model ): 
            filename.write(' {: 08.4E} {: 08.4E} {: 08.4E}'.format( i,j,k )+'\n' )

        return


    def plot(self, temperature, field, time, moment, model, popt):

        if args.model == 'single': label_fit=r'$\tau={:06.2f}\ $(s)'.format(popt[0])

        elif args.model == 'stretched': label_fit = r'$\tau={:06.2f}$ (s), $\beta={:04.3f}$'.format(popt[0], popt[1])

        # Plot the figure.
        fig, ax = plt.subplots( 1, 1, sharex=True, sharey=True, figsize=(4.5,3.5))
        ax.plot(time, moment, lw=0, marker='o', fillstyle='none', label=str(temperature)+' K, '+str(field)+' Oe')
        ax.plot(time, model, lw=1.5, label=label_fit)

        ax.legend(loc=0, fontsize='10', numpoints=1, ncol=1, frameon=False)
        ax.set_ylabel('Moment')
        ax.set_xlabel('time (s)')

        fig.tight_layout()

        if args.save_plots:
            _filename = DC_filename+'_{}K_{}Oe_{}'.format(temperature, field, args.model)
            fig.savefig(_filename+'.png', dpi=200)
            print('\n    Plot of magnetisation decay saved as\n     {}'.format(_filename))

        if not args.hide_plots:
            plt.show()

        plt.close('all')

        return


class Process_Waveform():

    def __init__(self, user_args):
        # Identify how many <filename> are given to initialise the class
        # appropriately.
        self.file_names = []
        for arg in user_args.input:
            # use glob to find files matching wildcards; if a string does not
            # contain a wildcard, glob will return it as is.
            self.file_names += glob(arg)

        self.time_col = user_args.time_col - 1
        self.temp_col = user_args.temp_col - 1
        self.field_col = user_args.field_col - 1
        self.mom_col = user_args.moment_col - 1
        self.momerr_col = self.mom_col + 1
        self.applied_field = user_args.field
        self.field_window = user_args.field_window
        self.filenumber = 0
        # Get the number of frequencies to be discarded.
        # self.discard_freq = user_args.discard_freq

    def __call__(self, filename):

        # Define the filename containing the data.
        self.filename = filename

        # Define the path used to store the results.
        self.folder = os.path.join(os.getcwd(), self.filename.split('.')[0])

        # Store the data in memory.
        data = self.read_data(self.filename)

        # Identify data points belonging to the same frequency within file and
        # discard data points measured at zero-fields (tails between waveforms)
        time, field, moment = self.slice_data(data[0], data[1], data[2])

        # Define the number of frequencies in the file
        self.nfreqs = len(time)

        # Plot the individual data blocks.
        self.plot_waveform_data(time, field, moment)

        # Calculate complex-valued susceptibility.
        susceptibility = self.calculate_susceptibility(
            time, field, moment, data[-1]
            )

        # Write the results to file.
        self.write_output(susceptibility)

        # Update the iterator.
        # self.filenumber += 1

        return susceptibility

    '''
    # Use @singledispatchmethod to implement multiple class constructors.
    # The base method works on the one-file several-frequencies case.
    @singledispatchmethod
    def generic_method(self, filename):
        # Define the filename containing the data.
        self.filename = filename

        # Define the path used to store the results.
        self.folder = os.path.join(os.getcwd(), self.filename.split('.')[0])

        # Store the data in memory.
        data = self.read_data(self.filename)

        # Identify data points belonging to the same frequency within file.
        time, field, moment = self.slice_data(data[0], data[1], data[2])

        # Define the number of frequencies in the file
        self.nfreqs = len(time)

        # Plot the individual data blocks.
        self.plot_waveform_data(time, field, moment)

        # Calculate complex-valued susceptibility.
        susceptibility = self.calculate_susceptibility(
            time, field, moment, data[-1]
            )

        # Write the results to file.
        self.write_output(susceptibility)

        return susceptibility

    # The alternative method works on the one-file one-frequency case.
    @generic_method.register(list)
    def _multiple_files(self, arg):

        # Define an empty list to store the AC susceptibilities.
        susceptibility = []

        for filename in arg:
            # Define the filename containing the data.
            self.filename = filename

            # Define the path used to store the results.
            self.folder = os.path.join(os.getcwd(), self.filename.split('.')[0])

            # Store the data in memory.
            data = self.read_data(self.filename)

            time, field, moment = [data[0]], [data[1]], [data[2]]

            # Define the number of frequencies in the file
            self.nfreqs = len(time)

            # Plot the individual data blocks.
            self.plot_waveform_data(time, field, moment)

            # Calculate complex-valued susceptibility.
            susc = self.calculate_susceptibility(
                time, field, moment, data[-1]
                )

            # Write the results to file.
            self.write_output(susc)

            # Store
            susceptibility.append(susc[0])

        return susceptibility
    '''

    def read_data(self, filename):
        """
        Read and store input file's data.

        Parameters
        ----------
            filename : str
                filename containing data.

        Returns
        -------
            Time: array
                Array containing measurement times.
            Field: array
                Array containing the magnetic fields employed.
            Moment: array
                Array containing the magnetic moments measured.
            Temperature: float
                Rounded temperature(s).

        Raise
        -----
            ValueError
                If data section cannot be found in file.
        """

        # Get the info section and line number where the data starts.
        with open(filename, 'r') as f:
            data_section = False
            data_starts_from = 0
            for line in f:
                data_starts_from += 1
                # If [Data] containing line reached, add one (headers) and exit.
                if args.data_pointer in line:
                    data_starts_from += 1
                    break
                else:
                    data_section = True
        if not data_section:
            exit(
                '\nValueError:\nThe section containing data cannot be found.\
     Please revise --data_pointer argument.'
                )

        # Read in data using the user-defined columns. Note -1 as index starts at 0
        # integer 5 points to M err
        data = np.loadtxt(
            filename,
            delimiter=',',
            skiprows=data_starts_from,
            usecols=(
                self.time_col, self.temp_col, self.field_col,
                self.mom_col, self.momerr_col
                )
            )

        # Define the returned arrays.
        Time = data[:, 0]
        Field = data[:, 2]
        Moment = data[:, 3]
        M_err = data[:, 4]
        Temperature = np.round(data[:, 1][0], decimals=1)

        # Define the indices of the measured points that are not noisy (to keep).
        # create a boolean mask for noisy data. Threshold is 0.3. We keep data
        # points whose associated error is smaller than threshold
        # (only True entries)
        mask = M_err/Moment <= 0.3

        return Time[mask], Field[mask], Moment[mask], Temperature

    def slice_data(self, Times, Fields, Moments):
        """
        Define data blocks for each frequency.

        Parameters
        ----------
            Times: array
                Array containing measurement times referred to first point.
            Fields: array
                Array containing the magnetic fields employed.
            Moments: array
                Array containing the magnetic moments measured.

        Returns
        -------
            Time: array
                Multi dimensional array containing measurement times referred to
                first point.
            Field: array
                Multi dimensional array containing the magnetic fields employed.
            Moment: array
                Multi dimensional array containing the magnetic moments measured.
        Raise
        -----
            Error
                If no block was stored.
        """

        # Retrieve the indices associated with a change of frequency: when the
        # field flattens.
        '''
        indxs = []
        for it, val in Fields:
            if val > self.field_window[0] and val < self.field_window[1]:
                indxs += it
        '''
        indxs = np.where(
            (Fields > self.field_window[0]) & (Fields < self.field_window[1])
            )[0]

        if len(indxs) <= 1:
            exit("""\n***Error***\nNo waveform data identified in {}.\n
     Please make sure the initial and final point are collected at zero field,
     as this is how the program parses the data.""".format(self.filename))

        # Ignore data points that accidentally fall within the field_window.
        delete = []
        for i in range(1, len(indxs)-1):
            if abs(indxs[i-1] - indxs[i]) > 1 and abs(indxs[i] - indxs[i+1]) > 1:
                delete += indxs[i]
                # maybe I should use indxs.pop() so I dont loop over delete.
        indxs = [i for i in indxs if i not in delete]

        '''
        indxs contains the indices where Field flattens.
        From these, I need the first and last element to ignore the data within
        the waveform measurement.
        For that, I use groupby from itertools.

        https://docs.python.org/3/library/itertools.html#itertools.groupby

        itertools.groupby(iterable, key=None)

         Make an iterator that returns consecutive keys (k) and groups (g) from
        the iterable (enumerate).
         The key is a function (lambda) computing a key value for each element,
        where ix[0] and ix[1] are the it and val, respectively.
         The iterable needs to already be sorted on the same key function.
         groupby generates a break or new group every time the value of the key
        function changes (which is why it is usually necessary to have sorted
        the data using the same key function).
        Example:
            indxs = [ 0, 1, 458, 459, 460, 461, 1282, 1283 ]
            ix: [ 0, 0, -456, -456, -456, -1276, -1276 ]
                 ^      ^                 ^
                 |      |                 |

         The returned group (g) is itself an iterator sharing the underlying
        iterable with groupby(). Because the source is shared, when the
        groupby() object is advanced, the previous group is no longer visible.
        So, if that data is needed later, it should be stored as a list (lims).
        '''

        # Store all breaks identified by groupby (where field is flat).
        lims = []
        for k, g in groupby(enumerate(indxs), lambda ix: ix[0] - ix[1]):
            lims.append(list(g))

        # Collect first and last element of list; these are tuples with index
        # and value of indxs array, respectively.
        limits = []
        for i in lims:
            limits.append((i[0][1], i[-1][1]))

        # Define the data intervals from the limits
        bounds = []
        for i in range(len(limits)-1):
            bounds.append((limits[i][1]+1, limits[i+1][0]-1))

        Time = [
            Times[i:j] for i, j in bounds
            ]

        Field = [
            Fields[i:j] for i, j in bounds
            ]

        Moment = [
            Moments[i:j] for i, j in bounds
            ]

        '''
        *** This section has been superseeded by the groupby code ***
        # calculate pair-wise differences between consecutive elements.
        # this will be used to identify how many data points separate each
        # frequency to later shift the indices accordingly and get the initial
        # and final points of each block in the middle of the zero-field data.
        diffs = np.diff(indxs)
        # print(diffs)

        # reset the pivot index (when the data jumps) to its orginal value.
        # example:
        # indxs = [ 1, 2, 3, 15, 16, 17, 18, 30, 31, 32]
        # diffs = [1, 1, 12, 1, 1, 1, 12, 1, 1]
        # diffs = [12, 12]
        # cumsum = [12, 24]
        # if I were to use the indices in cumsum as such, they would refer to
        # indices 3 positions earlier. To fix that, I need to add the number of 1s.
        counter = 0
        for it, val in enumerate(diffs):
            if val == 1:
                counter += 1
            else:
                diffs[it] += counter
                counter = 0

        # Define how many zero-field points are measured between each frequency.
        counter = 0
        shift = []
        for it, val in enumerate(diffs):
            if val == 1:
                counter += 1
            else:
                shift.append(counter)
                counter = 0

        # store only the indices associated with the start of a new block.
        diffs = diffs[diffs != 1]

        # define the cumulative sum.
        csum = np.cumsum(diffs)

        # add zero as first element
        csum = np.insert(csum, 0, 0)

        # apply the shift to go from middle point to middle point
        for it, val in enumerate(shift):
            csum[it] += int(val/2)

        print(csum)
        exit()

        Time = [
            Times[csum[i]:csum[i+1]] for i in range(len(csum)-1)
            ]

        Field = [
            Fields[csum[i]:csum[i+1]] for i in range(len(csum)-1)
            ]

        Moment = [
            Moments[csum[i]:csum[i+1]] for i in range(len(csum)-1)
            ]
        '''

        '''
        if len(Time) != args.nfrequencies:
            exit('\n***Error***\nParsed data finds {} blocks, inconsistent with\
     indicated {} nfrequencies.\n\
     Please use a different --field_window option.'.format(
                len(Time), args.nfrequencies)
            )
        '''

        return Time, Field, Moment

    def plot_waveform_data(self, Time, Field, Moment):
        """
        Plot data for each frequency.

        Parameters
        ----------
            Time: array
                Multi dimensiona array containing measurement times referred to
                first point.
            Field: array
                Multi dimensiona array containing the magnetic fields employed.
            Moment: array
                Multi dimensiona array containing the magnetic moments measured.
        """

        # Calculate discrete Fourier transform of the data.
        ft_H, ft_M, freq = self.calculate_FT(Time, Field, Moment)

        # Loop over each frequency
        for i in range(self.nfreqs):

            # Define canvas object.
            fig, (ax1, ax2) = plt.subplots(
                2, 1, sharex=False, sharey=False, figsize=(6, 4))
            ax3 = ax1.twinx()
            ax4 = ax2.twinx()

            # Refer the time to first point within each set.
            time = Time[i]-Time[i][0]

            # Get only the positive values of the fourier transforms.
            pos_indx = np.where(freq[i] > 0.)
            # I could also use the actual structure of the array.
            # pos_indx = freq[1:len(time)/2]

            # Plot the data.
            ax1.plot(time, Field[i], lw=1.5, c='k', label='Field')
            ax3.plot(time, Moment[i], lw=1.5, c='tab:blue', label='Moment')
            ax2.plot(
                freq[i][pos_indx], np.abs(ft_H[i][pos_indx]),
                lw=1.5, c='k', label='Field')
            ax4.plot(
                freq[i][pos_indx], np.abs(ft_M[i][pos_indx]),
                lw=1.5, c='tab:blue', label='Field')
            ax1.yaxis.label.set_color('k')
            ax3.yaxis.label.set_color('tab:blue')
            ax2.yaxis.label.set_color('k')
            ax4.yaxis.label.set_color('tab:blue')
            ax2.set_xscale('log')

            # Set the labels.
            ax1.set_xlabel('Time (s)')
            # ax2.set_xlim([0., max(freq[i])])  # 10*abs(freq[i][idx])
            ax2.set_xlabel('Frequency (Hz)')
            ax1.set_ylabel('Field (Oe)')
            ax3.set_ylabel('Moment (emu)')
            ax2.set_ylabel(r'|FT$^{D}$ (H)|')
            ax4.set_ylabel(r'|FT$^{D}$ (M)|')

            # Matplotlib magic.
            fig.tight_layout()

            # Define the index of maximum FT.
            idx_H = np.argmax(np.abs(ft_H[i]))

            # Save the figure.
            plt.savefig(
                '{}_{}_{:6.5f}{}'.format(
                    self.folder,
                    'freq',
                    abs(freq[i][idx_H]),
                    '.png'
                    ),
                dpi=300
                )

            # Show individual plots if requested.
            if args.show:
                plt.show()
            plt.close('all')

        return

    def calculate_FT(self, Time, Field, Moment):
        """
        Calculates the discrete Fourier transform for each frequency.

        See
        https://numpy.org/doc/stable/reference/routines.fft.html
        https://stackoverflow.com/questions/3694918/how-to-extract-frequency-associated-with-fft-values-in-python

        Parameters
        ----------
            Time: array
                Multi dimensional array containing measurement times referred to
                first point.
            Field: array
                Multi dimensional array containing the magnetic fields employed.
            Moment: array
                Multi dimensional array containing the magnetic moments measured.
        Returns
        -------
            ft_f: array
                Array of complex numbers containing the Fourier transform of field.
            ft_m: array
                Array of complex numbers containing the Fourier transform of moment.
            freq: array
                Array of frequency (Hz) associated with the coefficients.
        """

        ft_f = []
        ft_m = []
        freq = []

        # Loop over each frequency
        for i in range(self.nfreqs):

            # Set each data set to zero seconds.
            t = Time[i]-Time[i][0]

            # Calculate the sample spacing (inverse of sampling rate).
            # Sampling rate is defined as npoints/measurement_time.
            spacing = t[-1]/len(t)

            # Retreive the associated frequencies.
            freq.append(
                np.fft.fftfreq(len(t), d=spacing)
                )

            # Caculate the Fourier transform of field and moment.
            ft_f.append(np.fft.fft(Field[i]))
            ft_m.append(np.fft.fft(Moment[i]))

        return ft_f, ft_m, freq

    def calculate_ACDrive(self, Field):
        """
        Calculates the associated AC drive for each frequency.

        Parameters
        ----------
            Field: array
                Multi dimensional array containing the magnetic fields employed.
        Returns
        -------
            AC_Drive: list
                Multi dimenstional list containing AC drive for each frequency.
        """

        # Define empty list to store the values.
        AC_Drive = []

        # Loop over each frequency
        for i in range(self.nfreqs):

            # Sort the field values of each frequency.
            sorted_field = np.sort(Field[i])

            # Average the 50% lowest and highest values.
            lowest = np.mean(sorted_field[:int(len(sorted_field)/2)])
            highest = np.mean(sorted_field[int(len(sorted_field)/2)::])

            # Calculate AC drive
            AC_Drive.append(
                abs((highest - lowest)/2.)
                )

        return AC_Drive

    def calculate_susceptibility(self, Time, Field, Moment, Temperature):
        """
        Calculates magnetic susceptibility.

        Parameters
        ----------
            Time: array
                Multi dimensiona array containing measurement times referred to
                first point.
            Field: array
                Multi dimensiona array containing the magnetic fields employed.
            Moment: array
                Multi dimensiona array containing the magnetic moments measured.
            Temperature: float
                Rounded temperature(s).
        Returns
        -------
            sus: list
                List of tuples containing the temperature, peak at the
                fundamental field frequency, real, imaginary susceptibilities,
                phase angle and AC drive for each frequency measured.
                Sorted by frequency values.
        """

        # Calculate discrete Fourier transform of the data.
        ft_F, ft_M, freq = self.calculate_FT(Time, Field, Moment)

        # Calculate AC drives.
        AC_Drive = self.calculate_ACDrive(Field)

        # Define an empty list to collect the calculated susceptibilities.
        sus = []

        '''
        # Define the data points to be kept.
        discard = self.discard_freq[self.filenumber] - 1
        keep = [i for i in list(range(self.nfreqs)) if i not in discard]
        print(self.filenumber, self.discard_freq[self.filenumber], keep)
        '''

        # Loop over each frequency
        for i in range(self.nfreqs):
        #for i in keep:

            #pos_indx = freq[1:len(Time)/2]

            # Find the index of largest FT field & moment.
            idx_F = np.argmax(np.abs(ft_F[i]))
            idx_M = np.argmax(np.abs(ft_M[i]))  # *** USELESS ***

            # Define the susceptibility as M/H at maximum (emu/Oe)
            # chi = np.abs(ft_M[i][idx_M])/np.abs(ft_F[i][idx_F])
            chi = np.abs(ft_M[i][idx_F])/np.abs(ft_F[i][idx_F])

            # Define the phase angle (rad) of the ratio between field and moment
            # spectra at their fundamental frequency.
            # It is the ratio because any function of the type:
            # Acos(X) + Bsin(X) = Ccos(X + phasefactor)
            # This phasefactor angle is determined by the ratio of A/B
            # phi = abs(np.angle((ft_F[i][idx_F]/ft_M[i][idx_M]), deg=False))
            phi = abs(np.angle((ft_F[i][idx_F]/ft_M[i][idx_F]), deg=False))

            # Define the real and imaginary susceptibility components.
            chi_re = abs(chi*np.cos(phi))
            chi_im = abs(chi*np.sin(phi))

            # Populate the list.
            sus.append(
                (Temperature, abs(freq[i][idx_F]), chi_re, chi_im, phi,
                    AC_Drive[i])
                )

        # Sort by frequency values (2nd element of tuples).
        return sorted(sus, key=lambda x: x[1])

    def plot_susceptibilies(self, sus):
        """
        Plot susceptibility results.

        Parameters
        ----------
            sus: list
                List of tuples containing the temperature, peak at the
                fundamental field frequency, real, imaginary susceptibilities,
                phase angle and AC drive for each frequency measured.
                Sorted by temperature & frequency values.
        """

        # Define canvas object.
        fig, (ax1, ax2) = plt.subplots(
            2, 1, sharex=True, sharey=False, figsize=(6, 4)
            )

        # Unpack temperatures.
        temps = [i[0] for i in sus]

        # Define isothermal data blocks.
        for it, temp in enumerate(np.unique(temps)):

            # Unpack the data.
            indxs = [ij for ij, val in enumerate(temps) if val == temp]

            freq = [i[1] for i in sus[indxs[0]:indxs[-1]]]
            sus_re = [i[2] for i in sus[indxs[0]:indxs[-1]]]
            sus_im = [i[3] for i in sus[indxs[0]:indxs[-1]]]

            # Plot data
            ax1.plot(freq, sus_re, marker='o', lw=1, label='{} K'.format(temp))
            ax2.plot(freq, sus_im, marker='o', lw=1, label='{} K'.format(temp))


        # Set the legend.
        for axis in [ax1, ax2]:
            axis.legend(
                loc=0, fontsize='x-small', markerscale=0.6,
                handlelength=0.4, labelspacing=0.2, handletextpad=0.4,
                numpoints=1, ncol=int(len(np.unique(temps))/2), frameon=False
                )

        ax2.set_xscale('log')
        ax2.set_xlabel('Wave Frequency (Hz)')
        ax1.set_ylabel(r'$\chi^{,}$  (emu)')
        ax2.set_ylabel(r'$\chi^{,,}$ (emu)')
        plt.show()
        plt.close('all')

        return

    def write_output(self, sus):
        """
        Write susceptibility results to file.

        Parameters
        ----------
            sus: list
                List of tuples containing the temperature, peak at the
                fundamental field frequency, real, imaginary susceptibilities,
                phase angle and AC drive for each frequency measured.
                Sorted by temperature & frequency values.
        """

        # Create the file.
        with open(self.folder+'_waveform.out', 'w') as f:
            f.write(
                '{}, {}, {}, {}, {}, {}'.format(
                    'Temperature (K)',
                    'Frequency (Hz)',
                    'chi_inphase (emu/Oe)',
                    'chi_outofphase (emu/Oe)',
                    'phi (rad)',
                    'AC Drive (Oe)'
                )
            )
            for i in sus:
                f.write(
                    '\n {:4.1f}, {: 12.8f}, {: 12.8e}, {: 12.8e}, {: 7.4f}, {: 7.4f}'.format(*i)
                    )

        return

    def prepare_ccfit_AC_input(self, Field, sus):
        """
        Create a common input file for CC-FIT2 for AC module.

        Parameters
        ----------
            Field: float
                Applied field value (Oe).
            sus: list
                List of tuples containing the temperature, peak at the
                fundamental field frequency, real, imaginary susceptibilities,
                phase angle and AC drive for each frequency measured.
                Sorted by frequency values.
        """

        # Create the file.
        # if self.onefreqperfile:
        if len(self.file_names) == 1:
            path = self.folder
        else:
            path = 'job'

        with open(path+'_toccfit.dat', 'w') as f:
            f.write('{}\n'.format('[Data]'))
            f.write(
                '{},{},{},{},{},{},{},'.format(
                    'Field (Oe)',
                    'Temperature (K)',
                    'Frequency (Hz)',
                    'AC X\' (emu/Oe)',
                    'AC X" (emu/Oe)',
                    'phi',
                    'Drive Amplitude (Oe)'
                )
            )
            for i in sus:
                f.write(
                    '\n {:4.1f}, {:6.3f}, {: 12.8f}, {: 12.8e}, {: 12.8e}, {: 7.4f}, {: 7.4f}'.format(Field, *i)
                    )

        return

    def flatten_and_sort(self, t):
        """
        Flattens and sorts (by temperature, then frequency) the final results.

        Parameters
        ----------
            t: nested list.
                List of tuples containing the temperature, peak at the
                fundamental field frequency, real, imaginary susceptibilities,
                phase angle and AC drive for each frequency measured.
                Sorted by frequency values.
        Returns
        -------
            sus: list
                List of tuples containing the temperature, peak at the
                fundamental field frequency, real, imaginary susceptibilities,
                phase angle and AC drive for each frequency measured.
                Sorted by temperature & frequency values.
        """
        flattened = [item for sublist in t for item in sublist]
        # Sort by temp and frequency values (1st & 2nd element of tuples).
        return sorted(flattened, key=lambda x: (x[0], x[1]))

    def sort_results(self, t):
        """
        Sorts (by frequency) the final results.

        Parameters
        ----------
            t: list.
                List of tuples containing the temperature, peak at the
                fundamental field frequency, real, imaginary susceptibilities,
                phase angle and AC drive for each frequency measured.
                Sorted by frequency values.
        Returns
        -------
            sus: list
                List of tuples containing the temperature, peak at the
                fundamental field frequency, real, imaginary susceptibilities,
                phase angle and AC drive for each frequency measured.
                Sorted by frequency values.
        """
        # Sort by frequency values (second element of tuples).
        return sorted(susc, key=lambda tup: tup[1], reverse=False)


def parse_input():
    """
    Parse user-defined options.

    Parameters
    ----------

    Returns
    -------
        args: object
            object containing the user-defined options.

    Raise
    -----
        Error
            If optional guesses in DC mode are incorrect.
    """

    description = """
    Program to extract the relaxation times from AC or DC data and fit their
    relaxation profile.

    Available modules:
        CC-FIT2_cmd AC ...
        CC-FIT2_cmd Waveform ...
        CC-FIT2_cmd DC ...
        CC-FIT2_cmd RelaxationProfile ...
    """

    epilog = """
    To display options for a specific module, use CC-FIT2_cmd.py module -h
    """

    parser = argparse.ArgumentParser(
        description=description,
        epilog=epilog,
        formatter_class=argparse.RawDescriptionHelpFormatter
        )

    # create the top-level parser
    subparsers = parser.add_subparsers(dest='exe_mode')

    # AC mode
    description_AC = """
    Extract relaxation times from AC susceptibilities using the Debye model and
    (optional) fit the resulting relaxation profile.
    """

    AC_parser = subparsers.add_parser(
        'AC',
        description=description_AC,
        formatter_class=argparse.RawDescriptionHelpFormatter
        )
    AC_parser.add_argument(
        'input_file',
        metavar='<filename>',
        type=str,
        help='''File containing the AC raw data.
        See Manual for expected format.'''
        )
    AC_parser.add_argument(
        'mass',
        metavar='<value>',
        type=float,
        help='Sample mass (mg).'
        )
    AC_parser.add_argument(
        'MW',
        metavar='<value>',
        type=float,
        help='Sample molecular weight (g/mol).'
        )
    AC_parser.add_argument(
        '--process',
        metavar='<Option>',
        choices=['plot', 'susc', 'all'],
        default='all',
        help='''What to do: "plot" just shows the raw data; "susc" fits only
        the AC data; "all" fits AC data and relaxation profile.
        Options: plot, susc, all.
        Default: all.'''
        )
    AC_parser.add_argument(
        '--discard_off',
        action='store_true',
        help='Fit the susceptibilities despite not showing a peak.'
        )
    AC_parser.add_argument(
        '--susc_vs_T',
        action='store_true',
        help='Plot susceptibilities vs temperature.'
        )
    AC_parser.add_argument(
        '--round',
        metavar='<Int>',
        type=int,
        default=2,
        help='Temperature rounding. See Manual. Default: 2.'
        )
    AC_parser.add_argument(
        '--round_field',
        metavar='<Int>',
        type=int,
        default=2,
        help='Decimal field rounding. Default: 2.'
        )
    AC_parser.add_argument(
        '--select_T',
        action='store_true',
        help='Select the temperatures to fit.'
        )
    AC_parser.add_argument(
        '--sigma',
        metavar='<Int>',
        type=int,
        default=1,
        help='log normal confidence interval. Default: 1 = 68%%.'
        )
    AC_parser.add_argument(
        '--data_pointer',
        metavar='<Str>',
        type=str,
        default='[Data]',
        help='String used to locate data in ac.dat file. Default: [Data]'
        )
    AC_parser.add_argument(
        '--filter_flats',
        action='store_true',
        help='Activate removing flat lines.'
        )
    AC_parser.add_argument(
        '--error_fit',
        metavar='<Value>',
        type=float,
        default=1E-06, 
        help='Threshold to discard flat lines. Default: 1E-06.'
        ) # Defined as the sqrt( sum(square( linear(val_x, *linear_popt) -  val_y )) ). The larger this value, the tighter the constraint.\n
    AC_parser.add_argument(
        '--verbose',
        action='store_true',
        help='Print the read-in values from the file indicated.'
        )

    # Waveform
    description_Waveform = """
    Extract AC susceptibilities from waveform data.
    See PCCP, 2019, 21, 22302-22307 for method's details.
    """
    epilog_Waveform = """
    This module creates a $NAME_toccfit.dat file to be passed to the AC module.
    """
    Waveform_parser = subparsers.add_parser(
        'Waveform',
        description=description_Waveform,
        epilog=epilog_Waveform,
        formatter_class=argparse.RawDescriptionHelpFormatter
        )
    Waveform_parser.add_argument(
        'input',
        type=str,
        metavar='<filename(s)>',
        nargs='+',
        help='SQUID file(s). Supports shell-style wildcards, e.g. data_*.dat.'
        )
    Waveform_parser.add_argument(
        '--time_col',
        type=int,
        metavar='<value>',
        default=2,
        help='Column number used to read "Time stamp (s)" values. Default=2.'
        )
    Waveform_parser.add_argument(
        '--temp_col',
        type=int,
        metavar='<value>',
        default=3,
        help='Column number used to read "Temperature (K)" values. Default=3.'
        )
    Waveform_parser.add_argument(
        '--field_col',
        type=int,
        metavar='<value>',
        default=4,
        help='Column number used to read "Field (Oe)" values. Default=4.'
        )
    Waveform_parser.add_argument(
        '--moment_col',
        type=int,
        metavar='<value>',
        default=5,
        help='Column number used to read "Moment (emu)" values. Default=5.'
        )
    Waveform_parser.add_argument(
        '--field',
        type=float,
        metavar='<value>',
        default=0.,
        help='Central field value used to measure waveform data. Default=0.0.'
        )
    Waveform_parser.add_argument(
        '--data_pointer',
        metavar='<str>',
        type=str,
        default='[Data]',
        help='String used to locate data in <filename(s)>. Default: "[Data]"'
        )
    Waveform_parser.add_argument(
        '--field_window',
        metavar='<float>',
        type=float,
        nargs=2,
        default=[-0.5, 0.5],
        help="""Min & max field values (Oe) used to define a waveform block.
        Default: -0.5 0.5"""
        )
    Waveform_parser.add_argument(
        '--show',
        action='store_true',
        help='Show individual Fourier transformed plots.'
        )
    '''
    Waveform_parser.add_argument(
        '--discard_freq',
        metavar='<int>',
        type=int,
        nargs='*',
        default=[],
        help="""Outlier frequency number(s) to be discarded for each <filename>.
        You will have to inspect waveform plots and then run the program again.
        0 for None, 1 for 1st, 2 for 2nd, etc. Default: None."""
        )
    '''

    # DC mode
    description_DC = """
    Extract relaxation times from magnetisation decays using exponentials and
    (optional) fit the resulting relaxation profile.
    """
    DC_parser = subparsers.add_parser(
        'DC',
        description=description_DC,
        formatter_class=argparse.RawDescriptionHelpFormatter
        )
    DC_parser.add_argument(
        'input_file',
        metavar='<filename>',
        type=str,
        help='''File containing the DC magnetisation decay data.
        See Manual for expected format.'''
        )
    DC_parser.add_argument(
        '--process',
        metavar='<Option>',
        choices=['decays', 'all'],
        default='all',
        help='''What to do: "decays" fits only the magnetisation decays; "all" continues to fit the relaxation profile.
        Options: decays, all.
        Default: all.'''
        )
    DC_parser.add_argument(
        '--model',
        metavar='<Option>',
        type=str,
        default='single',
        choices=['single', 'stretched'],
        help='''Exponential function to fit the data.
        Options: single, stretched.
        Default: single.'''
        )
    DC_parser.add_argument(
        '--M_eq',
        metavar='<Option>',
        type=str,
        default='exp',
        help='''M_eq value to use in the model.
        See Manual for options.
        Default: exp.'''
        )
    DC_parser.add_argument(
        '--guess',
        metavar='<number>',
        nargs='+',
        type=float,
        default=[400.],
        help='''Initial guess used to fit the coldest temperature.
        Number of variables depends on the indicated model.
        Default: Single (tau) -> 400; Stretched (tau, beta) -> 400, 0.95.'''
        )
    DC_parser.add_argument(
        '--discard',
        metavar='<number>',
        type=int,
        default=2,
        help='''Discard initial data points measured under a saturating DC field.
        Default: 2.'''
        )
    DC_parser.add_argument(
        '--hide_plots',
        action='store_true',
        help='''Do not show the individual mag decay plots.'''
        )
    DC_parser.add_argument(
        '--save_plots',
        action='store_true',
        help='''Save the individual mag decay plots.'''
        )
    DC_parser.add_argument(
        '--verbose',
        action='store_true',
        help='Print the read-in values from the file indicated.'
        )

    # Relaxation Profile
    description_RelaxationProfile = """
    Fit the field- and/or temperature-dependence of relaxation times.
    """
    RelaxationProfile_parser = subparsers.add_parser(
        'RelaxationProfile',
        description=description_RelaxationProfile,
        formatter_class=argparse.RawDescriptionHelpFormatter
        )
    RelaxationProfile_parser.add_argument(
        'input_file',
        type=str,
        help='File containing the relaxation times.'
        )
    RelaxationProfile_parser.add_argument(
        '--process',
        metavar='<Option>',
        choices=['tau_vs_T', 'tau_vs_H'],
        default='tau_vs_T',
        help='''Fit either the temperature- or field-dependence of taus (isofield or isothermal). 
        Options: tau_vs_T, tau_vs_H.
        Default: tau_vs_T.'''
        )
    RelaxationProfile_parser.add_argument(
        '--infield',
        action='store_true',
        help='''Enable a Direct term in --process = tau_vs_T.'''
        )
    RelaxationProfile_parser.add_argument(
        '--sigma',
        metavar='<Int>',
        type=int,
        default=1,
        help='''lognorm confidence interval (AC data).
        Default: 1 = 68%%.'''
        )
    '''
    RelaxationProfile_parser.add_argument(
        '--deconvolute', action='store_true',
        help='Extract two relaxation times from taus with a large associated alpha. STILL NOT AVAILABLE!'
        )
    '''
    RelaxationProfile_parser.add_argument(
        '--verbose',
        action='store_true',
        help='Print the read-in values from the file indicated.'
        )

    _args = parser.parse_args()

    # Validate options.
    if _args.exe_mode == 'DC':
        # Validate Meq.
        if _args.M_eq != 'free' and _args.M_eq != 'exp' and _args.M_eq != 'multiple':
            try:    float(_args.M_eq) 
            except: exit('\n***Error***:\nThe indicated M_eq: {} is not valid.\n'.format(_args.M_eq))

        # Validate guess input.
        if _args.guess[0] < 0.0: 
            exit('\n***Error***:\nTau cannot be negative.')
        if _args.model == 'single' and len(_args.guess) > 1: 
            exit('\n***Error***:\nSingle exponential model function only takes one argument (tau) as guess.')
        if _args.model == 'single' and _args.M_eq == 'free' and _args.M_eq != 'multiple':
            _args.guess.append(0.1) # append a dummy guess for Meq
        if _args.model == 'stretched':
            if len(_args.guess) == 1 and _args.M_eq != 'free':
                _args.guess.append(0.95) # append the guess for beta
            if len(_args.guess) == 1 and _args.M_eq == 'free': #  
                _args.guess.append(0.95)
                _args.guess.append(1.0) # append the guess for beta and for Meq
            if len(_args.guess) == 2 and _args.M_eq == 'free': #  
                _args.guess.append(1.0) # append the guess for Meq
            if _args.guess[1] > 1.0: 
                exit('\n***Error***:\nBeta parameter cannot be larger than one.')

    return _args


def check_version():
    try:
        response = requests.get("http://www.nfchilton.com/cc.version.cmd")
        if response.text != "3.1":
            print("\n********************************************************")
            print("New version v"+response.text+" available at www.nfchilton.com/cc-fit")
            print("********************************************************\n")
    except:
        pass
    return


def min_max_ticks_with_zero(axdata, nticks):
    # Calculates tick positions including zero given a specified number of ticks either size of zero

    # Add on extra tick for some reason
    nticks += 1

    lowticks = np.linspace(-np.max(np.abs(axdata)), 0, nticks)
    highticks = np.linspace(np.max(np.abs(axdata)), 0, nticks)
    ticks = np.append(np.append(lowticks[:-1],[0.0]), np.flip(highticks[:-1]))

    return ticks, np.max(np.abs(axdata))


def set_rate_xy_lims(ax, rate, yerr_plt, temperature):

    # Define limits of y axis
    # Upper limit from rounding up to nearest power of 10
    # Lower from rounding down to nearest power of 10
    y_lower = 10**np.floor(np.log10(np.nanmin([rate, rate+yerr_plt[1,:], rate-yerr_plt[0,:]])))
    y_upper = 10**np.ceil(np.log10(np.nanmax([rate, rate+yerr_plt[1,:], rate-yerr_plt[0,:]])))
    
    if np.isnan(y_lower):
        y_lower = y_upper / 10
    if np.isnan(y_upper):
        y_upper = y_lower / 10

    ax.set_ylim([y_lower, y_upper])

    set_rate_x_lims(ax, temperature)


def set_rate_x_lims(ax, temperature):
    # Linspace between experimental temperature bounds
    tmaxmin = [temperature[0],temperature[-1]]
    
    # Define limits of x axis
    upper_oom = np.floor(np.log10(np.nanmax(tmaxmin)))
    lower_oom = np.floor(np.log10(np.nanmin(tmaxmin)))

    oom_rounds = {0:0,1:0,2:1,3:2}

    x_upper = rounding(np.nanmax(tmaxmin), 10**oom_rounds[upper_oom])
    x_lower = rounding(np.nanmin(tmaxmin), 10**oom_rounds[lower_oom])
    x_lower_tick = rounding(np.nanmin(tmaxmin), 10**(oom_rounds[lower_oom]+1))

    # If there is overlap of data with y axes then reround
    if abs(x_lower - np.nanmin(tmaxmin)) < 2.5:
        x_lower = rounding(x_lower, 10**(oom_rounds[lower_oom]+1), 'down')
    if abs(x_upper - np.nanmax(tmaxmin)) < 2.5:
        x_upper = rounding(x_upper, 10**(oom_rounds[upper_oom]), 'up')

    # If there is still overlap then shift and round
    if abs(x_lower - np.nanmin(tmaxmin)) < 2.5:
        x_lower = rounding(x_lower*0.95, 10**(oom_rounds[lower_oom]+1)/2, 'down')
    if abs(x_upper - np.nanmax(tmaxmin)) < 2.5:
        x_upper = rounding(x_upper*0.95, 10**(oom_rounds[upper_oom])/2, 'up')

    # Check for zero limit
    if x_lower == 0.:
        x_lower = rounding(np.nanmin(tmaxmin), 1, 'down')
        if x_lower != 1.:
            x_lower -= 1.
        else:
            x_lower *=0.5
        x_lower_tick = 5.

    # Set limits
    if tmaxmin[1] - tmaxmin[0] < 10.:
        x_lower = tmaxmin[0]-.5
        x_upper = tmaxmin[1]+0.5
    elif tmaxmin[0] < 5.:
        x_lower = tmaxmin[0]-.5
        
    ax.set_xlim([x_lower, x_upper])

    # Set tick formatting
    ax.xaxis.set_major_formatter(ScalarFormatter())
    ax.xaxis.set_minor_formatter(NullFormatter())

    # Disable minor ticks on xaxis 
    ax.xaxis.set_tick_params(which='minor', bottom=False, labelrotation=45.)
    
    # Calculate spacing between max and min temperatures and use this to define
    # spacing between ticks   
    min_steps = {0:1, 50:10, 100:15, 150:25, 200:35, 250:50, 300:60}
    t_range = rounding(rounding(tmaxmin[1]) - rounding(tmaxmin[0]),50)

    # If temperature range is very large then use 100 K spacing
    if t_range > 300 :
        tick_step = 100
    # Else use dictionary above to specify spacing
    else:
        tick_step = min_steps[t_range]

    # Generate tick positions using spacing
    x_tick_vals = np.arange(x_lower_tick, x_upper + tick_step, tick_step)

    # If lower x limit is lower than lowest tick position, add on tick at lower x limit
    if x_lower_tick > x_lower:
        x_tick_vals = np.hstack([x_tick_vals, x_lower])

    # Apply tick positions to axis
    ax.set_xticks(x_tick_vals)

    return


def rounding(x, base=10, direction='up'):
    if direction == 'up':
        return base * np.ceil(x/base)
    if direction == 'down':
        return base * np.floor(x/base)


def create_output_files(args, _field=None, model_AC=None, _output=None, model_RP=None ):

    # Create (i)   folder to store the results,
    #        (ii)  file to write the parameters of the fitting,
    #        (iii) file to write the values of the model function.

    job_name = args.input_file.replace( '.','_', args.input_file.count('.') -1 ).split('.')[0]
    subfolder = 'results_'+job_name+'_'+args.exe_mode
    results_dir = os.path.join( os.getcwd(), subfolder )
    if not os.path.exists(results_dir): # (i)
        os.mkdir(results_dir)

    # Define the headers for the params file of the RelaxationProfile.
    if model_RP is not None:

        # Define the RP function to be used, which defines the structure of the file to which the results will be written.
        if   model_RP == 'Orbach':
            headers = ['log_10[tau_0] (log_10[s])', 'log_10[tau_0]_err (log_10[s])', 'U-eff (K)', 'U-eff_err (K)']

        elif model_RP == 'Raman':
            headers = ['log_10[C] (log_10[s^-1 K^-n])', 'log_10[C]_err (log_10[s^-1 K^-n])', 'n', 'n_err']

        elif model_RP == 'QTM':
            headers = ['log_10[tau_QTM] (log_10[s])', 'log_10[tau_QTM]_err (log_10[s])']

        elif model_RP == 'Orbach + Raman':
            headers = [
                'log_10[tau_0] (log_10[s])', 'log_10[tau_0]_err (log_10[s])', 'U-eff (K)', 'U-eff_err (K)',
                'log_10[C] (log_10[s^-1 K^-n])', 'log_10[C]_err (log_10[s^-1 K^-n])', 'n', 'n_err'
                ]

        elif model_RP == 'Orbach + QTM':
            headers = [
                'log_10[tau_0] (log_10[s])', 'log_10[tau_0]_err (log_10[s])', 'U-eff (K)', 'U-eff_err (K)',
                'log_10[tau_QTM] (log_10[s])', 'log_10[tau_QTM]_err (log_10[s])'
                ]

        elif model_RP == 'Raman + QTM':
            headers = [
                'log_10[C] (log_10[s^-1 K^-n])', 'log_10[C]_err (log_10[s^-1 K^-n])', 'n', 'n_err',
                'log_10[tau_QTM] (log_10[s])', 'log_10[tau_QTM]_err (log_10[s])'
                ]

        elif model_RP == 'Orbach + Raman + QTM':
            headers = [
                'log_10[tau_0] (log_10[s])', 'log_10[tau_0]_err (log_10[s])', 'U-eff (K)', 'U-eff_err (K)',
                'log_10[C] (log_10[s^-1 K^-n])', 'log_10[C]_err (log_10[s^-1 K^-n])', 'n', 'n_err',
                'log_10[tau_QTM] (log_10[s])', 'log_10[tau_QTM]_err (log_10[s])'
                ]

        elif model_RP == 'Direct':
            headers = [
                'log_10[A] (log_10[s^-1 K^-1])', 'log_10[A]_err (log_10[s^-1 K^-1])'
                ]

        elif model_RP == 'Orbach + Direct':
            headers = [
                'log_10[tau_0] (log_10[s])', 'log_10[tau_0]_err (log_10[s])', 'U-eff (K)', 'U-eff_err (K)',
                'log_10[A] (log_10[s^-1 K^-1])', 'log_10[A]_err (log_10[s^-1 K^-1])'
                ]

        elif model_RP == 'Raman + Direct':
            headers = [
                'log_10[C] (log_10[s^-1 K^-n])', 'log_10[C]_err (log_10[s^-1 K^-n])', 'n', 'n_err',
                'log_10[A] (log_10[s^-1 K^-1])', 'log_10[A]_err (log_10[s^-1 K^-1])'
                ]

        elif model_RP == 'Orbach + Raman + Direct':
            headers = [
                'log_10[tau_0] (log_10[s])', 'log_10[tau_0]_err (log_10[s])', 'U-eff (K)', 'U-eff_err (K)',
                'log_10[C] (log_10[s^-1 K^-n])', 'log_10[C]_err (log_10[s^-1 K^-n])', 'n', 'n_err',
                'log_10[A] (log_10[s^-1 K^-1])', 'log_10[A]_err (log_10[s^-1 K^-1])'
                ]

        elif model_RP == 'QTM(H) + Raman-II + Ct' or model_RP == 'QTM(H) + Raman-II + Ct - constrained':
            headers = [
                'log_10[tau_QTM] (log_10[s])', 'log_10[tau_QTM]_err (log_10[s])', 'log_10[tau_QTM(H)] (log_10[(Oe^-p)])', 'log_10[tau_QTM(H)]_err (log_10[s Oe^p])', 'p', 'p_err',
                'log_10[CII] (log_10[s^-1 Oe^-m])', 'log_10[CII]_err (log_10[s^-1 Oe^-m])', 'm', 'm_err',
                'log_10[Ct] (log_10[s^-1])', 'log_10[Ct]_err (log_10[s^-1])'
                ]

        elif model_RP == 'Brons-Van-Vleck & Raman-II':
            headers = [
                'log_10[e] (log_10[Oe^-2])', 'log_10[e]_err (log_10[Oe^-2])', 'log_10[f] (log_10[Oe^-2])', 'log_10[f]_err (log_10[Oe^-2])',
                'log_10[CII] (log_10[s^-1 Oe^-m])', 'log_10[CII]_err (log_10[s^-1 Oe^-m])', 'm', 'm_err',
                ]

        elif model_RP == 'Brons-Van-Vleck & Ct' or model_RP == 'Brons-Van-Vleck & Ct - constrained':
            headers = [
                'log_10[e] (log_10[Oe^-2])', 'log_10[e]_err (log_10[Oe^-2])', 'log_10[f] (log_10[Oe^-2])', 'log_10[f]_err (log_10[Oe^-2])',
                'log_10[Ct] (log_10[s^-1])', 'log_10[Ct]_err (log_10[s^-1])'
                ]



    if args.exe_mode == 'AC':

        if   _output == 'susc':

            _Debye_filename = os.path.join( results_dir, str(job_name+'_{}_{}sigma_{:3.1f}Oe').format(model_AC, str(args.sigma), _field) )

            # Create the file to write the Debye model function parameters and fitted points.
            _Debye_params = open(_Debye_filename+'_params.out', 'w')
            _Debye_fit    = open(_Debye_filename+'_fit.out',    'w')

            # Define the Debye function to be used, which defines the bounds and the structure of the file to which the results will be written.
            if   model_AC == 'Debye':
                headers = [
                    'T (K)', 'tau (s)', 'tau_err (s)', 'chi_S (cm^3mol^-1)', 'chi_S_err (cm^3mol^-1)', 'chi_T (cm^3mol^-1)', 'chi_T_err (cm^3mol^-1)'
                    ]

            elif model_AC == 'Generalised':
                headers = [
                    'T (K)', 'tau (s)', 'tau_ln_ESD-up (s)', 'tau_ln_ESD-lw (s)', 'tau_err (s)', 'chi_S (cm^3mol^-1)', 'chi_S_err (cm^3mol^-1)', 'chi_T (cm^3mol^-1)', 'chi_T_err (cm^3mol^-1)', 'alpha', 'alpha_err'
                    ]

            elif model_AC == 'Bimodal':
                headers = [
                    'T (K)', 'tau1 (s)', 'tau1_err (s)', 'D_chi1 (cm^3mol^-1)', 'D_chi1_err (cm^3mol^-1)', 'alpha1', 'alpha1_err', 'tau2 (s)', 'tau2_err', 'D_chi2 (cm^3mol^-1)', 'D_chi2_err (cm^3mol^-1)', 'alpha2', 'alpha2_err', 'chi_total (cm^3mol^-1)', 'chi_total_err (cm^3mol^-1)'
                    ]

            # Write the headers for params file.
            for i in headers:
                _Debye_params.write(' {} '.format( i ) )
            _Debye_params.write( "\n" )

            return _Debye_filename, _Debye_params, _Debye_fit


        elif _output == 'all':

            _RP_filename = os.path.join( results_dir, str(job_name+'_{}_{:3.1f}Oe').format(model_RP.replace(' ', ''), _field) )

            # Create the file to write the Debye model function parameters and fitted points.
            _RP_params = open(_RP_filename+'_params.out', 'w')
            _RP_fit    = open(_RP_filename+'_fit.out',    'w')


            # Write the headers for params file.
            for i in headers:
                _RP_params.write(' {} '.format( i ) )
            _RP_params.write( "\n" )

            # Write the headers for fit file.
            _RP_fit.write(
                '{:^15} {:^15}'.format('T (K)', 'tau^-1 (s^-1)')+"\n"
                )

            return _RP_filename, _RP_params, _RP_fit
                

    elif args.exe_mode == 'DC':

        if   _output == 'decays':

            _DC_filename = os.path.join( results_dir, job_name+'_{}_Meq-{}_{:3.1f}Oe'.format(args.model, args.M_eq, _field) )

            _DC_params = open( _DC_filename+'_params.out', 'w' )
            _DC_fit = open( _DC_filename+'_fit.out', 'w' )

            if args.model == 'single':

                _DC_params.write('{:^10} {:^10} {:^10} {:^10} {:^10} {:^10}'.format(
                                  'T (K)', 'Field (Oe)', 'M_0', 'M_eq', 'tau (s)', 'tau_err')+'\n')

            elif args.model == 'stretched':

                _DC_params.write('{:^10} {:^10} {:^10} {:^10} {:^10} {:^10} {:^10} {:^10} {:^10} {:^10}'.format(
                                  'T (K)', 'Field (Oe)', 'M_0', 'M_eq', 'tau (s)', 'tau_err', 'beta', 'beta_err', 'tau_ESD_up', 'tau_ESD_lw')+'\n')

            return _DC_filename, _DC_params, _DC_fit

        elif _output == 'all':

            _RP_filename = os.path.join( results_dir, job_name+'_{}_{:3.1f}Oe'.format(model_RP.replace(' ', ''), _field) )

            # Create the file to write the Debye model function parameters and fitted points.
            _RP_params = open(_RP_filename+'_params.out', 'w')
            _RP_fit    = open(_RP_filename+'_fit.out',    'w')
         

            # Write the headers for params file.
            for i in headers:
                _RP_params.write(' {} '.format( i ) )
            _RP_params.write( "\n" )

            # Write the headers for fit file.
            _RP_fit.write(
                '{:^15} {:^15}'.format('T (K)', 'tau^-1 (s^-1)')+"\n"
                )

            return _RP_filename, _RP_params, _RP_fit
        

    else:

        _RP_filename = os.path.join( results_dir, job_name+'_{}'.format(model_RP.replace(' ', '')) )

        # Create the file to write the Debye model function parameters and fitted points.
        _RP_params = open(_RP_filename+'_params.out', 'w')
        _RP_fit    = open(_RP_filename+'_fit.out',    'w')


        # Write the headers for params file.
        for i in headers:
            _RP_params.write(' {} '.format( i ) )
        _RP_params.write( "\n" )

        # Write the headers for fit file.
        if   args.process == 'tau_vs_T': _RP_fit.write( '{:^15} {:^15}'.format('T (K)',  'tau^-1 (s^-1)')+"\n" )
        elif args.process == 'tau_vs_H': _RP_fit.write( '{:^15} {:^15}'.format('H (Oe)', 'tau^-1 (s^-1)')+"\n" )

        return _RP_filename, _RP_params, _RP_fit


# ------------------------- END OF FUNCTIONS  -------------------------
if __name__ == '__main__':

    # Check the user version
    check_version()

    # Parse the input and store it in args
    args = parse_input()

    # Treat the AC data according to the indicated --process.
    if args.exe_mode == 'AC':

        # Initialise Process_AC
        process_AC = Process_AC()

        # Read-in the data and order it by field and temperature.
        RawDatalist, exp_real, exp_im, wave_freq, range_T, DC_field = process_AC.get_data(args.input_file)

        # Loop over all the measured fields.
        for it_field, field in enumerate(DC_field):

            # Plot only.
            if args.process == 'plot' and not args.select_T:
                process_AC.only_plot(
                    exp_real[it_field],
                    exp_im[it_field],
                    wave_freq[it_field],
                    [(it, val) for it, val in enumerate(range_T[it_field])],
                    field
                    )
                continue

            print('\nField: {:6.2f} Oe\n\n    Part I: Fit the susceptibility components using Debye theory.'.format(field))

            # Define fitting function.
            model_function, param, selected_T = process_AC.select_and_estimate(
                exp_real[it_field], exp_im[it_field], wave_freq[it_field], range_T[it_field], field
                )

            # Define the output files.
            Debye_filename, Debye_params_result, Debye_fit = create_output_files(
                args, _field=field, model_AC=model_function, _output='susc'
                )
    
            # Fit the data.
            fitted_data = process_AC.Debye_model(
                exp_real[it_field], exp_im[it_field], wave_freq[it_field], field, model_function, param, selected_T
                )

            # Show temperature dependence of susceptibilities if indicated.
            if args.susc_vs_T:

                process_AC.susc_vs_T(
                    RawDatalist, field, fitted_data, Debye_filename
                    )

            # Proceed to fit the relaxation profile after having finished fitting the AC data.
            if args.process == 'all':

                print('\n\n    Part II: Fit the relaxation rates\' temperature dependence.\n')
                if len(fitted_data[field]) <= 2:
                    print('    ***Warning***:\n    Not enough data points to fit the relaxation profile.')
                    continue

                if model_function == 'Bimodal':
                    print('\n    ***Warning***:\n    Two distinct relaxation processes detected in: {}.\n    Prepare two separate files for each process and then run RelaxationProfile --AC Generalised on them.'.format(Debye_filename+'_params.out'))
                    continue
                    # Possible solution: make read_rates() return each temperature and tau as a tuple, and then loop over them.
                    # Ex: temperature, tau = ( [T1, T2, ...], [T1, T2, ...] ), ([tau1, tau2, ...],[tau1, tau2, ...])
                    # and then loop over len(temperature). This would be valid for all debye, generalised bimodal models.
                    # create_output_file should have an identifier for each set

                # Instantiate the Relaxation_Profile class.
                x = Relaxation_profile()

                # Read-in the rates.
                temperature, tau = x.read_rates(
                    Debye_filename+'_params.out', args.exe_mode, AC_model=model_function
                    )

                # Select the model function to be used.
                # Set field to zero so Direct is never activated in AC module.
                x.select_model_rates(
                    temperature, 1./tau, 0, _process='tau_vs_T'
                    )

    # Treat the DC data according to the indicated --process.
    elif args.exe_mode == 'DC':

        # Initialise Process_DC.
        process_DC = Process_DC( args )

        # Read the input file.
        data = process_DC.read_input(args.input_file, args.discard)

        # loop over the different fields.
        for field, decays in data.items():

            print('\nField: {:6.2f} Oe\n\n    Part I: Fit the magnetisation decays.'.format(field))

            # Define Meq in case --M_eq Multiple is indicated.
            if args.M_eq == 'multiple':
                if not os.path.isfile('Multiple_Meq_{}Oe.txt'.format(int(field))):
                    exit('\n    ***Error***:\n    --M_eq multiple has been indicated but Multiple_Meq_{}Oe.txt file is not present.'.format(int(field)))
                else:
                    meq = np.loadtxt('Multiple_Meq_{}Oe.txt'.format(int(field)), skiprows=1)

            # Define the output files.
            DC_filename, DC_params, DC_fit = create_output_files( 
                args, _field=field, _output='decays' 
                )

            # Define the model function to use.
            process_DC.define_DC_model( args )

            # Fit, plot and save the data.
            process_DC.fit_decays( field, decays, DC_filename, DC_params, DC_fit )


            # Proceed to fit the relaxation profile after having finished fitting the DC data.
            if args.process == 'all':

                print('\n\n    Part II: Fit the relaxation rates\' temperature dependence.\n')
                if len(decays[0]) <= 2:
                    print('    ***Warning***:\n    Not enough data points to fit the relaxation profile.')
                    continue

                # Instantiate the Relaxation_Profile class.
                DC_RP = Relaxation_profile()

                # Read-in the rates.
                temperature, tau = DC_RP.read_rates(
                    DC_filename+'_params.out', args.exe_mode, DC_model=args.model
                    )

                # Select the model function to be used. 
                DC_RP.select_model_rates(
                    temperature, 1./tau, field, _process='tau_vs_T'
                    )

    # Treat the data according to the indicated --process.
    if args.exe_mode == 'Waveform':

        # Multiple constructors implemented with call().

        # Define an empty list to store the AC susceptibilities.
        suscep = []

        # Initialise Process_Waveform
        Waveform = Process_Waveform(args)

        # Loop over the provided data files.
        for filename in Waveform.file_names:

            # Call the initialised class with the appropriate input.
            suscep.append(
                Waveform(filename)
                )

        # Flatten the list.
        final = Waveform.flatten_and_sort(suscep)

        # Show the final susceptibilities.
        Waveform.plot_susceptibilies(final)

        # Write the input file to CCFIT
        Waveform.prepare_ccfit_AC_input(Waveform.applied_field, final)

        '''
        # Multiple constructors implemented with @singledispatchmethod decorator.
        # Initialise Process_Waveform
        Waveform = Process_Waveform(args)

        # args.dat_file is always a list. I need to figure out how to emulate 
        # passing args.dat_file[0] (str) when a single file.
        Left here. It all works, just need to sort out the data type thingy
        Not added to git.

        susc = Waveform.generic_method(args.dat_file)

        # Sort the list of tuples.
        final = Waveform.sort_results(susc)

        # Show the final susceptibilities.
        Waveform.plot_susceptibilies(final)

        # Write the input file to CCFIT
        Waveform.prepare_ccfit_AC_input(Waveform.applied_field, final)
        '''

    # Treat the data according to the indicated --process.
    elif args.exe_mode == 'RelaxationProfile':

        if   args.process == 'tau_vs_T': print('\n\n    Fit the relaxation rates\' temperature dependence.\n')
        elif args.process == 'tau_vs_H': print('\n\n    Fit the relaxation rates\' field dependence.\n')

        # Instantiate the Relaxation_Profile class.
        RP = Relaxation_profile()

        # Read-in the rates.
        xvals, tau = RP.read_rates(
            args.input_file, args.exe_mode
            )

        # Select the model function to be used. 
        RP.select_model_rates(
            xvals, 1./tau, args.infield, _process=args.process
            )


# THINGS TO DO:
# 1. When fitting the RelaxationProfile, if the field is != 0, add the possibility of a direct term. DONE.
# 2. Add deconvolute option.
# 3. In RelaxationProfile, add the option to fit rates vs field, rather than rates vs T. DONE.
# 4. in Cole-Cole and susc_comp plots, the bar plot is linear. If T = [1, 2, 3, 20, 50] it gets the colors
#    wrong. DONE.
# 5. when --select_T, the window freezes. Ask for data and then close the plot manually?
# 6. Define the colors of the different processes and match the lines shown with the corresponding sliders.
# 7. When --M_eq multiple, meq is passed to the class as a global variable: UGLY!


# https://mail.scipy.org/pipermail/scipy-user/2013-April/034406.html
# https://stackoverflow.com/questions/30779712/show-matplotlib-colorbar-instead-of-legend-for-multiple-plots-with-gradually-cha 
# https://stackoverflow.com/questions/12208634/fitting-only-one-parameter-of-a-function-with-many-parameters-in-python
# https://stackoverflow.com/questions/14854339/in-scipy-how-and-why-does-curve-fit-calculate-the-covariance-of-the-parameter-es
# https://www.youtube.com/watch?v=Jl-Ye38qkRc Curve_fit in python
# https://stackoverflow.com/questions/6618515/sorting-list-based-on-values-from-another-list
# https://micropore.wordpress.com/2017/02/04/python-fit-with-error-on-y-axis/
# https://stackoverflow.com/questions/42388139/how-to-compute-standard-deviation-errors-with-scipy-optimize-least-squares
# https://mathematica.stackexchange.com/questions/33685/how-does-mathematica-estimate-error-on-fit-parameters
