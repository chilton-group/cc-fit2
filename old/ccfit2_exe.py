#!/usr/bin/env python3

# This is the ccfit2 ac wrapper used by the CC-FIT2.exe executable

from ccfit2 import gui, utils, cli

if __name__ == "__main__":

    # Object to store user configuration
    user_cfg = utils.UserConfig()

    # Filename window
    gui.filename_entry(user_cfg)

    # Mass and MW window
    gui.mass_mw_entry(user_cfg)

    # Call ccfit2 cli parser for ac mode
    cli.read_args(
        [
            "ac",
            "{}".format(user_cfg.file_name),
            "{:f}".format(user_cfg.mass),
            "{:f}".format(user_cfg.mw),
            "--ccfit2_exe"
        ]
    )
