import ccfit2.waveform as wfrm
import ccfit2.ac as ac
import pytest
import io
import os


def test_headers(generic, supported):
    '''
    Tests supported and generic headers

    '''

    assert isinstance(supported, dict)
    assert isinstance(generic, list)

    for key, vals in supported.items():
        assert isinstance(key, str)
        assert key in generic

        assert isinstance(vals, list)

        for val in vals:
            assert isinstance(val, str)


# test headers
pytest.mark.parametrize(
    'generic, supported',
    [
        [wfrm.HEADERS_GENERIC, wfrm.HEADERS_SUPPORTED],
    ]
)(test_headers)

# Get path of website HEAD directory
HEAD = os.path.abspath(os.path.dirname(__file__)) + '/'


class TestFT:

    def test_save_model(self):

        files = [
            HEAD + 'data/waveform/test_set/Dy_example_waveform_0p32mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_0p56mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_1mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_1p8mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_2p4mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_3p2mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_4p2mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_5p5mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_7p5mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_10mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_12mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_16mHz.dat',
            HEAD + 'data/waveform/test_set/Dy_example_waveform_21mHz.dat'
        ]

        all_experiments = wfrm.Experiment.from_files(files)

        ac_experiments = []

        # Fourier transform each temperature's set of experiments
        # and create corresponding AC Experiment to store AC data
        for experiments in all_experiments:
            # Carry out Fourier transform of moment and field
            ft_experiments = [
                wfrm.FTExperiment.from_experiment(experiment)
                for experiment in experiments
            ]

            # Create ac.Experiment object from Fourier transform results
            ac_experiments.append(
                wfrm.FTExperiment.create_ac_experiment(
                    ft_experiments, experiments, mass=None, mw=None
                )
            )

        fio = io.StringIO()

        ac.save_ac_magnetometer_file(
            ac_experiments,
            file_name=fio,
            verbose=False
        )
        return
