import ccfit2.ac as ac
import numpy as np
import numpy.testing as npte
import pytest
import io
import copy
import os


# Get current working directory
HEAD = os.path.abspath(os.path.dirname(__file__)) + '/'


def test_headers(generic, supported):
    '''
    Tests supported and generic headers

    '''

    assert isinstance(supported, dict)
    assert isinstance(generic, list)

    for key, vals in supported.items():
        assert isinstance(key, str)
        assert key in generic

        assert isinstance(vals, list)

        for val in vals:
            assert isinstance(val, str)


# test headers
pytest.mark.parametrize(
    'generic, supported',
    [
        [ac.HEADERS_GENERIC, ac.HEADERS_SUPPORTED],
        [ac.ERROR_HEADERS_GENERIC, ac.ERROR_HEADERS_SUPPORTED]
    ]
)(test_headers)


class TestMeasurement:
    '''
    Tests for ac.Measurement class
    '''

    def test_initialization(self):
        measurement = ac.Measurement(100.0, 300.0, 1.0, 2.0, 1000.0, 500.0)
        assert measurement.dc_field == 100.0
        assert measurement.temperature == 300.0
        assert measurement.real_sus == 1.0
        assert measurement.imag_sus == 2.0
        assert measurement.ac_freq == 1000.0
        assert measurement.ac_field == 500.0

    def test_rep_temperature_property(self):
        # Test the rep_temperature property and setter
        measurement = ac.Measurement(100.0, 300.0, 1.0, 2.0, 1000.0, 500.0)
        measurement.rep_temperature = 400.0
        assert measurement.rep_temperature == 400.0

    def test_rep_dc_field_property(self):
        # Test the rep_dc_field property and setter
        measurement = ac.Measurement(100.0, 300.0, 1.0, 2.0, 1000.0, 500.0)
        measurement.rep_dc_field = 200.0
        assert measurement.rep_dc_field == 200.0

    def test_condition_susc_method(self):
        # Test the condition_susc method
        valid_sus = "1.0"
        invalid_sus = "-1.0"
        nan_sus = "invalid_value"

        assert ac.Measurement.condition_susc(valid_sus) == 1.0
        assert np.isnan(ac.Measurement.condition_susc(invalid_sus))
        assert np.isnan(ac.Measurement.condition_susc(nan_sus))


@pytest.fixture
def mini_experiment():
    mini = ac.Experiment(
        10, [10], [10], [10], [10], 10, [10], [10]
    )
    return mini


@pytest.fixture
def working_exp_1field():

    experiments = ac.Experiment.from_file(
        HEAD + 'data/ac/standard.ac.dat', 50, 2000
    )

    return experiments


@pytest.fixture
def working_exp_2field():
    # Two fields, 7 and 11 temperatures
    experiments = ac.Experiment.from_file(
        HEAD + 'data/ac/two_fields.ac.dat', 50, 2000
    )

    return experiments


class TestExperiment:
    '''
    Tests for ac.Experiment class
    '''

    def test_initialization(self):
        temp_list = [10, 20, 30]
        re_susc_list = [1., 2., 3.]
        im_susc_list = [3., 4., 5.]
        ac_freq_list = [100., 200., 300.]
        dc_field_list = [0., 0., 0.]
        ac_field_list = [1., 1., 1.]

        # List input
        experiment = ac.Experiment(
            rep_temperature=np.mean(temp_list),
            raw_temperatures=temp_list,
            real_sus=re_susc_list,
            imag_sus=im_susc_list,
            ac_freqs=ac_freq_list,
            rep_dc_field=np.mean(dc_field_list),
            dc_fields=dc_field_list,
            ac_fields=ac_field_list
        )

        temp_arr = np.asarray(temp_list)
        re_susc_arr = np.asarray(re_susc_list)
        im_susc_arr = np.asarray(im_susc_list)
        ac_freq_arr = np.asarray(ac_freq_list)
        dc_field_arr = np.asarray(dc_field_list)
        ac_field_arr = np.asarray(ac_field_list)

        npte.assert_array_equal(experiment.dc_fields, dc_field_arr)
        npte.assert_array_equal(experiment.raw_temperatures, temp_arr)
        npte.assert_array_equal(experiment.real_sus, re_susc_arr)
        npte.assert_array_equal(experiment.imag_sus, im_susc_arr)
        npte.assert_array_equal(experiment.ac_freqs, ac_freq_arr)
        npte.assert_array_equal(experiment.ac_fields, ac_field_arr)
        assert experiment.rep_dc_field == np.mean(dc_field_list)
        assert experiment.rep_temperature == np.mean(temp_list)

        # Array input
        experiment = ac.Experiment(
            rep_temperature=np.mean(temp_arr),
            raw_temperatures=temp_arr,
            real_sus=re_susc_arr,
            imag_sus=im_susc_arr,
            ac_freqs=ac_freq_arr,
            rep_dc_field=np.mean(dc_field_arr),
            dc_fields=dc_field_arr,
            ac_fields=ac_field_arr
        )
        npte.assert_array_equal(experiment.dc_fields, dc_field_arr)
        npte.assert_array_equal(experiment.raw_temperatures, temp_arr)
        npte.assert_array_equal(experiment.real_sus, re_susc_arr)
        npte.assert_array_equal(experiment.imag_sus, im_susc_arr)
        npte.assert_array_equal(experiment.ac_freqs, ac_freq_arr)
        npte.assert_array_equal(experiment.ac_fields, ac_field_arr)
        assert experiment.rep_dc_field == np.mean(dc_field_arr)
        assert experiment.rep_temperature == np.mean(temp_arr)

    def test_file_load(self, working_exp_1field, working_exp_2field):
        '''
        Load files
        and check number of fields and number of temperatures

        This tests a lot of stuff, since from_file calls
        measurement.from_file and experiment.from_measurements
        '''

        # Standard, 1 Field, 19 Temperatures
        assert len(working_exp_1field) == 1
        assert len(working_exp_1field[0]) == 19

        # 2 fields, 7 temperatures and 11 temperatures
        assert len(working_exp_2field) == 2
        assert len(working_exp_2field[0]) == 7
        assert len(working_exp_2field[1]) == 11

        # Broken file
        with pytest.raises(ValueError):
            ac.Experiment.from_file(
                HEAD + 'data/ac/wrong_headers.ac.dat', 50, 2000
            )

    def test_save_ac_file(self, mini_experiment):

        fio = io.StringIO()

        # Single experiment
        ac.save_ac_magnetometer_file(mini_experiment, file_name=fio)

        experiments = [mini_experiment, mini_experiment]

        # List of experiments
        ac.save_ac_magnetometer_file(experiments, file_name=fio)

        return


class TestSingleModels:

    @pytest.mark.parametrize(
        'model',
        [
            ac.DebyeModel,
            ac.GeneralisedDebyeModel,
            ac.HavriliakNegamiModel,
            ac.ColeDavidsonModel
        ]
    )
    def test_init(self, model: ac.Model, mini_experiment):

        # all floats
        model(
            fit_vars={
                var: 0.
                for var in model.PARNAMES
            },
            fix_vars={},
            experiment=mini_experiment
        )

        # all guesses
        model(
            fit_vars={
                var: 'guess'
                for var in model.PARNAMES
            },
            fix_vars={},
            experiment=mini_experiment
        )

        # catch duplicates
        with pytest.raises(ValueError):
            model(
                fit_vars={
                    var: 'guess'
                    for var in model.PARNAMES
                },
                fix_vars={
                    var: 'guess'
                    for var in model.PARNAMES
                },
                experiment=mini_experiment
            )

    def test_flat(self):

        x = [1, 2, 3, 4, 5]
        y = [1, 2, 3, 4, 5]

        assert ac.Model.flat(x, y, 0.0001)
        assert ac.Model.flat(np.asarray(x), np.asarray(y), 0.0001)

        y = np.asarray(x)**2

        assert not ac.Model.flat(x, y, 0.0001)

    @pytest.mark.parametrize(
        'model_class',
        [
            ac.DebyeModel,
            ac.GeneralisedDebyeModel,
            ac.HavriliakNegamiModel,
            ac.ColeDavidsonModel
        ]
    )
    def test_save_model(self, model_class: ac.Model):

        # Standard, 1 Field, 19 Temperatures
        experiments = ac.Experiment.from_file(
            HEAD + 'data/ac/standard.ac.dat', 50, 2000
        )[0]

        models = [
            copy.copy(model_class(
                fit_vars={
                    var: 'guess'
                    for var in model_class.PARNAMES
                },
                fix_vars={},
                experiment=experiment
            ))
            for experiment in experiments
        ]

        prev_fit = models[0].fit_vars
        for mdl, experiment in zip(models, experiments):

            for key in mdl.fit_vars.keys():
                mdl.fit_vars[key] = prev_fit[key]

            mdl.fit_to(
                experiment
            )

            # Use these fitted params as next guess, if fit successful
            if mdl.fit_status:
                prev_fit = mdl.final_var_values
            # Else use this model's guess as next guess
            else:
                prev_fit = mdl.fit_vars

        fio = io.StringIO()
        ac.write_model_data(experiments, models, fio)
        ac.write_model_params(models, fio)


class TestMultiModels:

    @pytest.mark.parametrize(
        'model',
        [
            ac.DoubleGDebyeEqualChiModel,
            ac.DoubleGDebyeModel,
        ]
    )
    def test_init(self, model: ac.Model):

        # all floats
        model(
            fit_vars={
                var: 0.
                for var in model.PARNAMES
            },
            fix_vars={},
            experiment=ac.Experiment(
                10,
                10,
                [10, 20, 30, 40],
                [10, 20, 30, 20],
                [10, 20, 30, 40],
                [10, 20, 30, 40],
                [10, 20, 30, 40],
                [10, 20, 30, 40]
            )
        )

        # all guesses
        model(
            fit_vars={
                var: 'guess'
                for var in model.PARNAMES
            },
            fix_vars={},
            experiment=ac.Experiment(
                10,
                10,
                [10, 20, 30, 40],
                [10, 20, 30, 20],
                [10, 20, 30, 40],
                [10, 20, 30, 40],
                [10, 20, 30, 40],
                [10, 20, 30, 40]
            )
        )

        # catch duplicates
        with pytest.raises(ValueError):
            model(
                fit_vars={
                    var: 'guess'
                    for var in model.PARNAMES
                },
                fix_vars={
                    var: 'guess'
                    for var in model.PARNAMES
                },
                experiment=ac.Experiment(
                    10,
                    10,
                    [10, 20, 30, 40],
                    [10, 20, 30, 20],
                    [10, 20, 30, 40],
                    [10, 20, 30, 40],
                    [10, 20, 30, 40],
                    [10, 20, 30, 40]
                )
            )

    @pytest.mark.parametrize(
        'model_class',
        [
            ac.DoubleGDebyeEqualChiModel,
            ac.DoubleGDebyeModel
        ]
    )
    def test_save_model(self, model_class):

        # Standard, 1 Field, 19 Temperatures
        experiments = ac.Experiment.from_file(
            HEAD + 'data/ac/two_maxima.ac.dat', 50, 2000
        )[0]

        models = [
            copy.copy(model_class(
                fit_vars={
                    var: 'guess'
                    for var in model_class.PARNAMES
                },
                fix_vars={},
                experiment=experiment
            ))
            for experiment in experiments
        ]

        prev_fit = models[0].fit_vars
        for mdl, experiment in zip(models, experiments):

            for key in mdl.fit_vars.keys():
                mdl.fit_vars[key] = prev_fit[key]

            mdl.fit_to(
                experiment
            )

            # Use these fitted params as next guess, if fit successful
            if mdl.fit_status:
                prev_fit = mdl.final_var_values
            # Else use this model's guess as next guess
            else:
                prev_fit = mdl.fit_vars

        fio = io.StringIO()
        ac.write_model_data(experiments, models, fio)
        ac.write_model_params(models, fio)

        return
