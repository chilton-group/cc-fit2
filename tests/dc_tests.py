import ccfit2.dc as dc
import numpy as np
import numpy.testing as npte
import pytest
import io
import copy
import os


def test_headers(generic, supported):
    '''
    Tests supported and generic headers

    '''

    assert isinstance(supported, dict)
    assert isinstance(generic, list)

    for key, vals in supported.items():
        assert isinstance(key, str)
        assert key in generic

        assert isinstance(vals, list)

        for val in vals:
            assert isinstance(val, str)


# test headers
pytest.mark.parametrize(
    'generic, supported',
    [
        [dc.HEADERS_GENERIC, dc.HEADERS_SUPPORTED],
    ]
)(test_headers)

# Get path of website HEAD directory
HEAD = os.path.abspath(os.path.dirname(__file__)) + '/'


class TestMeasurement:
    '''
    Tests for dc.Measurement class
    '''

    def test_initialization(self):
        measurement = dc.Measurement(100.0, 300.0, 500.0, 1.0)
        assert measurement.dc_field == 100.0
        assert measurement.temperature == 300.0
        assert measurement.time == 500.0
        assert measurement.moment == 1.0

    def test_rep_temperature_property(self):
        # Test the rep_temperature property and setter
        measurement = dc.Measurement(100.0, 300.0, 500.0, 1.0)
        measurement.rep_temperature = 400.0
        assert measurement.rep_temperature == 400.0

    def test_rep_dc_field_property(self):
        # Test the rep_dc_field property and setter
        measurement = dc.Measurement(100.0, 300.0, 500.0, 1.0)
        measurement.rep_dc_field = 200.0
        assert measurement.rep_dc_field == 200.0


class TestExperiment:
    '''
    Tests for dc.Experiment class
    '''

    def test_initialization(self):
        temp_list = [10, 20, 30]
        dc_field_list = [100., 200., 300.]
        time_list = [0., 0., 0.]
        moment_list = [1., 1., 1.]

        # List input
        experiment = dc.Experiment(
            rep_temperature=np.mean(temp_list),
            raw_temperatures=temp_list,
            dc_fields=dc_field_list,
            rep_dc_field=np.mean(dc_field_list),
            times=time_list,
            moments=moment_list
        )

        temp_arr = np.asarray(temp_list)
        dc_field_arr = np.asarray(dc_field_list)
        time_arr = np.asarray(time_list)
        moment_arr = np.asarray(moment_list)

        npte.assert_array_equal(experiment.dc_fields, dc_field_arr)
        npte.assert_array_equal(experiment.raw_temperatures, temp_arr)
        npte.assert_array_equal(experiment.times, time_arr)
        npte.assert_array_equal(experiment.moments, moment_arr)
        npte.assert_array_equal(experiment.dc_fields, dc_field_arr)
        assert experiment.rep_dc_field == np.mean(dc_field_list)
        assert experiment.rep_temperature == np.mean(temp_list)

        # Array input
        experiment = dc.Experiment(
            rep_temperature=np.mean(temp_arr),
            raw_temperatures=temp_arr,
            dc_fields=dc_field_arr,
            rep_dc_field=np.mean(dc_field_arr),
            times=time_arr,
            moments=moment_arr
        )
        npte.assert_array_equal(experiment.dc_fields, dc_field_arr)
        npte.assert_array_equal(experiment.raw_temperatures, temp_arr)
        npte.assert_array_equal(experiment.times, time_arr)
        npte.assert_array_equal(experiment.moments, moment_arr)
        npte.assert_array_equal(experiment.dc_fields, dc_field_arr)
        assert experiment.rep_dc_field == np.mean(dc_field_arr)
        assert experiment.rep_temperature == np.mean(temp_arr)

    def test_file_load(self):
        '''
        Load files
        and check number of fields and number of temperatures

        This tests a lot of stuff, since from_file calls
        measurement.from_file and experiment.from_measurements
        '''

        # VSM standard set
        # 1 field, 9 temperatures
        # Load measurements

        files = [
            HEAD + 'data/dc/vsm_zf/VSM_2K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_3K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_4K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_6K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_9K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_13K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_16K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_20K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_23K_0Oe.dat'
        ]

        all_measurements = [
            dc.Measurement.from_file(file)
            for file in files
        ]

        _all_measurements = []

        # Trim measurements to ignore values at saturating and
        # decaying field
        for measurements in all_measurements:
            diff_thresh = 0.5
            # Forward difference of field
            field_diff = np.abs(
                np.diff(
                    [measurement.dc_field for measurement in measurements]
                )
            )
            # Flip field diff and find first value > threshold, then index to
            # unflipped
            n_pts = len(measurements)
            cut_at = n_pts - np.argmax(np.flip(field_diff) > diff_thresh) - 1

            # Cut to stable field values only
            measurements = measurements[cut_at:]
            _all_measurements += measurements

        # Replace all measurements with trimmed measurements
        all_measurements = _all_measurements

        experiments = dc.Experiment.from_measurements(all_measurements)

        assert len(experiments) == 1
        assert len(experiments[0]) == 9


class TestSingleModels:

    @pytest.mark.parametrize(
        'model',
        [
            dc.ExponentialModel
        ]
    )
    def test_init(self, model: dc.Model):

        # all floats
        model(
            fit_vars={
                var: 0.
                for var in model.PARNAMES
            },
            fix_vars={},
            experiment=dc.Experiment(
                10, 10, [10], [10], [10], [10]
            )
        )

        # all guesses
        model(
            fit_vars={
                var: 'guess'
                for var in model.PARNAMES
            },
            fix_vars={},
            experiment=dc.Experiment(
                10, 10, [10], [10], [10], [10]
            )
        )

        # catch duplicates
        with pytest.raises(ValueError):
            model(
                fit_vars={
                    var: 'guess'
                    for var in model.PARNAMES
                },
                fix_vars={
                    var: 'guess'
                    for var in model.PARNAMES
                },
                experiment=dc.Experiment(
                    10, 10, [10], [10], [10], [10]
                )
            )

    def test_save_model(self):

        files = [
            HEAD + 'data/dc/vsm_zf/VSM_2K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_3K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_4K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_6K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_9K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_13K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_16K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_20K_0Oe.dat',
            HEAD + 'data/dc/vsm_zf/VSM_23K_0Oe.dat'
        ]

        all_measurements = [
            dc.Measurement.from_file(file)
            for file in files
        ]

        _all_measurements = []

        # Trim measurements to ignore values at saturating and
        # decaying field
        for measurements in all_measurements:
            diff_thresh = 0.5
            # Forward difference of field
            field_diff = np.abs(
                np.diff(
                    [measurement.dc_field for measurement in measurements]
                )
            )
            # Flip field diff and find first value > threshold, then index to
            # unflipped
            n_pts = len(measurements)
            cut_at = n_pts - np.argmax(np.flip(field_diff) > diff_thresh) - 1

            # Cut to stable field values only
            measurements = measurements[cut_at:]
            _all_measurements += measurements

        # Replace all measurements with trimmed measurements
        all_measurements = _all_measurements

        experiments = dc.Experiment.from_measurements(all_measurements)[0]

        models = [
            copy.copy(dc.ExponentialModel(
                fit_vars={
                    'tau*': 100.,
                    'beta': 0.95
                },
                fix_vars={
                    't_offset': 0.,
                    'm_0': 'guess',
                    'm_eq': 0.
                },
                experiment=experiment
            ))
            for experiment in experiments
        ]

        for mdl, experiment in zip(models, experiments):
            mdl.fit_to(
                experiment
            )

        fio = io.StringIO()
        dc.write_model_data(experiments, models, fio)
        dc.write_model_params(models, fio)

        return


class TestMultiModels:

    @pytest.mark.parametrize(
        'model',
        [
            dc.DoubleExponentialModel
        ]
    )
    def test_init(self, model: dc.Model):

        # all floats
        model(
            fit_vars={
                var: 0.
                for var in model.PARNAMES
            },
            fix_vars={},
            experiment=dc.Experiment(
                10, 10, [10], [10], [10], [10]
            )
        )

        # all guesses
        model(
            fit_vars={
                var: 'guess'
                for var in model.PARNAMES
            },
            fix_vars={},
            experiment=dc.Experiment(
                10, 10, [10], [10], [10], [10]
            )
        )

        # catch duplicates
        with pytest.raises(ValueError):
            model(
                fit_vars={
                    var: 'guess'
                    for var in model.PARNAMES
                },
                fix_vars={
                    var: 'guess'
                    for var in model.PARNAMES
                },
                experiment=dc.Experiment(
                    10, 10, [10], [10], [10], [10]
                )
            )

    def test_save_model(self):

        files = [
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_700Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_800Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_82Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_600Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_68Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_450Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_500Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_50Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_300Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_32Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_350Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_39Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_400Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_120Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_150Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_173Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_200Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_224Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_250Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_1000Oe.dat',
            HEAD + 'data/dc/dc_isothermal_infield/dc_magdec_4K_100Oe.dat'
        ]

        all_measurements = [
            dc.Measurement.from_file(file)
            for file in files
        ]

        _all_measurements = []

        # Trim measurements to ignore values at saturating and
        # decaying field
        for measurements in all_measurements:
            diff_thresh = 0.5
            # Forward difference of field
            field_diff = np.abs(
                np.diff(
                    [measurement.dc_field for measurement in measurements]
                )
            )
            # Flip field diff and find first value > threshold, then index to
            # unflipped
            n_pts = len(measurements)
            cut_at = n_pts - np.argmax(np.flip(field_diff) > diff_thresh) - 1

            # Cut to stable field values only
            measurements = measurements[cut_at:]
            _all_measurements += measurements

        # Replace all measurements with trimmed measurements
        all_measurements = _all_measurements

        experiments = dc.Experiment.from_measurements(all_measurements)[0]

        models = [
            copy.copy(dc.DoubleExponentialModel(
                fit_vars={
                    'tau*1': 100.,
                    'tau*2': 2000.,
                    'beta1': 0.95,
                    'beta2': 0.95,
                    'frac': 0.5
                },
                fix_vars={
                    't_offset': 0.,
                    'm_0': 'guess',
                    'm_eq': 0.
                },
                experiment=experiment
            ))
            for experiment in experiments
        ]

        for mdl, experiment in zip(models, experiments):
            mdl.fit_to(
                experiment
            )

        fio = io.StringIO()
        dc.write_model_data(experiments, models, fio)
        dc.write_model_params(models, fio)

        return
