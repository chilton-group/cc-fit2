import ccfit2.relaxation as rx
import numpy.testing as npte
import io
import os

# Get path of website HEAD directory
HEAD = os.path.abspath(os.path.dirname(__file__)) + '/'


class TestTDataset:
    '''
    Tests for TDataset Class
    '''

    def test_initialization(self):

        # Without bounds
        rates = [10, 20, 30, 40, 50]
        temperatures = [100, 110, 120, 130, 140]
        dataset = rx.TDataset(rates, temperatures)

        npte.assert_array_equal(dataset.rates, rates)
        npte.assert_array_equal(dataset.temperatures, temperatures)

    def test_file_load(self):
        '''
        Load files and checks number of datapoints
        '''

        dataset = rx.TDataset.from_ccfit2_csv(
            [
                HEAD + 'data/relaxation/ac_generalised_debye_params.csv',
                HEAD + 'data/relaxation/dc_exponential_params.csv'
            ]
        )

        assert len(dataset.rates) == 26

        return

    def test_fit(self):
        '''
        Fits test data
        '''

        dataset = rx.TDataset.from_ccfit2_csv(
            [
                HEAD + 'data/relaxation/ac_generalised_debye_params.csv',
                HEAD + 'data/relaxation/dc_exponential_params.csv'
            ]
        )

        multilogmodel = rx.MultiLogTauTModel(
            [
                rx.LogOrbachModel,
                rx.LogRamanModel,
                rx.LogQTMModel
            ],
            fit_vars=[
                {'R': 1., 'n': 5},
                {'Q': 0.01},
                {'u_eff': 1000, 'A': 2},
            ],
            fix_vars=[{}, {}, {}]
        )

        multilogmodel.fit_to(dataset, verbose=False)

        assert multilogmodel.fit_status

        return

    def test_save_model(self):

        dataset = rx.TDataset.from_ccfit2_csv(
            [
                HEAD + 'data/relaxation/ac_generalised_debye_params.csv',
                HEAD + 'data/relaxation/dc_exponential_params.csv'
            ]
        )

        multilogmodel = rx.MultiLogTauTModel(
            [
                rx.LogOrbachModel,
                rx.LogRamanModel,
                rx.LogQTMModel
            ],
            fit_vars=[
                {'R': 1., 'n': 5},
                {'Q': 0.01},
                {'u_eff': 1000, 'A': 2},
            ],
            fix_vars=[{}, {}, {}]
        )

        multilogmodel.fit_to(dataset, verbose=False)

        fio = io.StringIO()
        rx.write_model_data(dataset, multilogmodel, fio)
        rx.write_model_params(multilogmodel, fio)


class TestHDataset:
    '''
    Tests for HDataset Class
    '''

    def test_initialization(self):

        # Without bounds
        rates = [10, 20, 30, 40, 50]
        fields = [100, 110, 120, 130, 140]
        dataset = rx.HDataset(rates, fields)

        npte.assert_array_equal(dataset.rates, rates)
        npte.assert_array_equal(dataset.fields, fields)

    def test_file_load(self):
        '''
        Load files and checks number of datapoints
        '''

        dataset = rx.HDataset.from_ccfit2_csv(
            [
                HEAD + 'data/relaxation/tau_vs_h_test.csv'
            ]
        )

        assert len(dataset.rates) == 20

        return

    def test_fit(self):
        '''
        Fits test data
        '''

        dataset = rx.HDataset.from_ccfit2_csv(
            [
                HEAD + 'data/relaxation/tau_vs_h_test.csv'
            ]
        )

        multilogmodel = rx.MultiLogTauHModel(
            [
                rx.LogRamanIIModel,
                rx.LogFDQTMModel,
                rx.LogConstantModel
            ],
            fit_vars=[
                {'Q': 1., 'Q_H': -8, 'p': 2.},
                {'C': -14, 'm': 4},
                {'Ct': -4},
            ],
            fix_vars=[{}, {}, {}]
        )

        multilogmodel.fit_to(dataset, verbose=False)

        assert multilogmodel.fit_status

        return

    def test_save_model(self):

        dataset = rx.HDataset.from_ccfit2_csv(
            [
                HEAD + 'data/relaxation/tau_vs_h_test.csv'
            ]
        )

        multilogmodel = rx.MultiLogTauHModel(
            [
                rx.LogRamanIIModel,
                rx.LogFDQTMModel,
                rx.LogConstantModel
            ],
            fit_vars=[
                {'Q': 1., 'Q_H': -8, 'p': 2.},
                {'C': -14, 'm': 4},
                {'Ct': -4},
            ],
            fix_vars=[{}, {}, {}]
        )

        multilogmodel.fit_to(dataset, verbose=False)

        fio = io.StringIO()
        rx.write_model_data(dataset, multilogmodel, fio)
        rx.write_model_params(multilogmodel, fio)


class TestHTDataset:
    '''
    Tests for HTDataset Class
    '''

    def test_initialization(self):

        # Without bounds
        rates = [10, 20, 30, 40, 50]
        fields = [0, 0, 1000, 1000, 1000]
        temperatures = [100, 110, 120, 130, 140]
        dataset = rx.HTDataset(rates, fields, temperatures)

        npte.assert_array_equal(dataset.rates, rates)
        npte.assert_array_equal(dataset.fields, fields)
        npte.assert_array_equal(dataset.temperatures, temperatures)

    def test_file_load(self):
        '''
        Load files and checks number of datapoints
        '''

        dataset = rx.HTDataset.from_ccfit2_csv(
            [
                HEAD + 'data/relaxation/tau_vs_h_test.csv',
                HEAD + 'data/relaxation/tau_vs_t_var_fields_test.csv'
            ]
        )

        assert len(dataset.rates) == 146

        return

    def test_fit(self):
        '''
        Fits test data
        '''

        dataset = rx.HTDataset.from_ccfit2_csv(
            [
                HEAD + 'data/relaxation/tau_vs_h_test.csv',
                HEAD + 'data/relaxation/tau_vs_t_var_fields_test.csv'
            ]
        )

        multilogmodel = rx.MultiLogTauHTModel(
            [
                rx.LogFTDRamanIModel,
                rx.LogFTDOrbachModel,
                rx.LogFTDQTMModel,
                rx.LogFTDRamanIIDirectModel
            ],
            fit_vars=[
                {'R': -4.3415, 'n': 4.0728},
                {'A': -8.4647, 'u_eff': 928.95},
                {'Q': -1.0491, 'Q_H': 7.6930, 'p': 3.6994},
                {'G': -14.505, 'x': 3.5221}
            ],
            fix_vars=[{}, {}, {}, {'y': 1}]
        )

        multilogmodel.fit_to(dataset, verbose=False)

        assert multilogmodel.fit_status

        return

    def test_save_model(self):

        dataset = rx.HTDataset.from_ccfit2_csv(
            [
                HEAD + 'data/relaxation/tau_vs_h_test.csv',
                HEAD + 'data/relaxation/tau_vs_t_var_fields_test.csv'
            ]
        )

        multilogmodel = rx.MultiLogTauHTModel(
            [
                rx.LogFTDRamanIModel,
                rx.LogFTDOrbachModel,
                rx.LogFTDQTMModel,
                rx.LogFTDRamanIIDirectModel
            ],
            fit_vars=[
                {'R': -4.3415, 'n': 4.0728},
                {'A': -8.4647, 'u_eff': 928.95},
                {'Q': -1.0491, 'Q_H': 7.6930, 'p': 3.6994},
                {'G': -14.505, 'x': 3.5221}
            ],
            fix_vars=[{}, {}, {}, {'y': 1}]
        )

        multilogmodel.fit_to(dataset, verbose=False)

        fio = io.StringIO()
        rx.write_model_params(multilogmodel, fio)

