## Quick Information
<!-- This is to help replicate the issue as closely as possible !-->
- **Operating System:** Windows/MacOS/Linux <!-- Delete as appropriate !-->
- **Version of ccfit2:** <!-- Obtained with `pip show ccfit2` !-->
- **Python version:** <!-- Obtained with `python --version` !-->

## Steps to reproduce
<!-- Either the command you entered, or a Minimal Working Example of your code !-->

## What Happened?
<!-- A brief description of what happened when you tried to perform the above action !-->

## Expected result
<!-- What should have happened when you performed the actions !-->

## Screenshots
<!-- Any relevant screenshots which show the issue !-->
